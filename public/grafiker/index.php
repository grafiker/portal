<html lang="DE">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="index, nofollow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="web_author" content="Grafiger.de" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Grafiker-Design</title>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js" crossorigin="anonymous"></script>

<script>
	$(document).ready(function(){
    $('.openPopupSmall').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-content').load(dataURL,function(){
            $('#modal-content-login').modal({show:true});
        });
    }); 
	$('.openPopupBig').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-content').load(dataURL,function(){
            $('#modal-content-register').modal({show:true});
        });
    });
		$.get('job', function(data) {
			$('.jobindex').html(data);
		}); 
		$.get('projektrechner', function(data) {
			$('.projektrechnerindex').html(data);
		});
});

function load_divcontent(content, divindex) {
	$('.' + divindex).html('<div class="loadingdiv text-center"><img src="images/loading.gif" class="loadingimg" /></div>');
	var content = content;
	var divindex = divindex;
		$.get(content, function(data) {
			$('.' + divindex).html(data);	
		})
	}

	function del_ask() {
    return confirm("Wirklich löschen?");
	}
	</script>
</head>
<body>
<div class="container">
    <div class="alloverheader">
        <div class="logo float-left">
                <img src="./images/logo.png" class="logo" alt="grafiker Logo">
        </div>
        <div class="menu float-right">

                <nav class="navbar navbar-expand-lg navbar-light bg-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                      
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                          <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Pins </a>
                            </li>
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Jobs</a>
                            </li>
                            <!--<li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Dropdown
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                            </li>-->
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Projektrechner</a>
                            </li>
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Profilsuche</a>
                            </li>
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Shop starten</a>
                            </li>
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Hier werben</a>
                            </li>      
                          </ul>
                        </div>
                      </nav>
        </div><div class="usermenu float-right">login | registrieren</div>
        <div class="clearfix"></div>
    </div> <!-- Ende Header -->

    <div class="content">
          <div class="searchandmenu float-left">
              <div class="search">
                      <label class="searchoverlabel">Finde</label><select class="typsearching" id="form_typsearching" name="typsearching"><option value="grafiker">Grafiker|in</option></select>
                      <br />
                      <label class="searchoverlabel">für</label><select class="grsearching" id="form_grsearching" name="grsearching"><option value="Bildretusche">Bildretusche</option></select><br />
                      <label class="registerlabel">Profil anlegen</label>
              </div>
          </div>
          <div class="jobhauptdiv float-left">
            <div class="botton_jobindex">
              <label class="startbutton">Jobangebot einstellen</label>
            </div>
          <div class="jobindex">
            <div class="loadingdiv text-center">
              <img src="images/loading.gif" class="loadingimg" />
            </div>
          </div>
        </div>
        <div class="projektrechnerhauptdiv float-left">
            <div class="botton_projektrechnerindex">
            <label class="startbutton">Projektrechner</label>
        </div>
        <div class="projektrechnerindex">
          <div class="loadingdiv text-center">
            <img src="images/loading.gif" class="loadingimg" />
          </div>
        </div>
    </div>
<div class="clearfix"></div> 
        <div class="pinsmenu">
                    <label class="pinsmenulabelpic">Pins</label><br />
                    <label class="pinsmenulabel">Alle Projekte</label><label class="pinsmenulabel">Retusche</label><label class="pinsmenulabel">Layout</label><label class="pinsmenulabel">DTP</label><label class="pinsmenulabel">HTML</label><label class="pinsmenulabel">Mods</label><label class="pinsmenulabel">mehr Filter</label>
            </div>
    </div>
    <div class="clearfix"></div><!-- Ende Content -->

<div class="alloverpins">
<?php
for($i = 1; $i < 21; $i++)
{
  if($i % 6 == 0)
  {
    ?>
    <div class="pin float-left">
                        <div class="pinautordiv actiondiv">
                                
                        </div>
                  </div>

    <?php
  }
  else
  {
    ?>
<div class="pin float-left">
                        <div class="pinautordiv">
                                <div class="viewsdiv">
                                        12d 126 <img src="./images/views.png" class="views" alt="Views">
                                </div>
                                <div class="ratingdiv">
                                        <img src="./images/rating.png" class="rating" alt="Rating">
                                </div>
                                <img src="./images/pinautor.png" class="pinautor" alt="Pinautor">
                        </div>
                        <div class="pintitle">
                            
                        </div>
                        <div class="pinautorminidiv float-left">
                                <img src="./images/pinautormini.png" class="pinautormini" alt="Pinuserpic">
                        </div>
                        <div class="pinautornamediv float-left">
                            
                        </div>
                  </div>
    <?php
  }
}
?>
                  <div class="clearfix"></div>
                </div>
<div class="clearfix"></div> <!-- Ende Pins -->
</div>
</body>
</html>