<?php

namespace App\Controller;

use App\AppBundle\Form\UserType;
use App\Controller\Mobile_Detect;
use App\AppBundle\Form\UsereditType;
use App\Document\Jobs;
use App\AppBundle\Form\JobsFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceTendersController extends Controller
{
    private $userManager;
    function __construct(ManagerRegistry $doctrine_mongodb, UserManagerInterface $userManager, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_userManager = $userManager;
        $this->urlGenerator = $urlGenerator;
    }
    /**
     * @Route("/service/tenders", name="service_tenders")
     */
    public function index()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}
        $builder = $this->createForm(JobsFormType::class);
        return $this->render('service_tenders/index.html.twig', [
            'form' => $builder->createView(),
            'controller_name' => 'ServiceTendersController',
            'header' => true,
        ]);
    }
    /**
     * @Route("/service/tenders/neu/{id}", name="service_tenders_new")
     */
    public function noHeadersAction(Request $request, $id)
    {
        $detect = new Mobile_Detect;
        if( $detect->isMobile() && !$detect->isTablet() ){
 $mobilcheck = "mobil";
        } else if(!$detect->isMobile() && $detect->isTablet()) {
            $mobilcheck = "tablet";
        } else {
            $mobilcheck = "pc";
        }
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }

        if(!$userexist) {
            //die("ENDE");
        }
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $builder = $this->createForm(JobsFormType::class);
        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            //\var_dump($request->request->all());
            //\var_dump($task);
            //\var_dump($_POST);
            $tasks = new Jobs();

            $format = 'd.m.Y H:i';
            $date = \DateTime::createFromFormat($format, $task["date_entree"] . " " . $task["time_entree"]);
            $dates = \DateTime::createFromFormat($format, date('d.m.Y H:i'));
            
            //\var_dump($task["date_entree"] . " " . $task["time_entree"]);
            //\var_dump($date);
            //secho "Format: $format; " . $date->format('d.m.Y H:i') . "\n";
            $pdf = array();
            $upload_dir = 'upload/uploads/ausschreibungen/';
            if(!is_dir($upload_dir)){
                mkdir($upload_dir, 0777);
                chmod($upload_dir, 0777);
            }

        $file_name=$_FILES["form"]["name"]["docpdf1"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf1"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors = false;
        $error = 1;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
           $error=2;
        }
        $filename = array();
        if(!$errors) {
            //echo "ERROR: " . $error;
            $pdf[0]=md5($tmp[0].time()).'.'.$tmp[1];
            $filename[0]=$file_name;
            //\var_dump($pdf);
            move_uploaded_file($file_tmp,$upload_dir.$pdf[0]);
        }

        $file_name=$_FILES["form"]["name"]["docpdf2"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf2"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        
        $errors = false;
        $error = 1;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
           $error=2;
        }
        
        if(!$errors) {
            //echo "ERROR: " . $error;
            $pdf[1]=md5($tmp[0].time()).'.'.$tmp[1];
            $filename[1]=$file_name;
            //\var_dump($pdf);
            move_uploaded_file($file_tmp,$upload_dir.$pdf[1]);
        }

        $file_name=$_FILES["form"]["name"]["docpdf3"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf3"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        
        $errors = false;
        $error = 1;
        if(in_array($file_ext,$expensions)=== false){
            $errors=true;
            $error=2;
        }
        
        if(!$errors) {
            //echo "ERROR: " . $error;
            $pdf[2]=md5($tmp[0].time()).'.'.$tmp[1];
            $filename[2]=$file_name;
            //\var_dump($pdf);
            move_uploaded_file($file_tmp,$upload_dir.$pdf[2]);
        }
        if(!$userexist) {
            $tasks->setUsername("kein Mitglied");
        } else {
            $tasks->setUsername($users->getID());
        }

            //$tasks->setUsername($users->getID());
            $tasks->setAstitle($task["astitle"]);
            $tasks->setContent($task["content"]);
            $tasks->setAktiv(false);
            $tasks->setBrauche($task["offertype"]);
            $tasks->setPrice($task["price"]);
            $tasks->setJobTyp($_GET["type"]);
            $tasks->setService([$task["service1"], $task["service2"], $task["service3"], $task["service4"]]);
            $tasks->setStartdate($dates->format('d.m.Y H:i'));
            $tasks->setEnddate($date->format('d.m.Y H:i'));
            $tasks->setDocpdf([$pdf]);
            $tasks->setDocname([$filename]);
            $tasks->setCacheid($_GET["cacheid"]);
            $this->_doctrine_mongodb->getManager()->persist($tasks);
            $this->_doctrine_mongodb->getManager()->flush();
            $lastID = $tasks->getID();
//\var_dump($lastID);
if(!$userexist) {
    $builder = $this->createForm(UserType::class);
return $this->render('service_tenders/register.html.twig', [
    'allwidget' => $thiswidget,
    'id' => $id,
    'lastid' => $lastID,
    'widgetid' => $id,
    'lastid' => $lastID,
    'jobid' => $lastID,
    'form' => $builder->createView(),
    'type' => $_GET["type"],
    'jobtype' => $_GET["type"],
    'controller_name' => 'ServiceTendersController',
    'mobilcheck' => $mobilcheck,
]);
} else {
    return new RedirectResponse($this->urlGenerator->generate('service_tenders_control', array('id' => $id, 'lastid' => $lastID, )));
}
        } else {
            $thiswidgetcache = $emdsend->getRepository('App:JobsCache')->findOneBy(["id"=>$_GET["cacheid"]]);
        return $this->render('service_tenders/neu.html.twig', [
            'allwidget' => $thiswidget,
            'id' => $id,
            'form' => $builder->createView(),
            'type' => $_GET["type"],
            'jobtype' => $_GET["type"],
            'thiswidgetcache' => $thiswidgetcache,
            'controller_name' => 'ServiceTendersController',
            'mobilcheck' => $mobilcheck,
        ]);
    }
    }
    /**
     * @Route("/service/tenders/payok/{id}/{lastid}", name="service_tenders_payok")
     */
    public function payokAction(Request $request, $lastid, $id, \Swift_Mailer $mailer)
    {
        if(isset($_GET["widgetusername"])) {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $users = $emdsend->getRepository('App:User')->findOneBy(["username"=>$_GET["widgetusername"]]);

        $usermail = $users->getEmail();
        $subjetsys = "Eintragung Ihrer Job-Ausschreibung.";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
        ->setTo($usermail)
        ->setBody('Vielen Dank für Ihre Job-Ausschreibung!<br /><br />Ihre Rechnung erhalten Sie in einer separaten Email.<br /><br />Hier kommen Sie zur Übersicht aller Jobausschreibungen::<br /><a href="https://grafiker.de/jobliste">https://grafiker.de/jobliste</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
        $mailer->send($message);

        $subjetsys = "NA - Job-Ausschreibung ".$users->getFirmenname() ." ".$users->getFirstName() ." ".$users->getLastName() ."";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
        ->setTo('hbroeskamp@printshopcreator.com')
        ->setBody('KUNDENNUMMER BEI FASTBILL: ' .$users->getID(). '<br /><br />Vielen Dank für Ihre Job-Ausschreibung!<br /><br />Ihre Rechnung erhalten Sie in einer separaten Email.<br /><br />Hier kommen Sie zur Übersicht aller Jobausschreibungen::<br /><a href="https://grafiker.de/jobliste">https://grafiker.de/jobliste</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
        $mailer->send($message);
            return $this->render('service_tenders/payokay.html.twig', [
                'widgetend' => "grafiker",
                'controller_name' => 'ServiceTendersController',
            ]);
        } else {
        //$users = $this->container->get('security.token_storage')->getToken()->getUser();
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(["id"=>$lastid]);
        $users = $emdsend->getRepository('App:User')->findOneBy(["id"=>$thistenders->getUsername()]);

        $usermail = $users->getEmail();
        $subjetsys = "Eintragung Ihrer Ausschreibung.";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
        ->setTo($usermail)
        ->setBody('Vielen Dank für Ihre Ausschreibung!<br /><br />Ihre Rechnung erhalten Sie in einer separaten Email.<br /><br />Hier kommen Sie zur Übersicht Ihrer Ausschreibung und zu den Angeboten der Grafiker:<br /><a href="https://grafiker.de/service/tenderslist/customer/'.$users->getID().'">https://grafiker.de/service/tenderslist/customer/'.$users->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
        $mailer->send($message);

        $subjetsys = "NA ".$users->getFirmenname() ." ".$users->getFirstName() ." ".$users->getLastName() ."";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
        ->setTo('hbroeskamp@printshopcreator.com')
        ->setBody('KUNDENNUMMER BEI FASTBILL: ' .$users->getID(). '<br /><br />Vielen Dank für Ihre Ausschreibung!<br /><br />Ihre Rechnung erhalten Sie in einer separaten Email.<br /><br />Hier kommen Sie zur Übersicht Ihrer Ausschreibung und zu den Angeboten der Grafiker:<br /><a href="https://grafiker.de/service/tenderslist/customer/'.$users->getID().'">https://grafiker.de/service/tenderslist/customer/'.$users->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
        $mailer->send($message);
        }
        return $this->render('service_tenders/payokay.html.twig', [
            'widgetend' => "nografiker",
            'allwidget' => $thiswidget,
            'username' => $users->getUsername(),
            'userid' => $users->getID(),
            'jobtype' => $thistenders->getJobTyp(),
            'widgetid' => $id,
            'jobid' => $lastid,
            'controller_name' => 'ServiceTendersController',
        ]);
    }

    /**
     * @Route("/service/tenders/ende/{id}/{lastid}", name="service_tenders_ende")
     */
    public function endeAction(Request $request, $lastid, $id, \Swift_Mailer $mailer)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(["id"=>$lastid]);
        $thistenders->setUsername($users->getID());
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();

        return $this->render('service_tenders/ende.html.twig', [
            'allwidget' => $thiswidget,
            'username' => $users->getUsername(),
            'userid' => $users->getID(),
            'jobtype' => $thistenders->getJobTyp(),
            'widgetid' => $id,
            'jobid' => $lastid,
            'controller_name' => 'ServiceTendersController',
        ]);
    }

    /**
     * @Route("/service/tenders/kontroll/{id}/{lastid}", name="service_tenders_control")
     */
    public function controlAction(Request $request, $lastid, $id)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();

if(!$users->getFastbill()) {
        if($users->getFirmenname() != "") {
            $btype = "business";
        } else {
            $btype = "consumer";
        }
        if($users->getAnrede() == "Herr") {
            $anrede = "mr";
        } else if($users->getAnrede() == "Frau") {
            $anrede = "mrs";
        }
        $countrycode = $this->countrycode($users->getLand());
                $apiUrl = "https://my.fastbill.com/api/1.0/api.php";
                $email = "hbroeskamp@printshopcreator.com";
                $apiKey = "52595899aa26ea1211aaf35581c1bb26giuMhAG0WPgecwBmKqQQsq4YbVNrtJs4" ;
                $debug = 1;
                $data = array(
                    'SERVICE' => "customer.create",
                    'Data' => array(
                        'CUSTOMER_NUMBER' => $users->getID(),
                        'CUSTOMER_TYPE' => $btype,
                        'ORGANIZATION' => $users->getFirmenname(),
                        'EMAIL' => $users->getEmail(),
                        'COUNTRY_CODE' => $countrycode,
                        'SALUTATION' => $anrede,
                        'FIRST_NAME' => $users->getFirstName(),
                        'LAST_NAME' => $users->getLastName(),
                        'ADDRESS' => $users->getStrassenummer(),
                        'ZIPCODE' => $users->getPlz(),
                        'CITY' => $users->getWohnort(),
                        'PHONE' => $users->getTelefon(),
                        'VAT_ID' => $users->getUstid(),
                     )
                  );
                  
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $apiUrl);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => 'Authorization: Basic ' . base64_encode($email.':'.$apiKey)));
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $data ));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
                        curl_setopt($ch, CURLOPT_VERBOSE, ($debug ? 1 : 0));
        
                        $exec = curl_exec($ch);
        
                        $result = json_decode($exec,true);
        
                        curl_close($ch);
                        $emdsend = $this->_doctrine_mongodb->getManager();
                        $thisuser = $emdsend->getRepository('App:User')->findOneBy(["id"=>$users->getID()]);
                        $thisuser->setFastbill($result["RESPONSE"]["CUSTOMER_ID"]);
                        $this->_doctrine_mongodb->getManager()->persist($thisuser);
                        $this->_doctrine_mongodb->getManager()->flush();
        } else {
            $apiUrl = "https://my.fastbill.com/api/1.0/api.php";
            $email = "hbroeskamp@printshopcreator.com";
            $apiKey = "52595899aa26ea1211aaf35581c1bb26giuMhAG0WPgecwBmKqQQsq4YbVNrtJs4" ;
        $debug = 1;

        if($users->getFirmenname() != "") {
            $btype = "business";
        } else {
            $btype = "consumer";
        }
        if($users->getAnrede() == "Herr") {
            $anrede = "mr";
        } else if($users->getAnrede() == "Frau") {
            $anrede = "mrs";
        }
        $countrycode = $this->countrycode($users->getLand());

        $data = array(
            'SERVICE' => "customer.update",
            'Data' => array(
                'CUSTOMER_ID' => $users->getFastbill(),
                'CUSTOMER_TYPE' => $btype,
                'ORGANIZATION' => $users->getFirmenname(),
                'EMAIL' => $users->getEmail(),
                'COUNTRY_CODE' => $countrycode,
                'SALUTATION' => $anrede,
                'FIRST_NAME' => $users->getFirstName(),
                'LAST_NAME' => $users->getLastName(),
                'ADDRESS' => $users->getStrassenummer(),
                'ZIPCODE' => $users->getPlz(),
                'CITY' => $users->getWohnort(),
                'PHONE' => $users->getTelefon(),
                'VAT_ID' => $users->getUstid(),
            )
          );
        }


        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(["id"=>$lastid]);
        $thistenders->setUsername($users->getID());
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();
        $builder = $this->createForm(UsereditType::class, $users);
        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            $this->_doctrine_mongodb->getManager()->persist($task);
            $this->_doctrine_mongodb->getManager()->flush();
            if($thistenders->getJobTyp() == "standard") {
                $toroutes = "service_tenders_payok";
                $thistenders->setAktiv(true);
                $this->_doctrine_mongodb->getManager()->persist($thistenders);
                $this->_doctrine_mongodb->getManager()->flush();
            } else {
            $toroutes = "service_tenders_ende";
            }
            return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id, 'lastid' => $lastid)));
        }
        return $this->render('service_kontrolldata/index.html.twig', [
            'form' => $builder->createView(),
            'allwidget' => $thiswidget,
            'username' => $users->getUsername(),
            'userid' => $users->getID(),
            'jobtype' => $thistenders->getJobTyp(),
            'widgetid' => $id,
            'jobid' => $lastid,
            'controller_name' => 'ServiceMasterdataController',
        ]);
    }

    /**
     * @Route("/service/register/{id}/{lastid}", name="service_tenders_register")
     */
    public function registerAction(Request $request, $lastid, $id)
    {
        $userManager = $this->get('fos_user.user_manager');
//\var_dump($request->request->all()["user"]);
$confirmationToken = md5($request->request->all()["user"]["username"].$request->request->all()["user"]["email"]);
$user = $userManager->createUser();
if($request->request->all()["user"]["username"] != "") {
$user->setUsername($request->request->all()["user"]["username"]);
$user->setPlainPassword($request->request->all()["user"]["passwort"]);
$user->setGast(false);
} else {
    $emdsend = $this->_doctrine_mongodb->getManager();
    $thisgast = $emdsend->getRepository('App:User')->findBy(array("gast" => true));
    $gastzahl = count($thisgast)+1;
    $user->setUsername("GAST" . $gastzahl); 
    $user->setPlainPassword("GASTLOGIN");
    $user->setGast(true);
}
$user->setEmail($request->request->all()["user"]["email"]);
$user->setEmailCanonical($request->request->all()["user"]["email"]);
$user->setEnabled(true);
$user->setConfirmationToken($confirmationToken);
$user->setNotifications(2);
$user->setFirstLogin(true);
$user->setAnrede($request->request->all()["user"]["anrede"]);
$user->setFirstName($request->request->all()["user"]["firstName"]);
$user->setLastName($request->request->all()["user"]["lastName"]);
$user->setAgb(1);
$user->setDatenschutz(1);
$user->setRoles(["ROLE_CUSTOMER"]);
$user->setFirmenname($request->request->all()["user"]["firmenname"]);
$user->setUstid($request->request->all()["user"]["ustid"]);
$user->setTelefon($request->request->all()["user"]["telefon"]);
$user->setStrassenummer($request->request->all()["user"]["strassenummer"]);
$user->setWohnort($request->request->all()["user"]["wohnort"]);
$user->setLand($request->request->all()["user"]["land"]);
$userManager->updateUser($user);

if($request->request->all()["user"]["anrede"] == "Firma") {
    $btype = "business";
} else {
    $btype = "consumer";
}
if($request->request->all()["user"]["anrede"] == "Herr") {
    $anrede = "mr";
} else if($request->request->all()["user"]["anrede"] == "Frau") {
    $anrede = "mrs";
}
$countrycode = $this->countrycode($request->request->all()["user"]["land"]);
//echo $countrycode;
$emdsend = $this->_doctrine_mongodb->getManager();
if(!isset($_POST["widgetjobregister"])) {
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(["id"=>$lastid]);
        $thistenders->setUsername($user->getID());
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();
}
        $apiUrl = "https://my.fastbill.com/api/1.0/api.php";
        $email = "hbroeskamp@printshopcreator.com";
        $apiKey = "52595899aa26ea1211aaf35581c1bb26giuMhAG0WPgecwBmKqQQsq4YbVNrtJs4" ;
        $debug = 1;
        $data = array(
            'SERVICE' => "customer.create",
            'Data' => array(
                'CUSTOMER_NUMBER' => $user->getID(),
                'CUSTOMER_TYPE' => $btype,
                'ORGANIZATION' => $request->request->all()["user"]["firmenname"],
                'EMAIL' => $request->request->all()["user"]["email"],
                'COUNTRY_CODE' => $countrycode,
                'SALUTATION' => $anrede,
                'FIRST_NAME' => $request->request->all()["user"]["firstName"],
                'LAST_NAME' => $request->request->all()["user"]["lastName"],
                'ADDRESS' => $request->request->all()["user"]["strassenummer"],
                'ZIPCODE' => $request->request->all()["user"]["plz"],
                'CITY' => $request->request->all()["user"]["wohnort"],
                'PHONE' => $request->request->all()["user"]["telefon"],
                'VAT_ID' => $request->request->all()["user"]["ustid"],
             )
          );
          
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $apiUrl);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => 'Authorization: Basic ' . base64_encode($email.':'.$apiKey)));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $data ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
                curl_setopt($ch, CURLOPT_VERBOSE, ($debug ? 1 : 0));

                $exec = curl_exec($ch);

                $result = json_decode($exec,true);

                curl_close($ch);
                $thisuser = $emdsend->getRepository('App:User')->findOneBy(["id"=>$user->getID()]);
                $thisuser->setFastbill($result["RESPONSE"]["CUSTOMER_ID"]);
                $this->_doctrine_mongodb->getManager()->persist($thisuser);
                $this->_doctrine_mongodb->getManager()->flush();
                if($thistenders->getJobTyp() == "standard") {
                    $toroutes = "service_tenders_payok";
                    $thistenders->setAktiv(true);
                    $this->_doctrine_mongodb->getManager()->persist($thistenders);
                    $this->_doctrine_mongodb->getManager()->flush();
                    return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id, 'lastid' => $lastid)));
                } else {                

if(isset($_POST["widgetjobregister"])) {
    return new RedirectResponse($this->urlGenerator->generate('service_tenders_jobs', array('id' => $id, 'widgetusername' => $user->getUsername())));
} else {
        return $this->render('service_tenders/ende.html.twig', [
            'allwidget' => $thiswidget,
            'username' => $user->getUsername(),
            'userid' => $user->getID(),
            'widgetid' => $id,
            'lastid' => $lastid,
            'jobid' => $lastid,
            'jobtype' => $thistenders->getJobTyp(),
            'controller_name' => 'ServiceTendersController',
        ]);
    }
}
    }
    /**
     * @Route("/usernamecheck/{username}", name="service_tenders_username")
     */
    public function usernameAction(Request $request, $username)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $usercheckdb = $emdsend->getRepository('App:User')->findOneBy(["username"=>$username]);
        if($usercheckdb) {
            $usercheck = 1;
        } else {
            $usercheck = 0;
        }
        return $this->render('service_tenders/username.html.twig', [
            'username' => $usercheck,
            'controller_name' => 'ServiceTendersController',
        ]);
    }
    /**
     * @Route("/mailcheck/{mail}", name="service_tenders_mail")
     */
    public function mailAction(Request $request, $mail)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $usercheckdb = $emdsend->getRepository('App:User')->findOneBy(["email"=>$mail]);
        if($usercheckdb) {
            $mail = 1;
        } else {
            $mail = 0;
        }
        return $this->render('service_tenders/mail.html.twig', [
            'mail' => $mail,
            'controller_name' => 'ServiceTendersController',
        ]);
    }
    /**
     * @Route("/service/tenderslist/grafiker", name="service_tenders_grafikerlist")
     */
    public function grafikerlistAction(Request $request)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findBy(array("aktiv" => true),array('enddate' => 'ASC'));
        $thistendersokay = $emdsend->getRepository('App:Jobs')->findBy(array("jobokay" => true));
        $thistenderscacheowner = array();
        $thistenderscachegrafiker = array();
        $thiswidget = array();
        foreach($thistenders as $thistenderscacheid) {
            $thistenderscache = $emdsend->getRepository('App:JobsCache')->findOneBy(array("id" => $thistenderscacheid->getCacheid()));
            $thiswidgetout = $emdsend->getRepository('App:ServiceWidget')->findOneBy(array("id" => $thistenderscacheid->getPayout()["widgetid"]));
            $thiswidget[$thistenderscacheid->getID()] = $thiswidgetout;        
            $controlarray = array("productName", "productID", "productPriceCurrency", "productPriceExTax", "productPriceTax", "productPriceTaxValue", "productPriceIncTax", "productGrafikerKeywords");
            foreach($thistenderscache->getDatacache() as $key => $thistenderscacheall) {
                if (!in_array($key, $controlarray)) {
                $thistenderscacheowner[$thistenderscacheid->getID()][$key] = $thistenderscacheall;
                } else {
                $thistenderscachegrafiker[$thistenderscacheid->getID()][$key] = $thistenderscacheall;
                }
                
            }
        }

        
$tendersofferokay = array();
$tendersoffermyokay = array();
$tendersoffermystorno = array();
foreach($thistendersokay as $thistendersokay) {
    if($thistendersokay->getJobokay()) {
    $tendersofferokay[] = $thistendersokay->getID();
    }
    $thistendersinlistcheck = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("username" => $users->getID(), "jobid" => $thistendersokay->getID(), "jobokay" => true));
    if($thistendersinlistcheck) {
        $tendersoffermyokay[$thistendersokay->getID()] = true;
    } else {
        $tendersoffermyokay[$thistendersokay->getID()] = false;
    }
}
foreach($thistenders as $thistendersstorno) {
    $thistendersinlisstorno = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("username" => $users->getID(), "jobid" => $thistendersstorno->getID(), "storno" => true));
    if($thistendersinlisstorno) {
        $tendersoffermystorno[$thistendersstorno->getID()] = true;
    } else {
        $tendersoffermystorno[$thistendersstorno->getID()] = false;
    }
}
//\var_dump($tendersoffermystorno);
$thistendersinlist = $emdsend->getRepository('App:Jobsoffers')->findBy(array("username" => $users->getID()));

$tendersoffer = array();
foreach($thistendersinlist as $inlist) {
    $tendersoffer[] = $inlist->getJobid();
}
$docname = array();
$countoffers = array();
    foreach($thistenders as $thistendersdoc) {
        $thistendersincount = $emdsend->getRepository('App:Jobsoffers')->findBy(array("jobid" => $thistendersdoc->getID()));
        $countoffers[$thistendersdoc->getID()] = count($thistendersincount);
        foreach($thistendersdoc->getDocname() as $key => $thistendersdocname) {
            $docname[$thistendersdoc->getID()][$key] = $thistendersdocname;
        }
    }
    $serficecheck = array();
    foreach($thistenders as $key => $services) {
        foreach($services->getService() as $key => $service) {
    if($service == true) {
        $serficecheck[$services->getID()][$key] = $key;
    }
    }
    }
        return $this->render('service_tenderslist_grafiker/index.html.twig', [
            'controller_name' => 'ServiceTendersController',
            'tenderslist' => $thistenders,
            'docname' => $docname,
            'tendersoffer' => $tendersoffer,
            'tendersofferokay' => $tendersofferokay,
            'tendersoffermyokay' => $tendersoffermyokay,
            'tendersoffermystorno' => $tendersoffermystorno,
            'countoffers' => $countoffers,
            'serficecheck' => $serficecheck,
            'thistenderscacheowner' => $thistenderscacheowner,
            'thistenderscachegrafiker' => $thistenderscachegrafiker,
            'thiswidget' => $thiswidget,
        ]);
    }

    /**
     * @Route("/service/tenderslist/customer/{uid}", name="service_tenders_customerlist")
     */
    public function customerlistAction($uid, Request $request)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findBy(array("username" => $uid, "aktiv" => true),array('enddate' => 'ASC'));
        $serficecheck = array();
        $thiswidget = array();
        $thistenderscacheowner = array();
        $thistenderscachegrafiker = array();
        foreach($thistenders as $thistenderscacheid) {
        $thistenderscache = $emdsend->getRepository('App:JobsCache')->findOneBy(array("id" => $thistenderscacheid->getCacheid()));
        $thiswidgetout = $emdsend->getRepository('App:ServiceWidget')->findOneBy(array("id" => $thistenderscacheid->getPayout()["widgetid"]));
        $thiswidget[$thistenderscacheid->getID()] = $thiswidgetout;        
        $controlarray = array("productName", "productID", "productPriceCurrency", "productPriceExTax", "productPriceTax", "productPriceTaxValue", "productPriceIncTax", "productGrafikerKeywords");
        foreach($thistenderscache->getDatacache() as $key => $thistenderscacheall) {
            if (!in_array($key, $controlarray)) {
            $thistenderscacheowner[$thistenderscacheid->getID()][$key] = $thistenderscacheall;
            } else {
            $thistenderscachegrafiker[$thistenderscacheid->getID()][$key] = $thistenderscacheall;
            }
            
        }
    }
    //\var_dump($thistenders);
foreach($thistenders as $key => $services) {
    foreach($services->getService() as $key => $service) {
if($service == true) {
    $serficecheck[$services->getID()][$key] = $key;
}
}
}
        $tendersoffer = array();
        $tendersoffercount = array();
        $tendersofferall = array();
        $tendersoffergrafiker = array();
        foreach($thistenders as $thistendersoffers) {
        $thistendersinlist = $emdsend->getRepository('App:Jobsoffers')->findBy(array("jobid" => $thistendersoffers->getID()));
        $tendersoffercount[$thistendersoffers->getID()] = count($thistendersinlist);
                if($thistendersinlist) {
                    foreach($thistendersinlist as $thistendersinlist) {
                        $thistendersgrafiker = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistendersinlist->getUsername()));
                        $tendersoffer[] = $thistendersinlist->getJobid();
                        $tendersofferall[$thistendersoffers->getID()][$thistendersinlist->getUsername()] = $thistendersinlist;
                        $tendersoffergrafiker[$thistendersinlist->getUsername()] = $thistendersgrafiker->getUsername();
                    }
                } else {
                    $tendersofferall[$thistendersoffers->getID()]["keinMitglied"] = "";
                }
        }
$docname = array();
    foreach($thistenders as $thistendersdoc) {
        foreach($thistendersdoc->getDocname() as $key => $thistendersdocname) {
            $docname[$thistendersdoc->getID()][$key] = $thistendersdocname;
        }
    }
//\var_dump($thistenders);
        return $this->render('service_tenderslist_customer_blank/index.html.twig', [
            'controller_name' => 'ServiceTendersController',
            'tenderslist' => $thistenders,
            'docname' => $docname,
            'tendersoffer' => $tendersoffer,
            'tendersoffercount' => $tendersoffercount,
            'tendersofferall' => $tendersofferall,
            'tendersoffergrafiker' => $tendersoffergrafiker,
            'serficecheck' => $serficecheck,
            'thistenderscacheowner' => $thistenderscacheowner,
            'thistenderscachegrafiker' => $thistenderscachegrafiker,
            'thiswidget' => $thiswidget,

        ]);
    }

    function countrycode($searchland) {
        $laenderen['en']['AF'] = "Afghanistan"; $laender['AF'] = "Afghanistan"; 
        $laenderen['en']['AL'] = "Albania"; $laender['AL'] = "Albanien"; 
        $laenderen['en']['AS'] = "American Samoa"; $laender['AS'] = "Amerikanisch Samoa"; 
        $laenderen['en']['AD'] = "Andorra"; $laender['AD'] = "Andorra"; 
        $laenderen['en']['AO'] = "Angola"; $laender['AO'] = "Angola"; 
        $laenderen['en']['AI'] = "Anguilla"; $laender['AI'] = "Anguilla"; 
        $laenderen['en']['AQ'] = "Antarctica"; $laender['AQ'] = "Antarktis"; 
        $laenderen['en']['AG'] = "Antigua and Barbuda"; $laender['AG'] = "Antigua und Barbuda"; 
        $laenderen['en']['AR'] = "Argentina"; $laender['AR'] = "Argentinien"; 
        $laenderen['en']['AM'] = "Armenia"; $laender['AM'] = "Armenien"; 
        $laenderen['en']['AW'] = "Aruba"; $laender['AW'] = "Aruba"; 
        $laenderen['en']['AT'] = "Austria"; $laender['AT'] = "Österreich"; 
        $laenderen['en']['AU'] = "Australia"; $laender['AU'] = "Australien"; 
        $laenderen['en']['AZ'] = "Azerbaijan"; $laender['AZ'] = "Aserbaidschan"; 
        $laenderen['en']['BS'] = "Bahamas"; $laender['BS'] = "Bahamas"; 
        $laenderen['en']['BH'] = "Bahrain"; $laender['BH'] = "Bahrain"; 
        $laenderen['en']['BD'] = "Bangladesh"; $laender['BD'] = "Bangladesh"; 
        $laenderen['en']['BB'] = "Barbados"; $laender['BB'] = "Barbados"; 
        $laenderen['en']['BY'] = "Belarus"; $laender['BY'] = "Weißrussland"; 
        $laenderen['en']['BE'] = "Belgium"; $laender['BE'] = "Belgien"; 
        $laenderen['en']['BZ'] = "Belize"; $laender['BZ'] = "Belize"; 
        $laenderen['en']['BJ'] = "Benin"; $laender['BJ'] = "Benin"; 
        $laenderen['en']['BM'] = "Bermuda"; $laender['BM'] = "Bermuda"; 
        $laenderen['en']['BT'] = "Bhutan"; $laender['BT'] = "Bhutan"; 
        $laenderen['en']['BO'] = "Bolivia"; $laender['BO'] = "Bolivien"; 
        $laenderen['en']['BA'] = "Bosnia and Herzegovina"; $laender['BA'] = "Bosnien Herzegowina"; 
        $laenderen['en']['BW'] = "Botswana"; $laender['BW'] = "Botswana"; 
        $laenderen['en']['BV'] = "Bouvet Island"; $laender['BV'] = "Bouvet Island"; 
        $laenderen['en']['BR'] = "Brazil"; $laender['BR'] = "Brasilien"; 
        $laenderen['en']['BN'] = "Brunei Darussalam"; $laender['BN'] = "Brunei Darussalam"; 
        $laenderen['en']['BG'] = "Bulgaria"; $laender['BG'] = "Bulgarien"; 
        $laenderen['en']['BF'] = "Burkina Faso"; $laender['BF'] = "Burkina Faso"; 
        $laenderen['en']['BI'] = "Burundi"; $laender['BI'] = "Burundi"; 
        $laenderen['en']['KH'] = "Cambodia"; $laender['KH'] = "Kambodscha"; 
        $laenderen['en']['CM'] = "Cameroon"; $laender['CM'] = "Kamerun"; 
        $laenderen['en']['CA'] = "Canada"; $laender['CA'] = "Kanada"; 
        $laenderen['en']['CV'] = "Cape Verde"; $laender['CV'] = "Kap Verde"; 
        $laenderen['en']['KY'] = "Cayman Islands"; $laender['KY'] = "Cayman Inseln"; 
        $laenderen['en']['CF'] = "Central African Republic"; $laender['CF'] = "Zentralafrikanische Republik"; 
        $laenderen['en']['TD'] = "Chad"; $laender['TD'] = "Tschad"; 
        $laenderen['en']['CL'] = "Chile"; $laender['CL'] = "Chile"; 
        $laenderen['en']['CN'] = "China"; $laender['CN'] = "China"; 
        $laenderen['en']['CO'] = "Colombia"; $laender['CO'] = "Kolumbien"; 
        $laenderen['en']['KM'] = "Comoros"; $laender['KM'] = "Comoros"; 
        $laenderen['en']['CG'] = "Congo"; $laender['CG'] = "Kongo"; 
        $laenderen['en']['CK'] = "Cook Islands"; $laender['CK'] = "Cook Inseln"; 
        $laenderen['en']['CR'] = "Costa Rica"; $laender['CR'] = "Costa Rica"; 
        $laenderen['en']['CI'] = "Côte d'Ivoire"; $laender['CI'] = "Elfenbeinküste"; 
        $laenderen['en']['HR'] = "Croatia"; $laender['HR'] = "Kroatien"; 
        $laenderen['en']['CU'] = "Cuba"; $laender['CU'] = "Kuba"; 
        $laenderen['en']['CZ'] = "Czech Republic"; $laender['CZ'] = "Tschechien"; 
        $laenderen['en']['DK'] = "Denmark"; $laender['DK'] = "Dänemark"; 
        $laenderen['en']['DJ'] = "Djibouti"; $laender['DJ'] = "Djibouti"; 
        $laenderen['en']['DO'] = "Dominican Republic"; $laender['DO'] = "Dominikanische Republik"; 
        $laenderen['en']['TP'] = "East Timor"; $laender['TP'] = "Osttimor"; 
        $laenderen['en']['EC'] = "Ecuador"; $laender['EC'] = "Ecuador"; 
        $laenderen['en']['EG'] = "Egypt"; $laender['EG'] = "Ägypten"; 
        $laenderen['en']['SV'] = "El salvador"; $laender['SV'] = "El Salvador"; 
        $laenderen['en']['GQ'] = "Equatorial Guinea"; $laender['GQ'] = "Äquatorial Guinea"; 
        $laenderen['en']['ER'] = "Eritrea"; $laender['ER'] = "Eritrea"; 
        $laenderen['en']['EE'] = "Estonia"; $laender['EE'] = "Estland"; 
        $laenderen['en']['ET'] = "Ethiopia"; $laender['ET'] = "Äthiopien"; 
        $laenderen['en']['FK'] = "Falkland Islands"; $laender['FK'] = "Falkland Inseln"; 
        $laenderen['en']['FO'] = "Faroe Islands"; $laender['FO'] = "Faroe Inseln"; 
        $laenderen['en']['FJ'] = "Fiji"; $laender['FJ'] = "Fiji"; 
        $laenderen['en']['FI'] = "Finland"; $laender['FI'] = "Finland"; 
        $laenderen['en']['FR'] = "France"; $laender['FR'] = "Frankreich"; 
        $laenderen['en']['GF'] = "French Guiana"; $laender['GF'] = "Französisch Guiana"; 
        $laenderen['en']['PF'] = "French Polynesia"; $laender['PF'] = "Französisch Polynesien"; 
        $laenderen['en']['GA'] = "Gabon"; $laender['GA'] = "Gabon"; 
        $laenderen['en']['GM'] = "Gambia"; $laender['GM'] = "Gambia"; 
        $laenderen['en']['GE'] = "Georgia"; $laender['GE'] = "Georgien"; 
        $laenderen['en']['DE'] = "Germany"; $laender['DE'] = "Deutschland"; 
        $laenderen['en']['GH'] = "Ghana"; $laender['GH'] = "Ghana"; 
        $laenderen['en']['GI'] = "Gibraltar"; $laender['GI'] = "Gibraltar"; 
        $laenderen['en']['GR'] = "Greece"; $laender['GR'] = "Griechenland"; 
        $laenderen['en']['GL'] = "Greenland"; $laender['GL'] = "Grönland"; 
        $laenderen['en']['GD'] = "Grenada"; $laender['GD'] = "Grenada"; 
        $laenderen['en']['GP'] = "Guadeloupe"; $laender['GP'] = "Guadeloupe"; 
        $laenderen['en']['GU'] = "Guam"; $laender['GU'] = "Guam"; 
        $laenderen['en']['GT'] = "Guatemala"; $laender['GT'] = "Guatemala"; 
        $laenderen['en']['GN'] = "Guinea"; $laender['GN'] = "Guinea"; 
        $laenderen['en']['GY'] = "Guyana"; $laender['GY'] = "Guyana"; 
        $laenderen['en']['HT'] = "Haiti"; $laender['HT'] = "Haiti"; 
        $laenderen['en']['VA'] = "Vatican"; $laender['VA'] = "Vatikan"; 
        $laenderen['en']['HN'] = "Honduras"; $laender['HN'] = "Honduras"; 
        $laenderen['en']['HU'] = "Hungary"; $laender['HU'] = "Ungarn"; 
        $laenderen['en']['IS'] = "Iceland"; $laender['IS'] = "Island"; 
        $laenderen['en']['IN'] = "India"; $laender['IN'] = "Indien"; 
        $laenderen['en']['ID'] = "Indonesia"; $laender['ID'] = "Indonesien"; 
        $laenderen['en']['IR'] = "Iran"; $laender['IR'] = "Iran"; 
        $laenderen['en']['IQ'] = "Iraq"; $laender['IQ'] = "Irak"; 
        $laenderen['en']['IE'] = "Ireland"; $laender['IE'] = "Irland"; 
        $laenderen['en']['IL'] = "Israel"; $laender['IL'] = "Israel"; 
        $laenderen['en']['IT'] = "Italy"; $laender['IT'] = "Italien"; 
        $laenderen['en']['JM'] = "Jamaica"; $laender['JM'] = "Jamaika"; 
        $laenderen['en']['JP'] = "Japan"; $laender['JP'] = "Japan"; 
        $laenderen['en']['JO'] = "Jordan"; $laender['JO'] = "Jordanien"; 
        $laenderen['en']['KZ'] = "Kazakstan"; $laender['KZ'] = "Kasachstan"; 
        $laenderen['en']['KE'] = "Kenya"; $laender['KE'] = "Kenia"; 
        $laenderen['en']['KI'] = "Kiribati"; $laender['KI'] = "Kiribati"; 
        $laenderen['en']['KW'] = "Kuwait"; $laender['KW'] = "Kuwait"; 
        $laenderen['en']['KG'] = "Kyrgystan"; $laender['KG'] = "Kirgistan"; 
        $laenderen['en']['LA'] = "Lao"; $laender['LA'] = "Laos"; 
        $laenderen['en']['LV'] = "Latvia"; $laender['LV'] = "Lettland"; 
        $laenderen['en']['LB'] = "Lebanon"; $laender['LB'] = "Libanon"; 
        $laenderen['en']['LS'] = "Lesotho"; $laender['LS'] = "Lesotho"; 
        $laenderen['en']['LI'] = "Liechtenstein"; $laender['LI'] = "Liechtenstein"; 
        $laenderen['en']['LT'] = "Lithuania"; $laender['LT'] = "Litauen"; 
        $laenderen['en']['LU'] = "Luxembourg"; $laender['LU'] = "Luxemburg"; 
        $laenderen['en']['MO'] = "Macau"; $laender['MO'] = "Macau"; 
        $laenderen['en']['MK'] = "Macedonia "; $laender['MK'] = "Mazedonien"; 
        $laenderen['en']['MG'] = "Madagascar"; $laender['MG'] = "Madagaskar"; 
        $laenderen['en']['MW'] = "Malawi"; $laender['MW'] = "Malawi"; 
        $laenderen['en']['MY'] = "Malaysia"; $laender['MY'] = "Malaysia"; 
        $laenderen['en']['MV'] = "Maldives"; $laender['MV'] = "Malediven"; 
        $laenderen['en']['ML'] = "Mali"; $laender['ML'] = "Mali"; 
        $laenderen['en']['MT'] = "Malta"; $laender['MT'] = "Malta"; 
        $laenderen['en']['MR'] = "Mauritania"; $laender['MR'] = "Mauretanien"; 
        $laenderen['en']['MU'] = "Mauritius"; $laender['MU'] = "Mauritius"; 
        $laenderen['en']['YT'] = "Mayotte"; $laender['YT'] = "Mayotte"; 
        $laenderen['en']['MX'] = "Mexico"; $laender['MX'] = "Mexiko"; 
        $laenderen['en']['FM'] = "Micronesia"; $laender['FM'] = "Mikronesien"; 
        $laenderen['en']['MD'] = "Moldova"; $laender['MD'] = "Moldavien"; 
        $laenderen['en']['MC'] = "Monaco"; $laender['MC'] = "Monaco"; 
        $laenderen['en']['MN'] = "Mongolia"; $laender['MN'] = "Mongolei"; 
        $laenderen['en']['MS'] = "Montserrat"; $laender['MS'] = "Montserrat"; 
        $laenderen['en']['MA'] = "Morocco"; $laender['MA'] = "Marokko"; 
        $laenderen['en']['MZ'] = "Mozambique"; $laender['MZ'] = "Mosambik"; 
        $laenderen['en']['MM'] = "Myanmar"; $laender['MM'] = "Myanmar"; 
        $laenderen['en']['NA'] = "Namibia"; $laender['NA'] = "Namibia"; 
        $laenderen['en']['NR'] = "Nauru"; $laender['NR'] = "Nauru"; 
        $laenderen['en']['NP'] = "Nepal"; $laender['NP'] = "Nepal"; 
        $laenderen['en']['NL'] = "Netherlands"; $laender['NL'] = "Niederlande"; 
        $laenderen['en']['NZ'] = "New Zealand"; $laender['NZ'] = "Neuseeland"; 
        $laenderen['en']['NI'] = "Nicaragua"; $laender['NI'] = "Nicaragua"; 
        $laenderen['en']['NE'] = "Niger"; $laender['NE'] = "Niger"; 
        $laenderen['en']['NG'] = "Nigeria"; $laender['NG'] = "Nigeria"; 
        $laenderen['en']['NU'] = "Niue"; $laender['NU'] = "Niue"; 
        $laenderen['en']['NF'] = "Norfolk Island"; $laender['NF'] = "Norfolk Inseln"; 
        $laenderen['en']['KP'] = "North Korea"; $laender['KP'] = "Nord Korea"; 
        $laenderen['en']['NO'] = "Norway"; $laender['NO'] = "Norwegen"; 
        $laenderen['en']['OM'] = "Oman"; $laender['OM'] = "Oman"; 
        $laenderen['en']['PK'] = "Pakistan"; $laender['PK'] = "Pakistan"; 
        $laenderen['en']['PW'] = "Palau"; $laender['PW'] = "Palau"; 
        $laenderen['en']['PA'] = "Panama"; $laender['PA'] = "Panama"; 
        $laenderen['en']['PG'] = "Papua New Guinea"; $laender['PG'] = "Papua Neu Guinea"; 
        $laenderen['en']['PY'] = "Paraguay"; $laender['PY'] = "Paraguay"; 
        $laenderen['en']['PE'] = "Peru"; $laender['PE'] = "Peru"; 
        $laenderen['en']['PH'] = "Philippines"; $laender['PH'] = "Philippinen"; 
        $laenderen['en']['PL'] = "Poland"; $laender['PL'] = "Polen"; 
        $laenderen['en']['PT'] = "Portugal"; $laender['PT'] = "Portugal"; 
        $laenderen['en']['PR'] = "Puerto Rico"; $laender['PR'] = "Puerto Rico"; 
        $laenderen['en']['RO'] = "Romania"; $laender['RO'] = "Rumänien"; 
        $laenderen['en']['RU'] = "Russia"; $laender['RU'] = "Russland"; 
        $laenderen['en']['RW'] = "Rwanda"; $laender['RW'] = "Ruanda"; 
        $laenderen['en']['WS'] = "Samoa"; $laender['WS'] = "Samoa"; 
        $laenderen['en']['SM'] = "San Marino"; $laender['SM'] = "San Marino"; 
        $laenderen['en']['SA'] = "Saudi Arabia"; $laender['SA'] = "Saudi-Arabien"; 
        $laenderen['en']['SN'] = "Senegal"; $laender['SN'] = "Senegal"; 
        $laenderen['en']['SRB'] = "Serbien"; $laender['SRB'] = "Serbien"; 
        $laenderen['en']['SC'] = "Seychelles"; $laender['SC'] = "Seychellen"; 
        $laenderen['en']['SL'] = "Sierra Leone"; $laender['SL'] = "Sierra Leone"; 
        $laenderen['en']['SG'] = "Singapore"; $laender['SG'] = "Singapur"; 
        $laenderen['en']['SK'] = "Slovakia"; $laender['SK'] = "Slovakei"; 
        $laenderen['en']['SB'] = "Solomon Islands"; $laender['SB'] = "Solomon Inseln"; 
        $laenderen['en']['SO'] = "Somalia"; $laender['SO'] = "Somalia"; 
        $laenderen['en']['ZA'] = "South Africa"; $laender['ZA'] = "Südafrika"; 
        $laenderen['en']['KR'] = "South Korea"; $laender['KR'] = "Südkorea"; 
        $laenderen['en']['ES'] = "Spain"; $laender['ES'] = "Spanien"; 
        $laenderen['en']['LK'] = "Sri Lanka"; $laender['LK'] = "Sri Lanka"; 
        $laenderen['en']['SD'] = "Sudan"; $laender['SD'] = "Sudan"; 
        $laenderen['en']['SR'] = "Suriname"; $laender['SR'] = "Suriname"; 
        $laenderen['en']['SZ'] = "Swaziland"; $laender['SZ'] = "Swasiland"; 
        $laenderen['en']['SE'] = "Sweden"; $laender['SE'] = "Schweden"; 
        $laenderen['en']['CH'] = "Switzerland"; $laender['CH'] = "Schweiz"; 
        $laenderen['en']['SY'] = "Syria"; $laender['SY'] = "Syrien"; 
        $laenderen['en']['TW'] = "Taiwan"; $laender['TW'] = "Taiwan"; 
        $laenderen['en']['TJ'] = "Tajikistan"; $laender['TJ'] = "Tadschikistan"; 
        $laenderen['en']['TZ'] = "Tanzania"; $laender['TZ'] = "Tansania"; 
        $laenderen['en']['TH'] = "Thailand"; $laender['TH'] = "Thailand"; 
        $laenderen['en']['TG'] = "Togo"; $laender['TG'] = "Togo"; 
        $laenderen['en']['TO'] = "Tonga"; $laender['TO'] = "Tonga"; 
        $laenderen['en']['TT'] = "Trinidad and Tobago"; $laender['TT'] = "Trinidad und Tobago"; 
        $laenderen['en']['TN'] = "Tunisia"; $laender['TN'] = "Tunesien"; 
        $laenderen['en']['TR'] = "Turkey"; $laender['TR'] = "Türkei"; 
        $laenderen['en']['TM'] = "Turkmenistan"; $laender['TM'] = "Turkmenistan"; 
        $laenderen['en']['TV'] = "Tuvalu"; $laender['TV'] = "Tuvalu"; 
        $laenderen['en']['UG'] = "Uganda"; $laender['UG'] = "Uganda"; 
        $laenderen['en']['UA'] = "Ukraine"; $laender['UA'] = "Ukraine"; 
        $laenderen['en']['AE'] = "United Arab Emirates"; $laender['AE'] = "Vereinigte Arabische Emirate"; 
        $laenderen['en']['GB'] = "United Kingdom"; $laender['GB'] = "Vereinigtes Königreich"; 
        $laenderen['en']['US'] = "United States of America"; $laender['US'] = "Vereinigte Staaten von Amerika"; 
        $laenderen['en']['UY'] = "Uruguay"; $laender['UY'] = "Uruguay"; 
        $laenderen['en']['UZ'] = "Uzbekistan"; $laender['UZ'] = "Usbekistan"; 
        $laenderen['en']['VU'] = "Vanuatu"; $laender['VU'] = "Vanuatu"; 
        $laenderen['en']['VE'] = "Venezuela"; $laender['VE'] = "Venezuela"; 
        $laenderen['en']['VN'] = "Vietnam"; $laender['VN'] = "Vietnam"; 
        $laenderen['en']['VG'] = "Virgin Islands"; $laender['VG'] = "Virgin Islands"; 
        $laenderen['en']['EH'] = "Western Sahara"; $laender['EH'] = "Westsahara"; 
        $laenderen['en']['YE'] = "Yemen"; $laender['YE'] = "Jemen"; 
        $laenderen['en']['YU'] = "Yugoslavia"; $laender['YU'] = "Jugoslavien"; 
        $laenderen['en']['ZR'] = "Zaire"; $laender['ZR'] = "Zaire"; 
        $laenderen['en']['ZM'] = "Zambia"; $laender['ZM'] = "Sambia"; 
        $laenderen['en']['ZW'] = "Zimbabwe"; $laender['ZW'] = "Simbabwe";

        foreach($laender as $key => $laendersearch) {
            if($searchland == $laendersearch) {
                $fundkey = $key;
            } else {

            }
        }
        return $fundkey;
    }
}
