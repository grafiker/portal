<?php

namespace App\Controller;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserprofilDeleteController extends Controller
{
    /**
     * @Route("/profile/delete", name="userprofil_delete")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {
        $temp = (array) $request->request->all();
        $passwordValid = false;
        $sendask = false;
        $ispasswordValid = false;
        //$currentPassword = "bartasan";
        if(isset($temp["password"])) {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        /* Checking current user password with given password param from $reqest */
        $passwordValid = $passwordEncoder->isPasswordValid($users,$temp["password"] );
        }
        if($passwordValid) {
            $subjetsys = "Anfrage für Profillöschung ";
            $body = "Das Mitglied: ".$users->getUsername()." möchte sich gerne von grafiker.de löschen lassen!<br />Der Link zum Profil ist: <a target=\"_blank\" hraf=\"https://grafiker.de/member/" . $users->getUsername() . "\">https://grafiker.de/member/" . $users->getUsername() . "</a><br /><br />Als Löschgrund hat das Mitglied folgendes angegeben:<br />" . $temp["grund"];
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('userdelete@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody($body);
            $mailer->send($message);

            $sendask = true;
            $ispasswordValid = true;
        } else {
            $ispasswordValid = false;
        }
        return $this->render('userprofil/delete.html.twig', [
            'controller_name' => 'UserprofilDeleteController',
            'anfragesend' => $sendask,
            'ispasswordValid' => $ispasswordValid
        ]);
    }
}
