<?php

namespace App\Controller;

class MembersearchfilterController
{
    public function memberFilterOutput($dm, $filter, $searching, $anz, $page, $sort)
    {
        $anz = $anz;
        $page = $page*$anz;
        $sort = 'DESC';
        if($filter != "alle" and $filter != "stundenpreis" and $filter != "plz" and $filter != "skills" and $filter != "language" and $filter != "spezieals") {

            //var_dump($searching);
        $searchQuery = array(
            '$or' => array(
                array(
                    $filter => new \MongoRegex('/'.$searching.'/si'),
                    ),
                )
            );
        $userdata = $dm->getRepository('App:User')->findBy($searchQuery,array('updatedAt' => $sort), $anz, $page);
        } else if($filter == "skills" and $filter != "language" and $filter != "spezieals" and $filter != "alle") {
            //var_dump($searching);
            if($searching == 'Druckerei' or $searching == 'druckerei') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe6' => true,
                            ),
                        )
                    );
            } else if($searching == 'Grafiker') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe1' => true,
                            ),
                        )
                    );
            } else if($searching == 'Fotograf') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe2' => true,
                            ),
                        )
                    );
            } else if($searching == 'Programmierer' or $searching == 'entwickler' or $searching == 'Entwickler') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe3' => true,
                            ),
                        )
                    );
            } else if($searching == 'Texter') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe4' => true,
                            ),
                        )
                    );
            } else if($searching == 'Illustrator') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe5' => true,
                            ),
                        )
                    );
            } else if($searching == 'Sonstiges') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe7' => true,
                            ),
                        )
                    );
            } else if($searching == 'Auftraggeber') {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'berufsgruppe8' => true,
                            ),
                        )
                    );
            }
            $userdata = $dm->getRepository('App:User')->findBy($searchQuery,array('updatedAt' => $sort), $anz, $page);
        } else if($filter != "skills" and $filter == "language" and $filter != "alle") {
            $searchQuery = array(
                '$or' => array(
                    array(
                        $searching => true,
                        ),
                    )
                );
            $userdata = $dm->getRepository('App:User')->findBy($searchQuery,array('updatedAt' => $sort), $anz, $page);
        } else if($filter == "spezieals" and $filter != "alle") {
            $searchfilter = "skillsCloud";
            $userdata = $dm->getRepository('App:User')->findBy(array($searchfilter =>$searching),array('updatedAt' => $sort), $anz, $page);
        } else if($filter == "plz" or $filter == "stundenpreis" and $filter != "alle") {
            $searchfilter = $filter;
            $searching = (int)$searching;
            $userdata = $dm->getRepository('App:User')->findBy(array($searchfilter =>$searching),array('updatedAt' => $sort), $anz, $page);
        } else if($filter == "alle") {
            if(intval($searching)) { $plzsuche = intval($searching); } else { $plzsuche = 123456789; }
            if($searching == "Admin") {
            $searchQuery = array(
                '$or' => array(
                    array(
                        'roles' => new \MongoRegex('/'.$searching.'/si'),
                        ),
                    )
                );
            } else {
                $searchQuery = array(
                    '$or' => array(
                        array(
                            'username' => $searching,
                            ),
                        array(
                            'wohnort' => $searching,
                            ),
                        array(
                            'berufsbezeichnung' => new \MongoRegex('/'.$searching.'/si'),
                            ),
                        array(
                            'plz' => $plzsuche,
                            ),
                        array(
                            'land' => $searching,
                            ),
                        array(
                            'firma' => new \MongoRegex('/'.$searching.'/si'),
                            ),
                        array(
                            'beschreibung' => new \MongoRegex('/'.$searching.'/si'),
                            ),
                        array(
                            'roles' => new \MongoRegex('/'.$searching.'/si'),
                            ),
                        )
                    );
            }
            
            if($searching == "neuste") {
                $userdata = $dm->getRepository('App:User')->findBy(array(),array('createdAt' => $sort), $anz, $page);
                //var_dump($searching);
            } else {
            $userdata = $dm->getRepository('App:User')->findBy($searchQuery,array('updatedAt' => $sort), $anz, $page);
            }
        }
        $dm->flush();
        return $userdata;
    }
}
