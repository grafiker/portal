<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WiderrufController extends AbstractController
{
    /**
     * @Route("/widerruf", name="widerruf")
     */
    public function index()
    {
        return $this->render('widerruf/index.html.twig', [
            'controller_name' => 'WiderrufController',
        ]);
    }
}
