<?php

namespace App\Controller;

use App\Document\User;
use App\Document\Pins;
use App\Document\Job;
use App\Document\Network;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ApicallController extends Controller
{
    /**
     * @Route("/apicall/{von}/{bis}/{test}", name="apicall")
     */
    public function index($von, $bis, $test)
    {
$eintragung = false;
for($i = $von; $i <= $bis; $i++) {
    $task = new User();
        $url = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019';
        $json = file_get_contents($url);

$data_json_decoded = json_decode($json);
switch(json_last_error()) {
    case JSON_ERROR_NONE:
    $username = $data_json_decoded->{'username'};
    
    $em = $this->get('doctrine_mongodb')->getManager();
    $pincheck = $em->getRepository('App:User')->findOneBy(["username"=>$username]);
            $data = array();
            if (!$pincheck) {
                $eintragung = true;
            }
if($eintragung) {
    $task->setUsername($username);
    $vorname = $data_json_decoded->{'vorname'};
    $task->setFirstName($vorname);
    $nachname = $data_json_decoded->{'nachname'};
    $task->setLastName($nachname);
    $firma = $data_json_decoded->{'firma'};
    $task->setFirma($firma);
    $einkommen = $data_json_decoded->{'einkommen'};
    $task->setStundenpreis($einkommen);
    $birthdate = $data_json_decoded->{'birthdate'};
    $tag = date("j", strtotime($birthdate));
    $monat = date("n", strtotime($birthdate));
    $jahr = date("Y", strtotime($birthdate));
    $task->setGebDay($tag);
    $task->setGebMon($monat);
    $task->setGebYear($jahr);
    $aktiv = $data_json_decoded->{'aktiv'};
    if($aktiv == "1") {
        $task->setEnabled(true);
    } else {
        $task->setEnabled(false);
    }

    $email = $data_json_decoded->{'email'};
    $task->setEmail($email);
    $website = $data_json_decoded->{'website'};
    $task->setEmailCanonical($email);
    $telefon = $data_json_decoded->{'telefon'};
    $task->setTelefon($telefon);
    $fax = $data_json_decoded->{'fax'};
    $mobil = $data_json_decoded->{'mobil'};
    $task->setMobil($mobil);
    $street = $data_json_decoded->{'street'};
    $task->setStrassenummer($street);
    $plz = $data_json_decoded->{'plz'};
    $task->setPlz($plz);
    $ort = $data_json_decoded->{'ort'};
    $task->setWohnort($ort);
    $states = $data_json_decoded->{'states'};
    $land = $data_json_decoded->{'land'};
    $task->setLand($land);
    $aboutme = $data_json_decoded->{'aboutme'};
    $task->setBeschreibung($aboutme);
    $password = $data_json_decoded->{'md5password'};
    $task->setOldPassword($password);
    //$task->setPassword($password);
    $profession = $data_json_decoded->{'profession'};
    $task->setBerufsbezeichnung($profession);

    $lastlogin = date(DATE_ATOM, $data_json_decoded->{'lastlogin'});
    $date = \DateTime::createFromFormat('U', $data_json_decoded->{'lastlogin'});
            //$date->setTimezone(new \DateTimeZone('UTC'));
    $task->setLastLogin($date);

    $languages = $data_json_decoded->{'languages'};
    $extra_languages = $data_json_decoded->{'extra_languages'};
    $task->setAspeak($extra_languages);
    //deutsch,englisch,franzoesisch,italienisch,russisch,spanisch,aspeak
    $languas=explode(",",$languages);
    if (in_array("1", $languas)) {
        $task->setDeutsch(true);
    } else {
        $task->setDeutsch(false);
    }
    if (in_array("2", $languas)) {
        $task->setEnglisch(true);
    } else {
        $task->setEnglisch(false);
    }
    if (in_array("3", $languas)) {
        $task->setFranzoesisch(true);
    } else {
        $task->setFranzoesisch(false);
    }
    if (in_array("4", $languas)) {
        $task->setItalienisch(true);
    } else {
        $task->setItalienisch(false);
    }
    if (in_array("5", $languas)) {
        $task->setRussisch(true);
    } else {
        $task->setRussisch(false);
    }
    if (in_array("6", $languas)) {
        $task->setSpanisch(true);
    } else {
        $task->setSpanisch(false);
    }
    if (in_array("7", $languas)) {
        $task->setChinesisch(true);
    } else {
        $task->setChinesisch(false);
    }
    $avatar = $data_json_decoded->{'avatar'};
    $task->setUserpic($avatar);
if($avatar && $test == 0) {
    $avatar = str_replace(" ", "%20", $avatar);
    $contents=file_get_contents("https://www.grafiker.de/pages/images/members/".$avatar);
    //$contents1= str_replace("url entfernen","",$bildadresse);
    $file_headers = @get_headers("https://www.grafiker.de/pages/images/members/".$avatar);
    if($file_headers[0] != 'HTTP/1.1 404 Not Found') {
    $upload_dir = 'upload/uploads/userpics/';
    $savefile = fopen($upload_dir.$avatar, "w");
    fwrite($savefile, $contents);
    fclose($savefile);
    $url =(isset($_SERVER['HTTPS'])?'https':'http').'://' . $_SERVER['HTTP_HOST'];  
    echo'<img src="'.$url.'/'.$upload_dir.$avatar.'" /><br />';
    }
} else if($avatar && $test == 1) {
    $avatar = str_replace(" ", "%20", $avatar);
    echo'<img src="https://www.grafiker.de/pages/images/members/'.$avatar.'" /><br />';
}
    //var_dump($languas);
    echo $username;
    echo $vorname;
    echo $nachname;
    echo $firma;
    echo $einkommen;
    echo $tag . "." . $monat . "." . $jahr;
    echo $lastlogin;
    echo $aktiv;
    echo $email;
    echo $website;
    echo $telefon;
    echo $fax;
    echo $mobil;
    echo $street;
    echo $plz;
    echo $ort;
    echo $states;
    echo $land;
    echo $aboutme;
    echo $profession;
    echo $languages;
    echo $extra_languages;
}
    echo"<br />";
    break;
    case JSON_ERROR_DEPTH:
        echo ' - Maximale Stacktiefe überschritten';
    break;
    case JSON_ERROR_STATE_MISMATCH:
        echo ' - Unterlauf oder Nichtübereinstimmung der Modi';
    break;
    case JSON_ERROR_CTRL_CHAR:
        echo ' - Unerwartetes Steuerzeichen gefunden';
    break;
    case JSON_ERROR_SYNTAX:
        echo ' - Syntaxfehler, ungültiges JSON';
    break;
    case JSON_ERROR_UTF8:
        echo ' - Missgestaltete UTF-8 Zeichen, möglicherweise fehlerhaft kodiert';
    break;
    default:
        echo ' - Unbekannter Fehler';
    break;
}
//var_dump($json);
//var_dump($data_json_decoded);

//var_dump($json);
if($eintragung) {
$urlskills = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&skills=1';
        $jsonskills = file_get_contents($urlskills);
$urlskilltime = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&skilltime=1';
        $jsonskilltime = file_get_contents($urlskilltime);
$jsonskilltime = substr($jsonskilltime, 0, -1);
$jsonskilltime=explode(",",$jsonskilltime);


$urlskillkatid = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&katid=1';
        $jsonskillkatid = file_get_contents($urlskillkatid);
$jsonskillkatid = substr($jsonskillkatid, 0, -1);
$jsonskillkatid=explode(",",$jsonskillkatid);
//var_dump($jsonskillkatid);
foreach($jsonskillkatid as $jsonskillkatid) {
if($jsonskillkatid == 1) {
    $jsonskillkatid = "Grafiker";
    $setbg[0] = true;
} 
if($jsonskillkatid == 2) {
    $jsonskillkatid = "Programmierer";
    $setbg[2] = true;
} 
if($jsonskillkatid == 3) {
    $jsonskillkatid = "Fotograf";
    $setbg[1] = true;
} 
if($jsonskillkatid == 4) {
    $jsonskillkatid = "Texter";
    $setbg[3] = true;
} 
if($jsonskillkatid == 5) {
    $jsonskillkatid = "Illustrator";
    $setbg[4] = true;
} 
if($jsonskillkatid == 6) {
    $jsonskillkatid = "Sonstiges";
    $setbg[6] = true;
}
$setbg[5] = false;
    echo $jsonskillkatid . "<br />";
}
//echo "HIER" . "1" . $setbg1 . "2" . $setbg2 . "3" . $setbg3 . "4" . $setbg4 . "5" . $setbg5 . "7" . $setbg7;
if(isset($setbg[0])) {
$task->setBerufsgruppe1(true);
} else {
    $task->setBerufsgruppe1(false);  
}
if(isset($setbg[1])) {
$task->setBerufsgruppe2(true);
} else {
    $task->setBerufsgruppe2(false);  
}
if(isset($setbg[2])) {
$task->setBerufsgruppe3(true);
} else {
    $task->setBerufsgruppe3(false);  
}
if(isset($setbg[3])) {
$task->setBerufsgruppe4(true);
} else {
    $task->setBerufsgruppe4(false);  
}
if(isset($setbg[4])) {
$task->setBerufsgruppe5(true);
} else {
    $task->setBerufsgruppe5(false);  
}
$task->setBerufsgruppe6(false);
if(isset($setbg[6])) {
$task->setBerufsgruppe7(true);
} else {
    $task->setBerufsgruppe7(false);  
}
//var_dump($setbg);
$jsonskills = substr($jsonskills, 0, -1);
$jsonskills=explode(",",$jsonskills);
echo"Skills:<br />";
$counter=0;
$skillarray = array();
$skillinarray = array();
foreach($jsonskills as $jsonskills) {
    $jsonskills = str_replace(" ", "_", $jsonskills);
    $jsonskills = str_replace(".", "_", $jsonskills);
    $jsonskills = str_replace("/", "&", $jsonskills);
    $skillarray[$jsonskills] = $jsonskilltime[$counter];
    $skillinarray[$jsonskills] = $jsonskills;
    echo $jsonskills . ' ' . $jsonskilltime[$counter] . "<br />";
    $counter++;
}
if($jsonskills) {
$task->setSkills($skillarray);
$task->setSkillsCloud($skillinarray);
}
//var_dump($skillarray);

    //$name = $data_json_decodedskills->{'name'};
    //echo $name;
$dm = $this->get('doctrine_mongodb')->getManager();
if($test == 0) {
$dm->persist($task);
} else {
var_dump($task);
}
$dm->flush();
}
}

        return $this->render('apicall/index.html.twig', [
            'controller_name' => 'ApicallController',
        ]);
    }

    /**
     * @Route("/apicall/showcases/{von}/{bis}/{test}", name="showcase_apicall")
     */
    public function showcaseAction($von, $bis, $test)
    {
$close=true;
if($close) {
    for($i = $von; $i <= $bis; $i++) {
        $eintragung = false; 
        $task = new Pins();


$urlpics = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&showcases=1&picsindb=1';
$jsonpics = file_get_contents($urlpics);
$jsonpics = substr($jsonpics, 0, -1);
$jsonpics=explode(",",$jsonpics);
//var_dump($jsonpics);

if("" != $jsonpics[0]) {

echo"<br />";
if($test == 0) {
$jsonpicsnew = array();
foreach($jsonpics as $pics) {
$pics = str_replace(" ", "%20", $pics);
    //$contents1= str_replace("url entfernen","",$bildadresse);
    $file_headers = @get_headers("https://www.grafiker.de/pages/images/portfolio/images/cards/".$pics);
    if($file_headers[0] != 'HTTP/1.1 404 Not Found') {
    $contents=file_get_contents("https://www.grafiker.de/pages/images/portfolio/images/cards/".$pics);
    $upload_dir = 'upload/uploads/pinpics/';
    $savefile = fopen($upload_dir.$pics, "w");
    fwrite($savefile, $contents);
    fclose($savefile);
    $url =(isset($_SERVER['HTTPS'])?'https':'http').'://' . $_SERVER['HTTP_HOST'];  
    echo'<img src="'.$url.'/'.$upload_dir.$pics.'" /><br />';

    $jsonpicsnew[] .= $pics;

    /*$contents=file_get_contents("https://www.grafiker.de/pages/images/portfolio/images/previews/".$pics);
    //$contents1= str_replace("url entfernen","",$bildadresse);
    $file_headers = @get_headers("https://www.grafiker.de/pages/images/portfolio/images/previews/".$pics);
    if($file_headers[0] != 'HTTP/1.1 404 Not Found') {
    $upload_dir = 'upload/uploads/pinpics/';
    $savefile = fopen($upload_dir.$pics, "w");
    fwrite($savefile, $contents);
    fclose($savefile);
    $url =(isset($_SERVER['HTTPS'])?'https':'http').'://' . $_SERVER['HTTP_HOST'];  
    echo'<img src="'.$url.'/'.$upload_dir.$pics.'" /><br />';
    }*/
  }
}

$task->setPicname($jsonpicsnew);
if(isset($jsonpicsnew[0])) {
$task->setThumbnail($jsonpicsnew[0]);
}
$task->setPinkategorie("Showcases");
}
echo"<br />";

if($test == 0) {

        $url = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&showcases=1';
        $json = file_get_contents($url);

        $data_json_decoded = json_decode($json);
        switch(json_last_error()) {
            case JSON_ERROR_NONE:
            $id = $data_json_decoded->{'id'};
            $rewritename = $data_json_decoded->{'rewrite_name'};
            $header = $data_json_decoded->{'header'};
            $content = $data_json_decoded->{'text'};
            $credate = $data_json_decoded->{'cre_date'};

            $credate = date(DATE_ATOM, strtotime($credate));
            $date = \DateTime::createFromFormat('U', $credate);

            echo $id;
            echo $rewritename;
            echo $header;
            echo $content;
            echo $credate;
            $task->setTitle($header);
            $task->setAstitle("Startinsert");
            $task->setBewerten(true);
            $task->setRechte(true);
            $task->setAktiv(true);
            $task->setStartdate($credate);
            $task->setContent($content);
            $task->setKommentare(false);

            $regheader = str_replace(" ", "_", $header);
            $regheader = str_replace(".", "_", $regheader);
            $regheader = str_replace("/", "&", $regheader);
            $em = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $em->getRepository('App:Pins')->findOneBy(["pinlink"=>$regheader.'_'.$id]);
                    $data = array();
                    if (!$pincheck) {
                        $eintragung = true;
                    } else {
                        $eintragung = false;
                    }
            $task->setPinlink($regheader.'_'.$id);
            break;
            case JSON_ERROR_DEPTH:
                echo ' - Maximale Stacktiefe überschritten';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Unterlauf oder Nichtübereinstimmung der Modi';
            break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Unerwartetes Steuerzeichen gefunden';
            break;
            case JSON_ERROR_SYNTAX:
                echo ' - Syntaxfehler, ungültiges JSON';
            break;
            case JSON_ERROR_UTF8:
                echo ' - Missgestaltete UTF-8 Zeichen, möglicherweise fehlerhaft kodiert';
            break;
            default:
                echo ' - Unbekannter Fehler';
            break;
        }


        $urluser = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&showcases=1&usernameindb=1';
        $jsonuser = file_get_contents($urluser);
        $jsonuser=explode(",",$jsonuser);
        var_dump($jsonuser);
        echo"<br />";
   if($eintragung) {
        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:User')->findOneBy(["username"=>$jsonuser[0]]);
                $data = array();
                if (!$pincheck) {
                    $eintragung = false;
                } else {
                    $eintragung = true;
                }
            }
        $task->setUsername($jsonuser[0]);
        $task->setWohnort($jsonuser[1]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        if($eintragung and $test == 0 and isset($jsonpicsnew[0])) {
        $dm->persist($task);
        } else {
        var_dump($task);
        }
        $dm->flush();
    }
    }
}
}
        return $this->render('apicall/index.html.twig', [
            'controller_name' => 'ApicallController',
        ]);
    }

    /**
     * @Route("/apicall/jobs/{von}/{bis}/{test}", name="jobs_apicall")
     */
    public function jobsAction($von, $bis, $test)
    {
        $eintragung = false;
        for($i = $von; $i <= $bis; $i++) {
            $eintragung = false; 
            $task = new Job();
    
    
    $urlpics = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&jobs=1';
    $json = file_get_contents($urlpics);

$data_json_decoded = json_decode($json);
switch(json_last_error()) {
    case JSON_ERROR_NONE:
    //var_dump($data_json_decoded);
    //echo"<br /><br />";
    $id = $data_json_decoded->{'id'};
    $titel = $data_json_decoded->{'titel'};
    $artid = $data_json_decoded->{'artid'};
    $username = $data_json_decoded->{'username'};
    $firma = $data_json_decoded->{'firma'};
    $titel = $data_json_decoded->{'titel'};
    $beschreibung = $data_json_decoded->{'beschreibung'};
    $about_us = $data_json_decoded->{'about_us'};
    $wirsuchen = $data_json_decoded->{'wirsuchen'};
    $zusatz = $data_json_decoded->{'zusatz'};
    $kontaktinfo = $data_json_decoded->{'kontaktinfo'};
    $aktiv = $data_json_decoded->{'aktiv'};
    $startdatum = $data_json_decoded->{'startdatum'};
    //$startdatumaustimestamp = $data_json_decoded->{'startdatumaustimestamp'};
    //$enddatumaustimestamp = $data_json_decoded->{'enddatumaustimestamp'};
    $workgroup_id = $data_json_decoded->{'workgroup_id'};
    $contact_name = $data_json_decoded->{'contact_name'};
    $contact_tel = $data_json_decoded->{'contact_tel'};
    $contact_email = $data_json_decoded->{'contact_email'};
    if($artid == 1) {
        $artid = "Ausbildung / Praktikum";
    } else if($artid == 2) {
        $artid = "Festanstellung";
    } else if($artid == 3) {
        $artid = "Freelancer";
    }
    if($workgroup_id == 1) {
        $workgroup_id = "entwickler";
    } else if($workgroup_id == 2) {
        $workgroup_id ="grafiker";
    } else if($workgroup_id == 3) {
        $workgroup_id ="fotograf";
    } else if($workgroup_id == 4) {
        $workgroup_id ="texter";
    } else if($workgroup_id == 6) {
        $workgroup_id ="illustrator";
    }
    $em = $this->get('doctrine_mongodb')->getManager();
    $jobusercheck = $em->getRepository('App:User')->findOneBy(["username"=>$username]);
            $data = array();
            if ($jobusercheck) {
                $eintragung = true;
            }
if($eintragung) {
    echo $artid;
    $task->setJobTyp($artid);
    echo "<br />";
    echo $titel;
    $task->setAstitle($titel);
    $regheader = str_replace(" ", "_", $titel);
    $regheader = str_replace(".", "_", $regheader);
    $regheader = str_replace("/", "&", $regheader);
    $task->setJoblink($regheader.'_'.$id);
    echo "<br />";
    echo $username;
    $task->setUsername($username);
    echo $firma;
    if($firma != "") {
        $task->setFirma($firma); 
    } else {
        $task->setFirma($username); 
    }
    echo "<br />";
    echo $beschreibung;
    $beschreibung = $beschreibung . '<br /><br />' . $about_us . '<br /><br />' . $wirsuchen . '<br /><br />' . $zusatz  . '<br /><br />' . $kontaktinfo;
    $task->setContent($beschreibung);
    echo "<br />";
    echo $about_us;
    echo "<br />";
    echo $wirsuchen;
    echo "<br />";
    echo $zusatz;
    echo "<br />";
    echo $kontaktinfo;
    echo "<br />";
    echo $aktiv;
    if($aktiv == 1) {
        $task->setAktiv(true);
    } else {
        $task->setAktiv(false);
    }
    echo "<br />";

    $startdatumaustimestamp = date(DATE_ATOM, $data_json_decoded->{'startdatumaustimestamp'});
    $startdate = \DateTime::createFromFormat('U', $data_json_decoded->{'startdatumaustimestamp'});
    echo $startdatumaustimestamp;
    $task->setStartdate($startdatumaustimestamp);
    echo"<br />";
    $enddatumaustimestamp = date(DATE_ATOM, $data_json_decoded->{'enddatumaustimestamp'});
    $startdate = \DateTime::createFromFormat('U', $data_json_decoded->{'enddatumaustimestamp'});
    $task->setEnddate($enddatumaustimestamp);
    echo $enddatumaustimestamp;
    echo"<br />";
    echo $workgroup_id;
    $task->setFinde($workgroup_id);
    echo"<br />";
    echo $contact_name;
    $task->setContactName($contact_name);
    echo"<br />";
    echo $contact_tel;
    $task->setContactTel($contact_tel);
    echo"<br />";
    echo $contact_email;
    $task->setContactEmail($contact_email);
    $task->setFor('Alle');
    $task->setJobort('');
    echo"<br />";
    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($task);
    $dm->flush();
}
    var_dump($task);
    echo"<br />";
    echo"<br />";
    break;
            case JSON_ERROR_DEPTH:
                echo ' - Maximale Stacktiefe überschritten';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Unterlauf oder Nichtübereinstimmung der Modi';
            break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Unerwartetes Steuerzeichen gefunden';
            break;
            case JSON_ERROR_SYNTAX:
                echo ' - Syntaxfehler, ungültiges JSON';
            break;
            case JSON_ERROR_UTF8:
                echo ' - Missgestaltete UTF-8 Zeichen, möglicherweise fehlerhaft kodiert';
            break;
            default:
                echo ' - Unbekannter Fehler';
            break;
        }
    }
    
    return $this->render('apicall/index.html.twig', [
        'controller_name' => 'ApicallController',
    ]);
    }
        /**
     * @Route("/apicall/network/{von}/{bis}/{test}", name="jobs_apicall")
     */
    public function networkAction($von, $bis, $test)
    {
        $eintragung = false;
        for($i = $von; $i <= $bis; $i++) {
            $eintragung = false; 
            $task = new Network();
    
    
    $urlpics = 'https://www.grafiker.de/calluserapi/?datensatz='.$i.'&pw=grafiker2019&network=1';
    $json = file_get_contents($urlpics);

    $data_json_decoded = json_decode($json);

switch(json_last_error()) {
    case JSON_ERROR_NONE:
    //var_dump($data_json_decoded);
    $username = $data_json_decoded->{'username'};    
    $friend = $data_json_decoded->{'friend'}; 
    $request = $data_json_decoded->{'request'}; 
    $reason = $data_json_decoded->{'reason'}; 
    $acceptreason = $data_json_decoded->{'acceptreason'}; 
    $credate = $data_json_decoded->{'credate'}; 
    $credate = date(DATE_ATOM, strtotime($credate));
    $date = \DateTime::createFromFormat('U', $credate);
    $em = $this->get('doctrine_mongodb')->getManager();
    $friendcheck = $em->getRepository('App:Network')->findOneBy(array("username"=>$username, "friend"=>$friend));

            if (!$friendcheck) {
                $friendcheck = true;
            } else {
                $friendcheck = false;
            }
    $usercheck = $em->getRepository('App:Network')->findOneBy(array("username"=>$friend, "friend"=>$username));
//var_dump($usercheck);
        if (!$usercheck) {
            $usercheck = true;
        } else {
            $usercheck = false;
        }
    if($friendcheck and $usercheck) {
    $task->setUsername($username);   
    $task->setFriend($friend);   
    $task->setRequest($request);   
    $task->setReason($reason);   
    $task->setAcceptreason($acceptreason);   
    $task->setStartdate($credate);   
    $task->setAccepted(1);   
    //echo $friend;
    var_dump($task);
    echo"<br />";
    echo"<br />";
    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($task);
    $dm->flush();
    }
    break;
            case JSON_ERROR_DEPTH:
                echo ' - Maximale Stacktiefe überschritten';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Unterlauf oder Nichtübereinstimmung der Modi';
            break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Unerwartetes Steuerzeichen gefunden';
            break;
            case JSON_ERROR_SYNTAX:
                echo ' - Syntaxfehler, ungültiges JSON';
            break;
            case JSON_ERROR_UTF8:
                echo ' - Missgestaltete UTF-8 Zeichen, möglicherweise fehlerhaft kodiert';
            break;
            default:
                echo ' - Unbekannter Fehler';
            break;
    }
        }
        return $this->render('apicall/index.html.twig', [
            'controller_name' => 'ApicallController',
        ]);
    }
    /**
     * @Route("/apicall/delete", name="delete_apicall")
     */
    public function deleteAction()
    {
$dm = $this->get('doctrine_mongodb')->getManager();
$userdatagesamt = $dm->createQueryBuilder('App:Job')->remove()->getQuery()->execute();

/*function deleteFilesFromDirectory($ordnername){
//überprüfen ob das Verzeichnis überhaupt existiert
if (is_dir($ordnername)) {
//Ordner öffnen zur weiteren Bearbeitung
if ($dh = opendir($ordnername)) {
//Schleife, bis alle Files im Verzeichnis ausgelesen wurden
while (($file = readdir($dh)) !== false) {
//Oft werden auch die Standardordner . und .. ausgelesen, diese sollen ignoriert werden
if ($file!="." AND $file !="..") {
//Files vom Server entfernen
unlink("".$ordnername."".$file."");
}
}
//geöffnetes Verzeichnis wieder schließen
closedir($dh);
}
}
}
 
//Funktionsaufruf - Directory immer mit endendem / angeben
deleteFilesFromDirectory("upload/uploads/pinpics/");
*/    
    return $this->render('apicall/index.html.twig', [
        'controller_name' => 'ApicallController',
    ]);
}
}
