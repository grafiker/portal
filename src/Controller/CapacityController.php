<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class CapacityController extends Controller
{
    /**
     * @Route("/capacity", name="capacity")
     */
    public function index(Request $request)
    {
        $newarray=$request->request->all();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $dm->getRepository('App:User')->findOneByUsername($newarray["username"]);

            $user->setCapacity($newarray["capacity"]);
            $ausgabe="Employ = true";

        $dm->persist($user);
        $dm->flush();
        return $this->render('capacity/index.html.twig', [
            'controller_name' => 'CapacityController',
        ]);
    }
}
