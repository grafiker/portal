<?php

namespace App\Controller;
use App\Document\Pins;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class LoaddataController extends Controller
{
    /**
     * @Route("/loaddata/{pinID}/{filter}", name="loaddata")
     */
    public function index(Request $request, $pinID, $filter)
    {
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
if($filter != "all") {
        $filter = explode("-", $filter);
        if($filter[0] != "Search") {
        if($users == $filter[1]) {
            $ownerfilter = $filter[1];
            $userfilter = '';
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        } else if('ALL' == $filter[1]) {
            $ownerfilter = '';
            $userfilter = '';
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        } else {
            $ownerfilter = '';
            $userfilter = $filter[1];
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        }
    } else {
        $ownerfilter = '';
        $userfilter = '';
        $suchnach = $filter[0].'-'.$filter[1];
        $newfilter = $filter[0].'-'.$filter[1];
    }
} else {
        $ownerfilter = '';
        $userfilter = '';
        $suchnach = 'all';
        $newfilter = $suchnach;
}
        //var_dump($suchnach);
        $pindatas = new PinoutputController();
        $userdata = new CheckUserdataController();
        $page=$pinID/10;
        $anzprosite=10;
        $pinlastLogin=false;
        $useremploy=false;
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pindata = $pindatas->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), $anzprosite, $page,'DESC',$suchnach,$ownerfilter,$userfilter,7,'Startinsert');
        //$pindata = $dm->getRepository('App:Pins')->findBy(array(),array('id' => 'DESC'), $anzprosite, $page);
        $fileexistarray=array();
        $filelinkarray=array();
        $wohnort=array();
        $userexist=array();
        foreach($pindata as $key => $pinuser) {
            //var_dump($pinuser);
            $dm = $this->get('doctrine_mongodb')->getManager();
                $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$pinuser->getUsername()]);
                if($userdatas) {
                $fileexistarray[$pinuser->getUsername()] = $userdatas->getUserpic();
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $wohnort[$pinuser->getUsername()] = $userdatas->getWohnort();
                $pinlastLogin[$pinuser->getUsername()] = $userdatas->getUpdatedAt();
                $useremploy[$pinuser->getUsername()] = $userdatas->getEmploy();
                $userexist[$pinuser->getUsername()] = true;
            } else {
                $userexist[$pinuser->getUsername()] = false;
            }
                $dm->flush();
        }
        //var_dump($wohnort);
        return $this->render('loaddata/index.html.twig', [
            'controller_name' => 'LoaddataController',
            'pinID' => $pinID,
            'pindata' => $pindata,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'wohnort' => $wohnort,
            'filter' => $newfilter,
            'userexist' => $userexist,
        ]);
    }
}
