<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RelaunchController extends AbstractController
{
    /**
     * @Route("/relaunch", name="relaunch")
     */
    public function index()
    {
        return $this->render('relaunch/index.html.twig', [
            'controller_name' => 'RelaunchController',
        ]);
    }
}
