<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServicePaysettingsController extends AbstractController
{
    /**
     * @Route("/service/paysettings", name="service_paysettings")
     */
    public function index()
    {
        return $this->render('service_paysettings/index.html.twig', [
            'controller_name' => 'ServicePaysettingsController',
        ]);
    }
}
