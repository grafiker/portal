<?php

namespace App\Controller;
use App\Document\User;
use App\Document\Pins;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class SitemapController extends Controller
{
    /**
     * @Route("/sitemap", name="sitemap_start")
     */
    public function createAction()
    {
        return $this->render('sitemap/start.html.twig', [
            'controller_name' => 'SitemapController',
        ]);
    }
    /**
     * @Route("/sitemap/all", name="sitemap_all")
     */
    public function allAction()
    {
        return $this->render('sitemap/all.html.twig', [
            'controller_name' => 'SitemapController',
        ]);
    }
    /**
     * @Route("/sitemap/{variable}/{pages}", name="sitemap")
     */
    public function index($variable, $pages)
    {
        if($pages != "all") {
$page = $pages-1;
$anz = 5000;
$page = $anz * $page;
        }
        $em = $this->get('doctrine_mongodb')->getManager();
        $userarray = array();
        if($variable == "user") {
        $user = $em->getRepository('App:User')->findBy(array(),array(),$anz,$page);
        //var_dump($user);
            foreach($user as $key => $user) {
                $userarray[$key] = $user->getUsername();
            }
        }
        $pinsarray = array();
        if($variable == "pins") {
        $pins = $em->getRepository('App:Pins')->findBy(["aktiv" => true]);
            foreach($pins as $key => $pins) {
                $pinsarray[$key] = $pins->getPinlink();
            }
        }
        $jobsarray = array();
        if($variable == "jobs") {
        $jobs = $em->getRepository('App:Job')->findBy(["aktiv" => true]);
            foreach($jobs as $key => $jobs) {
                $jobsarray[$key] = $jobs->getJoblink();
            }
        }
        $em->flush();
        return $this->render('sitemap/index.html.twig', [
            'controller_name' => 'SitemapController',
            'userarray' => $userarray,
            'pinsarray' => $pinsarray,
            'jobsarray' => $jobsarray,
            'variable' => $variable,
        ]);
    }
}
