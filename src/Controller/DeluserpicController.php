<?php

namespace App\Controller;

use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class DeluserpicController extends Controller
{
    /**
     * @Route("/deluserpic/{delthis}/{username}/{userpic}", name="deluserpic")
     */
    public function index($delthis, $username, $userpic)
    {
        if($delthis == "user") {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if($username == $users) {
            //var_dump("JA");
        $em = $this->get('doctrine_mongodb')->getManager();
        $userdb = $em->getRepository('App:User')->findOneBy(["username"=>$username]);

        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/userpics/".$userdb->getUserpic());

        $userdb->setUserpic('del');
        $em->flush();
        }
    } else if($delthis == "pinpics") { 
        $em = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $em->getRepository('App:Pins')->findOneBy(array("username"=>$username, "astitle" => "Pinstart"));
            
        $i=0;
        //var_dump(count($pincheck->getPicname()));
        if(count($pincheck->getPicname()) == 1) {
            $arraydel = array();
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pincheck->getPicname()[0]);
            $pincheck->setPicname($arraydel);
            //var_dump($pincheck->getPicname());
        } else {
                foreach($pincheck->getPicname() as $key => $value)
                {
                    //var_dump("JETZT 2!!!");
                    //var_dump($value);
                    if($value == $userpic) {
                        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$userpic);
                    //var_dump($value);
                    } else {
                        //var_dump($value);
                        $data[$key] = $value;
                        $pincheck->setPicname($data);
                    }
                    //$data[$key] = $value;
                    
                    //$pincheck->setPicname($data);
                    $i = $key;
                }
                //var_dump($userpic);

                
            }
            $em->flush();
    } else {
        if(isset($_GET["widgetusername"])) {
            $em = $this->get('doctrine_mongodb')->getManager();
            $user = $em->getRepository('App:User')->findOneBy(array('username' => $_GET["widgetusername"]));
            $users = $user->getUsername();
            $wohnort = $user->getWohnort();
        } else {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        if($username == $users) {
            //var_dump("JA");
        $em = $this->get('doctrine_mongodb')->getManager();
        $userdb = $em->getRepository('App:User')->findOneBy(["username"=>$username]);

        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/firma/".$userdb->getFirmapic());

        $userdb->setFirmapic('del');
        $em->flush();
    }
}
$userpic = array('uploaded' => $userpic );
        return $this->render('deluserpic/index.html.twig', [
            'controller_name' => 'DeluserpicController',
            'ausgabe' => json_encode($userpic),
        ]);

}
}