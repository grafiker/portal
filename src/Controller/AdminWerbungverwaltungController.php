<?php

namespace App\Controller;
use App\Document\Banner;
use App\AppBundle\Form\WerbungFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
class AdminWerbungverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/admin/werbungverwaltung/{is}", name="admin_werbungverwaltung")
     */
    public function createAction(Request $request, $is)
    {
        $task = new Banner();
        $builder = $this->createForm(WerbungFormType::class, $task);

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
        //var_dump($task);
        
        if(isset($temp["form"]["position"])) {
            //var_dump($temp["form"]);
            //["sponsorname"]
            //["bannerlinkView"]
            //["bannerlinkClick"]
            //["alttag"]
            //["quelltext"]
            //["reload"]
            //["maxKlicks"]
            //["maxViews"]
            $task->setSponsorname($temp["form"]["sponsorname"]);
            $task->setBannerlinkView($temp["form"]["bannerlinkView"]);
            $task->setBannerlinkClick($temp["form"]["bannerlinkClick"]);
            $task->setAlttag($temp["form"]["alttag"]);
            $task->setTarget($temp["form"]["target"]);
            $task->setQuelltext($temp["form"]["quelltext"]);
            $task->setReload($temp["form"]["reload"]);
            if($temp["form"]["maxKlicks"] == "") {
            $task->setMaxKlicks(100000000);
            $task->setReloadClicks(true);
            } else {
            $task->setMaxKlicks($temp["form"]["maxKlicks"]);
            $task->setReloadClicks(false);
            }
            if($temp["form"]["maxViews"] == "") {
            $task->setMaxViews(100000000);
            $task->setReloadViews(true);
            } else {
            $task->setMaxViews($temp["form"]["maxViews"]);
            $task->setReloadViews(false);
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            //var_dump($task);
        }
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $bannerobencount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'oben'));
        $bannerrechtscount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'rechts'));
        $bannermobilcount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'mobil'));
        $bannerpartnercount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'partner'));
        return $this->render('admin/werbungverwaltung.html.twig', [
            'controller_name' => 'AdminWerbungverwaltungController',
            'form' => $builder->createView(),
            'allbanneroben' => $bannerobencount,
            'allbannerrechts' => $bannerrechtscount,
            'allbannermobil' => $bannermobilcount,
            'allbannerpartner' => $bannerpartnercount,
            'delad' => false,
            'del' => $is,
        ]);
    }
    /**
     * Delete
     * @Route("/admin/werbungverwaltung/add/{delId}", name="admin_werbungverwaltung_del")
     */
    public function deleteAction(Request $request, $delId)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $delete = $em->getRepository('App:Banner')->findOneById($delId);
        //var_dump($delete);
        $em->remove($delete); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
        $em->flush(); 
        $task = new Banner();
        $builder = $this->createForm(WerbungFormType::class, $task);

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
        //var_dump($task);
        
        if(isset($temp["form"]["position"])) {
            //var_dump($temp["form"]);
            //["sponsorname"]
            //["bannerlinkView"]
            //["bannerlinkClick"]
            //["alttag"]
            //["quelltext"]
            //["reload"]
            //["maxKlicks"]
            //["maxViews"]
            $task->setSponsorname($temp["form"]["sponsorname"]);
            $task->setBannerlinkView($temp["form"]["bannerlinkView"]);
            $task->setBannerlinkClick($temp["form"]["bannerlinkClick"]);
            $task->setAlttag($temp["form"]["alttag"]);
            $task->setTarget($temp["form"]["target"]);
            $task->setQuelltext($temp["form"]["quelltext"]);
            $task->setReload($temp["form"]["reload"]);
            if($temp["form"]["maxKlicks"] == "") {
            $task->setMaxKlicks(100000000);
            $task->setReloadClicks(true);
            } else {
            $task->setMaxKlicks($temp["form"]["maxKlicks"]);
            $task->setReloadClicks(false);
            }
            if($temp["form"]["maxViews"] == "") {
            $task->setMaxViews(100000000);
            $task->setReloadViews(true);
            } else {
            $task->setMaxViews($temp["form"]["maxViews"]);
            $task->setReloadViews(false);
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            //var_dump($task);
        }
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $bannerobencount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'oben'));
        $bannerrechtscount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'rechts'));
        $bannermobilcount = $emdsend->getRepository('App:Banner')->findBy(array("position"=>'mobil'));
        return $this->render('admin/werbungverwaltung.html.twig', [
            'controller_name' => 'AdminWerbungverwaltungController',
            'form' => $builder->createView(),
            'allbanneroben' => $bannerobencount,
            'allbannerrechts' => $bannerrechtscount,
            'allbannermobil' => $bannermobilcount,
            'delad' => true,
            'del' => false,
        ]);
    }
    /**
     * Status
     * @Route("/admin/werbungverwaltung/status/{statusId}", name="admin_werbungverwaltung_status")
     */
    public function statusAction(Request $request, $statusId)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $statusNew = $em->getRepository('App:Banner')->findOneById($statusId);
        //var_dump($delete);
        //$em->remove($delete); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
        if($statusNew->getDisenable() == NULL) {
            $statusNew->setDisenable(true);
            //var_dump($statusNew->getDisenable());
        } else {
            $statusNew->setDisenable(NULL);
            //var_dump($statusNew->getDisenable());
        }
        $task = new Banner();
        $builder = $this->createForm(WerbungFormType::class, $task);

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();

        
        $bannerobencount = $em->getRepository('App:Banner')->findBy(array("position"=>'oben'));
        $bannerrechtscount = $em->getRepository('App:Banner')->findBy(array("position"=>'rechts'));
        $bannermobilcount = $em->getRepository('App:Banner')->findBy(array("position"=>'mobil'));
        $em->flush(); 
        return $this->render('admin/werbungverwaltung.html.twig', [
            'controller_name' => 'AdminWerbungverwaltungController',
            'form' => $builder->createView(),
            'allbanneroben' => $bannerobencount,
            'allbannerrechts' => $bannerrechtscount,
            'allbannermobil' => $bannermobilcount,
            'delad' => false,
            'del' => false,
            'status' => true,
        ]);
    }
}
