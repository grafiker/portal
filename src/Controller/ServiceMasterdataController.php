<?php

namespace App\Controller;

use App\AppBundle\Form\UsereditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceMasterdataController extends AbstractController
{
    function __construct(ManagerRegistry $doctrine_mongodb, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/service/masterdata", name="service_masterdata")
     */
    public function index(Request $request)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}

        $builder = $this->createForm(UsereditType::class, $users);
        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            $this->_doctrine_mongodb->getManager()->persist($task);
            $this->_doctrine_mongodb->getManager()->flush();
//\var_dump($task);
        }
        return $this->render('service_masterdata/index.html.twig', [
            'form' => $builder->createView(),
            'controller_name' => 'ServiceMasterdataController',
        ]);
    }
}
