<?php

namespace App\Controller;
use App\Document\Pins;
use App\Document\User;
use App\AppBundle\Form\AdminPinsEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class AdminPinverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/admin/pinverwaltung/{page}", name="admin_pinverwaltung")
     */
    public function createAction(Request $request, $page)
    {
        
        $userdata = new CheckUserdataController();
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            $filelinkarray=array();
            $reexistkarray=array();
            $anbexistkarray=array();
            $usernameId=array();
            foreach($pindata as $key => $pinuser) {
                $fileexistarray[$pinuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
            }
foreach($pindata as $key => $pinids) {
$pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 1));
$pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 2));
$userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $pinids->getUsername()));
$usernameId[$pinids->getId()] = $userdata->getId();
if($pdfcheckre) {
    $reexistkarray[$pinids->getId()] = $pdfcheckre->getRechnungsnummer();
    }
    if($pdfcheckanb) {
    $anbexistkarray[$pinids->getId()] = $pdfcheckanb->getRechnungsnummer();
    }
if(!$pdfcheckre) {
$reexistkarray[$pinids->getId()] = false;
}
if(!$pdfcheckanb) {
$anbexistkarray[$pinids->getId()] = false;
}
}
        return $this->render('admin/pinverwaltung.html.twig', array(
            'pindata' => $pindata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($pindatagesamt)/$anzprosite,
        ));
    }

    /**
     * edit
     * @Route("/admin/pinverwaltung/{pinID}/{action}", name="admin_pinverwaltung_edit")
     */
    
    public function editAction(Request $request, $pinID, $action)
    {
        $userdata = new CheckUserdataController();
        $task = new Pins();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);
if($pincheck->getPicname()) {
            foreach($pincheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics);
                } else {}
            }
        }
            
            if ($pincheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pincheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pincheck->getThumbnail());
            } else {}
            $emdel->remove($pincheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            foreach($pindata as $key => $pinuser) {
                $fileexistarray[$pinuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($pindata as $key => $pinids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $pinids->getUsername()));
                $usernameId[$pinids->getId()] = $userdata->getId();
                if($pdfcheckre) {
                    $reexistkarray[$pinids->getId()] = $pdfcheckre->getRechnungsnummer();
                    }
                    if($pdfcheckanb) {
                    $anbexistkarray[$pinids->getId()] = $pdfcheckanb->getRechnungsnummer();
                    }
                if(!$pdfcheckre) {
                $reexistkarray[$pinids->getId()] = false;
                }
                if(!$pdfcheckanb) {
                $anbexistkarray[$pinids->getId()] = false;
                }
                }
        return $this->render('admin/pinverwaltung.html.twig', array(
            'pindata' => $pindata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($pindatagesamt)/$anzprosite,
        ));
        } else if ($action == "edit" or $action == "all") {

        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Pinskategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
        }
        if(!$pincheck->getIsads()) {
            $isads = false;
        } else {
            $isads = $pincheck->getIsads();
        }
        var_dump($pincheck->getIsads());
        $newextarray=array(array("pinID" => $pinID,"pinkategorie" => $pincheck->getPinkategorie(),"isads" => $isads,"bewerten" => $pincheck->getBewerten(),"title" => $pincheck->getTitle(),"content" => $pincheck->getContent(),"rechte" => $pincheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminPinsEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $pindata = $dms->getRepository('App:Pins')->find($pinID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($pindata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics);
    } else {}
    }

                $pindata->setTitle($task["titel"]);
                $pindata->setPinkategorie($task["pinkategorie"]);
                $pindata->setContent($task["content"]);
                $pindata->setIsads($task["isads"]);
                $pindata->setBewerten($task["bewerten"]);
                $pindata->setPicname($array);
                $dms->flush();
            }
            $casepics = array();
            $casepics=$pincheck->getPicname(); 
        return $this->render('admin/pinedit.html.twig', array(
            'form' => $builder->createView(),
            'pinID' => $pinID,
            'username' => $pincheck->getUsername(),
            'picsarray' => $casepics,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);

        $pincheck->setAstitle("Startinsert");
        $pincheck->setAktiv(true);
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$pincheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('%%Systemmail%% Ihr Pin wurde freigeschaltet!')
            ->setBody('Ihr Pin mit dem Titel: <strong style="color:red;">' . $pincheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'DESC'), $anzprosite, $page);
            $fileexistarray=array();
            $filelinkarray=array();
            $usernameId=array();
            foreach($pindata as $key => $pinuser) {
                $fileexistarray[$pinuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($pindata as $key => $pinids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("pinid" => $pinids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $pinids->getUsername()));
                $usernameId[$pinids->getId()] = $userdata->getId();
                
                if($pdfcheckre) {
                $reexistkarray[$pinids->getId()] = $pdfcheckre->getRechnungsnummer();
                } else {
                    $reexistkarray[$pinids->getId()] = false;
                    }
                if($pdfcheckanb) {
                $anbexistkarray[$pinids->getId()] = $pdfcheckanb->getRechnungsnummer();
                } else {
                    $anbexistkarray[$pinids->getId()] = false;
                    }
                }
        return $this->render('admin/pinverwaltung.html.twig', array(
            'pindata' => $pindata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($pindatagesamt)/$anzprosite,
        ));
    }
    
    }

    function deleteFilesFromDirectory($ordnername){
        $this->deleteFilesFromDirectory("./upload/uploads/userpics/");
        //überprüfen ob das Verzeichnis überhaupt existiert
        if (is_dir($ordnername)) {
        //Ordner öffnen zur weiteren Bearbeitung
        if ($dh = opendir($ordnername)) {
        //Schleife, bis alle Files im Verzeichnis ausgelesen wurden
        while (($file = readdir($dh)) !== false) {
        //Oft werden auch die Standardordner . und .. ausgelesen, diese sollen ignoriert werden
        if ($file!="." AND $file !="..") {
        //Files vom Server entfernen
        unlink("".$ordnername."".$file."");
        }
        }
        //geöffnetes Verzeichnis wieder schließen
        closedir($dh);
        }
        }
        var_dump(is_dir($ordnername));
        }
         
        //Funktionsaufruf - Directory immer mit endendem / angeben
        
}
