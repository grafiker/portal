<?php

namespace App\Controller;
use App\Document\Article;
use App\Document\Ratingarticle;
use App\Document\Ratingarticleblock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class ArticleratingController extends Controller
{
    /**
     * @Route("/articlerating", name="articlerating")
     */
    public function index(Request $request)
    {
        $articletask = new Article();
        $task = new Ratingarticle();
        $taskblock = new Ratingarticleblock();
        $newarray=$request->request->all();
        var_dump($request->request->all());
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $ratingcheck = $emdel->getRepository('App:Rating')->findOneBy(["articleid"=>$newarray["articleid"]]);
        $emdelblock = $this->get('doctrine_mongodb')->getManager();
        $ratingblockcheck = $emdelblock->getRepository('App:Ratingblock')->findOneBy(array("articleid"=>$newarray["articleid"], "username"=>$newarray["username"]));
        
        if(isset($newarray["ratingdata"])) {
            if(!$ratingcheck) {
                $task->setArticleid($newarray["articleid"]);
                if($newarray["ratingdata"] == "0.5") {
                    $task->setR05($task->getR05()+1);
                } else if($newarray["ratingdata"] == "1") {
                    $task->setR10($task->getR10()+1);
                } else if($newarray["ratingdata"] == "1.5") {
                    $task->setR15($task->getR15()+1);
                } else if($newarray["ratingdata"] == "2") {
                    $task->setR20($task->getR20()+1);
                } else if($newarray["ratingdata"] == "2.5") {
                    $task->setR25($task->getR25()+1);
                } else if($newarray["ratingdata"] == "3") {
                    $task->setR20($task->getR30()+1);
                } else if($newarray["ratingdata"] == "3.5") {
                    $task->setR35($task->getR35()+1);
                } else if($newarray["ratingdata"] == "4") {
                    $task->setR40($task->getR40()+1);
                } else if($newarray["ratingdata"] == "4.5") {
                    $task->setR45($task->getR45()+1);
                } else if($newarray["ratingdata"] == "5") {
                    $task->setR50($task->getR50()+1);
                }
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($task);
                $dm->flush();
                $ratingin=$newarray["ratingdata"];
                $emdelarticle = $this->get('doctrine_mongodb')->getManager();
                $ratingcheckarticles = $emdelarticle->getRepository('App:Article')->findOneBy(["id"=>$newarray["articleid"]]);
                $ratingcheckarticles->setRating($ratingin);

            $taskblock->setArticleid($newarray["articleid"]);
            $taskblock->setUsername($newarray["username"]);
            $dmss = $this->get('doctrine_mongodb')->getManager();
            $dmss->persist($taskblock);
            $dmss->flush();
            } else {
                if(!$ratingblockcheck) {
                    if($newarray["ratingdata"] == "0.5") {
                        $ratingcheck->setR05($ratingcheck->getR05()+1);
                        } else if($newarray["ratingdata"] == "1") {
                            $ratingcheck->setR10($ratingcheck->getR10()+1);
                        } else if($newarray["ratingdata"] == "1.5") {
                            $ratingcheck->setR15($ratingcheck->getR15()+1);
                        } else if($newarray["ratingdata"] == "2") {
                            $ratingcheck->setR20($ratingcheck->getR20()+1);
                        } else if($newarray["ratingdata"] == "2.5") {
                            $ratingcheck->setR25($ratingcheck->getR25()+1);
                        } else if($newarray["ratingdata"] == "3") {
                            $ratingcheck->setR20($ratingcheck->getR30()+1);
                        } else if($newarray["ratingdata"] == "3.5") {
                            $ratingcheck->setR35($ratingcheck->getR35()+1);
                        } else if($newarray["ratingdata"] == "4") {
                            $ratingcheck->setR40($ratingcheck->getR40()+1);
                        } else if($newarray["ratingdata"] == "4.5") {
                            $ratingcheck->setR45($ratingcheck->getR45()+1);
                        } else if($newarray["ratingdata"] == "5") {
                            $ratingcheck->setR50($ratingcheck->getR50()+1);
                        }
                }
                    $r05=$ratingcheck->getR05();
                    $r10=$ratingcheck->getR10();
                    $r15=$ratingcheck->getR15();
                    $r20=$ratingcheck->getR20();
                    $r25=$ratingcheck->getR25();
                    $r30=$ratingcheck->getR30();
                    $r35=$ratingcheck->getR35();
                    $r40=$ratingcheck->getR40();
                    $r45=$ratingcheck->getR45();
                    $r50=$ratingcheck->getR50();
                    $gesrating=$r05+$r10+$r15+$r20+$r25+$r30+$r35+$r40+$r45+$r50;
                    $ges05=$r05*0.5;
                    $ges10=$r10*1;
                    $ges15=$r15*1.5;
                    $ges20=$r20*2;
                    $ges25=$r25*0.5;
                    $ges30=$r30*3;
                    $ges35=$r35*3.5;
                    $ges40=$r40*4;
                    $ges45=$r45*4.5;
                    $ges50=$r50*5;
                    $ratingresult=$ges05+$ges10+$ges15+$ges20+$ges25+$ges30+$ges35+$ges40+$ges45+$ges50;
                    $ratingresultend=$ratingresult/$gesrating;
                    $ratingin=round($ratingresultend, 2);
                    $emdelarticle = $this->get('doctrine_mongodb')->getManager();
                    $ratingcheckarticles = $emdelarticle->getRepository('App:Article')->findOneBy(["id"=>$newarray["articleid"]]);
                    $ratingcheckarticles->setRating($ratingin);
            }
        $newstring = $newarray["username"].';'.$ratingin.';'.$newarray["articleid"].';'.$newarray["ratingdata"];
        if(!$ratingblockcheck) {
        $taskblock->setArticleid($newarray["articleid"]);
        $taskblock->setUsername($newarray["username"]);
        $dmss = $this->get('doctrine_mongodb')->getManager();
        $dmss->persist($taskblock);
        $dmss->flush();
        }
        } else {
            if(!$ratingcheck) {
                $newstring = $newarray["articleid"].';0.00';
            } else {
                $r05=$ratingcheck->getR05();
                $r10=$ratingcheck->getR10();
                $r15=$ratingcheck->getR15();
                $r20=$ratingcheck->getR20();
                $r25=$ratingcheck->getR25();
                $r30=$ratingcheck->getR30();
                $r35=$ratingcheck->getR35();
                $r40=$ratingcheck->getR40();
                $r45=$ratingcheck->getR45();
                $r50=$ratingcheck->getR50();
                $gesrating=$r05+$r10+$r15+$r20+$r25+$r30+$r35+$r40+$r45+$r50;
                $ges05=$r05*0.5;
                $ges10=$r10*1;
                $ges15=$r15*1.5;
                $ges20=$r20*2;
                $ges25=$r25*0.5;
                $ges30=$r30*3;
                $ges35=$r35*3.5;
                $ges40=$r40*4;
                $ges45=$r45*4.5;
                $ges50=$r50*5;
                $ratingresult=$ges05+$ges10+$ges15+$ges20+$ges25+$ges30+$ges35+$ges40+$ges45+$ges50;
                $ratingresultend=$ratingresult/$gesrating;
                $ratingin=round($ratingresultend, 2);

            $newstring = $newarray["articleid"].';'.$ratingin;
            }
        }
        $emdel->flush();
        return $this->render('rating/index.html.twig', [
            'controller_name' => 'RatingController',
            'jsonoutput' => $newstring,
        ]);
    }
}
