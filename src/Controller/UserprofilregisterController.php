<?php

namespace App\Controller;
use App\Document\User;
use App\AppBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
class UserprofilregisterController extends Controller
{
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }
/**
 * @Route("/register", name="register")
 */
public function index(RequestStack $requestStack, Request $request) {
    $user = new User();
    $builder = $this->createForm(RegistrationType::class, $user);
        $builder->handleRequest($request);
    return $this->render('@FOSUser/Registration/register.html.twig', array(
        'form' => $builder->createView(),
    ));
}
/**
 * @Route("/userregister", name="userprofilregister")
 */
   public function registeruserAction(Request $request, \Swift_Mailer $mailer) {
$secretKey = "6LdYTHUUAAAAAFsO9xDKepMxlK8iMbD8zV8aceeB";
$ip = $_SERVER['REMOTE_ADDR'];
$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$ip);
$responseKeys = json_decode($response,true);
// Spamversuch
if(intval($responseKeys["success"]) !== 1) {
 $nospam = false;
} else {
 $nospam = true;
}

    $user = new User();
    $builder = $this->createForm(RegistrationType::class, $user);
        $builder->handleRequest($request);
        $task = $builder->getData();
        //var_dump($task->getPlainPassword());confirmationToken
        $dm = $this->get('doctrine_mongodb')->getManager();
        $username = $dm->createQueryBuilder('App:User')->field('username')->equals($task->getUsername())->count()->getQuery()->execute();
        $useremail = $dm->createQueryBuilder('App:User')->field('email')->equals($task->getEmail())->count()->getQuery()->execute();
if($task->getPlainPassword() and !$username and !$useremail and $nospam) {
    $confirmationToken = md5($task->getUsername().$task->getEmail());
    $succesfully = $this->register($task->getEmail(),$task->getUsername(),$task->getPlainPassword(),$task->getAnrede(),$task->getBerufsgruppe1(),$task->getBerufsgruppe2(),$task->getBerufsgruppe3(),$task->getBerufsgruppe4(),$task->getBerufsgruppe5(),$task->getBerufsgruppe6(),$task->getBerufsgruppe7(),$task->getBerufsgruppe8(),$task->getBerufsgruppe9(),$task->getGebMon(),$task->getGebDay(),$task->getGebYear(),$task->getFirstName(),$task->getLastName(),$confirmationToken);
} else {
    $succesfully = false;
}
//$succesfully=false;
//$email,$username,$password,$anrede,$berufsgruppe1,$berufsgruppe2,$berufsgruppe3,$berufsgruppe4,$berufsgruppe5,$berufsgruppe6,$berufsgruppe7,$berufsgruppe8,$berufsgruppe9,$gebMon,$gebDay,$gebYear,$firstName,$lastName
if($succesfully){
    $subjetsys = "Willkommen " . $task->getUsername();
    $bodytxt = "Hallo " . $task->getUsername() . "
    <br /><br />
    Besuchen Sie bitte folgende Seite, um Ihr Benutzerkonto zu bestätigen: https://grafiker.de/register/confirm/" . $confirmationToken . "
    <br /><br />
    Mit besten Grüßen,<br />
    das grafiker.de Team.";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom('register@grafiker.de')
    ->setTo($task->getEmail())
    ->setBody($bodytxt);

    $mailer->send($message);
    $maillog = new MaillogController();
    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "register@grafiker.de", $task->getEmail(), $subjetsys, $bodytxt);
        return $this->render('@FOSUser/Registration/check_email.html.twig', array(
            'user' => $user,
        ));
}else{
    return $this->render('@FOSUser/Registration/register.html.twig', array(
        'form' => $builder->createView(),
        'username' => $username,
        'usermail' => $useremail,
        'spam' => $nospam,
    ));
 }

return $this->render('userprofilregister/index.html.twig', [
    'controller_name' => 'UserprofilregisterController',
]);
}
 
   /**
    * This method registers an user in the database manually.
    *
    * @return boolean User registered / not registered
    **/
private function register($email,$username,$password,$anrede,$berufsgruppe1,$berufsgruppe2,$berufsgruppe3,$berufsgruppe4,$berufsgruppe5,$berufsgruppe6,$berufsgruppe7,$berufsgruppe8,$berufsgruppe9,$gebMon,$gebDay,$gebYear,$firstName,$lastName,$confirmationToken){    
$userManager = $this->get('fos_user.user_manager');

      // Or you can use the doctrine entity manager if you want instead the fosuser manager
      // to find 
//$em = $this->getDoctrine()->getManager();
//$usersRepository = $em->getRepository("mybundleuserBundle:User");
// or use directly the namespace and the name of the class 
// $usersRepository = $em->getRepository("mybundle\userBundle\Entity\User");
//$email_exist = $usersRepository->findOneBy(array('email' => $email));

$email_exist = $userManager->findUserByEmail($email);

      // Check if the user exists to prevent Integrity constraint violation error in the insertion
if($email_exist){
return false;
}

$user = $userManager->createUser();
$user->setUsername($username);
$user->setEmail($email);
$user->setEmailCanonical($email);
$user->setEnabled(false); // enable the user or enable it later with a confirmation token in the email
// this method will encrypt the password with the default settings :)
$user->setPlainPassword($password);
$user->setConfirmationToken($confirmationToken);
$user->setNotifications(2);
$user->setFirstLogin(true);
$user->setAnrede(true);
$user->setBerufsgruppe1($berufsgruppe1);
$user->setBerufsgruppe2($berufsgruppe2);
$user->setBerufsgruppe3($berufsgruppe3);
$user->setBerufsgruppe4($berufsgruppe4);
$user->setBerufsgruppe5($berufsgruppe5);
$user->setBerufsgruppe6($berufsgruppe6);
$user->setBerufsgruppe7($berufsgruppe7);
$user->setBerufsgruppe8($berufsgruppe8);
$user->setBerufsgruppe9($berufsgruppe9);
$user->setGebMon($gebMon);
$user->setGebDay($gebMon);
$user->setGebYear($gebYear);
$user->setFirstName($firstName);
$user->setLastName($lastName);
$user->setAgb(1);
$user->setDatenschutz(1);
$userManager->updateUser($user);

return true;
}
}
