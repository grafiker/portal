<?php
namespace App\Controller;
use App\Document\Contact;
use App\AppBundle\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ContactController extends Controller 
{
    /**
     * @Route("/contact", name="formular")
     */
    public function new(Request $request)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}
        $task = new Contact();
        $builder = $this->createForm(ContactFormType::class, $task);
        $builder->add('from', HiddenType::class, array(
            'data' => $username,
        ));
        /*$builder = $this->createFormBuilder($task)
            ->add('from', HiddenType::class, array(
                'data' => $username,
            ))
            ->add('anrede', ChoiceType::class, array(
                'choices' => array(
                    'Anrede' => array(
                        'Herr' => 'herr',
                        'Frau' => 'frau',
                    )
                ),
            ))

            ->add('frage',ChoiceType::class,
            array('label' => 'Abteilung:','choices' => array(
                    'Support' => 1,
                    'Marketing' => 2,
                    'Rechnung' => 3),
            'multiple'=>false,'expanded'=>true))

			->add('subject', TextType::class, ['label' => 'Betreff'])
            ->add('content', TextareaType::class, ['label' => 'Nachricht'])
            ->add('send', SubmitType::class, ['label' => 'Speichern'])
            ->getForm();*/

            $builder->handleRequest($request);

            if ($builder->isSubmitted() && $builder->isValid()) {
                $task = $builder->getData();
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($task);
                $dm->flush();

                $message = (new \Swift_Message('Hello Email'))
                ->setSubject('Hello Email')
                ->setFrom('send@example.com')
                ->setTo('adgigant@gmail.com');
                $this->get('mailer')->send($message);

                return $this->render('contact/ende.html.twig', array(
                    'name' => $task->getContent() . ' ' . $task->getSubject(),
                ));
            }


        return $this->render('contact/index.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'Nehmen Sie Kontakt mit uns auf',
        ));
    }
}
