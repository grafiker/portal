<?php

namespace App\Controller;
use App\Document\Newsletter;
use App\AppBundle\Form\NewsletterFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminNewsletterController extends Controller
{
    /**
     * @Route("/admin/newsletter", name="admin_newsletter")
     */
    public function index(Request $request)
    {
        $newsletterbuilder = $this->createForm(NewsletterFormType::class, array());
        $newsletterbuilder->handleRequest($request);

        if ($newsletterbuilder->isSubmitted() && $newsletterbuilder->isValid()) {
            $task = $newsletterbuilder->getData();
            //var_dump($task["letterDay"]);
            $newletter = new Newsletter();

            $newletter->setLettertyp($task["lettertyp"]);
            $newletter->setLetterDay($task["letterDay"]);
            $newletter->setLetterMon($task["letterMon"]);
            $newletter->setLetterYear($task["letterYear"]);
            $newletter->setMailertext($task["mailertext"]);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($newletter);
            $dm->flush();
            //var_dump($newletter);
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $letterdata = $dm->createQueryBuilder('App:Newsletter')->sort(array('letterDay' => 'DESC', 'letterMon' => 'DESC', 'letterYear' => 'DESC'))->limit(100)->getQuery()->toArray();
        return $this->render('admin/newsletter.html.twig', [
            'form' => $newsletterbuilder->createView(),
            'controller_name' => 'AdminNewsletterController',
            'letterdata' => $letterdata,
            'delete' => false,
        ]);
    }
    /**
     * @Route("/admin/newsletter/{delId}", name="admin_newsletter_delete")
     */
    public function deleteAction(Request $request, $delId)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $delete = $em->getRepository('App:Newsletter')->findOneById($delId);
        //var_dump($delete);
        $em->remove($delete); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
        $em->flush();
        return $this->render('admin/newsletter.html.twig', [
            'controller_name' => 'AdminNewsletterController',
            'delete' => true,
        ]);
    }
}
