<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MobilpinsController extends AbstractController
{
    /**
     * @Route("/mobilpins", name="mobilpins")
     */
    public function index()
    {
        return $this->render('mobilpins/index.html.twig', [
            'controller_name' => 'MobilpinsController',
        ]);
    }
}
