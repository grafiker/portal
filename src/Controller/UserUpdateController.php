<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserUpdateController extends AbstractController
{
    /**
     * @Route("/profile/update", name="user_update")
     */
    public function index()
    {
        return $this->render('userprofil/update.html.twig', [
            'controller_name' => 'UserUpdateController',
        ]);
    }
}
