<?php

namespace App\Controller;

use App\Document\Webhook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class WebhookController extends Controller
{

    /**
     * @Route("/webhook.php", name="webhook")
     */
    public function index()
    {
        $json = file_get_contents('php://input');
        $action = json_decode($json, true);
        
        $task = new Webhook();
        //$fp = fopen('test.txt', 'w');
        foreach($action as $key => $action) {
            //fwrite($fp, $key . ": " . $action . "\n")."\n";

            $nextarray[$key] = $action;
        }

        //fwrite($fp, implode ("\n", $action))."\n";
         /*fwrite($fp, "id: " . $action["id"])."\n";
         fwrite($fp, "Camp_id: " . $action["Camp_id"])."\n";
         fwrite($fp, "email: " . $action["email"])."\n";
         fwrite($fp, "Kampagnenname: " . $action["Kampagnenname"])."\n";
         fwrite($fp, "date_sent: " . $action["date_sent"])."\n";
         fwrite($fp, "Date_event: " . $action["Date_event"])."\n";
         fwrite($fp, "Event: " . $action["Event"])."\n";
         fwrite($fp, "Tag: " . $action["Tag"])."\n";
         fwrite($fp, "ts_sent: " . $action["Tag"])."\n";
         fwrite($fp, "Ts_event: " . $action["Ts_event"])."\n";
         fwrite($fp, "URL: " . $action["URL"])."\n";
         fwrite($fp, "ts: " . $action["ts"])."\n";*/
         //fclose($fp);
         $task->setWebhook($nextarray);
         $dm = $this->get('doctrine_mongodb')->getManager();
             $dm->persist($task);
             $dm->flush();
        

        
        return $this->render('webhook/index.html.twig', array(
            'controller_name' => 'WebhookController',
        ));
    }
}
