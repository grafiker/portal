<?php

namespace App\Controller;
use App\Document\User;

use App\AppBundle\Form\AdminUserexportFormType;
use App\AppBundle\Form\AdminEditFormType;
use App\AppBundle\Form\AdminGroupFormType;
use App\AppBundle\Form\AdminUsersearchFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminUserverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/admin/userverwaltung/{page}", name="admin_userverwaltung")
     */
    public function createAction(Request $request, $page)
    {
        $searching = new User();
        $searchingbuilder = $this->createForm(AdminUsersearchFormType::class, $searching);
        $searchingbuilder->handleRequest($request);

        if ($searchingbuilder->isSubmitted() && $searchingbuilder->isValid()) {
            $task = $searchingbuilder->getData();

            $dm = $this->get('doctrine_mongodb')->getManager();
            $searchQuery = array(
                '$or' => array(
                    array(
                        'username' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'firstName' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'lastName' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'firma' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'email' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    )
                );
            $userdata = $dm->getRepository('App:User')->findBy($searchQuery);

        if (!$userdata) {
            return $this->render('admin/searchuser.html.twig', array(
                'form' => $searchingbuilder->createView(),
                'userdata' => false,
                'searching' => $task->getSearching(),
            ));
            //throw $this->createNotFoundException('No User found');
        }

        return $this->render('admin/searchuser.html.twig', array(
            'form' => $searchingbuilder->createView(),
            'userdata' => $userdata,
            'searching' => $task->getSearching(),
        ));

        } else {
        $anzprosite = 100;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdatagesamt = $dm->createQueryBuilder('App:User')->count()->getQuery()->execute();
            $userdata = $dm->getRepository('App:User')->findBy(array(),array('id' => 'DESC'), $anzprosite, $page);
        if (!$userdata) {
            throw $this->createNotFoundException('No User found');
        }

        return $this->render('admin/userverwaltung.html.twig', array(
            'form' => $searchingbuilder->createView(),
            'userdata' => $userdata,
            'count' => round($userdatagesamt/$anzprosite),
            'page' => $page,
        ));
        }
        $dm->flush();
    }

    /**
     * Create
     * @Route("/admin/userverwaltungdisable/{page}", name="admin_userverwaltungdisabled")
     */
    public function disableAction(Request $request, $page)
    {
        $searching = new User();
        $searchingbuilder = $this->createForm(AdminUsersearchFormType::class, $searching);
        $searchingbuilder->handleRequest($request);

        if ($searchingbuilder->isSubmitted() && $searchingbuilder->isValid()) {
            $task = $searchingbuilder->getData();

            $dm = $this->get('doctrine_mongodb')->getManager();
            $searchQuery = array(
                '$or' => array(
                    array(
                        'username' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'firstName' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'lastName' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'firma' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    array(
                        'email' => new \MongoRegex('/'.$task->getSearching().'/si'),
                        ),
                    )
                );
            $userdata = $dm->getRepository('App:User')->findBy($searchQuery);

        if (!$userdata) {
            return $this->render('admin/searchuser.html.twig', array(
                'form' => $searchingbuilder->createView(),
                'userdata' => false,
                'searching' => $task->getSearching(),
            ));
            //throw $this->createNotFoundException('No User found');
        }

        return $this->render('admin/searchuser.html.twig', array(
            'form' => $searchingbuilder->createView(),
            'userdata' => $userdata,
            'searching' => $task->getSearching(),
        ));

        } else {
        $anzprosite = 100;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdatagesamt = $dm->createQueryBuilder('App:User')->field('enabled')->equals(false)->count()->getQuery()->execute();
            $userdata = $dm->getRepository('App:User')->findBy(array('enabled' => false),array('id' => 'DESC'), $anzprosite, $page);
        if (!$userdata) {
            throw $this->createNotFoundException('No User found');
        }

        return $this->render('admin/userverwaltung.html.twig', array(
            'form' => $searchingbuilder->createView(),
            'userdata' => $userdata,
            'count' => round($userdatagesamt/$anzprosite),
            'page' => $page,
        ));
        }
        $dm->flush();
    }

    /**
     * edit
     * @Route("/admin/userverwaltung/{userID}/{action}", name="admin_userverwaltung_edit")
     */
    
    public function editAction(Request $request, $userID, $action, \Swift_Mailer $mailer)
    {
            $em = $this->get('doctrine_mongodb')->getManager();
            $user = $em->getRepository('App:User')->findOneBy(["id"=>$userID]);

            if($action == "del" or $action == "notok") {
                $userdelpic = false;
            $userdelfirmapic = false;
            $bewerbungsdocs = array();
            $bewerbungsmail = false;
            $jobdocs = array();
            $pinsdelpics = array();
            $pindeltumb = false;
            $databasecounter = array();
            $bewerbungsdocscount = array();
            $jobdocscount = array();
            $pinpicsdelcount = array();

//Was passiert beim löschen eines Mitglieds???
$m = $this->container->get('doctrine_mongodb.odm.default_connection'); //Mongeverbindung wird hergestellt
$username = $user->getUsername();
$usermail = $user->getEmail();
$db = $m->selectDatabase('grafiker'); //Datenbank wird ausgewählt
$usermailout = urlencode($usermail);
//User wird bei Sendblue gelöscht
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/" . $usermailout,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "DELETE",
              CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "api-key: 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",
                "content-type: application/json"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            /*if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }*/
$threads = $em->getRepository('App:Thread')->findBy(array('createdBy' => $user));
    //var_dump($threads);
if($threads) {
    $databasecounter['Thread'] = $em->createQueryBuilder('App:Thread')->field('createdBy')->equals($user)->count()->getQuery()->execute();
    foreach($threads as $threads) {
    $em->remove($threads); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
    $em->flush(); 
    }
}
    $mails = $em->getRepository('App:Message')->findBy(array('sender' => $user));
    //var_dump($mails);
if($mails) {
    $databasecounter['Message'] = $em->createQueryBuilder('App:Message')->field('createdBy')->equals($user)->count()->getQuery()->execute();
    foreach($mails as $mails) {
    $em->remove($mails);  // Lösche alle gestarteteten mails vom User aus MongoDB löschen
    $em->flush(); 
    }
}
foreach($db->listCollections() as $listall) // eine Schleife mit den Tabellen wird gestartet
{
    $colnames = $listall->getCollection()->getCollectionName(); // Namen aller Tabellen wird in der Schleife ermittelt

//Als erstes werden einige Statistikdaten für die Mail und Adminausgabe geschrieben.
if($colnames == "Network") {
        $searchQuery = array(
            '$or' => array(
                array(
                    'username' => $username,
                    ),
                array(
                    'friend' => $username,
                    ),
                )
            );
        $finallby = $delmember->getRepository('App:'.$colnames)->findBy($searchQuery);
        $databasecounter[$colnames] = count($finallby);
    } else {
    $databasecounter[$colnames] = $em->createQueryBuilder('App:' . $colnames)->field('username')->equals($username)->count()->getQuery()->execute();
}

$delmember = $this->get('doctrine_mongodb')->getManager(); //Mongeverbindung wird hergestellt für löschen der Mongo-Einträge erstellt
if($colnames != 'Network') {
    $finallby = $delmember->getRepository('App:'.$colnames)->findBy(array('username' => $username)); // es wird in sämtlichen Tabellen nach dem zu löschenden Usernamen gesucht 
} else { // Wenn der Tabellenname Network ist, gucke auch nach dem Friend Namen um auch diese Netzwerkverbindung zu löschen!
    $searchQuery = array(
        '$or' => array(
            array(
                'username' => $username,
                ),
            array(
                'friend' => $username,
                ),
            )
        );
    $finallby = $delmember->getRepository('App:'.$colnames)->findBy($searchQuery);
}
    foreach($finallby as $finallby) //es wird eine weitere Schleife gestartet um alle gefundenen Einträge einzeln zu behandeln
{
    if($colnames == "User") { // Ist der Tabellenname User, lade die Bilddaten vom Userbild und vom Firmenbild um es zu löschen
        $userdelpic = $finallby->getUserpic();
        if($userdelpic != "del" and $userdelpic != "") {
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/userpics/".$userdelpic);
        $userpiccounter = 1;
        } else {
        $userpiccounter = 1;    
        }
        $userdelfirmapic = $finallby->getFirmapic();
        if($userdelfirmapic != "del" and $userdelfirmapic != "") {
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/firma/".$userdelfirmapic);
            $userfirmapiccounter = 1;
        } else {
        $userfirmapiccounter = 1;    
        }

//USER MUSS HIER NOCH AUS DEM MAILER GELÖSCHT WERDEN        


    }
    if($colnames == "Bewerbung") { // Ist der Tabellenname Bewerbung, lade die PDF Dateien und die MailID meiner eigenen Bewerbungen um sie zu löschen
        $bewerbungsdocs = $finallby->getDocpdf();
        $bewerbungsmail = $finallby->getMailID();
        foreach($bewerbungsdocs as $key => $value)
        {
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/bewerbung/".$value);
            $bewerbungsdocscount[$key] = count($bewerbungsdocs);
            $i = $key;
        }
    }
    if($colnames == "Job") { // Ist der Tabellenname Job, lade die PDF Dateien um sie zu löschen
        $jobdocs = $finallby->getDocpdf();
        foreach($jobdocs as $key => $value)
        {
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/".$value);
            $jobdocscount[$key] = count($jobdocs);
            $i = $key;
        }
        
    }
    if($colnames == "Pins") { // Ist der Tabellenname Pins, lade die Pinpics und den Thumbnail Namen um es zu löschen
        $pinsdelpics = $finallby->getPicname();
        
        
        $i=0;
        foreach($pinsdelpics as $key => $value)
        {
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$value);
            $pinpicsdelcount[$key] = count($pinsdelpics);
            $i = $key;
        }
        $pindeltumb = $finallby->getThumbnail();
        if($pindeltumb != "") {
            unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pindeltumb);
        }
    }
$delmember->remove($finallby); // Lösche alle gefundenen MongoDB einträge um den User komplett zu entfernen    
$delmember->flush(); 
}
}
ksort($databasecounter);
//Als nächstes werden Die Mails an den User und den Admin gesendet!
if($action == "notok") {
    $subjetsys = "Grafiker - Profileintrag - Löschhinweis";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom('account@grafiker.de')
    ->setTo($usermail)
    ->setBody('Hallo ' . $username . ',<br />danke dass Du Dich auf unserer Plattform registriert hast.<br /><br />Unser Portal ist aber nur für Grafiker, Fotografen, Designer, Druckereien gedacht. Aus diesem Grund müssen wir Deinen Account leider wieder löschen.<br /><br /><br />Viele Grüsse,<br />Das Grafiker Team<br /><br /><br /><br />Hello ' . $username . ',<br />Thank you for registering on our platform. Unfortunately, our portal is only for graphic artists, photographers, designers, printers. For this reason, we have to delete your account.<br /><br /><br />Many greetings<br />Your Grafiker Team');
    $mailer->send($message);
    $maillog = new MaillogController();
    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "account@grafiker.de", $usermail, $subjetsys,"Hallo " . $username . ",<br />danke dass Du Dich auf unserer Plattform registriert hast.<br /><br />Unser Portal ist aber nur für Grafiker, Fotografen, Designer, Druckereien gedacht. Aus diesem Grund müssen wir Deinen Account leider wieder löschen.<br /><br /><br />Viele Grüsse,<br />Das Grafiker Team<br /><br /><br /><br />Hello " . $username . ",<br />Thank you for registering on our platform. Unfortunately, our portal is only for graphic artists, photographers, designers, printers. For this reason, we have to delete your account.<br /><br /><br />Many greetings<br />Your Grafiker Team");

    $subjetsys = "Account von User: " . $username . " gelöscht!";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom('account@grafiker.de')
    ->setTo('mail@grafiker.de')
    ->setBody("Hallo Admin, der Benutzer mit dem Usernamen: " . $username . " wurde erfolgreich aus dem System gelöscht!<br /><br />");
    $mailer->send($message);
    $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "account@grafiker.de", "mail@grafiker.de", $subjetsys, "<strong><h1>Nachweis über Userlöschung:</h1></strong><br /><br />" . $this->renderView(
                'admin/deluserendmail.html.twig',
                array(
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount)
                )));

} else {
$subjetsys = "Ihr Account wurde von grafiker.de gelöscht!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('account@grafiker.de')
            ->setTo($usermail)
            ->setBody('Hallo ' . $username . ',<br />Dein Account wurde soeben wie gewünscht von unseren Server restlos gelöscht!<br /><br />Solltes du Bilder oder Dokumente auf grafiker.de geladen haben, wurden natürlich bei der Löschung auch diese Dateien mit von unseren Server entfernt!<br /><br />Wir wünschen Dir weiterhin viel Erfolg für die Zukunft!<br /><br />Mit freundlichen Grüßen,<br />Dein grafiker.de Team<br /><br />Hier noch ein Auszug aus unseren System, was alles von Ihnen aus unseren System gelöscht wurde:<br /><br />' .
            $this->renderView(
                'admin/deluserendmail.html.twig',
                array(
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount)
                )));
            $mailer->send($message);
            $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "account@grafiker.de", $usermail, $subjetsys, 'Hallo ' . $username . ',<br />Dein Account wurde soeben wie gewünscht von unseren Server restlos gelöscht!<br /><br />Solltes du Bilder oder Dokumente auf grafiker.de geladen haben, wurden natürlich bei der Löschung auch diese Dateien mit von unseren Server entfernt!<br /><br />Wir wünschen Dir weiterhin viel Erfolg für die Zukunft!<br /><br />Mit freundlichen Grüßen,<br />Dein grafiker.de Team<br /><br />Hier noch ein Auszug aus unseren System, was alles von Ihnen aus unseren System gelöscht wurde:<br /><br />' .
            $this->renderView(
                'admin/deluserendmail.html.twig',
                array(
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount)
                )));
            $subjetsys = "Account von User: " . $username . " gelöscht!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('account@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody("<strong><h1>Nachweis über Userlöschung:</h1></strong><br /><br />" . $this->renderView(
                'admin/deluserendmail.html.twig',
                array(
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount)
                )));
            $mailer->send($message);
            $maillog = new MaillogController();
                    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "angebot@grafiker.de", "mail@grafiker.de", $subjetsys, "<strong><h1>Nachweis über Userlöschung:</h1></strong><br /><br />" . $this->renderView(
                        'admin/deluserendmail.html.twig',
                        array(
            'username' => $username,
            'userdelpic' => $userdelpic,
            'userdelfirmapic' => $userdelfirmapic,
            'bewerbungsdocs' => $bewerbungsdocs,
            'bewerbungsmail' => $bewerbungsmail,
            'jobdocs' => $jobdocs,
            'pinsdelpics' => $pinsdelpics,
            'pindeltumb' => $pindeltumb,
            'databasecounter' => $databasecounter,
            'pinpicsdelcount' => array_sum($pinpicsdelcount),
            'userpiccounter' => $userpiccounter,
            'userfirmapiccounter' => $userfirmapiccounter,
            'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
            'jobdocscount' => array_sum($jobdocscount)
                        )));
}
$em->remove($user);
return $this->render('admin/deluserend.html.twig', array(
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount),

));
            } else if($action == "edit") {

                $task = new User();
                $builder = $this->createForm(AdminEditFormType::class, $user);

                $builder->handleRequest($request);
                if ($builder->isSubmitted() && $builder->isValid()) {
                    $task = $builder->getData();
                
                    
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $userdata = $dm->getRepository('App:User')->find($userID);

                    if (!$userdata) {
                        throw $this->createNotFoundException('No product found for id '.$id);
                    }

                    $userdata->setFirstName($task->getFirstName());
                    $userdata->setLastName($task->getLastName());
                    $userdata->setStrassenummer($task->getStrassenummer());
                    $userdata->setPLZ($task->getPLZ());
                    $userdata->setWohnort($task->getWohnort());
                    $userdata->setTelefon($task->getTelefon());
                    $userdata->setMobil($task->getMobil());
                    $userdata->setBerufsbezeichnung($task->getBerufsbezeichnung());
                    $userdata->setFirma($task->getFirma());
                    $userdata->setBeschreibung($task->getBeschreibung());
                    $userdata->setUsername($task->getUsername());
                    $userdata->setGebDay($task->getGebDay());
                    $userdata->setgebMon($task->getgebMon());
                    $userdata->setgebYear($task->getgebYear());
                    $userdata->setStundenpreis($task->getStundenpreis());
                    $userdata->setEmail($task->getEmail());
                    $dm->flush();


                    return $this->render('admin/useredit-end.html.twig', array(
                        'userID' => $user->getID(),
                        'firstName' => $task->getFirstName(),
                        'lastName' => $task->getLastName(),
                    ));
                } else {
                    return $this->render('admin/useredit.html.twig', array(
                        'form' => $builder->createView(),
                        'userID' => $user->getID(),
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                    ));
                }
            } else  if($action == "group") {
                $task = new User();
                $builder = $this->createForm(AdminGroupFormType::class, $user);

                    $builder->handleRequest($request);

                    if ($builder->isSubmitted() && $builder->isValid()) {
                        $task = $builder->getData();
                    
                        
                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $userdata = $dm->getRepository('App:User')->find($userID);
    
                        if (!$userdata) {
                            throw $this->createNotFoundException('No product found for id '.$id);
                        }

                        $userdata->setRoles($task->getRoles());
                        $dm->flush();

                        return $this->render('admin/groupedit.html.twig', array(
                            'form' => $builder->createView(),
                            'userID' => $user->getID(),
                            'firstName' => $user->getFirstName(),
                            'lastName' => $user->getLastName(),
                            'roles' => $user->getRoles(),
                        ));


                    } else {


                return $this->render('admin/groupedit.html.twig', array(
                    'form' => $builder->createView(),
                    'userID' => $user->getID(),
                    'firstName' => $user->getFirstName(),
                    'lastName' => $user->getLastName(),
                    'roles' => $user->getRoles(),
                ));
            }
            } else {
                /*nix machen..... */
            }
            $em->flush();
            
    }


    /**
     * Create
     * @Route("/admin/userexport/{page}", name="admin_userexport")
     */
    public function exportAction(Request $request, $page)
    {
        set_time_limit(3000);
        $newarray=$request->request->all();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $csvausgabe = "keine";

if(isset($newarray['form']["schleifevon"]) and isset($newarray['form']["schleifebis"])) {
    if(isset($newarray['form']["montagsmail"]) and isset($newarray['form']["aktiv"])) {
        $csvausgabe = "Montagsmailer";
        $dz=fopen("upload/uploads/pdf/Montagsmailer-".$newarray['form']["schleifevon"]."-".$newarray['form']["schleifebis"].".csv","w"); //w = Inhalt ersetzen und a = ans ende schreiben...
     } else if(!isset($newarray['form']["aktiv"]) and !isset($newarray['form']["montagsmail"])) {
        $csvausgabe = "Inaktive";
        $dz=fopen("upload/uploads/pdf/Inaktive-".$newarray['form']["schleifevon"]."-".$newarray['form']["schleifebis"].".csv","w"); //w = Inhalt ersetzen und a = ans ende schreiben...
    } else if(!isset($newarray['form']["montagsmail"]) and isset($newarray['form']["aktiv"])) {
        $dz=fopen("upload/uploads/pdf/System-".$newarray['form']["schleifevon"]."-".$newarray['form']["schleifebis"].".csv","w"); //w = Inhalt ersetzen und a = ans ende schreiben...
        $csvausgabe = "System";
    }
        if(!$dz)
          {
            echo "Schreibrechte fehlen!";
            exit;
          } else {


    if(isset($newarray['form']["montagsmail"]) and isset($newarray['form']["aktiv"])) {
    $serchdatasinDB = $dm->getRepository('App:User')->findBy(array('newslettermail' => NULL, 'enabled' => true),array('id' => 'ASC'), $newarray['form']["schleifebis"], $newarray['form']["schleifevon"]);
    } else if(!isset($newarray['form']["aktiv"]) and !isset($newarray['form']["montagsmail"])) {
    $serchdatasinDB = $dm->getRepository('App:User')->findBy(array('enabled' => false),array('id' => 'ASC'), $newarray['form']["schleifebis"], $newarray['form']["schleifevon"]); 
    } else if(!isset($newarray['form']["montagsmail"]) and isset($newarray['form']["aktiv"])) {
    $serchdatasinDB = $dm->getRepository('App:User')->findBy(array('enabled' => true),array('id' => 'ASC'), $newarray['form']["schleifebis"], $newarray['form']["schleifevon"]);
    }
    foreach($serchdatasinDB as $serchdatasinDB) {
    $string = "".$serchdatasinDB->getUsername().";".$serchdatasinDB->getFirstName().";".$serchdatasinDB->getLastName().";".$serchdatasinDB->getEmail().";".$serchdatasinDB->getNewsletterMail().";".$serchdatasinDB->getId()."\n";

    fputs($dz,$string);
    }
          }
    //echo"CSV Datei wurde erstellt! -- <a href=\"../wp-admin/edit.php?post_type=product&page=product_importer\">Hier geht es zum Import!</a> Pfad zur CSV: /api/csv/daten.csv";
	fclose($dz);
        }
        $builder = $this->createForm(AdminUserexportFormType::class);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
            }
            $dm->flush();
            if(isset($newarray['form']["schleifevon"])) {
                $newarray['form']["schleifevon"] = $newarray['form']["schleifevon"];
            } else {
                $newarray['form']["schleifevon"] = "NO";
            }
            if(isset($newarray['form']["schleifebis"])) {
                $newarray['form']["schleifebis"] = $newarray['form']["schleifebis"];
            } else {
                $newarray['form']["schleifebis"] = "NO";
            }
        return $this->render('admin/userexport.html.twig', [
            'controller_name' => 'AdminUserexportController',
            'form' => $builder->createView(),
            'von' => $newarray['form']["schleifevon"],
            'bis' => $newarray['form']["schleifebis"],
            'csvausgabe' => $csvausgabe,
        ]);
    }
}
