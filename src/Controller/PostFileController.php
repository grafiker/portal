<?php

namespace App\Controller;
use App\Document\Pins;
use App\Document\User;
use App\Document\Article;
use App\Document\Gutscheine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class PostFileController extends Controller
{
    /**
     * @Route("/post_file/{uploadtype}", name="post_file")
     */
    public function index($uploadtype)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $task = new Pins();
        $userdb = new User();
        $taskarticle = new Article();
        $taskgutscheine = new Gutscheine();
if(isset($_GET["widgetusername"])) {
    $user = $em->getRepository('App:User')->findOneBy(array('username' => $_GET["widgetusername"]));
    $username = $user->getUsername();
    $wohnort = $user->getWohnort();
} else {
if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
    $wohnort = $user->getWohnort();
} else {
    $username = 'Kein Mitglied!';
}
}
if($uploadtype == "pincrop") {
    $upload_dir = 'upload/uploads/pinpics/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
} else if($uploadtype == "articlecrop") {
    $upload_dir = 'upload/uploads/articlepics/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
} else if($uploadtype == "gutscheinecrop") {
    $upload_dir = 'upload/uploads/gutscheinepics/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
} else {
        $upload_dir = 'upload/uploads/'. $uploadtype .'/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
    }
$allowed_ext = array('jpg','jpeg','png','gif');


if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	$this->exit_status('Error! Wrong HTTP method!');
}

if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){
	
	$pic = $_FILES['pic'];
    
    if(!in_array($this->get_extension($pic['name']),$allowed_ext)){
		$this->exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
	}	
    
    if("userpics" == $uploadtype) {
        // Move the uploaded file from the temporary 
        // directory to the uploads folder:
        $name = $upload_dir.'/'.$username . '_userpic';

        if(file_exists($name.".png")) {
          unlink($name.".png");
        }elseif(file_exists($name.".gif")) {
          unlink($name.".gif");
        } elseif(file_exists($name.".jpg")) {
          unlink($name.".jpg");
        }
        $filename = $_FILES['pic']["name"];
        
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = $username . '_userpic' . $file_ext;
        
        $userdb = $em->getRepository('App:User')->findOneBy(["username"=>$username]);
        $userdb->setUserpic($newfilename);
        $em->flush();
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){
            $this->exit_status('File was uploaded successfuly!');
        }    
    } elseif("firma" == $uploadtype) {
        // Move the uploaded file from the temporary 
        // directory to the uploads folder:
        $name = $upload_dir.'/'.$username . '_firma';

        if(file_exists($name.".png")) {
          unlink($name.".png");
        }elseif(file_exists($name.".gif")) {
          unlink($name.".gif");
        } elseif(file_exists($name.".jpg")) {
          unlink($name.".jpg");
        }
        $filename = $_FILES['pic']["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = $username . '_firma' . $file_ext;
        $em = $this->get('doctrine_mongodb')->getManager();
        $userdb = $em->getRepository('App:User')->findOneBy(["username"=>$username]);
        $userdb->setFirmapic($newfilename);
        $em->flush();
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){
            $this->exit_status('File was uploaded successfuly!');
        }    
    } elseif("pinpics" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
            $data = array();

            if($pincheck) {
                if(!$pincheck->getPicname()) {
                    //$em->remove($pincheck);
                    //$em->flush();
                    //$pincheck = false;
                    }  
            }


            if (!$pincheck) {
            $task->setUsername($username);
            $task->setWohnort($wohnort);
            $data[] = $newfilename;
            $task->setPicname($data);
            $task->setAstitle("Pinstart");
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            } else {
                $i=0;
                foreach($pincheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $pincheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $pincheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    } elseif("pincrop" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = 'thumbnail_' . $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
            $data = array();
            if ($pincheck) {
            $pincheck->setThumbnail($newfilename);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($pincheck);
            $dm->flush();
            } else {
                $i=0;
                foreach($pincheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $pincheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $pincheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    } elseif("articlepics" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
            $data = array();

            if($articlecheck) {
                if(!$articlecheck->getPicname()) {
                    //$em->remove($pincheck);
                    //$em->flush();
                    //$pincheck = false;
                    }  
            }


            if (!$articlecheck) {
            $taskarticle->setUsername($username);
            $taskarticle->setWohnort($wohnort);
            $data[] = $newfilename;
            $taskarticle->setPicname($data);
            $taskarticle->setAstitle("Articlestart");
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($taskarticle);
            $dm->flush();
            } else {
                $i=0;
                foreach($articlecheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $articlecheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $articlecheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    } elseif("gutscheinepics" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
            $data = array();

            if($gutscheinecheck) {
                if(!$gutscheinecheck->getPicname()) {
                    //$em->remove($pincheck);
                    //$em->flush();
                    //$pincheck = false;
                    }  
            }


            if (!$gutscheinecheck) {
            $taskgutscheine->setUsername($username);
            $taskgutscheine->setWohnort($wohnort);
            $data[] = $newfilename;
            $taskgutscheine->setPicname($data);
            $taskgutscheine->setAstitle("Gutscheinestart");
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($taskgutscheine);
            $dm->flush();
            } else {
                $i=0;
                foreach($gutscheinecheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $gutscheinecheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $gutscheinecheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    } elseif("articlecrop" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = 'thumbnail_' . $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
            $data = array();
            if ($articlecheck) {
                if($articlecheck->getThumbnail() != "") {
                    if(file_exists("./upload/uploads/articlepics/".$articlecheck->getThumbnail())) {
                        unlink("./upload/uploads/articlepics/".$articlecheck->getThumbnail());
                    }
                }
            $articlecheck->setThumbnail($newfilename);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($articlecheck);
            $dm->flush();
            } else {
                $i=0;
                foreach($articlecheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $articlecheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $articlecheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    } elseif("gutscheinecrop" == $uploadtype) {
        $picname=md5($pic['name'].time());
        $filename = $pic['name'];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $newfilename = 'thumbnail_' . $username . '_' . $picname .  $file_ext;
        if(move_uploaded_file($pic['tmp_name'], $upload_dir.$newfilename)){

            $em = $this->get('doctrine_mongodb')->getManager();
            $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
            $data = array();
            if ($gutscheinecheck) {
                if($gutscheinecheck->getThumbnail() != "") {
                    if(file_exists("./upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail())) {
                        unlink("./upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail());
                    }
                }
            $gutscheinecheck->setThumbnail($newfilename);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($gutscheinecheck);
            $dm->flush();
            } else {
                $i=0;
                foreach($gutscheinecheck->getPicname() as $key => $value)
                {
                    $data[$key] = $value;
                    $gutscheinecheck->setPicname($data);
                    $i = $key;
                }
                $data[$i+1] = $newfilename;
                $gutscheinecheck->setPicname($data);
            }
            
            $em->flush();
            $this->exit_status($newfilename);
        }    
    }
}

$this->exit_status('Something went wrong with your upload!');
    }

    function exit_status($str){
        echo json_encode(array('status'=>$str));
        exit;
    }

    function get_extension($file_name){
        $ext = explode('.', $file_name);
        $ext = array_pop($ext);
        return strtolower($ext);
    }
}
