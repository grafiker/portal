<?php

namespace App\Controller;

class GutscheineoutputController
{
    function gutscheineFilterOutput($dm, $anz, $sites, $sort, $filter, $owner, $autor, $location, $adminfilter) {
if(!is_numeric($sites)) {
    $sites = 'all';
}
        $filternew = explode("-", $filter);
        if(!isset($sites) or $sites == "all") {
            $page=0;
        } else {
            $page=$sites*$anz;
        }
        if(in_array("Search", $filternew)) {
            $searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$filternew[1].'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$filternew[1].'/si'),
                        ),
                    )
                );
            //$gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy($searchQuery,array('startdate' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy($searchQuery,array('startdate' => $sort), $anz, $page);
        $dm->flush();

        } else {
        if($owner == '' and  $autor == '') {
            $userfilter = '';
        } else if ($owner != '' and  $autor == '') {
            $userfilter = $owner;
        } else if ($owner == '' and  $autor != '') {
            $userfilter = $autor;          
        }
        if($filter == 'all')
        {
            if($owner == '' and  $autor == '') {
                //$gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter),array('startdate' => 'ASC'));
                $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'aktiv' => true),array('startdate' => $sort), $anz, $page);
            } else {
                //$gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'username' => $userfilter),array('startdate' => 'ASC'));
                $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'username' => $userfilter, 'aktiv' => true),array('startdate' => $sort), $anz, $page);
            }
        } else {
            if($owner == '' and  $autor == '') {
                //$gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'gutscheinekategorie' => $filter),array('startdate' => 'ASC'));
                $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'gutscheinekategorie' => $filter, 'aktiv' => true),array('startdate' => $sort), $anz, $page);
            } else {
                //$gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'gutscheinekategorie' => $filter, 'username' => $owner),array('startdate' => 'ASC'));
                $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => $adminfilter, 'gutscheinekategorie' => $filter, 'username' => $userfilter, 'aktiv' => true),array('startdate' => $sort), $anz, $page);
            }
        }
        $dm->flush();
    }
        return $gutscheinedata;
    }
}
