<?php

namespace App\Controller;

use App\AppBundle\Form\AdminMailerExportFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
class AdminMailuserexportController extends Controller
{
    /**
     * Create
     * @Route("/admin/mailuserexport/{go}/{anz}/{page}", name="admin_mailuserexport_start")
     */
    public function createAction(Request $request, $go, $anz, $page)
    {
        $stop = false;
        $anz = $anz;
        $altpage = $page;
        $page = $page;
        $page = $page*$anz;
        $developerList = 21;	
        $grafikerGrafiker = 20;		
        $grafikerFotograf = 19;		
        $grafikerProgrammierer = 18;		
        $grafikerTexter = 17;		
        $grafikerIllustrator = 16;		
        $grafikerDruckereiMitarbeiter = 15;		
        $grafikerDruckerei = 14;		
        $grafikerAuftraggeber = 13;		
        $grafikerMontagsmailer = 12;
        $grafikerSystemmailer = 11;
        $nextsite = "";
        $task = array();
        $builder = $this->createForm(AdminMailerExportFormType::class, $task);

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
//var_dump($temp);
        if(isset($temp["form"]["send"])) {
        $userdatagesamt = array();
        $serchdata1 = array();
        $serchdata2 = array();
        $serchdata3 = array();
        $serchdata4 = array();
        $serchdata5 = array();
        $serchdata6 = array();
        $serchdata7 = array();
        $serchdata8 = array();
        $dm = $this->get('doctrine_mongodb')->getManager();
        if(isset($temp["form"]["alle"])) {
            if(isset($temp["form"]["mailer"])) {
                $nextsite = "alle-mailer";
                $sendbluelist = $grafikerMontagsmailer;
                $serchdata1 = $dm->getRepository('App:User')->findBy(array("newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "alle-sysmailer";
                $sendbluelist = $grafikerSystemmailer;
                $serchdata1 = $dm->getRepository('App:User')->findBy(array(),array('id' => 'ASC'), $anz, $page);
            }
        }
        if(isset($temp["form"]["berufsgruppe8"])) {
        $berufsgruppe8 = true;
        if(isset($temp["form"]["mailer"])) {
        $nextsite = "auftraggeber-mailer";
        $sendbluelist = $grafikerAuftraggeber;
        $serchdata1 = $dm->getRepository('App:User')->findBy(array("berufsgruppe8" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        } else if(isset($temp["form"]["sysmailer"])) {
            $nextsite = "auftraggeber-sysmailer";
        $sendbluelist = $grafikerAuftraggeber;
            $serchdata1 = $dm->getRepository('App:User')->findBy(array("berufsgruppe8" => true),array('id' => 'ASC'), $anz, $page);
        }
    } else {
        $berufsgruppe8 = false;
    }
        if(isset($temp["form"]["berufsgruppe6"])) {
        $berufsgruppe6 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "druckerei-mailer";
        $sendbluelist = $grafikerDruckerei;
            $serchdata2 = $dm->getRepository('App:User')->findBy(array("berufsgruppe6" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "druckerei-sysmailer";
        $sendbluelist = $grafikerDruckerei;
                $serchdata2 = $dm->getRepository('App:User')->findBy(array("berufsgruppe6" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe6 = false;
    }
        if(isset($temp["form"]["berufsgruppe9"])) {
        $berufsgruppe9 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "druckereiMitarbeiter-mailer";
        $sendbluelist = $grafikerDruckereiMitarbeiter;
            $serchdata3 = $dm->getRepository('App:User')->findBy(array("berufsgruppe9" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "druckereiMitarbeiter-sysmailer";
        $sendbluelist = $grafikerDruckereiMitarbeiter;
                $serchdata3 = $dm->getRepository('App:User')->findBy(array("berufsgruppe9" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe9 = false;
    }
    if(isset($temp["form"]["berufsgruppe1"])) {
        $berufsgruppe1 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "grafiker-mailer";
        $sendbluelist = $grafikerGrafiker;
            $serchdata4 = $dm->getRepository('App:User')->findBy(array("berufsgruppe1" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "grafiker-sysmailer";
        $sendbluelist = $grafikerGrafiker;
                $serchdata4 = $dm->getRepository('App:User')->findBy(array("berufsgruppe1" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe1 = false;
    }
    if(isset($temp["form"]["berufsgruppe2"])) {
        $berufsgruppe2 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "fotograf-mailer";
        $sendbluelist = $grafikerFotograf;
            $serchdata5 = $dm->getRepository('App:User')->findBy(array("berufsgruppe2" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $serchdata5 = $dm->getRepository('App:User')->findBy(array("berufsgruppe2" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe2 = false;
    }
    if(isset($temp["form"]["berufsgruppe3"])) {
        $berufsgruppe3 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "programmierer-mailer";
        $sendbluelist = $grafikerProgrammierer;
            $serchdata6 = $dm->getRepository('App:User')->findBy(array("berufsgruppe3" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "programmierer-sysmailer";
        $sendbluelist = $grafikerProgrammierer;
                $serchdata6 = $dm->getRepository('App:User')->findBy(array("berufsgruppe3" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe3 = false;
    }
    if(isset($temp["form"]["berufsgruppe4"])) {
        $berufsgruppe4 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "texter-mailer";
        $sendbluelist = $grafikerTexter;
            $serchdata7 = $dm->getRepository('App:User')->findBy(array("berufsgruppe4" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "texter-sysmailer";
        $sendbluelist = $grafikerTexter;
                $serchdata7 = $dm->getRepository('App:User')->findBy(array("berufsgruppe4" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe4 = false;
    }
    if(isset($temp["form"]["berufsgruppe5"])) {
        $berufsgruppe5 = true;
        if(isset($temp["form"]["mailer"])) {
            $nextsite = "illustrator-mailer";
        $sendbluelist = $grafikerIllustrator;
            $serchdata8 = $dm->getRepository('App:User')->findBy(array("berufsgruppe5" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
            } else if(isset($temp["form"]["sysmailer"])) {
                $nextsite = "illustrator-sysmailer";
        $sendbluelist = $grafikerIllustrator;
                $serchdata8 = $dm->getRepository('App:User')->findBy(array("berufsgruppe5" => true),array('id' => 'ASC'), $anz, $page);
            }
    } else {
        $berufsgruppe5 = false;
    }
    if(isset($temp["form"]["mailer"])) {
        $mailer = true;
    } else {
        $mailer = false;
    }
    if(isset($temp["form"]["sysmailer"])) {
        $sysmailer = true;
    } else {
        $sysmailer = false;
    }

    $userdatagesamt = $serchdata1 + $serchdata2 + $serchdata3 + $serchdata4 + $serchdata5 + $serchdata6 + $serchdata7 + $serchdata8;
    //var_dump($userdatagesamt);
    $bs = array_unique ( $userdatagesamt );
    //var_dump($bs);
    //var_dump(count($bs) .'<br />');
    $datainput = array();
    foreach($bs as $key => $bs) {
        $datainput[$key] =array('' . $bs->getEmail() . ';' . iconv('UTF-8', 'Windows-1252', $bs->getFirstName()) . ';' . iconv('UTF-8', 'Windows-1252', $bs->getLastName()));
//var_dump($bs->getEmail());

if(isset($temp["form"]["alle"]) and isset($temp["form"]["sysmailer"])) {
//CREATE USER FROM SENDBLUE
$curl = curl_init();
        
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{
      \"email\":\"".$bs->getEmail()."\",
      \"attributes\": {\"name\":\"".$bs->getLastName()."\",\"vorname\":\"".$bs->getFirstName()."\",\"uuid\":\"".$bs->getId()."\"},
      \"emailBlacklisted\":false,
      \"smsBlacklisted\":false,
      \"updateEnabled\":false,
      \"listIds\":[".$sendbluelist."]
    }",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "api-key: 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",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  //echo "cURL Error #:" . $err;
} else {
  echo $response;
  $obj = json_decode($response);
  if(isset($obj->{'id'})) {
  //$bs->setSendBlueID(intval($obj->{'id'}));
  //$dm->flush();
  }
}
    } else {

        $curl = curl_init();
        $usermailout = urlencode($bs->getEmail());
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/".$usermailout."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => "{\"listIds\":[".$sendbluelist."]}",
          CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }

    }
    } 

    if(count($userdatagesamt) < 100) {
        $stop = true;
    }
//var_dump($datainput);
        }
        
        //var_dump($stop);
        return $this->render('admin/mailuserexport.html.twig', [
            'controller_name' => 'AdminMailuserexportController',
            'form' => $builder->createView(),
            'go' => $go,
            'page' => $altpage,
            'anz' => $anz,
            'nextsite' => $nextsite,
            'stop' => $stop,
        ]);
    }

    /**
     * Next
     * @Route("/admin/mailuserexport/{go}/{anz}/{page}/{next}", name="admin_mailuserexport_next")
     */
    public function nextAction(Request $request, $go, $anz, $page, $next)
    {
        $stop = false;
        $teile = explode("-", $next);
        $anz = $anz;
        $altpage = $page;
        $page = $page;
        $page = $page*$anz;
        $dm = $this->get('doctrine_mongodb')->getManager();
        if($teile[0] == "devlist" and $teile[1] == "mailer") {	
        $developerList = 21;
        }
        if($teile[0] == "grafiker" and $teile[1] == "mailer") {		
        $sendbluelist = 20;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe1" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);	
        }
        if($teile[0] == "fotograf" and $teile[1] == "mailer") {		
        $sendbluelist = 19;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe2" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[0] == "programmierer" and $teile[1] == "mailer") {			
        $sendbluelist = 18;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe3" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[0] == "texter" and $teile[1] == "mailer") {			
        $sendbluelist = 17;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe4" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[0] == "illustrator" and $teile[1] == "mailer") {			
        $sendbluelist = 16;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe5" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[0] == "druckereiMitarbeiter" and $teile[1] == "mailer") {			
        $sendbluelist = 15;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe9" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[0] == "druckerei" and $teile[1] == "mailer") {			
        $sendbluelist = 14;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe6" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }	
        if($teile[0] == "auftraggeber" and $teile[1] == "mailer") {	
        $sendbluelist = 13;
        $serchdata = $dm->getRepository('App:User')->findBy(array("berufsgruppe8" => true, "newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        }
        if($teile[1] == "mailer" and $teile[0] == "alle") {		
        $sendbluelist = 12;
        }
        if($teile[1] == "sysmailer" and $teile[0] == "alle") {
        $sendbluelist = 11;
        }
        $task = array();
        $builder = $this->createForm(AdminMailerExportFormType::class, $task);
      
        if($teile[1] == "mailer" and $teile[0] == "alle") {
        $serchdata = $dm->getRepository('App:User')->findBy(array("newsletterMail" => NULL),array('id' => 'ASC'), $anz, $page);
        } else {
        $serchdata = $dm->getRepository('App:User')->findBy(array(),array('id' => 'ASC'), $anz, $page);
        }
        $datainput = array();
        foreach($serchdata as $key => $bs) {
            $datainput[$key] = array('' . $bs->getEmail() . ';' . iconv('UTF-8', 'Windows-1252', $bs->getFirstName()) . ';' . iconv('UTF-8', 'Windows-1252', $bs->getLastName()));

if($teile[1] != "sysmailer") {
            //CREATE USER FROM SENDBLUE
            $curl = curl_init();
            $usermailout = urlencode($bs->getEmail());
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/".$usermailout."",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_POSTFIELDS => "{\"listIds\":[".$sendbluelist."]}",
              CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "api-key: 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",
                "content-type: application/json"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }
    } 
    if($teile[1] == "sysmailer" and $teile[0] == "alle") { 
     //CREATE USER FROM SENDBLUE
     $curl = curl_init();
        
     curl_setopt_array($curl, array(
       CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 30,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "POST",
       CURLOPT_POSTFIELDS => "{
           \"email\":\"".$bs->getEmail()."\",
           \"attributes\": {\"name\":\"".$bs->getLastName()."\",\"vorname\":\"".$bs->getFirstName()."\",\"uuid\":\"".$bs->getId()."\"},
           \"emailBlacklisted\":false,
           \"smsBlacklisted\":false,
           \"updateEnabled\":false,
           \"listIds\":[".$sendbluelist."]
         }",
       CURLOPT_HTTPHEADER => array(
         "accept: application/json",
         "api-key: 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",
         "content-type: application/json"
       ),
     ));
     
     $response = curl_exec($curl);
     $err = curl_error($curl);
     
     curl_close($curl);
     
     if ($err) {
       echo "cURL Error #:" . $err;
     } else {
       echo $response;
       $obj = json_decode($response);
       if(isset($obj->{'id'})) {
       $bs->setSendBlueID(intval($obj->{'id'}));
       $dm->flush();
       }
     }   
    }
        }
        //var_dump($datainput);
        if(count($serchdata) < 100) {
            $stop = true;
        }
        var_dump(count($serchdata));
        return $this->render('admin/mailuserexport.html.twig', [
            'controller_name' => 'AdminMailuserexportController',
            'form' => $builder->createView(),
            'go' => $go,
            'page' => $altpage,
            'anz' => $anz,
            'nextsite' => $next,
            'stop' => $stop,
        ]);
    }
}
