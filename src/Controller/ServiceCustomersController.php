<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceCustomersController extends AbstractController
{
    /**
     * @Route("/service/customers", name="service_customers")
     */
    public function index()
    {
        return $this->render('service_customers/index.html.twig', [
            'controller_name' => 'ServiceCustomersController',
        ]);
    }
}
