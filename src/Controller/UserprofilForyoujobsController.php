<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserprofilForyoujobsController extends AbstractController
{
    /**
     * @Route("/profile/foryoujobs", name="userprofil_foryoujobs")
     */
    public function index()
    {
        return $this->render('userprofil/foryoujobs.html.twig', [
            'controller_name' => 'UserprofilForyoujobsController',
        ]);
    }
}
