<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserprofilSkillsController extends Controller
{
    /**
     * Create
     * @Route("/profile/skills", name="userprofil_skills")
     */
    public function createAction(Request $request)
    {
        $ok = false;
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $newarray = array();
        $newarray=$request->request->all();
        $nextarray = array();
        foreach($newarray as $key => $value)
        {
            if($value == "false") {
                $nextarray[$key] = false;
            } else {
                $nextarray[$key] = $value;
                $nextinarray[$key] = $key;
            }          
        }
if($nextarray) {
    $user->setSkills($nextarray);
    $user->setSkillsCloud($nextinarray);
    $ok = true;
}

        
        $username = $user->getUsername();
        $druckerei = $user->getBerufsgruppe6();
        $grafiker = $user->getBerufsgruppe1();
        $fotograf = $user->getBerufsgruppe2();
        $entwickler = $user->getBerufsgruppe3();
        $texter = $user->getBerufsgruppe4();
        $illustrator = $user->getBerufsgruppe5();
        $sonstiges = $user->getBerufsgruppe7();
        
            $dmskill = $this->get('doctrine_mongodb')->getManager();
            $skillsDruckerei = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Druckerei']);
            $skillsGrafiker = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Grafiker']);
            $skillsFotograf = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Fotograf']);
            $skillsEntwickler = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Programmierer']);
            $skillsTexter = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Texter']);
            $skillsIllustrator = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Illustrator']);
            $skillsSonstiges = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Sonstiges']);
            $druckereiarray=array();
            $grafikerarray=array();
            $fotografarray=array();
            $entwicklerarray=array();
            $texterarray=array();
            $illusatorarray=array();
            $sonstigesarray=array();
            $druckereiarray = $skillsDruckerei->getSkillname();
            $grafikerarray = $skillsGrafiker->getSkillname();
            $fotografarray = $skillsFotograf->getSkillname();
            $entwicklerarray = $skillsEntwickler->getSkillname();
            $texterarray = $skillsTexter->getSkillname();
            $illusatorarray = $skillsIllustrator->getSkillname();
            $sonstigesarray = $skillsSonstiges->getSkillname();

            $dmskill->flush();
            $userskills = array();
            $userskills=$user->getSkills();
        return $this->render('userprofil/skills.html.twig', [
            'controller_name' => 'UserprofilSkillsController',
            'druckerei' => $druckerei,
            'grafiker' => $grafiker,
            'fotograf' => $fotograf,
            'entwickler' => $entwickler,
            'texter' => $texter,
            'illustrator' => $illustrator,
            'sonstiges' => $sonstiges,
            'druckereiarray' => $druckereiarray,
            'grafikerarray' => $grafikerarray,
            'fotografarray' => $fotografarray,
            'entwicklerarray' => $entwicklerarray,
            'texterarray' => $texterarray,
            'illusatorarray' => $illusatorarray,
            'sonstigesarray' => $sonstigesarray,
            'userskills' => $userskills,
            'ok' => $ok,
        ]);
    }

    
}
