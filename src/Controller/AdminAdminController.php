<?php

namespace App\Controller;

use App\Document\ServiceAdmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
class AdminAdminController extends Controller
{
    /**
     * @Route("/service/admin", name="admin_admin")
     */
    public function index()
    {
        return $this->render('admin_admin/index.html.twig', [
            'controller_name' => 'AdminAdminController',
        ]);
    }

    /**
     * @Route("/service/printshop", name="admin_printshop")
     */
    public function printshopAction()
    {
        return $this->render('admin_printshop/index.html.twig', [
            'controller_name' => 'AdminAdminController',
        ]);
    }

    /**
     * @Route("/service/customer", name="admin_customer")
     */
    public function customerAction()
    {
        return $this->render('admin_customer/index.html.twig', [
            'controller_name' => 'AdminAdminController',
        ]);
    }
}
