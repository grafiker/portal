<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfiController extends AbstractController
{
    /**
     * @Route("/profi", name="profi")
     */
    public function index()
    {
        return $this->render('profi/index.html.twig', [
            'controller_name' => 'DruckereienController',
        ]);
    }
}
