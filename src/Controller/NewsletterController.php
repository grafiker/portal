<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class NewsletterController extends Controller
{
    /**
     * @Route("/montagsmailer", name="newsletter")
     */
    public function index()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $letterdata = $dm->createQueryBuilder('App:Newsletter')->sort(array('letterDay' => 'DESC', 'letterMon' => 'DESC', 'letterYear' => 'DESC'))->limit(100)->getQuery()->toArray();
//var_dump($letterdata);
        return $this->render('newsletter/index.html.twig', [
            'controller_name' => 'NewsletterController',
            'letterdetails' => false,
            'letterdata' => $letterdata,
        ]);
    }
    /**
     * @Route("/montagsmailer/{id}", name="newsletter_out")
     */
    public function createOutput($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $letterdata = $dm->createQueryBuilder('App:Newsletter')->sort(array('letterDay' => 'DESC', 'letterMon' => 'DESC', 'letterYear' => 'DESC'))->limit(1)->field('id')->equals($id)->getQuery()->toArray();

        return $this->render('newsletter/index.html.twig', [
            'controller_name' => 'NewsletterController',
            'letterdetails' => true,
            'letterdata' => $letterdata,
        ]);
    }
}
