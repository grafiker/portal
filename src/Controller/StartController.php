<?php

namespace App\Controller;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class StartController extends Controller
{

    /**
     * @Route("/start", name="start")
     */
    public function index()
    {
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if("Sniky" == $users) {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $dm->getRepository('App:User')->findOneByUsername('Sniky');
        $data = array();
        $data[0] = 'ROLE_ADMIN';
        $user->setRoles($data);
        $dm->persist($user);
        $dm->flush();
        }
        if("hbroeskamp" == $users) {
            $dm = $this->get('doctrine_mongodb')->getManager();
            $user = $dm->getRepository('App:User')->findOneByUsername('hbroeskamp');
            $data = array();
            $data[0] = 'ROLE_ADMIN';
            $user->setRoles($data);
            $dm->persist($user);
            $dm->flush();
            }
            if($users != "anon.") {
                $provider = $this->container->get('fos_message.provider');
                $mailanz = $provider->getNbUnreadMessages();
            } else {
                $mailanz = false;
            }
        return $this->render('start/index.html.twig', array(
            'controller_name' => 'StartController',
            'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'unreads' => $mailanz,
        ));
    }
}
