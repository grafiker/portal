<?php

namespace App\Controller;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserprofilController extends Controller
{
    /**
     * @Route("/userprofil", name="userprofil")
     */
    public function index()
    {
    $id="5bd2c9c1eee316000f01ce72";
    $dm = $this->get('doctrine_mongodb')->getManager();
    $userdata = $dm->getRepository('App:User')->findall();

    if (!$userdata) {
        throw $this->createNotFoundException('No product found for id '.$id);
    }

    //$product->setUsername('Sniky');
    $dm->flush();



        return $this->render('userprofil/index.html.twig', [
            'userdata' => $userdata,
        ]);
    }
}
