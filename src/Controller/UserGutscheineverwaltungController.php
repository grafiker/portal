<?php

namespace App\Controller;
use App\Document\Gutscheine;
use App\Controller\CheckUserdataController;
use App\AppBundle\Form\AdminGutscheineEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserGutscheineverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/profile/gutscheineverwaltung/{page}", name="user_gutscheineverwaltung")
     */
    public function createAction(RequestStack $requestStack, Request $request, $page)
    {
        $gutscheinedata = new GutscheineoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $gutscheinekats = $dm->getRepository('App:Gutscheinekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $gutscheinecounter = array(); 
        foreach($gutscheinekats as $key => $kats) {
            $gutscheinecounter[$kats->getGutscheinekatname()] = $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getGutscheinekatname(),'','',7,'Startinsert');
            if($gutscheinecounter[$kats->getGutscheinekatname()]) {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = true;
            } else {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = false;
            }
        }    
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('username' => $username),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
            if($users != "anon.") {
                $provider = $this->container->get('fos_message.provider');
                $mailanz = $provider->getNbUnreadMessages();
            } else {
                $mailanz = false;
            }
            $gutscheinelastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/gutscheineverwaltung.html.twig', array(
            'file_exists' => $users->getUserpic(),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'unreads' => $mailanz,
            'gutscheinedata' => $gutscheinedata,
            'gutscheinelastLogin' => $gutscheinelastLogin,
            'useremploy' => $useremploy,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
            'counter' => count($gutscheinedatagesamt),
            'gutscheinekats' => $gutscheinekats,
            'gutscheinecounterdata' => $gutscheinecounterdata,
        ));
    }

    /**
     * edit
     * @Route("/profile/gutscheineverwaltung/{gutscheineID}/{action}", name="user_gutscheineverwaltung_edit")
     */
    
    public function editAction(RequestStack $requestStack, Request $request, $gutscheineID, $action, \Swift_Mailer $mailer)
    {
        $gutscheinedata = new GutscheineoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $gutscheinekats = $dm->getRepository('App:Gutscheinekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $gutscheinecounter = array(); 
        foreach($gutscheinekats as $key => $kats) {
            $gutscheinecounter[$kats->getGutscheinekatname()] = $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getGutscheinekatname(),'','',7,'Startinsert');
            if($gutscheinecounter[$kats->getGutscheinekatname()]) {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = true;
            } else {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = false;
            }
        }    
        $task = new Gutscheine();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $gutscheinecheck = $emdel->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/gutscheineview/'.$gutscheinecheck->getGutscheinelink();
            $subjetsys = "Ein Beitrag wurde gelöscht!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('gutscheine@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von: ' .$gutscheinecheck->getUsername(). ' ein Beitrag auf Grafiker.de gelöscht!<br /><br />Der Link zum Beitrag war: ' . $linkToView);
            $mailer->send($message);     
            $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "gutscheine@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$gutscheinecheck->getUsername(). ' ein Beitrag auf Grafiker.de gelöscht!<br /><br />Der Link zum Beitrag war: ' . $linkToView);
            if($gutscheinecheck->getPicname()) {
            foreach($gutscheinecheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics);
                } else {}
            }
        }
            if ($gutscheinecheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail());
            } else {}
            $emdel->remove($gutscheinecheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $userdata = new CheckUserdataController();
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
                {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'DESC'), $anzprosite, $page);
            $gutscheinelastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/gutscheineverwaltung.html.twig', array(
            'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'gutscheinedata' => $gutscheinedata,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
            'counter' => count($gutscheinedatagesamt),
            'gutscheinekats' => $gutscheinekats,
            'gutscheinelastLogin' => $gutscheinelastLogin,
            'useremploy' => $useremploy,
            'gutscheinecounterdata' => $gutscheinecounterdata,
        ));
        } else if ($action == "edit" or $action == "all") {
            $userdata = new CheckUserdataController();
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
                {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
            } else {
                $username = 'Kein Mitglied!';
            }
        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Gutscheinekategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
        }
        $newextarray=array(array("gutscheineID" => $gutscheineID,"gutscheinekategorie" => $gutscheinecheck->getGutscheinekategorie(),"kommentare" => $gutscheinecheck->getKommentare(),"bewerten" => $gutscheinecheck->getBewerten(),"title" => $gutscheinecheck->getTitle(),"content" => $gutscheinecheck->getContent(),"rechte" => $gutscheinecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminGutscheineEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $gutscheinedata = $dms->getRepository('App:Gutscheine')->find($gutscheineID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($gutscheinedata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics);
    } else {}
    }
                $gutscheinecheck->setAstitle('Gutscheineinsert');
                $gutscheinedata->setTitle($task["titel"]);
                $gutscheinedata->setGutscheinekategorie($task["gutscheinekategorie"]);
                $gutscheinedata->setContent($task["content"]);
                $gutscheinedata->setIsads($task["isads"]);
                $gutscheinedata->setBewerten($task["bewerten"]);
                $gutscheinedata->setPicname($array);
                $dms->flush();
                $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/gutscheineview/'.$gutscheinedata->getGutscheinelink();
                $subjetsys = "Ein überarbeiteter Beitrag zum Freischalten!";
                $message = (new \Swift_Message())
                ->setSubject($subjetsys)
                ->setFrom('gutscheine@grafiker.de')
                ->setTo('mail@grafiker.de')
                ->setBody('Es wurde von: ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
                $mailer->send($message);
                $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "gutscheine@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);                

            }
            $casepics = array();
            $casepics=$gutscheinecheck->getPicname(); 
        return $this->render('userprofil/gutscheineedit.html.twig', array(
            'form' => $builder->createView(),
            'gutscheineID' => $gutscheineID,
            'username' => $gutscheinecheck->getUsername(),
            'picsarray' => $casepics,
            'gutscheinekats' => $gutscheinekats,
            'gutscheinecounterdata' => $gutscheinecounterdata,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);

        $gutscheinecheck->setAstitle("Startinsert");
        $gutscheinecheck->setAktiv(true);
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$gutscheinecheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('Ihr Beitrag wurde freigeschaltet!')
            ->setBody('Ihr Beitrag mit dem Titel: <strong style="color:red;">' . $gutscheinecheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'DESC'), $anzprosite, $page);

        return $this->render('userprofil/gutscheineverwaltung.html.twig', array(
            'gutscheinedata' => $gutscheinedata,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
            'gutscheinekats' => $gutscheinekats,
            'gutscheinecounterdata' => $gutscheinecounterdata,
        ));
    }
    }
}
