<?php
namespace App\Controller;
use App\AppBundle\Form\ProjektrechnerFormType;
use App\Document\Projektrechner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ProjektrechnerController extends Controller
{
    /**
     * @Route("/projektrechner", name="projektrechner")
     */
    public function new(Request $request)
    {

if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}
$dm = $this->get('doctrine_mongodb')->getManager();
$kategorien = $dm->getRepository('App:Projektrechnerkategorien')->findall();
$dm->flush();
$extarray=array();

foreach($kategorien as $key=>$value){
    $extarray[$value->getKatname()] = $value->getKatname();
  }
        
        $builder = $this->createForm(ProjektrechnerFormType::class, $extarray);
        $builder->add('username', HiddenType::class, array(
            'data' => $username,
        ));
        $builder->add('steps', HiddenType::class, array(
            'data' => 1,
        ));
        $task = new Projektrechner();
        /*$builder = $this->createFormBuilder($task)
        ->add('username', HiddenType::class, array(
            'data' => $username,
        ))
        ->add('steps', HiddenType::class, array(
            'data' => 1,
        ))
        ->add('prstart', ChoiceType::class, array(
            'label' => 'Projektart: ',
            'choices' => $extarray,
        ))
        ->add('stundensatz', TextType::class, array('label' => 'Stundensatz: ','data' => '55',))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-right'),'label' => 'weiter...'))
        ->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Profi-Projektrechner'))
        ->getForm();*/

        $builder->handleRequest($request);

if($task->getSteps()) {
    $builder = $this->createFormBuilder($task)
            ->add('username', HiddenType::class, array(
                'data' => $username,
            ))
            ->add('steps', HiddenType::class, array(
                'data' => 2,
            ))
            ->add('prstart', ChoiceType::class, array(
                'label' => 'Projektart: ',
                'choices' => $extarray,
            ))
            ->add('schwer', ChoiceType::class, array(
                'label' => 'Schwierigkeit: ',
                'choices' => array(
                    'Projektart' => array(
                        'einfach' => '1',
                        'mittel' => '2',
                        'schwer' => '3',
                    )
                ),
            ))
            ->add('stundensatz', TextType::class, array('label' => 'Stundensatz: ','data' => '55',))

            ->add('nart',ChoiceType::class,
            array('label' => 'Nutzungsart:','choices' => array(
                    'einfach' => 1,
                    'exklusiv' => 2),
            'multiple'=>false,'expanded'=>true))

            ->add('numfang',ChoiceType::class,
            array('label' => 'Nutzungsumfang:','choices' => array(
                    'klein' => 1,
                    'mittel' => 2,
                    'groß' => 3,),
            'multiple'=>false,'expanded'=>true))

            ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-right'),'label' => 'berechnen...'))
            ->getForm();
            $builder->handleRequest($request);

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);*/
            $dms = $this->get('doctrine_mongodb')->getManager();
            $katdauer = $dms->getRepository('App:Projektrechnerkategorien')->findOneByKatname($task->getPrstart());
            $dms->flush();

$basis_preis = round($task->getStundensatz()*$katdauer->getDauer(), 2);
if($task->getSchwer() == 1) {
    $wertSchwierigkeit = 0;
    $dauer=$katdauer->getDauer();
} else if($task->getSchwer() == 2) {
    $wertSchwierigkeit = $basis_preis * 0.5;
    $dauer=$katdauer->getDauer() * 1.5;
} else if($task->getSchwer() == 3) {
    $wertSchwierigkeit = $basis_preis * 1.0;
    $dauer=$katdauer->getDauer() * 2.0;
}
if($task->getNart() == 1) {
    $wertNutzungsart = 0;
} else if($task->getNart() == 2) {
    $wertNutzungsart = $basis_preis * 0.8;
}
if($task->getNumfang() == 1) {
    $wertNutzungsumfang = 0;
} else if($task->getNumfang() == 2) {
    $wertNutzungsumfang = $basis_preis * 0.5;
} else if($task->getNumfang() == 3) {
    $wertNutzungsumfang = $basis_preis * 1.2;
}

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);
            $dms = $this->get('doctrine_mongodb')->getManager();
            $katdauer = $dms->getRepository('App:Projektrechnerkategorien')->findOneByKatname($task->getPrstart());
            $dms->flush();*/

$dauer = ( !preg_match('/\.[1-9]+$/is',$dauer) ) ? intval($dauer) : number_format($dauer,2,',','.');
$preis_total = round($basis_preis+$wertSchwierigkeit+$wertNutzungsart+$wertNutzungsumfang,2);
$preis_total = number_format($preis_total,2,',','.');
$stundensatz = number_format($task->getStundensatz(),2,',','.');
return $this->render('projektrechner/laststep.html.twig', array(
        'ausgabedauer' => $dauer . 'h ',
        'ausgabestundensatz' =>  $stundensatz . ' €',
        'ausgabegesamt' => $preis_total . ' €',
    ));
} else {
        return $this->render('projektrechner/index.html.twig', array(
            'form' => $builder->createView(),
            'name' => $kategorien,
        ));
    }
}
}
