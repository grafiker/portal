<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceRequestsController extends AbstractController
{
    /**
     * @Route("/service/requests", name="service_requests")
     */
    public function index()
    {
        return $this->render('service_requests/index.html.twig', [
            'controller_name' => 'ServiceRequestsController',
        ]);
    }
}
