<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RichtlinienController extends AbstractController
{
    /**
     * @Route("/richtlinien", name="richtlinien")
     */
    public function index()
    {
        return $this->render('richtlinien/index.html.twig', [
            'controller_name' => 'RichtlinienController',
        ]);
    }
}
