<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PinpayController extends AbstractController
{
    /**
     * @Route("/pinpay", name="pinpay")
     */
    public function index()
    {
        return $this->render('pinpay/index.html.twig', [
            'controller_name' => 'PinpayController',
        ]);
    }
}
