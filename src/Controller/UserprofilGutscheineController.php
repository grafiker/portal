<?php

namespace App\Controller;
use App\AppBundle\Form\GutscheineFormType;
use App\AppBundle\Form\GutscheineCropFormType;
use App\AppBundle\Form\GutscheineEditFormType;
use App\Controller\CheckUserdataController;
use App\Document\Gutscheine;
use App\Controller\CropController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class UserprofilGutscheineController extends Controller
{
    /**
     * @Route("/profile/gutscheine/start", name="userprofil_gutscheine")
     */
    public function indexCreate(Request $request, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        
        $gutscheinedatesoldcheck = $emdel->getRepository('App:Gutscheine')->findOneBy(['username'=>$username, 'astitle'=>'Gutscheinestart', 'aktiv'=>true]);
        if($gutscheinedatesoldcheck) {
            //var_dump($gutscheinedates);
            $gutscheinedatesoldcheck->setAstitle("Gutscheineinsert");
            $emdel->flush();
            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/gutscheineview/'.$gutscheinedatesoldcheck->getGutscheinelink();
            $subjetsys = "Es gibt einen Beitrag zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('beitrag@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "beitrag@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
        }
        $gutscheinedatesoldcheck = $emdel->getRepository('App:Gutscheine')->findOneBy(['username'=>$username, 'astitle'=>'Ruhezustand', 'aktiv'=>true]);
        if($gutscheinedatesoldcheck) {
            //var_dump($gutscheinedates);
            $gutscheinedatesoldcheck->setAstitle("Gutscheinestart");
            $emdel->flush();
        }
        $gutscheinecheck = $emdel->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
    if($gutscheinecheck) {
        if($gutscheinecheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($gutscheinecheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/gutscheinepics/".$delpics)) {
                    //unlink("./upload/uploads/gutscheinepics/".$delpics);
                }
            }
        }
    }
    if($gutscheinecheck) {
        if(!$gutscheinecheck->getPicname()) {

        //$emdel->remove($gutscheinecheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
        $casepics = array();
        if (!$gutscheinecheck) {
            $gutscheineergebnis=0;
            //$casepics={};
            $gutscheineid = 0;
        } else {
            $gutscheineergebnis=1;
            $casepics=$gutscheinecheck->getPicname();
            $gutscheineid = $gutscheinecheck->getId();
        }
        $reset = false;
        $gutscheineergebnis=0;
        $builder = $this->createForm(GutscheineFormType::class);
        return $this->render('userprofil/gutscheine.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'gutscheineergebnis' => $gutscheineergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'gutscheineID' => $gutscheineid,
        ));
    }

/**
     * @Route("/profile/gutscheine/{gutscheineid}/editor", name="gutscheine_erditor")
     */
    public function createEdit(RequestStack $requestStack, Request $request, $gutscheineid, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $gutscheinedatesoldcheck = $emdel->getRepository('App:Gutscheine')->findOneBy(['username'=>$username, 'astitle'=>'Gutscheinestart', 'aktiv'=>NULL]);
        if($gutscheinedatesoldcheck) {
            //var_dump($gutscheinedates);
            $gutscheinedatesoldcheck->setAstitle("Ruhezustand");
            $emdel->flush();
        }

        $gutscheinedatesoldcheck = $emdel->getRepository('App:Gutscheine')->findOneBy(['username'=>$username, 'astitle'=>'Gutscheinestart', 'aktiv'=>true]);
        if($gutscheinedatesoldcheck) {
            //var_dump($gutscheinedates);
            if($gutscheinedatesoldcheck->getID() != $gutscheineid)
            $gutscheinedatesoldcheck->setAstitle("Gutscheineinsert");
            $emdel->flush();

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/gutscheineview/'.$gutscheinedatesoldcheck->getGutscheinelink();
            $subjetsys = "Es gibt einen Beitrag zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('gutscheines@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "gutscheines@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
        }
        
        $gutscheinedates = $emdel->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "id"=>$gutscheineid]);
        
        if($gutscheinedates) {
            //var_dump($gutscheinedates);
            $gutscheinedates->setAstitle("Gutscheinestart");
            $emdel->flush();
        }
        $gutscheinecheck = $emdel->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
    if($gutscheinecheck) {
        if($gutscheinecheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($gutscheinecheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/gutscheinepics/".$delpics)) {
                    //unlink("./upload/uploads/gutscheinepics/".$delpics);
                }
            }
        }
    }
    if($gutscheinecheck) {
        if(!$gutscheinecheck->getPicname()) {

        //$emdel->remove($gutscheinecheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
        $casepics = array();
        if (!$gutscheinecheck) {
            $gutscheineergebnis=0;
            //$casepics={};
            $gutscheineid = 0;
        } else {
            $gutscheineergebnis=1;
            $casepics=$gutscheinecheck->getPicname();
            $gutscheineid = $gutscheinecheck->getId();
        }
        $reset = false;
        $gutscheineergebnis=0;
        $builder = $this->createForm(GutscheineFormType::class);
        return $this->render('userprofil/gutscheinemedia.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'gutscheineergebnis' => $gutscheineergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'gutscheineID' => $gutscheineid,
        ));
    }
    /**
     * @Route("/profile/gutscheine/poststart", name="gutscheine_poststart")
     */
    public function createPoststart(Request $request)
    {
        $reset = false;
        $task = new Gutscheine();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Gutscheinekategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } else {}
        }
        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
        $casepics = array();
        $gutscheineergebnis=1;
        if($gutscheinecheck) {
        $casepics=$gutscheinecheck->getPicname();
        } else {

            $task->setUsername($username);
            $data[] = NULL;
            //$task->setPicname($data);
            $task->setAstitle("Gutscheinestart");
            $task->setGutscheinekategorie("Beiträge");
            $task->setPicname($casepics);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
            $casepics=$gutscheinecheck->getPicname();
        }
        $newextarray=array(array("gutscheinekategorie" => $gutscheinecheck->getGutscheinekategorie(),"isads" => $gutscheinecheck->getIsads(),"bewerten" => $gutscheinecheck->getBewerten(),"title" => $gutscheinecheck->getTitle(),"content" => $gutscheinecheck->getContent(),"rechte" => $gutscheinecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(GutscheineEditFormType::class, $newextarray);
            return $this->render('userprofil/gutscheinestart.html.twig', array(
                'form' => $builder->createView(),
                'controller_name' => 'ShowcaseController',
                'gutscheineergebnis' => $gutscheineergebnis,
                'picsarray' => $casepics,
                'reset' => $reset,
                'gutscheineid' => $gutscheinecheck->getId(),
            ));
        }
    }


    /**
     * @Route("/profile/gutscheine/{getdata}", name="gutscheine")
     */
    public function createAction(Request $request, $getdata, \Swift_Mailer $mailer)
    {
        $crop = new CropController();
        $task = new Gutscheine();
        $reset = false;
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            if($getdata == "reset") {
                $emdel = $this->get('doctrine_mongodb')->getManager();
                $gutscheinecheck = $emdel->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
//var_dump($gutscheinecheck);
                if($gutscheinecheck->getPicname()) {
                foreach($gutscheinecheck->getPicname() as $key => $delpics) {
                    if(file_exists("./upload/uploads/gutscheinepics/".$delpics)) {
                        unlink("./upload/uploads/gutscheinepics/".$delpics);
                    }
                }
            }
                $emdel->remove($gutscheinecheck);
                $emdel->flush();
                $reset = true;
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Gutscheinekategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
                } else {}
        }
    }
        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["username"=>$username, "astitle"=>"Gutscheinestart"]);
        $casepics = array();
        if (!$gutscheinecheck) {
            $gutscheineergebnis=0;
            $casepics[0]="keins";
            $builder = $this->createForm(GutscheineFormType::class, $extarray);
        } else {
            $gutscheineergebnis=1;
            $casepics=$gutscheinecheck->getPicname();
            $newextarray=array(array("gutscheinekategorie" => $gutscheinecheck->getGutscheinekategorie(),"isads" => $gutscheinecheck->getIsads(),"bewerten" => $gutscheinecheck->getBewerten(),"title" => $gutscheinecheck->getTitle(),"content" => $gutscheinecheck->getContent(),"rechte" => $gutscheinecheck->getRechte(), "data" => array($extarray)));
            $builder = $this->createForm(GutscheineEditFormType::class, $newextarray);
        }

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
        //var_dump($temp);
        if(isset($temp["form"]["gutscheinepay"])) {
            $gutscheinepay = true;
        } else {
            $gutscheinepay = false;
        }
        if(isset($temp["form"]["update"])) {
            $update = true;
        } else {
            $update = false;
        }
        
        if((isset($data["titel"])) or (isset($data["w"]))) {
            if((!isset($data["w"]) or ($data["w"] == 0))) {
                //var_dump("thumbnail_".$data["picselected"]);
            $date = \DateTime::createFromFormat('U', time()+3600);
            $date->setTimezone(new \DateTimeZone('UTC'));
            $gutscheinecheck->setGutscheineKategorie($data["gutscheinekategorie"]);
            $gutscheinecheck->setTitle($data["titel"]);
            $title = str_replace("!", "", $data["titel"]);
            $title = str_replace("?", "", $title);
            $title = str_replace("%", "", $title);
            $title = str_replace("&", "", $title);
            $title = str_replace(".....", "", $title);
            $title = str_replace("....", "", $title);
            $title = str_replace("...", "", $title);
            $title = str_replace(" - ", " ", $title);
            $title = str_replace("„", "", $title);
            $title = str_replace("”", "", $title);
            if(strlen($title) >= 100) {
            $gutscheinelinkindb=preg_replace("/[^ ]*$/", '', substr($title, 0, 100));
            $gutscheinelinkindb=substr($gutscheinelinkindb, 0, -1);
            } else {
                $gutscheinelinkindb = $title;
            }
            $gutscheinelinkindb = str_replace(" ", "_", $gutscheinelinkindb);
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('title' => $data["titel"]),array('id' => 'ASC'));
            $anzindb=count($gutscheinedatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $gutscheinelinkindb = $gutscheinelinkindb.'_'.$anzindb;
            } else {
                $gutscheinelinkindb = $gutscheinelinkindb;
            }
            //var_dump($data["picselected"]);
            if($data["picselected"] != "0") {
                $data["picselected"] = $data["picselected"];
                //var_dump($gutscheinecheck->getPicname()[0]);
            } else {
                $data["picselected"] = $gutscheinecheck->getPicname()[0];
                //var_dump($gutscheinecheck->getPicname()[0]);
            }
            //var_dump($data);
            $gutscheinecheck->setGutscheinelink($gutscheinelinkindb);
            $gutscheinecheck->setContent($data["content"]);
            $gutscheinecheck->setIsads($data["isads"]); 
            $gutscheinecheck->setBewerten($data["bewerten"]);
            $gutscheinecheck->setRechte($data["rechte"]);
            $gutscheinecheck->setStartDateDay($data["startDateDay"]);
            $gutscheinecheck->setStartDateMonat($data["startDateMonat"]);
            $gutscheinecheck->setStartDateYear($data["startDateYear"]);
            $gutscheinecheck->setEndDateDay($data["endDateDay"]);
            $gutscheinecheck->setEndDateMonat($data["endDateMonat"]);
            $gutscheinecheck->setEndDateYear($data["endDateYear"]);
            $gutscheinecheck->setAktiv(false);
            $gutscheinecheck->setStartdate($date); 
            //$gutscheinecheck->setThumbnail("thumbnail_".$data["picselected"]);      
            $em->flush();    
        }   
            

            $upload_dir = "upload/uploads/gutscheinepics"; 				// The directory for the images to be saved in
            $upload_path = $upload_dir."/";				// The path to where the image will be saved
            $large_image_prefix = "resize_"; 			// The prefix name to large image
            $thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
            $large_image_name = $data["picselected"];     // New name of the large image (append the timestamp to the filename)
            $thumb_image_name = "".$thumb_image_prefix.$data["picselected"];
            $large_image_location = $upload_path.$large_image_name;
            $thumb_image_location = $upload_path.$thumb_image_name;
            $thumb_width = 181;
            $thumb_height = 168;
            $max_width = 600;
            $width = $large_image_location;
			$height = $large_image_location;
			//Scale the image if it is greater than the width set above
			if ($width > $max_width){
				$scale = $max_width/$width;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}else{
				$scale = 1;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}		
            //$current_large_image_width = $crop->getWidth($large_image_location);
            //$current_large_image_height = $crop->getHeight($large_image_location);

            if (file_exists($large_image_location)){
                if(file_exists($thumb_image_location)){
                    $thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name."\" alt=\"Thumbnail Image\"/>";
                }else{
                    $thumb_photo_exists = "";
                }
                   $large_photo_exists = "<img src=\"".$upload_path.$large_image_name."\" alt=\"Large Image\"/>";
            } else {
                   $large_photo_exists = "";
                $thumb_photo_exists = "";
            }
            if ((isset($data["w"]) AND ($data["w"] != 0)) && strlen($large_photo_exists)>0) {
                //Get the new coordinates to crop the image.
                $x1 = $data["x1"];
                $y1 = $data["y1"];
                $x2 = $data["x2"];
                $y2 = $data["y2"];
                $w = $data["w"];
                $h = $data["h"];
                //Scale the image to the thumb_width set above
                $scale = $thumb_width/$w;
                //$cropped = $crop->resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
                $gutscheinecheck->setAstitle('Gutscheineinsert');
                $gutscheinecheck->setIsads($data["isads"]); 
                $gutscheinecheck->setBewerten($data["bewerten"]);
                $gutscheinecheck->setRechte($data["rechte"]);
                $em->flush();
            $subjetsys = "Neuer Beitrag zum freischalten!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('gutscheine@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde ein neuer Beitrag von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "gutscheine@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde ein neuer Beitrag von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
                return $this->render('userprofil/gutscheineend.html.twig', array(
                    'controller_name' => 'ShowcaseController',
                ));
            } else {

                $builder = $this->createForm(GutscheineCropFormType::class);
                $builder->add('picselected', HiddenType::class, array(
                    'data' => $data["picselected"],
                    'required' => false,
                ));
if(!in_array($data["gutscheinekategorie"], $nextextarray)) {
                $builder->add('gutscheinekategorie', ChoiceType::class, array(
                    'label' => 'Kategorie wechseln? ',
                    'choices' => $nextextarray,
                ));
                $builder->add('update', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Account updaten...'));
                $builder->add('gutscheinepay', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Beitrag kaufen...'));
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Beitrag mit neuer Kategorie eintragen...'));
} else {
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'gutscheineinsert bdnmini2 btn btn-primary pull-left'),'label' => 'Beitrag eintragen...'));
}

                


                $emp = $this->get('doctrine_mongodb')->getManager();
                $gutscheinekategoriecheck = $emp->getRepository('App:Gutscheinekategorien')->findOneBy(["gutscheinekatname"=>$data["gutscheinekategorie"]]);
                $rolein = 0;
                if(in_array("Admin", $gutscheinekategoriecheck->getRolegroup())) { $rolein = "Admin"; }
                if(in_array("Free", $gutscheinekategoriecheck->getRolegroup())) { $rolein = "Free"; }
                if(in_array("Standart", $gutscheinekategoriecheck->getRolegroup())) { $rolein = "Profi"; }
                if(in_array("Platin", $gutscheinekategoriecheck->getRolegroup())) { $rolein = "Platin"; }
                $emp->flush();
                $userdata = new CheckUserdataController();               
                return $this->render('userprofil/gutscheinenext.html.twig', array(
                    'form' => $builder->createView(),
                    'controller_name' => 'ShowcaseController',
                    'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $username),
                    'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $username),
                    'gutscheineergebnis' => $gutscheineergebnis,
                    'picsarray' => $casepics,
                    'gutscheineTitel' => $data["titel"],
                    'gutscheineKategorie' => $data["gutscheinekategorie"],
                    'gutscheineContent' => html_entity_decode(HashToLinkController::HashToLink($data["content"])),
                    'gutscheineroles' => $rolein,
                    'thumb_width' => $thumb_width,
                    'thumb_height' => $thumb_height,
                    'current_large_image_width' => $thumb_width,
                    'current_large_image_height' => $thumb_height,
                    'gutscheineauswahlpic' => $large_image_location,
                    'picselected' => $data["picselected"],
                    'reset' => $reset,
                ));
            }
        } else {

            return $this->render('userprofil/gutscheinemedia.html.twig', array(
                'form' => $builder->createView(),
                'controller_name' => 'ShowcaseController',
                'gutscheineergebnis' => $gutscheineergebnis,
                'picsarray' => $casepics,
                'reset' => $reset,
            ));
        }
        
    }
}
