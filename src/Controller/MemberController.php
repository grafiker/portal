<?php

namespace App\Controller;
use App\Document\Network;
use App\Document\Profilbesucher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
class MemberController extends Controller
{
    /**
     * Create
     * @Route("/member", name="member")
     */
    public function createAction()
    {
        $pindata = new PinoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
            }
        }       
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
//var_dump($users->getUsername());
        $fileexistarray = array();
        $filelinkarray = array();
        if($users == "anon.") {
            $selfuser = 3;
            $ownerfilter = '';
            $userfilter = '';
        } else {
            $selfuser = 1;
            $fileexistarray[$users->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users->getUsername());
            $filelinkarray[$users->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users->getUsername());
            $ownerfilter = $users->getUsername();
            $userfilter = '';
        }
        $upload_dir = 'upload/uploads/firma/';
        $name = $upload_dir.'/'.$users . '_firma';

        if(file_exists($name.".png")) {
          $logoexist=true;
          $logolink=$name.".png";
        }elseif(file_exists($name.".gif")) {
            $logoexist=true;
            $logolink=$name.".gif";
        } elseif(file_exists($name.".jpg")) {
            $logoexist=true;
            $logolink=$name.".jpg";
        } else {
            $logoexist=false;
            $logolink=false;
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$users]);
            
        $userskills = array();
        $userskills=$users->getSkills();
        $pinlastLogin[$users->getUsername()] = $users->getUpdatedAt();
        $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('member/index.html.twig', [
            'controller_name' => 'MemberController',
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'selfuser' => $selfuser,
            'userdata' => $userdatas,
            'userskills' => $userskills,
            'userdata' => $users,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'pincounterdata' => $pincounterdata,
            'counter' => count($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,'all','DESC','all',$ownerfilter,$userfilter,7,'Startinsert')),
            'pindata' => $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,'all','DESC','all',$ownerfilter,$userfilter,7,'Startinsert'),
            'pinkats' => $pinkats,
            'pincounter' => MediadatenController::mediadaten($dm, 'Pins', 'username', $users->getUsername()),
        ]);
        $dm->flush();
    }

    /**
     * Usersearch
     * @Route("/member/{username}", name="member_view")
     */
    public function searchAction(RequestStack $requestStack, Request $request, $username)
    {
        $besucher = new Profilbesucher();
        $pindata = new PinoutputController();
        $networknews = false;
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
            }
        }       
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
//var_dump($users);
        if($username == $users) {
            $selfuser = 1;
        } else {
            $selfuser = 0;
        }
        $date = \DateTime::createFromFormat('U', time()+3600);
        $date->setTimezone(new \DateTimeZone('UTC'));

if(!$selfuser and $users != "anon.") {
$profilbesucher = $dm->getRepository('App:Profilbesucher')->findOneBy(array('username' => $username, 'besucher' => $users->getUsername()));
if(!$profilbesucher) {
        $besucher->setUsername($username);
        $besucher->setBesucher($users->getUsername());
        $besucher->setViewdate($date);
        $besucher->setNew(true);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($besucher);
        $dm->flush();
    } else {
        //var_dump($profilbesucher);
        $profilbesucher->setUsername($username);
        $profilbesucher->setBesucher($users->getUsername());
        $profilbesucher->setViewdate($date);
        $profilbesucher->setNew(true);
        $dm->flush();
    }
}

$namegeteilt = explode("-", $username);
$namecount = count($namegeteilt);
//var_dump($namecount);
if($namecount == 1){
    $searchQuery = array(
        '$or' => array(
            array(
                'username' => $username,
                ),
            )
        ); 
                } else {
                    $searchQuery = array(
                        '$or' => array(
                            array(
                                'username' => $username,
                                ),
                            )
                        );                   
                }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $userdatas = $dm->getRepository('App:User')->findOneBy($searchQuery);
//var_dump($namegeteilt[1]);
        $upload_dir = 'upload/uploads/firma';
        $name = $upload_dir.'/'.$username . '_firma';

        if(file_exists($name.".png")) {
          $logoexist=true;
          $logolink=$name.".png";
        }elseif(file_exists($name.".gif")) {
            $logoexist=true;
            $logolink=$name.".gif";
        } elseif(file_exists($name.".jpg")) {
            $logoexist=true;
            $logolink=$name.".jpg";
        } else {
            $logoexist=false;
            $logolink=false;
        }
        $fileexistarray = array();
        $filelinkarray = array();
        if (!$userdatas) {
            $selfuser = 2;
        } else {
            $fileexistarray[$userdatas->getUsername()] = $userdatas->getUserpic();
            $filelinkarray[$userdatas->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $userdatas->getUsername());
        }

            if($selfuser == 1) {
                $ownerfilter = $username;
                $userfilter = '';
            } else {
                $ownerfilter = '';
                $userfilter = $username;
            }
            $sendmsg = 0;
            if(isset($_POST['betreffforuser'])) {
            $threadBuilder = $this->get('fos_message.composer')->newThread();
            $threadBuilder
                ->addRecipient($userdatas) // Retrieved from your backend, your user manager or ...
                ->setSender($users)
                ->setSubject($_POST['betreffforuser'])
                ->setBody($_POST['messegesforuser']);
             
             
            $sender = $this->get('fos_message.sender');
            
            $sender->send($threadBuilder->getMessage());
            $sendmsg = 1;
        }
        if($users != "anon.") {
        $em = $this->get('doctrine_mongodb')->getManager();
                $friendcheck = $em->getRepository('App:Network')->findOneBy(array("username"=>$username, "friend"=>$users->getUsername()));

                if (!$friendcheck) {
                    $friendcheck = false;
                } else {
                    $friendcheck = true;
                }
                $usercheck = $em->getRepository('App:Network')->findOneBy(array("username"=>$users->getUsername(), "friend"=>$username));

                if (!$usercheck) {
                    $usercheck = false;
                } else {
                    $usercheck = true;
                }
            } else {
                $usercheck = false;
                $friendcheck = false;
            }
        $newnetwork = array();
        if(isset($request->request->all()['form'])) {
            $newnetwork = $request->request->all()['form'];

            if(isset($newnetwork['networkask']) and ($usercheck != true and $friendcheck != true)) {
                $task = new Network();
                $date = \DateTime::createFromFormat('U', time()+3600);
                $date->setTimezone(new \DateTimeZone('UTC'));
                $task->setStartdate($date);
                $task->setUsername($users->getUsername());
                $task->setFriend($username);
                $task->setRequest($newnetwork['content']);
                $task->setReason($newnetwork['notiz']);
                $task->setAccepted(false);
                //var_dump($task);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($task);
                $dm->flush();
                $networknews = true;

                $emdjabsender = $this->get('doctrine_mongodb')->getManager();
                $usersender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>$username]);
                $threadBuilder = $this->get('fos_message.composer')->newThread();
                $threadBuilder
                ->addRecipient($usersender) // Retrieved from your backend, your user manager or ...
                ->setSender($users)
                ->setSubject('Netzwerkbeitrittsanfrage von ' . $users->getUsername())
                ->setBody($newnetwork['content'] . '<br /><br /><a href="' .$requestStack->getCurrentRequest()->getSchemeAndHttpHost(). '/profile/membernetwork/in">Klicken Sie bitte hier um Ihre offenen Anfragen zu sehen!</a>');
             
             
            $sender = $this->get('fos_message.sender');
            
            $sender->send($threadBuilder->getMessage());

            }
        }

        $userskills = array();
        $userskills=$userdatas->getSkills();
        $pinlastLogin[$userdatas->getUsername()] = $userdatas->getUpdatedAt();
        $useremploy[$userdatas->getUsername()] = $userdatas->getEmploy();
        return $this->render('member/index.html.twig', [
            'controller_name' => 'MemberController',
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'selfuser' => $selfuser,
            'userdata' => $userdatas,
            'sendmsg' => $sendmsg,
            'userskills' => $userskills,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'friendcheck' => $friendcheck,
            'usercheck' => $usercheck,
            'networknews' => $networknews,
            'pincounterdata' => $pincounterdata,
            'counter' => count($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,'all','DESC','all',$ownerfilter,$userfilter,7,'Startinsert')),
            'pindata' => $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,'all','DESC','all',$ownerfilter,$userfilter,7,'Startinsert'),
            'pinkats' => $pinkats,
            'pincounter' => MediadatenController::mediadaten($dm, 'Pins', 'username', $userdatas->getUsername()),
        ]);
        $dm->flush();
    }
}
