<?php

namespace App\Controller;

use App\Document\User;
use App\Document\JobsCache;
use App\Controller\Mobile_Detect;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\AppBundle\Form\ServiceWidgetFormType;
use App\AppBundle\Form\ServiceWidgetEditFormType;
use App\Document\ServiceWidget;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceWidgetController extends AbstractController
{
    function __construct(ManagerRegistry $doctrine_mongodb, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->urlGenerator = $urlGenerator;
    }
    /**
     * @Route("/service/widget", name="service_widget")
     */
    public function index(Request $request)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}
        $builder = $this->createForm(ServiceWidgetFormType::class, array());

        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            //\var_dump($task);

            $emdsend = $this->_doctrine_mongodb->getManager();
            $userwidget = $emdsend->getRepository('App:ServiceWidget')->findBy(["accountName"=>$users->getID()]);

            $emdsend = $this->_doctrine_mongodb->getManager();
            $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$task["ID"]]);

            if($userwidget) {
                $numbers = count($userwidget)+1;
            } else {
                $numbers = 1;
            }
            if($thiswidget) {
                $widget = $thiswidget;
            } else {
                $widget = new ServiceWidget();
            }
            $widget->setAccountName($users->getID());
            $widget->setAccountURL($task["accountURL"]);
            $widget->setCssColor($task["cssColor"]);
            $widget->setCssBackgroundColor($task["cssBackgroundColor"]);
            $widget->setCssMargin($task["cssMargin"]);
            $widget->setCssPadding($task["cssPadding"]);
            $widget->setCssPosition($task["cssPosition"]);
            $widget->setCssWidth($task["cssWidth"]);
            $widget->setCssText($task["cssText"]);
            $widget->setWidgetNumber($numbers);

            $this->_doctrine_mongodb->getManager()->persist($widget);
            $this->_doctrine_mongodb->getManager()->flush();
//\var_dump($task);
$toroutes = "service_widgetlist";
return new RedirectResponse($this->urlGenerator->generate($toroutes));
        } else {
            return $this->render('service_widget/index.html.twig', [
                'form' => $builder->createView(),
                'controller_name' => 'ServiceWidgetController',
            ]);
        }
    }

    /**
     * @Route("/service/widgetedit/{id}", name="service_widgetedit")
     */
    public function editAction(Request $request, $id)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}

        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $builder = $this->createForm(ServiceWidgetEditFormType::class, $thiswidget);

        return $this->render('service_widget/index.html.twig', [
            'id' => $id,
            'form' => $builder->createView(),
            'controller_name' => 'ServiceWidgetController',
        ]);
    }
    /**
     * @Route("/widget/start", name="service_widgetstart")
     */
    public function startAction(Request $request)
    {   
        $ipadresse = $_SERVER['HTTP_X_FORWARDED_FOR'];
        //$ipadresse = "192.0.0.1";
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:JobsCache')->findOneBy(["ipadresse"=>$ipadresse]);
if($thiswidget) {
    $thiswidgetcache = $emdsend->getRepository('App:Jobs')->findOneBy(["cacheid"=>$thiswidget->getID()]);
if(!$thiswidgetcache) {
        $thiswidget->setTimestamp(time());
        $thiswidget->setIpadresse($ipadresse);
        $thiswidget->setDatacache($_POST);
        $this->_doctrine_mongodb->getManager()->persist($thiswidget);
        $this->_doctrine_mongodb->getManager()->flush();

        $cacheid = $thiswidget->getID();
} else {
        $task = new JobsCache();
        $task->setTimestamp(time());
        $task->setIpadresse($ipadresse);
        $task->setDatacache($_POST);
        $this->_doctrine_mongodb->getManager()->persist($task);
        $this->_doctrine_mongodb->getManager()->flush();
        $cacheid = $task->getID();
}
} else {
        $task = new JobsCache();
        $task->setTimestamp(time());
        $task->setIpadresse($ipadresse);
        $task->setDatacache($_POST);
        $this->_doctrine_mongodb->getManager()->persist($task);
        $this->_doctrine_mongodb->getManager()->flush();
        $cacheid = $task->getID();
}
        return $this->render('service_widget/start.html.twig', [
            'controller_name' => 'ServiceWidgetController',
            'cacheid' => $cacheid,
        ]);
    }
    /**
     * @Route("/widget/output/{id}", name="service_widgetoutput")
     */
    public function outputAction(Request $request, $id)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);

        //\var_dump($_POST);
        return $this->render('service_widget/output.html.twig', [
            'id' => $id,
            'allwidget' => $thiswidget,
            'controller_name' => 'ServiceWidgetController',
        ]);
    }
    /**
     * @Route("/widget/designtenders/{id}", name="service_widgetdesigntenders")
     */
    public function designtendersAction(Request $request, $id)
    {
        $detect = new Mobile_Detect;
        if( $detect->isMobile() && !$detect->isTablet() ){
 $mobilcheck = "mobil";
        } else if(!$detect->isMobile() && $detect->isTablet()) {
            $mobilcheck = "tablet";
        } else {
            $mobilcheck = "pc";
        }
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $userberufsgruppe1 = $emdsend->createQueryBuilder('App:User')->field('berufsgruppe1')->equals(true)->count()->getQuery()->execute();
        if(isset($_GET["cacheid"])) {
        $thiswidgetcache = $emdsend->getRepository('App:JobsCache')->findOneBy(["id"=>$_GET["cacheid"]]);
        $ersatzcacheid = 0;
        } else {
         $ipadresse = $_SERVER['HTTP_X_FORWARDED_FOR'];
         //$ipadresse = "192.0.0.1";
         $_POST["productPriceIncTax"] = 0.00;
         $_POST["productName"] = "grafiker";
        $task = new JobsCache();
        $task->setTimestamp(time());
        $task->setDatacache($_POST);
        $task->setIpadresse($ipadresse);
        $this->_doctrine_mongodb->getManager()->persist($task);
        $this->_doctrine_mongodb->getManager()->flush();
        $ersatzcacheid = $task->getID();
        $thiswidgetcache = $emdsend->getRepository('App:JobsCache')->findOneBy(["id"=>$ersatzcacheid]);
        $_GET["cacheid"] = $ersatzcacheid;
        }
        return $this->render('service_widget/designtenders.html.twig', [
            'id' => $id,
            'allwidget' => $thiswidget,
            'grafikercount' => number_format($userberufsgruppe1, 0, ',', '.'),
            'thiswidgetcache' => $thiswidgetcache,
            'controller_name' => 'ServiceWidgetController',
            'cacheid' => $_GET["cacheid"],
            'mobilcheck' => $mobilcheck,
            'ersatzcacheid' => $ersatzcacheid,
        ]);
    }
}
