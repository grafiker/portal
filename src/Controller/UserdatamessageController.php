<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\HttpFoundation\RequestStack;
class UserdatamessageController extends Controller
{
    /**
     * @Route("/admin/userdatamessage/{userID}", name="userdatamessage")
     */
    public function index(RequestStack $requestStack, $userID, \Swift_Mailer $mailer)
    {

            $em = $this->get('doctrine_mongodb')->getManager();
            $user = $em->getRepository('App:User')->findOneBy(["id"=>$userID]);

            $userdelpic = false;
            $userdelfirmapic = false;
            $bewerbungsdocs = array();
            $bewerbungsmail = false;
            $jobdocs = array();
            $pinsdelpics = array();
            $pindeltumb = false;
            $databasecounter = array();
            $bewerbungsdocscount = array();
            $jobdocscount = array();
            $pinpicsdelcount = array();
            $zipcounter = 0;
            $alldbdata = array();
//Was passiert beim löschen eines Mitglieds???
$m = $this->container->get('doctrine_mongodb.odm.default_connection'); //Mongeverbindung wird hergestellt
$username = $user->getUsername();
$usermail = $user->getEmail();
$zip = new \ZipArchive();

$zipFilePath="upload/uploads/data/".$username."-UserSavedata.zip";
$zipFilePath2="upload/uploads/data/".$username."-PinPics.zip";
$upload_dir = 'upload/uploads/data/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
if(file_exists($zipFilePath)) {

        unlink ($zipFilePath); 

}
if ($zip->open($zipFilePath, \ZIPARCHIVE::CREATE) != TRUE) {
        die ("Could not open archive");
}
    //$zip->addFile("upload/uploads/userpics/Sniky_userpic.jpg","userpics/Sniky_userpic.jpg");

// close and save archive

 
$db = $m->selectDatabase('grafiker'); //Datenbank wird ausgewählt

$threads = $em->getRepository('App:Thread')->findByParticipants($user);
//var_dump($threads->getInboxThreads());
foreach($threads as $threads) {
    $participants = $threads->getParticipants();
    $zd = 0;
    foreach($participants as $participants) {
        if($zd == 0)
        {
            var_dump("Empfänger: "); 
        } else {
            var_dump("Sender: "); 
        }
    var_dump($participants->getUsername() . "<br />");
    $zd++;
    }
    var_dump($threads->getSubject() . "<br />");
    var_dump($threads->getId() . "<br />");
//$em->remove($threads); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
$em->flush(); 
}
$mails = $em->getRepository('App:Message')->findBySender($user);

foreach($mails as $mails) {
    var_dump($mails->getBody() . "<br />");
//$em->remove($mails);  // Lösche alle gestarteteten mails vom User aus MongoDB löschen
$em->flush(); 
}
foreach($db->listCollections() as $listall) // eine Schleife mit den Tabellen wird gestartet
{
$colnames = $listall->getCollection()->getCollectionName(); // Namen aller Tabellen wird in der Schleife ermittelt

$delmember = $this->get('doctrine_mongodb')->getManager(); //Mongeverbindung wird hergestellt für löschen der Mongo-Einträge erstellt
if($colnames != 'Network') {
$finallby = $delmember->getRepository('App:'.$colnames)->findBy(array('username' => $username)); // es wird in sämtlichen Tabellen nach dem zu löschenden Usernamen gesucht 
} else { // Wenn der Tabellenname Network ist, gucke auch nach dem Friend Namen um auch diese Netzwerkverbindung zu löschen!
$searchQuery = array(
    '$or' => array(
        array(
            'username' => $username,
            ),
        array(
            'friend' => $username,
            ),
        )
    );
$finallby = $delmember->getRepository('App:'.$colnames)->findBy($searchQuery);
}
foreach($finallby as $finallby) //es wird eine weitere Schleife gestartet um alle gefundenen Einträge einzeln zu behandeln
{
    $alldbdata[$colnames][] = $finallby;
if($colnames == "User") { // Ist der Tabellenname User, lade die Bilddaten vom Userbild und vom Firmenbild um es zu löschen
    $userdelpic = $finallby->getUserpic();
    if($userdelpic != "del" and $userdelpic != "") {
    //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/userpics/".$userdelpic);
    $zip->addFile("upload/uploads/userpics/".$userdelpic,"userpic/" . $userdelpic);
    $zipcounter++;
    $userpiccounter = 1;
    } else {
    $userpiccounter = 1;    
    }
    $userdelfirmapic = $finallby->getFirmapic();
    if($userdelfirmapic != "del" and $userdelfirmapic != "") {
        //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/firma/".$userdelfirmapic);
        $zip->addFile("upload/uploads/firma/".$userdelfirmapic,"firma/" . $userdelfirmapic);
        $zipcounter++;
        $userfirmapiccounter = 1;
    } else {
    $userfirmapiccounter = 1;    
    }
}
if($colnames == "Bewerbung") { // Ist der Tabellenname Bewerbung, lade die PDF Dateien und die MailID meiner eigenen Bewerbungen um sie zu löschen
    $bewerbungsdocs = $finallby->getDocpdf();
    $bewerbungsmail = $finallby->getMailID();
    foreach($bewerbungsdocs as $key => $value)
    {
        //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/bewerbung/".$value);
        $zip->addFile("upload/uploads/pdf/bewerbung/".$value,"bewerbungen/bewerbung-" . $key . ".pdf");
        $zipcounter++;
        $bewerbungsdocscount[$key] = count($bewerbungsdocs);
        $i = $key;
    }
}
if($colnames == "Job") { // Ist der Tabellenname Job, lade die PDF Dateien um sie zu löschen
    $jobdocs = $finallby->getDocpdf();
    foreach($jobdocs as $key => $value)
    {
        //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/".$value);
        $zip->addFile("upload/uploads/pdf/".$value,"ausschreibungen/ausschreibung-" . $key . ".pdf");
        $zipcounter++;
        $jobdocscount[$key] = count($jobdocs);
        $i = $key;
    }
    
}
if($colnames == "Pins") { // Ist der Tabellenname Pins, lade die Pinpics und den Thumbnail Namen um es zu löschen
    $pinsdelpics = $finallby->getPicname();
    $zipy = new \ZipArchive();
    if ($zipy->open($zipFilePath2, \ZIPARCHIVE::CREATE) != TRUE) {
        die ("Could not open archive");
    }
    $i=0;
    foreach($pinsdelpics as $key => $value)
    {
        $filename = "upload/uploads/pinpics/".$value;
        if (file_exists($filename)) {
            
            $zipy->addFile("upload/uploads/pinpics/".$value,"pinbilder/" . $value);
            $zipcounter++;
        }
        //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$value);
        $pinpicsdelcount[$key] = count($pinsdelpics);
        $i = $key;
    }
    $pindeltumb = $finallby->getThumbnail();
    if($pindeltumb != "") {
        //unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pindeltumb);
        $zip->addFile("upload/uploads/pinpics/".$pindeltumb,"pintitelbilder/" . $pindeltumb);
        $zipcounter++;
    }
    $zipy->close();
}
//$delmember->remove($finallby); // Lösche alle gefundenen MongoDB einträge um den User komplett zu entfernen    
$delmember->flush(); 
}
//Als nächstes werden einige Statistikdaten vür die Mail und Adminausgabe geschrieben.
if($colnames == "Thread") { 
$databasecounter[$colnames] = $em->createQueryBuilder('App:' . $colnames)->field('participants')->equals($user)->count()->getQuery()->execute();
} else if($colnames == "Message") {
$databasecounter[$colnames] = $em->createQueryBuilder('App:' . $colnames)->field('sender')->equals($user)->count()->getQuery()->execute();
} else if($colnames == "Network") {
$searchQuery = array(
    '$or' => array(
        array(
            'username' => $username,
            ),
        array(
            'friend' => $username,
            ),
        )
    );
$finallby = $delmember->getRepository('App:'.$colnames)->findBy($searchQuery);
$databasecounter[$colnames] = count($finallby);
} else {
$databasecounter[$colnames] = $em->createQueryBuilder('App:' . $colnames)->field('username')->equals($username)->count()->getQuery()->execute();
}

}
ksort($databasecounter);
if(file_exists("upload/uploads/data/".$username."-PinPics.zip")) {
$zip->addFile("upload/uploads/data/".$username."-PinPics.zip","pinbilder/PinPics.zip");
$zipcounter++;
}
/*$handle = fopen("upload/uploads/data/".$username."_dbdump.txt", "w");
fwrite ($handle, serialize($alldbdata));
fclose ($handle);*/
ob_start();
foreach($alldbdata as $alldbdata) {
print_r($alldbdata);
var_dump("<br /><br />");
}
file_put_contents("upload/uploads/data/".$username."_dbdump.html", ob_get_contents());
ob_end_clean();
$zip->addFile("upload/uploads/data/".$username."_dbdump.html","datenbankdump/dbdump.html");
//var_dump($alldbdata);
$zip->close();
//Als nächstes werden Die Mails an den User und den Admin gesendet!
if($zipcounter) {
$linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost()."/upload/uploads/data/".$username."-UserSavedata.zip";
$subjetsys = "Deine gespeicherten Daten auf grafiker.de!";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom('account@grafiker.de')
        ->setTo($usermail)
        ->setBody('Hallo ' . $username . ',<br />Wie angefordert, senden Wir dir heute deine bei uns gespeicherten Daten!<br /><br />Die folgenden Daten wurden in unserer Datenbank zu deinen Benutzernamen gefunden:<br /><br />' .
        $this->renderView(
            'admin/datauserendmail.html.twig',
            array(
'userdata' => $user,
'alldbdata' => $alldbdata,
'username' => $username,
'userdelpic' => $userdelpic,
'userdelfirmapic' => $userdelfirmapic,
'bewerbungsdocs' => $bewerbungsdocs,
'bewerbungsmail' => $bewerbungsmail,
'jobdocs' => $jobdocs,
'pinsdelpics' => $pinsdelpics,
'pindeltumb' => $pindeltumb,
'databasecounter' => $databasecounter,
'pinpicsdelcount' => array_sum($pinpicsdelcount),
'userpiccounter' => $userpiccounter,
'userfirmapiccounter' => $userfirmapiccounter,
'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
'jobdocscount' => array_sum($jobdocscount)
            )) . '<br /><br />Möchtest du auch noch deine bei uns Hochgeladenen Dateien?<br /><br />
            Dann kannst du diese <a href="'.$linkToView.'">hier</a> runterladen!');
        //$mailer->send($message);
        } else {
            $subjetsys = "Deine gespeicherten Daten auf grafiker.de!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('account@grafiker.de')
            ->setTo($usermail)
            ->setBody('Hallo ' . $username . ',<br />Wie angefordert, senden Wir dir heute deine bei uns gespeicherten Daten!<br /><br />Die folgenden Daten wurden in unserer Datenbank zu deinen Benutzernamen gefunden:<br /><br />' .
            $this->renderView(
                'admin/datauserendmail.html.twig',
                array(
    'userdata' => $user,
    'alldbdata' => $alldbdata,
    'username' => $username,
    'userdelpic' => $userdelpic,
    'userdelfirmapic' => $userdelfirmapic,
    'bewerbungsdocs' => $bewerbungsdocs,
    'bewerbungsmail' => $bewerbungsmail,
    'jobdocs' => $jobdocs,
    'pinsdelpics' => $pinsdelpics,
    'pindeltumb' => $pindeltumb,
    'databasecounter' => $databasecounter,
    'pinpicsdelcount' => array_sum($pinpicsdelcount),
    'userpiccounter' => $userpiccounter,
    'userfirmapiccounter' => $userfirmapiccounter,
    'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
    'jobdocscount' => array_sum($jobdocscount)
                )) . '<br /><br />Es wurden von Ihnen keine Dokumente oder Bilder auf dem Server von grafiker.de gefunden!');
            //$mailer->send($message);            
        }
/*
        $subjetsys = "Account von User: " . $username . " gelöscht!";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom('account@grafiker.de')
        ->setTo('mail@grafiker.de')
        ->setBody("<strong><h1>Nachweis über Userlöschung:</h1></strong><br /><br />" . $this->renderView(
            'admin/datauserendmail.html.twig',
            array(
'userdata' => $user,
'alldbdata' => $alldbdata,
'username' => $username,
'userdelpic' => $userdelpic,
'userdelfirmapic' => $userdelfirmapic,
'bewerbungsdocs' => $bewerbungsdocs,
'bewerbungsmail' => $bewerbungsmail,
'jobdocs' => $jobdocs,
'pinsdelpics' => $pinsdelpics,
'pindeltumb' => $pindeltumb,
'databasecounter' => $databasecounter,
'pinpicsdelcount' => array_sum($pinpicsdelcount),
'userpiccounter' => $userpiccounter,
'userfirmapiccounter' => $userfirmapiccounter,
'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
'jobdocscount' => array_sum($jobdocscount)
            )));
        $mailer->send($message);

*/
if(file_exists("upload/uploads/data/".$username."-PinPics.zip")) {
unlink("upload/uploads/data/".$username."-PinPics.zip");
}
unlink("upload/uploads/data/".$username."_dbdump.html");
return $this->render('admin/userdatamessage.html.twig',array(
'userdata' => $user,
'alldbdata' => $alldbdata,
'username' => $username,
'userdelpic' => $userdelpic,
'userdelfirmapic' => $userdelfirmapic,
'bewerbungsdocs' => $bewerbungsdocs,
'bewerbungsmail' => $bewerbungsmail,
'jobdocs' => $jobdocs,
'pinsdelpics' => $pinsdelpics,
'pindeltumb' => $pindeltumb,
'databasecounter' => $databasecounter,
'pinpicsdelcount' => array_sum($pinpicsdelcount),
'userpiccounter' => $userpiccounter,
'userfirmapiccounter' => $userfirmapiccounter,
'bewerbungsdocscount' => array_sum($bewerbungsdocscount),
'jobdocscount' => array_sum($jobdocscount),
                                                            )
                    );
    }
}
