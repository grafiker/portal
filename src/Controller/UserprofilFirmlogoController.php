<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserprofilFirmlogoController extends AbstractController
{
    /**
     * @Route("/profile/firmlogo", name="userprofil_firmlogo")
     */
    public function index()
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}
$upload_dir = 'upload/uploads/firma/';
$name = $upload_dir.'/'.$username . '_firma';

        if(file_exists($name.".png")) {
          $logoexist=true;
          $logolink=$name.".png";
        }elseif(file_exists($name.".gif")) {
            $logoexist=true;
            $logolink=$name.".gif";
        } elseif(file_exists($name.".jpg")) {
            $logoexist=true;
            $logolink=$name.".jpg";
        } else {
            $logoexist=false;
            $logolink=false;
        }
        return $this->render('userprofil/firmlogo.html.twig', [
            'controller_name' => 'UserprofilFirmlogoController',
            'logoexist' => $logoexist,
            'logolink' => $logolink,
        ]);
    }
}
