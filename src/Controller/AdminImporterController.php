<?php
namespace App\Controller;
use App\Document\Projektrechnerkategorien;
use App\Document\Pinskategorien;
use App\Document\Gutscheinekategorien;
use App\Document\Skills;
use App\Document\Pins;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminImporterController extends Controller
{
    /**
     * @Route("/admin/importer", name="admin_importer")
     */
    public function index()
    {
       /* $pinskategorien = array(
            array('name' => 'Showcases', 'role_group' => array('Free'), 'undergroups' => array(''), 'sort' => 1, 'aktiv' => 1)
            array('name' => 'Beiträge', 'role_group' => array('Standart'), 'undergroups' => array(''), 'sort' => 2, 'aktiv' => 1),
            array('name' => 'Forum Thema', 'role_group' => array('Admin'), 'undergroups' => array(''), 'sort' => 15, 'aktiv' => 1),
            array('name' => 'Gruppen', 'role_group' => array('Admin'), 'undergroups' => array(''), 'sort' => 16, 'aktiv' => 1),
            array('name' => 'Designvorlagen', 'role_group' => array('Free'), 'undergroups' => array('Logos', 'Geschäftsausstattung', 'Werbedrucke', 'Werbeartikel', 'Webseiten'), 'sort' => 5, 'aktiv' => 1),
            array('name' => 'Büroräume suche', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 7, 'aktiv' => 1),
            array('name' => 'Büroräume biete', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 8, 'aktiv' => 1),
            array('name' => 'Termin', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 9, 'aktiv' => 1),
            array('name' => 'Seminar', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 10, 'aktiv' => 1),
            array('name' => 'Gutscheine', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 6, 'aktiv' => 1),
            array('name' => 'Umfragen', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 11, 'aktiv' => 1),
            array('name' => 'Branchen News', 'picdir' => 'gutscheine', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 12, 'aktiv' => 1),
            array('name' => 'Technik News', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 3, 'aktiv' => 1),
            array('name' => 'Produkt News', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 4, 'aktiv' => 1),
            array('name' => 'Grafiker.de News', 'role_group' => array('Admin'), 'undergroups' => array(''), 'sort' => 13, 'aktiv' => 1),
            array('name' => 'Downloads', 'role_group' => array('Admin'), 'undergroups' => array(''), 'sort' => 14, 'aktiv' => 1),
            array('name' => 'Sonstiges', 'role_group' => array('Platin', 'Einzelkauf'), 'undergroups' => array(''), 'sort' => 17, 'aktiv' => 1)
        );

        foreach($pinskategorien AS $pin) {
            $em = $this->get('doctrine_mongodb')->getManager();
            $pinsdel = $em->getRepository('App:Pinskategorien')->findOneBy(["pinkatname"=>$pin['name']]);
            if($pinsdel) {
            $em->remove($pinsdel);
            }
            $em->flush();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pins = $dm->getRepository('App:Pinskategorien')->findOneByPinkatname($pin['name']);;
            if (!$pins) {
    
                    $task = new Pinskategorien();
                    $task->setPinkatname($pin['name']);
                    $task->setRoleGroup($pin['role_group']);
                    $task->setUndergroup($pin['undergroups']);
                    $task->setSort($pin['sort']);
                    $task->setAktiv($pin['aktiv']);
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $dm->persist($task);
                    $dm->flush();
            }
    }
*/
    $articlekategorien = array(
        array('name' => 'Gutscheine', 'role_group' => array('Admin'), 'undergroups' => array(''), 'sort' => 1, 'aktiv' => 1));

        foreach($articlekategorien AS $pin) {
            $em = $this->get('doctrine_mongodb')->getManager();
            $pinsdel = $em->getRepository('App:Gutscheinekategorien')->findOneBy(["gutscheinekatname"=>$pin['name']]);
            if($pinsdel) {
            $em->remove($pinsdel);
            }
            $em->flush();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pins = $dm->getRepository('App:Gutscheinekategorien')->findOneByGutscheinekatname($pin['name']);;
            if (!$pins) {
    
                    $task = new Gutscheinekategorien();
                    $task->setGutscheinekatname($pin['name']);
                    $task->setRoleGroup($pin['role_group']);
                    $task->setUndergroup($pin['undergroups']);
                    $task->setSort($pin['sort']);
                    $task->setAktiv($pin['aktiv']);
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $dm->persist($task);
                    $dm->flush();
            }
    }



        /*$projektrechnerkategorien  = array(
            array('typ_id' => '121','name' => 'Auftragsbestätigung','dauer' => '4.00'),
            array('typ_id' => '120','name' => 'Anhänger','dauer' => '4.50'),
            array('typ_id' => '118','name' => 'Aktie','dauer' => '28.00'),
            array('typ_id' => '4','name' => 'Formular','dauer' => '6.00'),
            array('typ_id' => '6','name' => 'Abzeichen','dauer' => '4.00'),
            array('typ_id' => '7','name' => 'Anfahrtsskizze / Anfahrtskarte','dauer' => '3.00'),
            array('typ_id' => '8','name' => 'Karte','dauer' => '3.00'),
            array('typ_id' => '9','name' => 'Anzeige','dauer' => '4.00'),
            array('typ_id' => '10','name' => 'Aufkleber','dauer' => '3.00'),
            array('typ_id' => '11','name' => 'Fahrzeugbeschriftung (PKW)','dauer' => '8.00'),
            array('typ_id' => '12','name' => 'Fahrzeugbeschriftung (Transporter)','dauer' => '12.00'),
            array('typ_id' => '13','name' => 'Fahrzeugbeschriftung (LKW)','dauer' => '14.00'),
            array('typ_id' => '14','name' => 'Banderole','dauer' => '4.00'),
            array('typ_id' => '15','name' => 'Bedienungsanleitung (je Seite)','dauer' => '3.00'),
            array('typ_id' => '16','name' => 'Anleitung (je Seite)','dauer' => '3.00'),
            array('typ_id' => '17','name' => 'Prospekt (je Innenseite)','dauer' => '5.00'),
            array('typ_id' => '18','name' => 'Prospekt (Konzeption)','dauer' => '8.00'),
            array('typ_id' => '19','name' => 'Prospekt (Titelseite)','dauer' => '5.00'),
            array('typ_id' => '20','name' => 'Bedienungsanleitung (Titelseite)','dauer' => '4.00'),
            array('typ_id' => '21','name' => 'Beipackzettel','dauer' => '8.00'),
            array('typ_id' => '22','name' => 'Eintrittskarte','dauer' => '7.00'),
            array('typ_id' => '23','name' => 'Bildbearbeitung (je Bild)','dauer' => '3.50'),
            array('typ_id' => '24','name' => 'Bildmarke','dauer' => '10.00'),
            array('typ_id' => '25','name' => 'Blisterverpackung','dauer' => '12.00'),
            array('typ_id' => '26','name' => 'Briefbogen','dauer' => '5.00'),
            array('typ_id' => '27','name' => 'Briefpapier','dauer' => '5.00'),
            array('typ_id' => '28','name' => 'Briefmarke','dauer' => '4.00'),
            array('typ_id' => '29','name' => 'Broschüre (je Innenseite)','dauer' => '4.00'),
            array('typ_id' => '30','name' => 'Broschüre (Titelseite)','dauer' => '5.00'),
            array('typ_id' => '31','name' => 'Buchgestaltung (Cover)','dauer' => '7.00'),
            array('typ_id' => '32','name' => 'Buchgestaltung (Seitenlayout)','dauer' => '4.00'),
            array('typ_id' => '33','name' => 'CD-/DVD-Cover','dauer' => '8.00'),
            array('typ_id' => '34','name' => 'Corporate Design','dauer' => '16.00'),
            array('typ_id' => '35','name' => 'Display','dauer' => '5.00'),
            array('typ_id' => '36','name' => 'Dose','dauer' => '8.00'),
            array('typ_id' => '37','name' => 'Einladung (einseitig)','dauer' => '3.00'),
            array('typ_id' => '38','name' => 'Einladung (zweiseitig)','dauer' => '5.00'),
            array('typ_id' => '39','name' => 'Fahne','dauer' => '4.50'),
            array('typ_id' => '40','name' => 'Fassadenbeschriftung','dauer' => '6.00'),
            array('typ_id' => '41','name' => 'Fax Formular','dauer' => '4.00'),
            array('typ_id' => '42','name' => 'Firmenschild','dauer' => '3.00'),
            array('typ_id' => '43','name' => 'Flyer (einseitig)','dauer' => '3.00'),
            array('typ_id' => '44','name' => 'Flyer (zweiseitig)','dauer' => '6.00'),
            array('typ_id' => '45','name' => 'Folder','dauer' => '5.00'),
            array('typ_id' => '46','name' => 'Gebrauchsanweisung (je Seite)','dauer' => '3.00'),
            array('typ_id' => '47','name' => 'Gebrauchsanweisung (Titelseite)','dauer' => '4.00'),
            array('typ_id' => '48','name' => 'Geschenkpapier','dauer' => '5.00'),
            array('typ_id' => '49','name' => 'Geschäftsbericht (je Innenseite)','dauer' => '4.00'),
            array('typ_id' => '50','name' => 'Geschäftsbericht (Titelseite)','dauer' => '5.00'),
            array('typ_id' => '51','name' => 'Banner (statisch)','dauer' => '1.00'),
            array('typ_id' => '52','name' => 'Banner (animiert)','dauer' => '2.00'),
            array('typ_id' => '53','name' => 'Banner (Flash)','dauer' => '4.00'),
            array('typ_id' => '54','name' => 'Getränkekarte','dauer' => '5.00'),
            array('typ_id' => '55','name' => 'Speisekarte (je Seite)','dauer' => '3.00'),
            array('typ_id' => '56','name' => 'Grußkarte','dauer' => '3.00'),
            array('typ_id' => '57','name' => 'Gutschein','dauer' => '3.00'),
            array('typ_id' => '58','name' => 'Icon','dauer' => '2.00'),
            array('typ_id' => '59','name' => 'Icon 3D','dauer' => '4.00'),
            array('typ_id' => '62','name' => 'Internetpräsenz (Layout ca. 10 Seiten)','dauer' => '18.00'),
            array('typ_id' => '61','name' => 'Internetpräsenz (je Seite)','dauer' => '2.00'),
            array('typ_id' => '63','name' => 'Kalender','dauer' => '5.00'),
            array('typ_id' => '64','name' => 'Karikatur','dauer' => '3.00'),
            array('typ_id' => '65','name' => 'Kuvert','dauer' => '3.00'),
            array('typ_id' => '66','name' => 'Lageplan','dauer' => '5.00'),
            array('typ_id' => '67','name' => 'Landkarte','dauer' => '3.00'),
            array('typ_id' => '68','name' => 'Lehrtafel','dauer' => '12.00'),
            array('typ_id' => '69','name' => 'Leuchtwerbung','dauer' => '13.00'),
            array('typ_id' => '70','name' => 'Leuchtwerbung','dauer' => '13.00'),
            array('typ_id' => '71','name' => 'Logo','dauer' => '12.00'),
            array('typ_id' => '72','name' => 'Mailing','dauer' => '6.00'),
            array('typ_id' => '73','name' => 'Mappe','dauer' => '9.00'),
            array('typ_id' => '74','name' => 'Maskottchen','dauer' => '8.00'),
            array('typ_id' => '75','name' => 'Medaille','dauer' => '5.00'),
            array('typ_id' => '76','name' => 'Münze','dauer' => '5.00'),
            array('typ_id' => '77','name' => 'Packzettel','dauer' => '8.00'),
            array('typ_id' => '78','name' => 'Piktogramm','dauer' => '5.00'),
            array('typ_id' => '79','name' => 'Plakat','dauer' => '8.00'),
            array('typ_id' => '80','name' => 'Plakette','dauer' => '3.00'),
            array('typ_id' => '81','name' => 'Poster','dauer' => '8.00'),
            array('typ_id' => '82','name' => 'Postkarte','dauer' => '3.00'),
            array('typ_id' => '83','name' => 'Rechnung','dauer' => '5.00'),
            array('typ_id' => '84','name' => 'Retusche','dauer' => '3.00'),
            array('typ_id' => '85','name' => 'Schaufensteraufkleber','dauer' => '8.00'),
            array('typ_id' => '86','name' => 'Schaufenstergestaltung','dauer' => '15.00'),
            array('typ_id' => '87','name' => 'Schautafel','dauer' => '6.00'),
            array('typ_id' => '88','name' => 'Screen-Design (je Seite)','dauer' => '3.00'),
            array('typ_id' => '89','name' => 'Siegel','dauer' => '4.00'),
            array('typ_id' => '90','name' => 'Stempel','dauer' => '3.00'),
            array('typ_id' => '91','name' => 'Symbol','dauer' => '2.00'),
            array('typ_id' => '92','name' => 'Taschenkalender','dauer' => '4.00'),
            array('typ_id' => '93','name' => 'Tischkalender','dauer' => '6.00'),
            array('typ_id' => '94','name' => 'Tragetasche','dauer' => '5.00'),
            array('typ_id' => '95','name' => 'Tube','dauer' => '4.00'),
            array('typ_id' => '96','name' => 'Tüte','dauer' => '4.00'),
            array('typ_id' => '119','name' => 'Angebotsmappe','dauer' => '7.00'),
            array('typ_id' => '98','name' => 'Umschlag','dauer' => '2.50'),
            array('typ_id' => '99','name' => 'Urkunde','dauer' => '4.00'),
            array('typ_id' => '100','name' => 'Verkaufsverpackung','dauer' => '16.00'),
            array('typ_id' => '101','name' => 'Versandkarton','dauer' => '5.00'),
            array('typ_id' => '102','name' => 'Visitenkarte (einseitig)','dauer' => '4.00'),
            array('typ_id' => '103','name' => 'Visitenkarte (zweiseitig)','dauer' => '6.00'),
            array('typ_id' => '104','name' => 'Wandkalender','dauer' => '6.00'),
            array('typ_id' => '105','name' => 'Wappen','dauer' => '5.00'),
            array('typ_id' => '106','name' => 'Internetpräsenz (Fotogalerie)','dauer' => '6.00'),
            array('typ_id' => '107','name' => 'Internetpräsenz (Kontaktformular)','dauer' => '5.00'),
            array('typ_id' => '108','name' => 'Wegbeschreibung','dauer' => '5.00'),
            array('typ_id' => '109','name' => 'Werbefigur','dauer' => '8.00'),
            array('typ_id' => '110','name' => 'Wortzeichen','dauer' => '7.00'),
            array('typ_id' => '111','name' => 'Zeitung (Konzept)','dauer' => '20.00'),
            array('typ_id' => '116','name' => 'Streichholzschachtel','dauer' => '3.00'),
            array('typ_id' => '113','name' => 'Zeitung (je Seite)','dauer' => '4.00'),
            array('typ_id' => '114','name' => 'Zeitung (Cover)','dauer' => '6.00'),
            array('typ_id' => '115','name' => 'Zeitung (ca. 10 Seiten)','dauer' => '50.00'),
            array('typ_id' => '117','name' => 'Zündholzschachtel','dauer' => '3.00'),
            array('typ_id' => '122','name' => 'Ausstellungsdesign','dauer' => '33.00'),
            array('typ_id' => '123','name' => 'Ausstellungsdesign (Konzept)','dauer' => '25.00'),
            array('typ_id' => '124','name' => 'Ausweis','dauer' => '3.00'),
            array('typ_id' => '125','name' => 'Bankformular','dauer' => '5.00'),
            array('typ_id' => '126','name' => 'Bauzaungestaltung (pro Zaun)','dauer' => '5.00'),
            array('typ_id' => '127','name' => 'Beihefter','dauer' => '10.00'),
            array('typ_id' => '128','name' => 'Beilage','dauer' => '8.00'),
            array('typ_id' => '129','name' => 'Cartoon','dauer' => '5.50'),
            array('typ_id' => '151','name' => 'Scheckkarte','dauer' => '10.00'),
            array('typ_id' => '131','name' => 'CD-/DVD-Booklet (10-12 Seiten)','dauer' => '23.00'),
            array('typ_id' => '132','name' => 'Einwickelpapier','dauer' => '7.00'),
            array('typ_id' => '133','name' => 'Etikett','dauer' => '8.50'),
            array('typ_id' => '134','name' => 'Explosionszeichnung','dauer' => '15.00'),
            array('typ_id' => '135','name' => 'Firmenschriftzug','dauer' => '15.00'),
            array('typ_id' => '136','name' => 'Flächengestaltung','dauer' => '19.00'),
            array('typ_id' => '137','name' => 'Formblatt','dauer' => '5.00'),
            array('typ_id' => '138','name' => 'Fotolayout','dauer' => '8.00'),
            array('typ_id' => '139','name' => 'Geschäftskarte','dauer' => '3.00'),
            array('typ_id' => '140','name' => 'Handzettel','dauer' => '3.00'),
            array('typ_id' => '141','name' => 'Homepage (Layout ca. 10 Seiten)','dauer' => '18.00'),
            array('typ_id' => '142','name' => 'Label','dauer' => '6.00'),
            array('typ_id' => '143','name' => 'Lotterielos','dauer' => '10.00'),
            array('typ_id' => '144','name' => 'Modeillustration','dauer' => '3.50'),
            array('typ_id' => '145','name' => 'Notentitel','dauer' => '7.00'),
            array('typ_id' => '146','name' => 'Overheadfolie','dauer' => '3.50'),
            array('typ_id' => '147','name' => 'Pfandbrief','dauer' => '25.00'),
            array('typ_id' => '148','name' => 'Presseillustration','dauer' => '6.00'),
            array('typ_id' => '149','name' => 'Re-Design (Geschäftspapier)','dauer' => '12.00'),
            array('typ_id' => '150','name' => 'Re-Design (Logo)','dauer' => '8.00'),
            array('typ_id' => '152','name' => 'Stundenplan','dauer' => '5.00'),
            array('typ_id' => '153','name' => 'Verkehrsmittelwerbung','dauer' => '11.00'),
            array('typ_id' => '154','name' => 'Verkehrszeichen','dauer' => '8.00'),
            array('typ_id' => '155','name' => 'Versandhülle','dauer' => '3.00'),
            array('typ_id' => '156','name' => 'Video-Clip (5min)','dauer' => '40.00'),
            array('typ_id' => '157','name' => 'Video-Clip (10min)','dauer' => '65.00'),
            array('typ_id' => '158','name' => 'Warenzeichen','dauer' => '28.00'),
            array('typ_id' => '159','name' => 'Zündholzbriefchen','dauer' => '3.00')
          );
foreach($projektrechnerkategorien AS $name) {
    $em = $this->get('doctrine_mongodb')->getManager();
    $kategoriedel = $em->getRepository('App:Skills')->findOneBy(["katname"=>$name['name']]);
    if($kategoriedel) {
    $em->remove($kategoriedel);
    }
    $em->flush();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorie = $dm->getRepository('App:Projektrechnerkategorien')->findOneByKatname($name['name']);;
        if (!$kategorie) {

                $task = new Projektrechnerkategorien();
                $task->setKatname($name['name']);
                $task->setDauer($name['dauer']);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($task);
                $dm->flush();
        }
}

$skills = array(
    array('beruf' => 'Druckerei', 'skillname' => array('Standarddrucksachen', 'Werbemittel', 'Web2Print Shops', 'Druckvorstufe', 'Druckweiterverarbeitung','Mailings','Produktionsbüro','Sonstiges')),
    array('beruf' => 'Grafiker', 'skillname' => array('Geschäftsdrucksachen','Werbedrucksachen','Webdesign','Design','3D Max', 'Adobe Acrobat', 'Adobe After-Effects', 'Adobe Dreamweaver', 'Adobe Flash', 'Adobe Illustrator', 'Adobe InDesign', 'Adobe Photoshop', 'Autodesk Softimage', 'Avid Media Composer', 'Blender', 'Cinema 4D', 'CinePaint', 'Corel Paint Shop Pro', 'Coreldraw', 'Digital Fusion', 'Final Cut', 'GIMP', 'Houdini', 'Inkscape', 'Lightwave', 'Maya', 'Motion', 'Paintshop', 'Premiere', 'QuarkXpress', 'RealFlow', 'Sonstiges')),
    array('beruf' => 'Fotograf', 'skillname' => array('Analogerfahrung', 'Digitalerfahrung', 'eigenes Studio', 'Foodfotografie', 'Outdoorerfahrung', 'Peoplefotografie', 'portables Studio', 'Produktfotografie', 'Reportagefotografie', 'Studioerfahrung', 'Technik&Industrie', 'Werbefotografie', 'Sonstiges')),
    array('beruf' => 'Programmierer', 'skillname' => array('Full-Stack','Frontend','Backend','Desktop','Server','Mobile App', 'ActionScript', 'Adobe Dreamweaver', 'Adobe Flash', 'Android', 'Angular JS', 'ASP, ASP.net', 'Bootstrap', 'c, c++ & c#', 'CSS', 'Delphi', 'Docker', 'HTML & XHTML & DHTML', 'iOS', 'Java', 'JavaScript', 'jQuery', 'Magento', 'noSQL', 'Perl', 'PHP', 'Python', 'Ruby', 'Shopware', 'SQL', 'Symfony', 'Visual Basic .Net', 'Wordpress', 'Zend', 'Sonstiges')),
    array('beruf' => 'Texter', 'skillname' => array('PIN, PR Beiträge','Dichtkunst', 'Lektorat', 'Präsentationen', 'Reime', 'Vorträge', 'Werbetexte', 'Übersetzungen', 'Sonstiges')),
    array('beruf' => 'Illustrator', 'skillname' => array('Acryl', 'Airbrush', 'Aquarell', 'Bleistift', 'Copics', 'Digitale Illustrattionen', 'Öl', 'Sonstiges')),
    array('beruf' => 'Sonstiges', 'skillname' => array('Grafiker.de Partner', 'Software', 'Verbände', 'Fachmagazine', 'Sonstiges'))
);

foreach($skills AS $skill) {
    $em = $this->get('doctrine_mongodb')->getManager();
    $skilldel = $em->getRepository('App:Skills')->findOneBy(["beruf"=>$skill['beruf']]);
    if($skilldel) {
    $em->remove($skilldel);
    }
    $em->flush();
    $dm = $this->get('doctrine_mongodb')->getManager();
    $skillsdb = $dm->getRepository('App:Skills')->findOneByBeruf($skill['beruf']);
    if (!$skillsdb) {

            $task = new Skills();
            $task->setBeruf($skill['beruf']);
            $task->setSkillname($skill['skillname']);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
    }
}
/*
$dm = $this->get('doctrine_mongodb')->getManager();
$userdatagesamt = $dm->createQueryBuilder('App:Message')->remove()->getQuery()->execute();
$userdatagesamt = $dm->createQueryBuilder('App:Thread')->remove()->getQuery()->execute();

$dm = $this->get('doctrine_mongodb')->getManager();
$dm->createQueryBuilder('App:Pins')->update()->multiple(true)->field('username')->set('snygo')->field('username')->equals('hbroeskamp')->getQuery()->execute();
*/
$dm->flush();
        return $this->render('admin/import.html.twig', [
            'controller_name' => 'fertig',
        ]);
    }
    /**
     * @Route("/admin/importer/delete/{what}", name="delete_admin_importer")
     */
    public function deleteAction($what)
    {
    $dm = $this->get('doctrine_mongodb')->getManager();
    $userdatagesamt = $dm->createQueryBuilder('App:'.$what)->remove()->getQuery()->execute();
    $dm->flush();
        return $this->render('admin/import.html.twig', [
            'controller_name' => 'fertig',
        ]);
    }
    /**
     * @Route("/admin/importer/killspam/all", name="delete_user_importer")
     */
    public function deleteUserAction()
    {
    $delmember = $this->get('doctrine_mongodb')->getManager();
    $killuser = "@mail.ru";
    $searchQuery = array(
        '$or' => array(
            array(
                'email' => new \MongoRegex('/'.$killuser.'/si'),
                ),
            )
        );
    $finallby = $delmember->getRepository('App:User')->findBy($searchQuery);
    foreach($finallby as $finallby) //es wird eine weitere Schleife gestartet um alle gefundenen Einträge einzeln zu behandeln
    {
    $delmember->remove($finallby); // Lösche alle gefundenen MongoDB einträge um den User komplett zu entfernen    
    $delmember->flush(); 
    }
        return $this->render('admin/import.html.twig', [
            'controller_name' => 'fertig',
        ]);
    }
}
