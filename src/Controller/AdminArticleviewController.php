<?php

namespace App\Controller;
use App\Document\Article;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminArticleviewController extends Controller
{
    /**
     * @Route("/admin/articleview/{articleid}", name="admin_articleview")
     */
    public function index($articleid)
    {
        $userdata = new CheckUserdataController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $articledata = $dm->getRepository('App:Article')->findBy(array('id' => $articleid));
        $picsarray = array();
        foreach($articledata as $key => $articlepics) {
            $picsarray = $articlepics->getPicname();
            $content = HashToLinkController::HashToLink($articlepics->getContent());
        }
        foreach($articledata as $key => $articleuser) {
            $fileexistarray[$articleuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
            $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
        }
        return $this->render('admin/articleview.html.twig', [
            'controller_name' => 'AdminArticleviewController',
            'articledata' => $articledata,
            'content' => $content,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'picsarray' => $picsarray,
        ]);
    }
}
