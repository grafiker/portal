<?php

namespace App\Controller;

class HashToLinkController
{
    function HashToLink($texttolink) {
        $tweet = $texttolink;
        $regex = "/#+([a-zA-Z0-9_]+)/";
        $text = preg_replace($regex, '<a href="/filter/Search/$1">#$1</a>', $tweet);
        
        return $text;
    }
}
