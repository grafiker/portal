<?php

namespace App\Controller;

use App\Document\Pins;

use App\AppBundle\Form\PinmeldungFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class PinmeldungController extends Controller
{
    /**
     * @Route("/pinmeldung/{pinid}", name="pinmeldung")
     */
    public function index(RequestStack $requestStack, Request $request, $pinid, \Swift_Mailer $mailer)
    {
        $builder = $this->createForm(PinmeldungFormType::class);
        $builder->handleRequest($request);

            if ($builder->isSubmitted() && $builder->isValid()) {
                $task = $builder->getData();
                //var_dump($task);
                $emdel = $this->get('doctrine_mongodb')->getManager();
                $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["id"=>$pinid]);
                //var_dump($pincheck->getPinlink());
                $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/pinview/'.$pincheck->getPinlink();
                $subjetsys = "Pinmeldung mit Grund: " . $task["grund"];
                $body = "Betreff der Meldung: " . $task["subject"] . "<br /><br />" . "Nachricht:<br />" . $task["content"] . "<br /><br />Mailadresse vom Melder: ".$task["emailadresse"]."<br />Link zum Pin: " . $linkToView;
                //var_dump($body);
                    $message = (new \Swift_Message())
                    ->setSubject($subjetsys)
                    ->setFrom('pinmeldung@grafiker.de')
                    ->setTo('mail@grafiker.de')
                    ->setBody($body);
                    $mailer->send($message);
                    $maillog = new MaillogController();
                    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "pinmeldung@grafiker.de", "mail@grafiker.de", $subjetsys, $body);
                return $this->render('pinmeldung/send.html.twig', [
                    'controller_name' => 'PinmeldungController',
                    'pinid' => $pinid,
                ]);
            }
        

        return $this->render('pinmeldung/index.html.twig', [
            'form' => $builder->createView(),
            'controller_name' => 'PinmeldungController',
            'pinid' => $pinid,
        ]);
    }

}
