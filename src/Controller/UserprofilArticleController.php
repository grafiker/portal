<?php

namespace App\Controller;
use App\AppBundle\Form\ArticleFormType;
use App\AppBundle\Form\ArticleCropFormType;
use App\AppBundle\Form\ArticleEditFormType;
use App\Controller\CheckUserdataController;
use App\Document\Article;
use App\Controller\CropController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class UserprofilArticleController extends Controller
{
    /**
     * @Route("/profile/article/start", name="userprofil_article")
     */
    public function indexCreate(Request $request, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        
        $articledatesoldcheck = $emdel->getRepository('App:Article')->findOneBy(['username'=>$username, 'astitle'=>'Articlestart', 'aktiv'=>true]);
        if($articledatesoldcheck) {
            //var_dump($articledates);
            $articledatesoldcheck->setAstitle("Articleinsert");
            $emdel->flush();
            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/articleview/'.$articledatesoldcheck->getArticlelink();
            $subjetsys = "Es gibt einen Beitrag zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('beitrag@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "beitrag@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
        }
        $articledatesoldcheck = $emdel->getRepository('App:Article')->findOneBy(['username'=>$username, 'astitle'=>'Ruhezustand', 'aktiv'=>true]);
        if($articledatesoldcheck) {
            //var_dump($articledates);
            $articledatesoldcheck->setAstitle("Articlestart");
            $emdel->flush();
        }
        $articlecheck = $emdel->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
    if($articlecheck) {
        if($articlecheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($articlecheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/articlepics/".$delpics)) {
                    //unlink("./upload/uploads/articlepics/".$delpics);
                }
            }
        }
    }
    if($articlecheck) {
        if(!$articlecheck->getPicname()) {

        //$emdel->remove($articlecheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
        $casepics = array();
        if (!$articlecheck) {
            $articleergebnis=0;
            //$casepics={};
            $articleid = 0;
        } else {
            $articleergebnis=1;
            $casepics=$articlecheck->getPicname();
            $articleid = $articlecheck->getId();
        }
        $reset = false;
        $articleergebnis=0;
        $builder = $this->createForm(ArticleFormType::class);
        return $this->render('userprofil/article.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'articleergebnis' => $articleergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'articleID' => $articleid,
        ));
    }

/**
     * @Route("/profile/article/{articleid}/editor", name="article_erditor")
     */
    public function createEdit(RequestStack $requestStack, Request $request, $articleid, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $articledatesoldcheck = $emdel->getRepository('App:Article')->findOneBy(['username'=>$username, 'astitle'=>'Articlestart', 'aktiv'=>NULL]);
        if($articledatesoldcheck) {
            //var_dump($articledates);
            $articledatesoldcheck->setAstitle("Ruhezustand");
            $emdel->flush();
        }

        $articledatesoldcheck = $emdel->getRepository('App:Article')->findOneBy(['username'=>$username, 'astitle'=>'Articlestart', 'aktiv'=>true]);
        if($articledatesoldcheck) {
            //var_dump($articledates);
            if($articledatesoldcheck->getID() != $articleid)
            $articledatesoldcheck->setAstitle("Articleinsert");
            $emdel->flush();

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/articleview/'.$articledatesoldcheck->getArticlelink();
            $subjetsys = "Es gibt einen Beitrag zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('articles@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "articles@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
        }
        
        $articledates = $emdel->getRepository('App:Article')->findOneBy(["username"=>$username, "id"=>$articleid]);
        
        if($articledates) {
            //var_dump($articledates);
            $articledates->setAstitle("Articlestart");
            $emdel->flush();
        }
        $articlecheck = $emdel->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
    if($articlecheck) {
        if($articlecheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($articlecheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/articlepics/".$delpics)) {
                    //unlink("./upload/uploads/articlepics/".$delpics);
                }
            }
        }
    }
    if($articlecheck) {
        if(!$articlecheck->getPicname()) {

        //$emdel->remove($articlecheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
        $casepics = array();
        if (!$articlecheck) {
            $articleergebnis=0;
            //$casepics={};
            $articleid = 0;
        } else {
            $articleergebnis=1;
            $casepics=$articlecheck->getPicname();
            $articleid = $articlecheck->getId();
        }
        $reset = false;
        $articleergebnis=0;
        $builder = $this->createForm(ArticleFormType::class);
        return $this->render('userprofil/articlemedia.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'articleergebnis' => $articleergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'articleID' => $articleid,
        ));
    }
    /**
     * @Route("/profile/article/poststart", name="article_poststart")
     */
    public function createPoststart(Request $request)
    {
        $reset = false;
        $task = new Article();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Articlekategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } else {}
        }
        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
        $casepics = array();
        $articleergebnis=1;
        if($articlecheck) {
        $casepics=$articlecheck->getPicname();
        } else {

            $task->setUsername($username);
            $data[] = NULL;
            //$task->setPicname($data);
            $task->setAstitle("Articlestart");
            $task->setArticlekategorie("Beiträge");
            $task->setPicname($casepics);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
            $casepics=$articlecheck->getPicname();
        }
        $newextarray=array(array("articlekategorie" => $articlecheck->getArticlekategorie(),"isads" => $articlecheck->getIsads(),"bewerten" => $articlecheck->getBewerten(),"title" => $articlecheck->getTitle(),"content" => $articlecheck->getContent(),"rechte" => $articlecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(ArticleEditFormType::class, $newextarray);
            return $this->render('userprofil/articlestart.html.twig', array(
                'form' => $builder->createView(),
                'controller_name' => 'ShowcaseController',
                'articleergebnis' => $articleergebnis,
                'picsarray' => $casepics,
                'reset' => $reset,
                'articleid' => $articlecheck->getId(),
            ));
        }
    }


    /**
     * @Route("/profile/article/{getdata}", name="article")
     */
    public function createAction(Request $request, $getdata, \Swift_Mailer $mailer)
    {
        $crop = new CropController();
        $task = new Article();
        $reset = false;
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            if($getdata == "reset") {
                $emdel = $this->get('doctrine_mongodb')->getManager();
                $articlecheck = $emdel->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
//var_dump($articlecheck);
                if($articlecheck->getPicname()) {
                foreach($articlecheck->getPicname() as $key => $delpics) {
                    if(file_exists("./upload/uploads/articlepics/".$delpics)) {
                        unlink("./upload/uploads/articlepics/".$delpics);
                    }
                }
            }
                $emdel->remove($articlecheck);
                $emdel->flush();
                $reset = true;
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Articlekategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getArticlekatname()] = $value->getArticlekatname();
                } else {}
        }
    }
        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["username"=>$username, "astitle"=>"Articlestart"]);
        $casepics = array();
        if (!$articlecheck) {
            $articleergebnis=0;
            $casepics[0]="keins";
            $builder = $this->createForm(ArticleFormType::class, $extarray);
        } else {
            $articleergebnis=1;
            $casepics=$articlecheck->getPicname();
            $newextarray=array(array("articlekategorie" => $articlecheck->getArticlekategorie(),"isads" => $articlecheck->getIsads(),"bewerten" => $articlecheck->getBewerten(),"title" => $articlecheck->getTitle(),"content" => $articlecheck->getContent(),"rechte" => $articlecheck->getRechte(), "data" => array($extarray)));
            $builder = $this->createForm(ArticleEditFormType::class, $newextarray);
        }

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
        //var_dump($temp);
        if(isset($temp["form"]["articlepay"])) {
            $articlepay = true;
        } else {
            $articlepay = false;
        }
        if(isset($temp["form"]["update"])) {
            $update = true;
        } else {
            $update = false;
        }
        
        if((isset($data["titel"])) or (isset($data["w"]))) {
            if((!isset($data["w"]) or ($data["w"] == 0))) {
                //var_dump("thumbnail_".$data["picselected"]);
            $date = \DateTime::createFromFormat('U', time()+3600);
            $date->setTimezone(new \DateTimeZone('UTC'));
            $articlecheck->setArticleKategorie($data["articlekategorie"]);
            $articlecheck->setTitle($data["titel"]);
            $title = str_replace("!", "", $data["titel"]);
            $title = str_replace("?", "", $title);
            $title = str_replace("%", "", $title);
            $title = str_replace("&", "", $title);
            $title = str_replace(".....", "", $title);
            $title = str_replace("....", "", $title);
            $title = str_replace("...", "", $title);
            $title = str_replace(" - ", " ", $title);
            $title = str_replace("„", "", $title);
            $title = str_replace("”", "", $title);
            if(strlen($title) >= 100) {
            $articlelinkindb=preg_replace("/[^ ]*$/", '', substr($title, 0, 100));
            $articlelinkindb=substr($articlelinkindb, 0, -1);
            } else {
                $articlelinkindb = $title;
            }
            $articlelinkindb = str_replace(" ", "_", $articlelinkindb);
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('title' => $data["titel"]),array('id' => 'ASC'));
            $anzindb=count($articledatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $articlelinkindb = $articlelinkindb.'_'.$anzindb;
            } else {
                $articlelinkindb = $articlelinkindb;
            }
            //var_dump($data["picselected"]);
            if($data["picselected"] != "0") {
                $data["picselected"] = $data["picselected"];
                //var_dump($articlecheck->getPicname()[0]);
            } else {
                $data["picselected"] = $articlecheck->getPicname()[0];
                //var_dump($articlecheck->getPicname()[0]);
            }
            //var_dump($data);
            $articlecheck->setArticlelink($articlelinkindb);
            $articlecheck->setContent($data["content"]);
            $articlecheck->setIsads($data["isads"]); 
            $articlecheck->setBewerten($data["bewerten"]);
            $articlecheck->setRechte($data["rechte"]);
            $articlecheck->setAktiv(false);
            $articlecheck->setStartdate($date); 
            //$articlecheck->setThumbnail("thumbnail_".$data["picselected"]);      
            $em->flush();    
        }   
            

            $upload_dir = "upload/uploads/articlepics"; 				// The directory for the images to be saved in
            $upload_path = $upload_dir."/";				// The path to where the image will be saved
            $large_image_prefix = "resize_"; 			// The prefix name to large image
            $thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
            $large_image_name = $data["picselected"];     // New name of the large image (append the timestamp to the filename)
            $thumb_image_name = "".$thumb_image_prefix.$data["picselected"];
            $large_image_location = $upload_path.$large_image_name;
            $thumb_image_location = $upload_path.$thumb_image_name;
            $thumb_width = 181;
            $thumb_height = 168;
            $max_width = 600;
            $width = $large_image_location;
			$height = $large_image_location;
			//Scale the image if it is greater than the width set above
			if ($width > $max_width){
				$scale = $max_width/$width;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}else{
				$scale = 1;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}		
            //$current_large_image_width = $crop->getWidth($large_image_location);
            //$current_large_image_height = $crop->getHeight($large_image_location);

            if (file_exists($large_image_location)){
                if(file_exists($thumb_image_location)){
                    $thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name."\" alt=\"Thumbnail Image\"/>";
                }else{
                    $thumb_photo_exists = "";
                }
                   $large_photo_exists = "<img src=\"".$upload_path.$large_image_name."\" alt=\"Large Image\"/>";
            } else {
                   $large_photo_exists = "";
                $thumb_photo_exists = "";
            }
            if ((isset($data["w"]) AND ($data["w"] != 0)) && strlen($large_photo_exists)>0) {
                //Get the new coordinates to crop the image.
                $x1 = $data["x1"];
                $y1 = $data["y1"];
                $x2 = $data["x2"];
                $y2 = $data["y2"];
                $w = $data["w"];
                $h = $data["h"];
                //Scale the image to the thumb_width set above
                $scale = $thumb_width/$w;
                //$cropped = $crop->resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
                $articlecheck->setAstitle('Articleinsert');
                $articlecheck->setIsads($data["isads"]); 
                $articlecheck->setBewerten($data["bewerten"]);
                $articlecheck->setRechte($data["rechte"]);
                $em->flush();
            $subjetsys = "Neuer Beitrag zum freischalten!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('article@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde ein neuer Beitrag von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "article@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde ein neuer Beitrag von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
                return $this->render('userprofil/articleend.html.twig', array(
                    'controller_name' => 'ShowcaseController',
                ));
            } else {

                $builder = $this->createForm(ArticleCropFormType::class);
                $builder->add('picselected', HiddenType::class, array(
                    'data' => $data["picselected"],
                    'required' => false,
                ));
if(!in_array($data["articlekategorie"], $nextextarray)) {
                $builder->add('articlekategorie', ChoiceType::class, array(
                    'label' => 'Kategorie wechseln? ',
                    'choices' => $nextextarray,
                ));
                $builder->add('update', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Account updaten...'));
                $builder->add('articlepay', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Beitrag kaufen...'));
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Beitrag mit neuer Kategorie eintragen...'));
} else {
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'articleinsert bdnmini2 btn btn-primary pull-left'),'label' => 'Beitrag eintragen...'));
}

                


                $emp = $this->get('doctrine_mongodb')->getManager();
                $articlekategoriecheck = $emp->getRepository('App:Articlekategorien')->findOneBy(["articlekatname"=>$data["articlekategorie"]]);
                $rolein = 0;
                if(in_array("Admin", $articlekategoriecheck->getRolegroup())) { $rolein = "Admin"; }
                if(in_array("Free", $articlekategoriecheck->getRolegroup())) { $rolein = "Free"; }
                if(in_array("Standart", $articlekategoriecheck->getRolegroup())) { $rolein = "Profi"; }
                if(in_array("Platin", $articlekategoriecheck->getRolegroup())) { $rolein = "Platin"; }
                $emp->flush();
                $userdata = new CheckUserdataController();               
                return $this->render('userprofil/articlenext.html.twig', array(
                    'form' => $builder->createView(),
                    'controller_name' => 'ShowcaseController',
                    'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $username),
                    'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $username),
                    'articleergebnis' => $articleergebnis,
                    'picsarray' => $casepics,
                    'articleTitel' => $data["titel"],
                    'articleKategorie' => $data["articlekategorie"],
                    'articleContent' => html_entity_decode(HashToLinkController::HashToLink($data["content"])),
                    'articleroles' => $rolein,
                    'thumb_width' => $thumb_width,
                    'thumb_height' => $thumb_height,
                    'current_large_image_width' => $thumb_width,
                    'current_large_image_height' => $thumb_height,
                    'articleauswahlpic' => $large_image_location,
                    'picselected' => $data["picselected"],
                    'reset' => $reset,
                ));
            }
        } else {

            return $this->render('userprofil/articlemedia.html.twig', array(
                'form' => $builder->createView(),
                'controller_name' => 'ShowcaseController',
                'articleergebnis' => $articleergebnis,
                'picsarray' => $casepics,
                'reset' => $reset,
            ));
        }
        
    }
}
