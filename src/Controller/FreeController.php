<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FreeController extends AbstractController
{
    /**
     * @Route("/free", name="free")
     */
    public function index()
    {
        return $this->render('free/index.html.twig', [
            'controller_name' => 'FreeController',
        ]);
    }
}
