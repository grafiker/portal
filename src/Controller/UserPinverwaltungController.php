<?php

namespace App\Controller;
use App\Document\Pins;
use App\Controller\CheckUserdataController;
use App\AppBundle\Form\AdminPinsEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserPinverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/profile/pinverwaltung/{page}", name="user_pinverwaltung")
     */
    public function createAction(RequestStack $requestStack, Request $request, $page)
    {
        $pindata = new PinoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
            }
        }    
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('username' => $username),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
            if($users != "anon.") {
                $provider = $this->container->get('fos_message.provider');
                $mailanz = $provider->getNbUnreadMessages();
            } else {
                $mailanz = false;
            }
            $pinlastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/pinverwaltung.html.twig', array(
            'file_exists' => $users->getUserpic(),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'unreads' => $mailanz,
            'pindata' => $pindata,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'count' => count($pindatagesamt)/$anzprosite,
            'counter' => count($pindatagesamt),
            'pinkats' => $pinkats,
            'pincounterdata' => $pincounterdata,
        ));
    }

    /**
     * edit
     * @Route("/profile/pinverwaltung/{pinID}/{action}", name="user_pinverwaltung_edit")
     */
    
    public function editAction(RequestStack $requestStack, Request $request, $pinID, $action, \Swift_Mailer $mailer)
    {
        $pindata = new PinoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
            }
        }    
        $task = new Pins();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/pinview/'.$pincheck->getPinlink();
            $subjetsys = "Ein Pin wurde gelöscht!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('pins@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von: ' .$pincheck->getUsername(). ' ein Pin auf Grafiker.de gelöscht!<br /><br />Der Link zum Pin war: ' . $linkToView);
            $mailer->send($message);     
            $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "pins@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$pincheck->getUsername(). ' ein Pin auf Grafiker.de gelöscht!<br /><br />Der Link zum Pin war: ' . $linkToView);
            if($pincheck->getPicname()) {
            foreach($pincheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics);
                } else {}
            }
        }
            if ($pincheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pincheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$pincheck->getThumbnail());
            } else {}
            $emdel->remove($pincheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $userdata = new CheckUserdataController();
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
                {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'DESC'), $anzprosite, $page);
            $pinlastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/pinverwaltung.html.twig', array(
            'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'pindata' => $pindata,
            'count' => count($pindatagesamt)/$anzprosite,
            'counter' => count($pindatagesamt),
            'pinkats' => $pinkats,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'pincounterdata' => $pincounterdata,
        ));
        } else if ($action == "edit" or $action == "all") {

        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Pinskategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
        }
        $newextarray=array(array("pinID" => $pinID,"pinkategorie" => $pincheck->getPinkategorie(),"kommentare" => $pincheck->getKommentare(),"bewerten" => $pincheck->getBewerten(),"title" => $pincheck->getTitle(),"content" => $pincheck->getContent(),"rechte" => $pincheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminPinsEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $pindata = $dms->getRepository('App:Pins')->find($pinID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($pindata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pinpics/".$delpics);
    } else {}
    }
                $pincheck->setAstitle('Pininsert');
                $pindata->setTitle($task["titel"]);
                $pindata->setPinkategorie($task["pinkategorie"]);
                $pindata->setContent($task["content"]);
                $pindata->setIsads($task["isads"]);
                $pindata->setBewerten($task["bewerten"]);
                $pindata->setPicname($array);
                $dms->flush();
                $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/pinview/'.$pindata->getPinlink();
                $subjetsys = "Ein überarbeiteter Pin zum Freischalten!";
                $message = (new \Swift_Message())
                ->setSubject($subjetsys)
                ->setFrom('pins@grafiker.de')
                ->setTo('mail@grafiker.de')
                ->setBody('Es wurde von: ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);
                $mailer->send($message);
                $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "pins@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);                

            }
            $casepics = array();
            $casepics=$pincheck->getPicname(); 
        return $this->render('userprofil/pinedit.html.twig', array(
            'form' => $builder->createView(),
            'pinID' => $pinID,
            'username' => $pincheck->getUsername(),
            'picsarray' => $casepics,
            'pinkats' => $pinkats,
            'pincounterdata' => $pincounterdata,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["id"=>$pinID]);

        $pincheck->setAstitle("Startinsert");
        $pincheck->setAktiv(true);
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$pincheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('Ihr Pin wurde freigeschaltet!')
            ->setBody('Ihr Pin mit dem Titel: <strong style="color:red;">' . $pincheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => 'Pininsert'),array('id' => 'DESC'), $anzprosite, $page);

        return $this->render('userprofil/pinverwaltung.html.twig', array(
            'pindata' => $pindata,
            'count' => count($pindatagesamt)/$anzprosite,
            'pinkats' => $pinkats,
            'pincounterdata' => $pincounterdata,
        ));
    }
    }
}
