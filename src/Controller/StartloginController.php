<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StartloginController extends AbstractController
{
    /**
     * @Route("/startlogin", name="startlogin")
     */
    public function index()
    {
        return $this->render('startlogin/index.html.twig', [
            'controller_name' => 'StartloginController',
        ]);
    }
}
