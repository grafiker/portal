<?php

namespace App\Controller;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserStartController extends Controller
{
    /**
     * @Route("/profile/start", name="user_start")
     */
    public function index()
    {
        $userdata = new CheckUserdataController();

        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $firstlogin = $users->getFirstLogin();

        if($firstlogin == true) {
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder->addRecipient($users);

        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $usersender = $emdsend->getRepository('App:User')->findOneBy(["username"=>'hbroeskamp']);
        $threadBuilder->setSender($usersender);
        $threadBuilder->setSubject('Herzlich Willkommen auf grafiker.de');
        $threadBuilder->setBody("Hallo ".$users->getUsername().",<br /> 
        <br />
        Schön dass Du Dich bei grafiker.de registriert hast. Am besten Du vervollständigst gleich noch Dein Profil indem Du<br /> 
        <br /><ul>
            <li>Dein Profilbild hinterlegst</li>
            <li>Auswählst wie viel Kapazität Du für neue Aufträge hast (für den Fall das Du welche haben möchtest)</li> 
            <li>Deine Skills angibst. Die möglichen Skills werden Dir entsprechend Deiner im Profil ausgewählten Berufsgruppen angezeigt.</li>
        </ul><br />
        Ich würde mich freuen, wenn Du mir noch eine kleine Message zurückschickst und mir z.B. ein bisschen darüber erzählst, warum Du Dich hier angemeldet hast, was Du erwartest, was Dir gefällt oder auch nicht, was Du gerne an neuen Funktionen und Möglichkeiten hättest oder was Dir sonst noch wichtig ist. <br />
        <br />
        Viele Grüsse <br />
        Heinz Bröskamp<br /> 
        Vom Grafiker.de Team");
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        $emd = $this->get('doctrine_mongodb')->getManager();
        $userupdate = $emd->getRepository('App:User')->findOneBy(["username"=>$users->getUsername()]);

//DELETE USER FROM SENDBLUE
/*$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/admin%40speedtreff.de",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "DELETE",
  CURLOPT_POSTFIELDS => "{
    \"email\":\"".$users->getEmail()."\"}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "api-key: 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"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}*/
        //CREATE USER FOR SENDBLUE - Systemmailer
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{
              \"email\":\"".$users->getEmail()."\",
              \"attributes\": {\"name\":\"".$users->getLastName()."\",\"vorname\":\"".$users->getFirstName()."\",\"uuid\":\"".$users->getId()."\",\"enabled\":\"true\",\"MONTAGSMAILER\":\"\"},
              \"emailBlacklisted\":false,
              \"smsBlacklisted\":false,
              \"updateEnabled\":false,
              \"listIds\":[11]
            }",
          CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          //echo "cURL Error #:" . $err;
        } else {
          //echo $response;
          $obj = json_decode($response);
          $userupdate->setSendBlueID(intval($obj->{'id'}));
        }

        //CREATE USER FOR SENDBLUE - Montagsmailer
        //$user->setNewsletterMail(NULL);
        //CREATE USER FROM SENDBLUE
        /*$curl = curl_init();
        $usermailout = urlencode($users->getEmail());
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/".$usermailout."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => "{\"listIds\":[12]}",
          CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          //echo "cURL Error #:" . $err;
        } else {
          //echo $response;
          $obj = json_decode($response);
          $userupdate->setSendBlueID(intval($obj->{'id'}));
        }*/

        $userupdate->setFirstLogin(false);
        $emd->flush();
        }
        if($users != "anon.") {
            $provider = $this->container->get('fos_message.provider');
            $mailanz = $provider->getNbUnreadMessages();
            $capacity = $users->getCapacity();
            $agb = $users->getAgb();
            $datenschutz = $users->getDatenschutz();
        } else {
            $mailanz = false;
            $capacity = false;
            $agb = "logout";
            $datenschutz = "logout";
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $besucherdatagesamt = $dm->createQueryBuilder('App:Profilbesucher')->field('new')->equals(true)->field('username')->equals($users->getUsername())->count()->getQuery()->execute();
        return $this->render('userprofil/start.html.twig', [
            'controller_name' => 'UserStartController',
            'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'unreads' => $mailanz,
            'capacity' => $capacity,
            'agb' => $agb,
            'datenschutz' => $datenschutz,
            'besucherdatagesamt' => $besucherdatagesamt,
        ]);
    }
}
