<?php

namespace App\Controller;
use App\Document\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class LoadarticleController extends Controller
{
    /**
     * @Route("/loadarticle/{articleID}/{filter}", name="loadarticle")
     */
    public function index(Request $request, $articleID, $filter)
    {
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
if($filter != "all") {
        $filter = explode("-", $filter);
        if($filter[0] != "Search") {
        if($users == $filter[1]) {
            $ownerfilter = $filter[1];
            $userfilter = '';
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        } else if('ALL' == $filter[1]) {
            $ownerfilter = '';
            $userfilter = '';
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        } else {
            $ownerfilter = '';
            $userfilter = $filter[1];
            $suchnach = $filter[0];
            $newfilter = $filter[0].'-'.$filter[1];
        }
    } else {
        $ownerfilter = '';
        $userfilter = '';
        $suchnach = $filter[0].'-'.$filter[1];
        $newfilter = $filter[0].'-'.$filter[1];
    }
} else {
        $ownerfilter = '';
        $userfilter = '';
        $suchnach = 'all';
        $newfilter = $suchnach;
}
        //var_dump($suchnach);
        $articledatas = new ArticleoutputController();
        $userdata = new CheckUserdataController();
        $page=$articleID/9;
        $anzprosite=9;
        $articlelastLogin=false;
        $useremploy=false;
        $dm = $this->get('doctrine_mongodb')->getManager();
        $articledata = $articledatas->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), $anzprosite, $page,'DESC',$suchnach,$ownerfilter,$userfilter,7,'Startinsert');
        //$articledata = $dm->getRepository('App:Article')->findBy(array(),array('id' => 'DESC'), $anzprosite, $page);
        $fileexistarray=array();
        $filelinkarray=array();
        $wohnort=array();
        $userexist=array();
        foreach($articledata as $key => $articleuser) {
            //var_dump($articleuser);
            $dm = $this->get('doctrine_mongodb')->getManager();
                $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$articleuser->getUsername()]);
                if($userdatas) {
                $fileexistarray[$articleuser->getUsername()] = $userdatas->getUserpic();
                $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
                $wohnort[$articleuser->getUsername()] = $userdatas->getWohnort();
                $articlelastLogin[$articleuser->getUsername()] = $userdatas->getUpdatedAt();
                $useremploy[$articleuser->getUsername()] = $userdatas->getEmploy();
                $userexist[$articleuser->getUsername()] = true;
            } else {
                $userexist[$articleuser->getUsername()] = false;
            }
                $dm->flush();
        }
        //var_dump($wohnort);
        return $this->render('loadarticle/index.html.twig', [
            'controller_name' => 'LoaddataController',
            'articleID' => $articleID,
            'articledata' => $articledata,
            'articlelastLogin' => $articlelastLogin,
            'useremploy' => $useremploy,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'wohnort' => $wohnort,
            'filter' => $newfilter,
            'userexist' => $userexist,
        ]);
    }
}
