<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class AdminMaillogController extends Controller
{
    /**
     * @Route("/admin/maillog", name="admin_maillog")
     */
    public function index()
    {
        $maildb = $this->get('doctrine_mongodb')->getManager();
        $suche = "";
        $searchQuery = array(
            '$or' => array(
                array(
                    'title' => new \MongoRegex('/'.$suche.'/si'),
                    ),
                array(
                    'content' => new \MongoRegex('/'.$suche.'/si'),
                    ),
                )
            );
        //$pindatagesamt = $dm->getRepository('App:Pins')->findBy($searchQuery,array('startdate' => 'ASC'));
        $mailout = $maildb->getRepository('App:Maillog')->findBy(array(),array('senddate' => 'DESC'), 200, 0);
        $maildb->flush();
        return $this->render('admin/maillog.html.twig', [
            'mailout' => $mailout,
        ]);
    }
}
