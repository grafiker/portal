<?php

namespace App\Controller;
use App\Document\Pins;
use App\Document\Ratingblock;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserPinviewController extends Controller
{
    /**
     * @Route("/pinview/{pinid}", name="user_pinview")
     */
    public function index($pinid)
    {
        $nextpindata = new Pins();
        $prevpindatab = new Pins();
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pindata = $dm->getRepository('App:Pins')->findBy(array('pinlink' => $pinid));

        $picsarray = array();
        foreach($pindata as $key => $pinpics) {

            $prevpindata = $dm->createQueryBuilder('App:Pins')->sort(array('startdate' => 'ASC'))->field('startdate')->gte($pinpics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('pinkategorie')->equals($pinpics->getPinkategorie())->getQuery()->toArray();
            $nextpindata = $dm->createQueryBuilder('App:Pins')->sort(array('startdate' => 'DESC'))->field('startdate')->lte($pinpics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('pinkategorie')->equals($pinpics->getPinkategorie())->getQuery()->toArray();

            $picsarray = $pinpics->getPicname();
            $content = HashToLinkController::HashToLink($pinpics->getContent());

        }
        foreach($pindata as $key => $pinuser) {
            $dmu = $this->get('doctrine_mongodb')->getManager();
        $userdatas = $dmu->getRepository('App:User')->findOneBy(["username"=>$pinuser->getUsername()]);
        $fileexistarray[$pinuser->getUsername()] = $userdatas->getUserpic();
        $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
        $pinlastLogin[$userdatas->getUsername()] = $userdatas->getUpdatedAt();
        $useremploy[$userdatas->getUsername()] = $userdatas->getEmploy();
        
        }
        $dms = $this->get('doctrine_mongodb')->getManager();
        foreach($pindata as $key => $pinpics) {
        $pindatas = $dms->getRepository('App:Pins')->find($pinpics->id);
        $pinviews = $pindatas->getPinviews()+1;
        $pindatas->setPinviews($pinviews);
        $date = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $olddate = $pindatas->getViewdate();
        $pindatas->setViewdate($date); 
$titlealt=$pindatas->getTitle();
        $emdelblock = $this->get('doctrine_mongodb')->getManager();
        $ratingblockcheck = $emdelblock->getRepository('App:Ratingblock')->findOneBy(array("pinid"=>$pinpics->id, "username"=>$username));
        if(!$ratingblockcheck) {
            $ratingok=true;
        } else {
            $ratingok=false;
        }
        $emdelblock->flush();
        }
        $dm->flush();

$title = str_replace("!", "", $titlealt);
$title = str_replace("?", "", $title);
$title = str_replace(".....", "", $title);
$title = str_replace("....", "", $title);
$title = str_replace("...", "", $title);
$title = str_replace(" - ", " ", $title);
$title = explode(" ",$title);
$pindatacount = array();
foreach($title as $key => $searchtitle) {
    

            /*$searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    )
            );*/

                $pindatacount[] = $dm->createQueryBuilder('App:Pins')->sort(array('isads' => 'DESC'))->field('content')->equals(new \MongoRegex('/'.$searchtitle.'/si'))->limit(6)->getQuery()->toArray();
                //$pindatacount[$key] = $dm->getRepository('App:Pins')->findBy($searchQuery,array('startdate' => 'DESC'));
}
$pinsfileexistarray=array();
$pinsfilelinkarray=array();
$userexist=array();
$wohnort=array();
$pinuseremploy=array();
$pinlastLoginUser=array();
foreach($pindatacount as $pindatacounts) {
    foreach($pindatacounts as $pindatacounts) {
        if($titlealt != $pindatacounts->getTitle()) {
            $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$pindatacounts->getUsername()]);
            if($userdatas) {
            $pinsfileexistarray[$pindatacounts->getUsername()] =$userdatas->getUserpic();
            $pinsfilelinkarray[$pindatacounts->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
            $wohnort[$pindatacounts->getUsername()] = $userdatas->getWohnort();
            $pinlastLoginUser[$pindatacounts->getUsername()] = $userdatas->getUpdatedAt();
            $pinuseremploy[$pindatacounts->getUsername()] = $userdatas->getEmploy();
            $userexist[$pindatacounts->getUsername()] = true;
            } else {
            $userexist[$pindatacounts->getUsername()] = false;
            }
        }
    }
}
//$pindatacount[] = array_unique($pindatacount);
shuffle($pindatacount);
        return $this->render('userprofil/pinview.html.twig', [
            'controller_name' => 'AdminPinviewController',
            'pindata' => $pindata,
            'contenttxt' => $content,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'picsarray' => $picsarray,
            'olddate' => $olddate,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'ratingok' => $ratingok,
            'pinviewcontroller' => 1,
            'nextpindata' => $nextpindata,
            'prevpindata' => $prevpindata,
            'pindatacount' => $pindatacount,
            'userexist' => $userexist,
            'pinsfileexistarray' => $pinsfileexistarray,
            'pinsfilelinkarray' => $pinsfilelinkarray,
            'wohnort' => $wohnort,
            'pinuseremploy' => $pinuseremploy,
            'pinlastLoginUser' => $pinlastLoginUser,
        ]);
    }

}
