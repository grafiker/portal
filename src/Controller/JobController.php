<?php
namespace App\Controller;
use App\Document\Job;
use App\AppBundle\Form\JobFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class JobController extends Controller
{
    /**
     * @Route("/job", name="job")
     */
    public function new(Request $request)
    {

if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $user = false;
    $username = 'Kein Mitglied!';
}
$upload_dir = 'upload/uploads/firma/';
$name = $upload_dir.'/'.$username . '_firma';

if($user != false and ($user->getFirmapic() and $user->getFirmapic() != 'del')) {
          $logoexist=true;
          $logolink=$user->getFirmapic();
          $logofirma = 1;
        } elseif($user != false and ($user->getUserpic() and $user->getUserpic() != 'del')) {
            $logoexist=true;
            $logolink=$user->getUserpic();
            $logofirma = 2;
        } else {
            $logoexist=false;
            $logolink=false;
            $logofirma = 3;
        }
        $task = new Job();
        $builder = $this->createFormBuilder($task)
        ->add('username', HiddenType::class, array(
            'data' => $username,
        ))
        /*->add('astitle', TextType::class, ['label' => 'Jobtitel:'])*/
        ->add('jobSuche', ChoiceType::class, array(
            'label' => 'Biete / Suche:',
            'choices' => array(
                'Ich biete einen Job für' => 'biete',
                'Ich suche einen Job als' => 'suche',
            ),
        ))
        ->add('jobTyp', ChoiceType::class, array(
            'label' => 'Jobart:',
            'choices' => array(
                    'Grafiker|in' => 'Grafiker',
                    'Druckerei/Vorstufe/Weiterverarbeitung' => 'Druckerei',
                    'Fotograf|in' => 'Fotograf',
                    'Entwickler|in' => 'Entwickler',
                    'Texter|in' => 'Texter',
                    'Illustrator|in' => 'Illustrator',
                    'Sonstige' => 'Sonstige',
            ),
        ))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-right'),'label' => 'weiter...'))
        ->getForm();

        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            //var_dump($request->request->all());
            $task = $builder->getData();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);*/
            $builder = $this->createForm(JobFormType::class, $request->request->all());
            $dmskill = $this->get('doctrine_mongodb')->getManager();
            $skillsDruckerei = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Druckerei']);
            $skillsGrafiker = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Grafiker']);
            $skillsFotograf = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Fotograf']);
            $skillsEntwickler = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Programmierer']);
            $skillsTexter = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Texter']);
            $skillsIllustrator = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Illustrator']);
            $skillsSonstiges = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Sonstiges']);
            $druckerei=array();
            $grafiker=array();
            $fotograf=array();
            $entwickler=array();
            $texter=array();
            $illusator=array();
            $sonstiges=array();
            $druckerei = $skillsDruckerei->getSkillname();
            $grafiker = $skillsGrafiker->getSkillname();
            $fotograf = $skillsFotograf->getSkillname();
            $entwickler = $skillsEntwickler->getSkillname();
            $texter = $skillsTexter->getSkillname();
            $illusator = $skillsIllustrator->getSkillname();
            $sonstiges = $skillsSonstiges->getSkillname();
            return $this->render('job/nextstep.html.twig', array(
                'name' => 'hier geht es weiter...',
                'form' => $builder->createView(),
                'druckerei' => $druckerei,
                'grafiker' => $grafiker,
                'fotograf' => $fotograf,
                'entwickler' => $entwickler,
                'texter' => $texter,
                'illustrator' => $illusator,
                'sonstiges' => $sonstiges,
                'logoexist' => $logoexist,
                'logolink' => $logolink,
                'logofirma' => $logofirma,
            ));
        }

        return $this->render('job/index.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'JobController',
        ));
    }
}
