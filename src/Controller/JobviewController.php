<?php

namespace App\Controller;

use App\Document\Job;
use App\Document\Bewerbung;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
class JobviewController extends Controller
{
    /**
     * @Route("/jobview/{joblink}", name="jobview")
     */
    public function index($joblink)
    {
        $userdatas = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $jobdata = $dm->getRepository('App:Job')->findBy(array('joblink' => $joblink));
        $myArray = (array)$jobdata;
        $checkdata = $dm->getRepository('App:Bewerbung')->findBy(array('username' => $username, 'jobID' => $myArray[0]->getId()));
        if(!$checkdata) {
            $formdeaktiv = false;
            $beworbenDate=false;
        } else {
            $formdeaktiv = true;
            $beworbenDate=$checkdata[0]->getBewerbungDate();
        }
        
        $upload_dir = 'upload/uploads/firma/';
        $name = $upload_dir.'/'.$myArray[0]->getUsername() . '_firma';
        
                if(file_exists($name.".png")) {
                  $logoexist=true;
                  $logolink=$name.".png";
                }elseif(file_exists($name.".gif")) {
                    $logoexist=true;
                    $logolink=$name.".gif";
                } elseif(file_exists($name.".jpg")) {
                    $logoexist=true;
                    $logolink=$name.".jpg";
                } else {
                    $logoexist=false;
                    $logolink=false;
                }
        $userdata = $dm->getRepository('App:User')->findBy(array('username' => $myArray[0]->getUsername()));
        $dm->flush();
        return $this->render('jobview/index.html.twig', [
            'controller_name' => 'JobviewController',
            'jobdata' => $jobdata,
            'userdata' => $userdata,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'formdeaktiv' => $formdeaktiv,
            'beworbenDate' => $beworbenDate,
        ]);
    }
    /**
     * sendmail
     * @Route("/jobview/{jobuser}/{joblink}", name="jobview_sendmail")
     */
    public function editAction(Request $request, $joblink, $jobuser)
    {
        $task = new Bewerbung();
        $newarray=$request->request->all()['betreffforuser'];
        $file_name=$_FILES["form"]["name"]["docpdf1"];
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $jobdata = $dm->getRepository('App:Job')->findBy(array('joblink' => $joblink));
        
        $myArray = (array)$jobdata;
        $upload_dir = 'upload/uploads/firma/';
        $name = $upload_dir.'/'.$myArray[0]->getUsername() . '_firma';
        
                if(file_exists($name.".png")) {
                  $logoexist=true;
                  $logolink=$name.".png";
                }elseif(file_exists($name.".gif")) {
                    $logoexist=true;
                    $logolink=$name.".gif";
                } elseif(file_exists($name.".jpg")) {
                    $logoexist=true;
                    $logolink=$name.".jpg";
                } else {
                    $logoexist=false;
                    $logolink=false;
                }
                if(isset($_POST['betreffforuser'])) {
                    $pdf = array();
                    $upload_dir = 'upload/uploads/pdf/bewerbung/';
                    if(!is_dir($upload_dir)){
                        mkdir($upload_dir, 0777);
                        chmod($upload_dir, 0777);
                    }
                    $file_name=$_FILES["form"]["name"]["docpdf1"];
                    $file_tmp=$_FILES["form"]["tmp_name"]["docpdf1"];
                    $tmp = explode('.', $file_name);
                    $file_ext = end($tmp);
                  
                    $expensions= array("pdf");
                    $errors = false;
                    if(in_array($file_ext,$expensions)=== false){
                       $errors=true;
                    }
                    
                    if(!$errors) {
                        $pdf[0]=md5($tmp[0].time()).'.'.$tmp[1];
                        move_uploaded_file($file_tmp,$upload_dir.$pdf[0]);
                        $pdf0 = true;
                    }
                    $file_name=$_FILES["form"]["name"]["docpdf2"];
                    $file_tmp=$_FILES["form"]["tmp_name"]["docpdf2"];
                    $tmp = explode('.', $file_name);
                    $file_ext = end($tmp);
                  
                    $expensions= array("pdf");
                    $errors = false;
                    if(in_array($file_ext,$expensions)=== false){
                       $errors=true;
                    }

                    if(!$errors) {
                        $pdf[1]=md5($tmp[0].time()).'.'.$tmp[1];
                        move_uploaded_file($file_tmp,$upload_dir.$pdf[1]);
                        $pdf1 = true;
                    }
if(isset($pdf0) or isset($pdf1)) {
    if(isset($pdf0) and isset($pdf1)) {
            $pdf0name=$pdf[0];
            $pdf1name=$pdf[1];
            $anhang = "Anhänge zu dieser Bewerbung:<br /><a target=\"_blank\" href=\"../upload/uploads/pdf/bewerbung/".$pdf0name."\"><img style=\"width: 70px; height: auto;\" src=\"../img/pdfimg.png\" /></a> <a target=\"_blank\" href=\"../upload/uploads/pdf/bewerbung/".$pdf1name."\"><img style=\"width: 70px; height: auto;\" src=\"../img/pdfimg.png\" /></a>";
    } else {
        if(isset($pdf0)) {
            $pdfname=$pdf[0];
        } 
        if(isset($pdf1)) {
            $pdfname=$pdf[1];
        }
        $anhang = "Anhang zu dieser Bewerbung:<br /><a target=\"_blank\" href=\"../upload/uploads/pdf/bewerbung/".$pdfname."\"><img style=\"width: 70px; height: auto;\" src=\"../img/pdfimg.png\" /></a>";
    }
    $ispdf = true;
} else {
    $ispdf = false;
}
$messageSubject = "Bewerbung auf Ausschreibung: " . $myArray[0]->getAstitle();
if($ispdf) {
    $messageBody = "Bewerber-Betreff:<br />" . $_POST['betreffforuser'] . "<br />Nachricht:<br />" . $_POST['messegesforuser'] . "<br /><br />" . $anhang;
} else {
    $messageBody = "Bewerber-Betreff:<br />" . $_POST['betreffforuser'] . "<br />Nachricht:<br />" . $_POST['messegesforuser'];
}

                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$jobuser]);
                    $threadBuilder = $this->get('fos_message.composer')->newThread();
                    $threadBuilder
                        ->addRecipient($userdatas) // Retrieved from your backend, your user manager or ...
                        ->setSender($users)
                        ->setSubject($messageSubject)
                        ->setBody($messageBody);
                     
                     
                    $sender = $this->get('fos_message.sender');
                    
                    $sender->send($threadBuilder->getMessage());
                    $maildatas = $dm->getRepository('App:Thread')->findBy(array(),array('_id' => -1), 1, 0);
                    $dm->flush();
                    $dateCreate = \DateTime::createFromFormat('U', time());
                    $task->setBewerbungDate($dateCreate);
                    $task->setUsername($username);
                    $task->setJobId($myArray[0]->getId());
                    $task->setMailId($maildatas[0]->getId());
                    $task->setDocpdf($pdf);
                    $task->setJobLink($joblink);
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $dm->persist($task);
                    $dm->flush();

                    $mailok = true;
                    //var_dump($maildatas[0]->getId());
                }
        $userdata = $dm->getRepository('App:User')->findBy(array('username' => $myArray[0]->getUsername()));
        $dm->flush();
        return $this->render('jobview/index.html.twig', [
            'controller_name' => 'JobviewController',
            'jobdata' => $jobdata,
            'userdata' => $userdata,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'formdeaktiv' => true,
            'mailok' => $mailok,
            'beworbenDate' => false,
        ]);
    }
}
