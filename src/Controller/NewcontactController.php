<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NewcontactController extends AbstractController
{
    /**
     * @Route("/newcontact", name="newcontact")
     */
    public function index()
    {
        return $this->render('newcontact/index.html.twig', [
            'controller_name' => 'NewcontactController',
        ]);
    }
}
