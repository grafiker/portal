<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceShopController extends AbstractController
{
    /**
     * @Route("/service/shop", name="service_shop")
     */
    public function index()
    {
        return $this->render('service_shop/index.html.twig', [
            'controller_name' => 'ServiceShopController',
        ]);
    }
}
