<?php

namespace App\Controller;
use App\Document\Pins;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class RestartpinController extends Controller
{
    /**
     * @Route("/restartpin/{username}", name="restartpin")
     */
    public function index($username)
    {
        $emdel = $this->get('doctrine_mongodb')->getManager();
                $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
                foreach($pincheck->getPicname() as $key => $delpics) {
                    if(file_exists("./upload/uploads/pinpics/".$delpics)) {
                        unlink("./upload/uploads/pinpics/".$delpics);
                    }
                }
                $emdel->remove($pincheck);
                $emdel->flush();
                $reset = true;
        return $this->render('restartpin/index.html.twig', [
            'controller_name' => 'RestartpinController',
        ]);
    }
}
