<?php

namespace App\Controller;
use App\Document\Infotext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
class AdminStartseiteController extends Controller
{
    /**
     * @Route("/admin/grafiker", name="admin_startseite")
     */
    public function index(Request $request)
    {

        $newarray=$request->request->all();
        if($newarray) {
        if(isset($newarray["starttxt"])) {
            if($newarray["starttxt"] == "1") {
                $em = $this->get('doctrine_mongodb')->getManager();
                $infotext = $em->getRepository('App:Infotext')->findAll();
                if($infotext) {
                    foreach($infotext as $infotext) {
                        $infotext->setInfotext($newarray["startmessage"]);
                    }
                } else {
                $task = new Infotext();
                $task->setInfotext($newarray["startmessage"]);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($task);
                $dm->flush();
                //var_dump($task);
//echo"".$newarray["startmessage"]."";
                }
            } 
            if($newarray["starttxt"] == "0") {
                $em = $this->get('doctrine_mongodb')->getManager();
                $infotext = $em->getRepository('App:Infotext')->findAll();
                foreach($infotext as $infotext) {
                $em->remove($infotext); // Lösche alle gestarteteten threads vom User aus MongoDB löschen    
                $em->flush(); 
                }
                }
            }
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $usergesamt = $dm->createQueryBuilder('App:User')->field('enabled')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe1 = $dm->createQueryBuilder('App:User')->field('berufsgruppe1')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe2 = $dm->createQueryBuilder('App:User')->field('berufsgruppe2')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe3 = $dm->createQueryBuilder('App:User')->field('berufsgruppe3')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe4 = $dm->createQueryBuilder('App:User')->field('berufsgruppe4')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe5 = $dm->createQueryBuilder('App:User')->field('berufsgruppe5')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe6 = $dm->createQueryBuilder('App:User')->field('berufsgruppe6')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe7 = $dm->createQueryBuilder('App:User')->field('berufsgruppe7')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe8 = $dm->createQueryBuilder('App:User')->field('berufsgruppe8')->equals(true)->count()->getQuery()->execute();
        $userberufsgruppe9 = $dm->createQueryBuilder('App:User')->field('berufsgruppe9')->equals(true)->count()->getQuery()->execute();
        $useroff = $dm->createQueryBuilder('App:User')->field('enabled')->equals(false)->count()->getQuery()->execute();
        $pinsgesamt = $dm->createQueryBuilder('App:Pins')->count()->getQuery()->execute();
        $pinsstart = $dm->createQueryBuilder('App:Pins')->field('astitle')->equals('Pinstart')->count()->getQuery()->execute();
        $pinsinsert = $dm->createQueryBuilder('App:Pins')->field('astitle')->equals('Pininsert')->count()->getQuery()->execute();
        $articlegesamt = $dm->createQueryBuilder('App:Article')->count()->getQuery()->execute();
        $articlestart = $dm->createQueryBuilder('App:Article')->field('astitle')->equals('Articlestart')->count()->getQuery()->execute();
        $articleinsert = $dm->createQueryBuilder('App:Article')->field('astitle')->equals('Articleinsert')->count()->getQuery()->execute();
        $gutscheinegesamt = $dm->createQueryBuilder('App:Article')->count()->getQuery()->execute();
        $gutscheinestart = $dm->createQueryBuilder('App:Article')->field('astitle')->equals('Articlestart')->count()->getQuery()->execute();
        $gutscheineinsert = $dm->createQueryBuilder('App:Article')->field('astitle')->equals('Articleinsert')->count()->getQuery()->execute();
        $projektrechner = $dm->createQueryBuilder('App:Projektrechner')->count()->getQuery()->execute();
        $jobsgesamt = $dm->createQueryBuilder('App:Job')->count()->getQuery()->execute();
        $jobsfalse = $dm->createQueryBuilder('App:Job')->field('aktiv')->equals(false)->field('enddate')->gte(new \DateTime())->count()->getQuery()->execute();
        $mailer = $dm->createQueryBuilder('App:User')->field('newsletterMail')->equals(NULL)->count()->getQuery()->execute();
        $_dateTime = new \DateTime('2 minutes ago');
        $useronlinedata = $dm->createQueryBuilder('App:User')->sort(array('id' => 'DESC'))->field('updatedAt')->gte($_dateTime)->count()->getQuery()->execute();

        $dm->flush();
$userinfo = false;
$em = $this->get('doctrine_mongodb')->getManager();
                $infotext = $em->getRepository('App:Infotext')->findAll();
                if($infotext) {
                    foreach($infotext as $infotext) {
                        $userinfo = $infotext->getInfotext();
                    }
                }
        return $this->render('admin/start.html.twig', [
            'controller_name' => 'AdminStartseiteController',
            'userinfo' => $userinfo,
            'usergesamt' => $usergesamt,
            'useroff' => $useroff,
            'pinsgesamt' => $pinsgesamt,
            'pinsstart' => $pinsstart, 
            'pinsinsert' => $pinsinsert,
            'articlegesamt' => $articlegesamt,
            'articlestart' => $articlestart, 
            'articleinsert' => $articleinsert,
            'gutscheinegesamt' => $gutscheinegesamt,
            'gutscheinestart' => $gutscheinestart, 
            'gutscheineinsert' => $gutscheineinsert,
            'jobsgesamt' => $jobsgesamt,
            'jobsfalse' => $jobsfalse,
            'projektrechner' => $projektrechner,
            'useronlinedata' => $useronlinedata,
            'mailer' => $mailer,
            'userberufsgruppe1' => $userberufsgruppe1,
            'userberufsgruppe2' => $userberufsgruppe2,
            'userberufsgruppe3' => $userberufsgruppe3,
            'userberufsgruppe4' => $userberufsgruppe4,
            'userberufsgruppe5' => $userberufsgruppe5,
            'userberufsgruppe6' => $userberufsgruppe6,
            'userberufsgruppe7' => $userberufsgruppe7,
            'userberufsgruppe8' => $userberufsgruppe8,
            'userberufsgruppe9' => $userberufsgruppe9,
        ]);
    }
}
