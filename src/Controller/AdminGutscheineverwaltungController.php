<?php

namespace App\Controller;
use App\Document\Gutscheine;
use App\Document\User;
use App\AppBundle\Form\AdminGutscheineEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class AdminGutscheineverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/admin/gutscheineverwaltung/{page}", name="admin_gutscheineverwaltung")
     */
    public function createAction(Request $request, $page)
    {
        
        $userdata = new CheckUserdataController();
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            $filelinkarray=array();
            $reexistkarray=array();
            $anbexistkarray=array();
            $usernameId=array();
            foreach($gutscheinedata as $key => $gutscheineuser) {
                $fileexistarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
                $filelinkarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
            }
foreach($gutscheinedata as $key => $gutscheineids) {
$pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 1));
$pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 2));
$userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $gutscheineids->getUsername()));
$usernameId[$gutscheineids->getId()] = $userdata->getId();
if($pdfcheckre) {
    $reexistkarray[$gutscheineids->getId()] = $pdfcheckre->getRechnungsnummer();
    }
    if($pdfcheckanb) {
    $anbexistkarray[$gutscheineids->getId()] = $pdfcheckanb->getRechnungsnummer();
    }
if(!$pdfcheckre) {
$reexistkarray[$gutscheineids->getId()] = false;
}
if(!$pdfcheckanb) {
$anbexistkarray[$gutscheineids->getId()] = false;
}
}
        return $this->render('admin/gutscheineverwaltung.html.twig', array(
            'gutscheinedata' => $gutscheinedata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
        ));
    }

    /**
     * edit
     * @Route("/admin/gutscheineverwaltung/{gutscheineID}/{action}", name="admin_gutscheineverwaltung_edit")
     */
    
    public function editAction(Request $request, $gutscheineID, $action)
    {
        $userdata = new CheckUserdataController();
        $task = new Gutscheine();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $gutscheinecheck = $emdel->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);
if($gutscheinecheck->getPicname()) {
            foreach($gutscheinecheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics);
                } else {}
            }
        }
            
            if ($gutscheinecheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$gutscheinecheck->getThumbnail());
            } else {}
            $emdel->remove($gutscheinecheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            foreach($gutscheinedata as $key => $gutscheineuser) {
                $fileexistarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
                $filelinkarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($gutscheinedata as $key => $gutscheineids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $gutscheineids->getUsername()));
                $usernameId[$gutscheineids->getId()] = $userdata->getId();
                if($pdfcheckre) {
                    $reexistkarray[$gutscheineids->getId()] = $pdfcheckre->getRechnungsnummer();
                    }
                    if($pdfcheckanb) {
                    $anbexistkarray[$gutscheineids->getId()] = $pdfcheckanb->getRechnungsnummer();
                    }
                if(!$pdfcheckre) {
                $reexistkarray[$gutscheineids->getId()] = false;
                }
                if(!$pdfcheckanb) {
                $anbexistkarray[$gutscheineids->getId()] = false;
                }
                }
        return $this->render('admin/gutscheineverwaltung.html.twig', array(
            'gutscheinedata' => $gutscheinedata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
        ));
        } else if ($action == "edit" or $action == "all") {

        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Gutscheinekategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getGutscheinekatname()] = $value->getGutscheinekatname();
        }
        if(!$gutscheinecheck->getIsads()) {
            $isads = false;
        } else {
            $isads = $gutscheinecheck->getIsads();
        }
        var_dump($gutscheinecheck->getIsads());
        $newextarray=array(array("gutscheineID" => $gutscheineID,"gutscheinekategorie" => $gutscheinecheck->getGutscheinekategorie(),"isads" => $isads,"bewerten" => $gutscheinecheck->getBewerten(),"title" => $gutscheinecheck->getTitle(),"content" => $gutscheinecheck->getContent(),"rechte" => $gutscheinecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminGutscheineEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $gutscheinedata = $dms->getRepository('App:Gutscheine')->find($gutscheineID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($gutscheinedata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/gutscheinepics/".$delpics);
    } else {}
    }

                $gutscheinedata->setTitle($task["titel"]);
                $gutscheinedata->setGutscheinekategorie($task["gutscheinekategorie"]);
                $gutscheinedata->setContent($task["content"]);
                $gutscheinedata->setIsads($task["isads"]);
                $gutscheinedata->setBewerten($task["bewerten"]);
                $gutscheinedata->setPicname($array);
                $dms->flush();
            }
            $casepics = array();
            $casepics=$gutscheinecheck->getPicname(); 
        return $this->render('admin/gutscheineedit.html.twig', array(
            'form' => $builder->createView(),
            'gutscheineID' => $gutscheineID,
            'username' => $gutscheinecheck->getUsername(),
            'picsarray' => $casepics,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $gutscheinecheck = $em->getRepository('App:Gutscheine')->findOneBy(["id"=>$gutscheineID]);

        $gutscheinecheck->setAstitle("Gutscheineinsert");
        if($gutscheinecheck->getAktiv() == false) {
        $gutscheinecheck->setAktiv(true);
        } else {
            $gutscheinecheck->setAktiv(false); 
        }
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$gutscheinecheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('%%Systemmail%% Ihr Beitrag wurde freigeschaltet!')
            ->setBody('Ihr Beitrag mit dem Titel: <strong style="color:red;">' . $gutscheinecheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $gutscheinedatagesamt = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'ASC'));
            $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('astitle' => 'Gutscheineinsert'),array('id' => 'DESC'), $anzprosite, $page);
            $fileexistarray=array();
            $filelinkarray=array();
            $usernameId=array();
            foreach($gutscheinedata as $key => $gutscheineuser) {
                $fileexistarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
                $filelinkarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($gutscheinedata as $key => $gutscheineids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("gutscheineid" => $gutscheineids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $gutscheineids->getUsername()));
                $usernameId[$gutscheineids->getId()] = $userdata->getId();
                
                if($pdfcheckre) {
                $reexistkarray[$gutscheineids->getId()] = $pdfcheckre->getRechnungsnummer();
                } else {
                    $reexistkarray[$gutscheineids->getId()] = false;
                    }
                if($pdfcheckanb) {
                $anbexistkarray[$gutscheineids->getId()] = $pdfcheckanb->getRechnungsnummer();
                } else {
                    $anbexistkarray[$gutscheineids->getId()] = false;
                    }
                }
        return $this->render('admin/gutscheineverwaltung.html.twig', array(
            'gutscheinedata' => $gutscheinedata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($gutscheinedatagesamt)/$anzprosite,
        ));
    }
    
    }

    function deleteFilesFromDirectory($ordnername){
        $this->deleteFilesFromDirectory("./upload/uploads/userpics/");
        //überprüfen ob das Verzeichnis überhaupt existiert
        if (is_dir($ordnername)) {
        //Ordner öffnen zur weiteren Bearbeitung
        if ($dh = opendir($ordnername)) {
        //Schleife, bis alle Files im Verzeichnis ausgelesen wurden
        while (($file = readdir($dh)) !== false) {
        //Oft werden auch die Standardordner . und .. ausgelesen, diese sollen ignoriert werden
        if ($file!="." AND $file !="..") {
        //Files vom Server entfernen
        unlink("".$ordnername."".$file."");
        }
        }
        //geöffnetes Verzeichnis wieder schließen
        closedir($dh);
        }
        }
        var_dump(is_dir($ordnername));
        }
         
        //Funktionsaufruf - Directory immer mit endendem / angeben
        
}
