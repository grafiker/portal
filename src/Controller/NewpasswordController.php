<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
class NewpasswordController extends Controller
{
    /**
     * @Route("/resetting/check-email", name="newpassword")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $secretKey = "6LdYTHUUAAAAAFsO9xDKepMxlK8iMbD8zV8aceeB";
        $ip = $_SERVER['REMOTE_ADDR'];
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        // Spamversuch
        if(intval($responseKeys["success"]) !== 1) {
         $nospam = false;
        } else {
         $nospam = true;
        }
if($nospam) {
    $searchQuery = array(
        '$or' => array(
            array(
                'username' => $_POST["username"],
                ),
            array(
                'email' => $_POST["username"],
                ),
            )
        );
    $dm = $this->get('doctrine_mongodb')->getManager();
    $userdata = $dm->getRepository('App:User')->findOneBy($searchQuery);
    //var_dump($userdata);
    if($userdata) {
        $userfund = true;
    $username = $userdata->getUsername();
    $usermail = $userdata->getEmail();

    $_dateTime = new \DateTime('120 minutes ago');
    $userlastask = $dm->createQueryBuilder('App:User')->field('username')->equals($username)->field('passwordRequestedAt')->gte($_dateTime)->count()->getQuery()->execute();    

    //var_dump($userlastask);

    if(!$userlastask) {
        $resetcode = md5($username.$usermail.time());
        $date = \DateTime::createFromFormat('U', time()+3600);
        $date->setTimezone(new \DateTimeZone('UTC'));
        $userdata->setPasswordRequestedAt($date);     
        $userdata->setConfirmationToken($resetcode);
    $subjetsys = "Passwortanfrage auf grafiker.de";
    $bodytxt = "Hallo " .  $username . " !<br />
    <br />
    Bitte klicken sie den folgenden Link um Ihr Passwort zurückzusetzen:<br />
    http://grafiker.de/resetting/reset/" . $resetcode . "<br />
    <br />
    Mit freundlichen Grüßen,<br />
    grafiker.de";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom('passwort@grafiker.de')
    ->setTo($usermail)
    ->setBody($bodytxt);

    $mailer->send($message);
    $maillog = new MaillogController();
    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "passwort@grafiker.de", $usermail, $subjetsys, $bodytxt);
    }
    
    $dm->flush();
} else {
    $userfund = false;
}
}     
//var_dump($userfund);
        return $this->render('newpassword/index.html.twig', [
            'controller_name' => 'NewpasswordController',
            'userfund' => $userfund,
        ]);
    }
}
