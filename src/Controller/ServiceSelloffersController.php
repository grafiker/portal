<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceSelloffersController extends AbstractController
{
    /**
     * @Route("/service/selloffers", name="service_selloffers")
     */
    public function index()
    {
        return $this->render('service_selloffers/index.html.twig', [
            'controller_name' => 'ServiceSelloffersController',
        ]);
    }
}
