<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UnsubscribeController extends Controller
{
    /**
     * @Route("/unsubscribe/{uuid}", name="unsubscribe")
     */
    public function index($uuid)
    {

        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $dm->getRepository('App:User')->findOneById($uuid);

 
                $user->setNewsletterMail(1);
                $user->setSendBlueID(NULL);
//var_dump($user->getEmail());
//DELETE USER FROM SENDBLUE

$usermailout = urlencode($user->getEmail());
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/".$usermailout,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PUT",
  CURLOPT_POSTFIELDS => "{
      \"email\":\"".$user->getEmail()."\",
      \"attributes\": {\"MONTAGSMAILER\":\"2\"},
      \"emailBlacklisted\":false,
      \"smsBlacklisted\":false,
      \"updateEnabled\":false,
      \"listIds\":[11]
    }",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "api-key: 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",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  //echo "cURL Error #:" . $err;
} else {
  //echo $response;
}

$dm->persist($user);
$dm->flush();
        return $this->render('unsubscribe/index.html.twig', [
            'controller_name' => 'UnsubscribeController',
            'emailadresse' => $user->getEmail(),
            'username' => $user->getUsername(),
        ]);
    }
}
