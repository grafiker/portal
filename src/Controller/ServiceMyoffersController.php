<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceMyoffersController extends AbstractController
{
    /**
     * @Route("/service/myoffers", name="service_myoffers")
     */
    public function index()
    {
        return $this->render('service_myoffers/index.html.twig', [
            'controller_name' => 'ServiceMyoffersController',
        ]);
    }
}
