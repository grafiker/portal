<?php

namespace App\Controller;
use App\Document\Maillog;

class MaillogController
{
    public function mailtoDb($dm, $sender, $empfaenger, $subjekt, $body)
    {
        $task = new Maillog();
        $date = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $task->setSenddate($date);
        $task->setSender($sender);
        $task->setEmpfaenger($empfaenger);
        $task->setSubjekt($subjekt);
        $task->setBody($body);
        $dm->persist($task);
        $dm->flush();
        return true;
    }
}
