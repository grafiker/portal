<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceBusinessentryController extends AbstractController
{
    /**
     * @Route("/service/businessentry", name="service_businessentry")
     */
    public function index()
    {
        return $this->render('service_businessentry/index.html.twig', [
            'controller_name' => 'ServiceBusinessentryController',
        ]);
    }
}
