<?php

namespace App\Controller;

class PinoutputController
{
    function pinFilterOutput($dm, $anz, $sites, $sort, $filter, $owner, $autor, $location, $adminfilter) {
if(!is_numeric($sites)) {
    $sites = 'all';
}
        $filternew = explode("-", $filter);
        if(!isset($sites) or $sites == "all") {
            $page=0;
        } else {
            $page=$sites*$anz;
        }
        if(in_array("Search", $filternew)) {
            $searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$filternew[1].'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$filternew[1].'/si'),
                        ),
                    )
                );
            //$pindatagesamt = $dm->getRepository('App:Pins')->findBy($searchQuery,array('startdate' => 'ASC'));
            $pindata = $dm->getRepository('App:Pins')->findBy($searchQuery,array('startdate' => $sort), $anz, $page);
        $dm->flush();

        } else {
        if($owner == '' and  $autor == '') {
            $userfilter = '';
        } else if ($owner != '' and  $autor == '') {
            $userfilter = $owner;
        } else if ($owner == '' and  $autor != '') {
            $userfilter = $autor;          
        }
        if($filter == 'all')
        {
            if($owner == '' and  $autor == '') {
                //$pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter),array('startdate' => 'ASC'));
                $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter),array('startdate' => $sort), $anz, $page);
            } else {
                //$pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'username' => $userfilter),array('startdate' => 'ASC'));
                $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'username' => $userfilter),array('startdate' => $sort), $anz, $page);
            }
        } else {
            if($owner == '' and  $autor == '') {
                //$pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'pinkategorie' => $filter),array('startdate' => 'ASC'));
                $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'pinkategorie' => $filter),array('startdate' => $sort), $anz, $page);
            } else {
                //$pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'pinkategorie' => $filter, 'username' => $owner),array('startdate' => 'ASC'));
                $pindata = $dm->getRepository('App:Pins')->findBy(array('astitle' => $adminfilter, 'pinkategorie' => $filter, 'username' => $userfilter),array('startdate' => $sort), $anz, $page);
            }
        }
        $dm->flush();
    }
        return $pindata;
    }
}
