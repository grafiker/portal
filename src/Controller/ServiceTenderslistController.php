<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceTenderslistController extends Controller
{
    private $userManager;
    function __construct(ManagerRegistry $doctrine_mongodb, UserManagerInterface $userManager, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_userManager = $userManager;
        $this->urlGenerator = $urlGenerator;
    }
    /**
     * @Route("/service/tenderslist", name="service_tenderslist")
     */
    public function index()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findBy(array("username" => $users->getID(), "aktiv" => true),array('enddate' => 'ASC'));
        $docname = array();
        foreach($thistenders as $thistendersdoc) {
            foreach($thistendersdoc->getDocname() as $key => $thistendersdocname) {
                $docname[$thistendersdoc->getID()][$key] = $thistendersdocname;
            }
        }
        return $this->render('service_tenderslist/index.html.twig', [
            'controller_name' => 'ServiceTenderslistController',
            'tenderslist' => $thistenders,
            'docname' => $docname,
        ]);
    }
}
