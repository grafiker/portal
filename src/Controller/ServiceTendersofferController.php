<?php

namespace App\Controller;

use App\Document\Jobsoffers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceTendersofferController extends Controller
{
    private $userManager;
    function __construct(ManagerRegistry $doctrine_mongodb, UserManagerInterface $userManager, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_userManager = $userManager;
        $this->urlGenerator = $urlGenerator;
    }
    /**
     * @Route("/service/tendersoffer/{id}", name="service_tendersoffer")
     */
    public function index(Request $request, $id, \Swift_Mailer $mailer)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(array("id" => $id));

        $thistenderscache = $emdsend->getRepository('App:JobsCache')->findOneBy(array("id" => $thistenders->getCacheid()));

        $thistenderscacheowner = array();
        $thistenderscachegrafiker = array();
        $controlarray = array("productName", "productID", "productPriceCurrency", "productPriceExTax", "productPriceTax", "productPriceTaxValue", "productPriceIncTax", "productGrafikerKeywords");
        foreach($thistenderscache->getDatacache() as $key => $thistenderscacheall) {
            if (!in_array($key, $controlarray)) {
            $thistenderscacheowner[$key] = $thistenderscacheall;
            } else {
            $thistenderscachegrafiker[$key] = $thistenderscacheall;
            }
        }
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$thistenders->getPayout()["widgetid"]]);
        if(isset($_POST["storno"]) AND $_POST["storno"] == "1") {
            $thistendersoffersstorno = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("jobid" => $id, "username" => $users->getID()));
            $thistenderuser = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistenders->getUsername()));
            $thistendersoffersstorno->setStorno(true);
            $this->_doctrine_mongodb->getManager()->persist($thistendersoffersstorno);
            $this->_doctrine_mongodb->getManager()->flush();

            $usermail = $thistenderuser->getEmail();
            $subjetsys = "Der Grafiker " . $users->getUsername() . " hat sein Angebot storniert.";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
            ->setTo($usermail)
            ->setBody('Der Grafiker ' . $users->getUsername() . ' hat sein Angebot für die Ausschreibung "'.$thistenders->getAstitle().'" storniert.<br /><br />Hier kommen Sie zur Übersicht Ihrer Ausschreibung und zu dem Angebot der Grafiker:<br /><a href="https://grafiker.de/service/tenderslist/customer/'.$thistenders->getUsername().'">https://grafiker.de/service/tenderslist/customer/'.$thistenders->getUsername().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
            $mailer->send($message);
    
            $subjetsys = "Der Grafiker " . $users->getUsername() . " hat sein Angebot storniert.";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
            ->setTo('hbroeskamp@printshopcreator.com')
            ->setBody('Der Grafiker ' . $users->getUsername() . ' hat sein Angebot für die Ausschreibung "'.$thistenders->getAstitle().'" storniert.<br /><br />Hier kommen Sie zur Übersicht Ihrer Ausschreibung und zu dem Angebot der Grafiker:<br /><a href="https://grafiker.de/service/tenderslist/customer/'.$thistenders->getUsername().'">https://grafiker.de/service/tenderslist/customer/'.$thistenders->getUsername().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
            $mailer->send($message);

            $toroutes = "service_tenders_grafikerlist";
            return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id)));
        }

        $serficecheck = array();
foreach($thistenders->getService() as $key => $service) {
if($service == true) {
    $serficecheck[$key] = $key;
}
}
$jobout = 0;
$jobforyou = 0;
if($thistenders->getJobokay()) {
    $jobout = 1;
    $thistendersokay = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("jobid" => $id, "username" => $users->getID(), "jobokay" => true));
    if($thistendersokay) {
        $jobforyou = 1;
    }
}
        $docname = array();
        foreach($thistenders->getDocname() as $key => $thistendersdocname) {
            $docname[$thistenders->getID()][$key] = $thistendersdocname;
        }
        //\var_dump($_POST);
        //\var_dump($_FILES);
        if(isset($_POST["offerform"])) {
        $upload_dir = 'upload/uploads/ausschreibungenoffers/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
        //\var_dump($_FILES["docpdf1"]);
        if(isset($_FILES["docpdf1"])) {
        $file_name=$_FILES["docpdf1"]["name"];
        $file_tmp=$_FILES["docpdf1"]["tmp_name"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        
        $expensions= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors = false;
        $error = 1;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
           $error=2;
        }
        $filename = array();
        $infilename = array();
        if(!$errors) {
            //echo "ERROR: " . $error;
            $pdf[0]=md5($tmp[0].time()).'.'.$tmp[1];
            $filename[0]=$file_name;
            //\var_dump($pdf);
            move_uploaded_file($file_tmp,$upload_dir.$pdf[0]);
        }
    }
        $file_name2=$_FILES["docpdf2"]["name"];
        $file_tmp2=$_FILES["docpdf2"]["tmp_name"];
        $tmp2 = explode('.', $file_name2);
        $file_ext2 = end($tmp2);
      
        $expensions2= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors2 = false;
        $error2 = 1;
        if(in_array($file_ext2,$expensions2)=== false){
           $errors2=true;
           $error2=2;
        }
        $filename2 = array();
        $infilename2 = array();
        if(!$errors2) {
            //echo "ERROR: " . $error;
            $pdf2[0]=md5($tmp2[0].time()).'.'.$tmp2[1];
            $filename2[0]=$file_name2;
            move_uploaded_file($file_tmp2,$upload_dir.$pdf2[0]);
        }

        $thistendersoffers = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("jobid" => $id, "username" => $users->getID()));
        $thistendersusers = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistenders->getUsername()));
        $format = 'd.m.Y H:i';
        $dates = \DateTime::createFromFormat($format, date('d.m.Y H:i'));
$newmessengerarray = array();
$chatarraya = array();
$newdoc2 = array();
$newdoc2name = array();
$datesarray = array();
$count= 0;
$indocarray = false;
if($thistendersoffers) {
    if($thistendersoffers->getChatdata() != NULL) {
        $count = count($thistendersoffers->getChatdata());
        $chatarraya = $thistendersoffers->getChatdata();
    }
    if($thistendersoffers->getDocpdf2() != NULL and isset($pdf2[0])) {
    $aktdates[] = date('d.m.Y H:i:s') . "::--::--::grafiker";
    \var_dump($datesarray);
    $newdoc2 = array_merge($thistendersoffers->getDocpdf2(), $pdf2);
    $newdoc2name = array_merge($thistendersoffers->getDocpdf2name(), $filename2);
    $datesarray = array_merge($thistendersoffers->getDoc2date(), $aktdates);
    $indocarray = true;
    } else {
        if(isset($pdf2[0])) {
        $datesarray[] = date('d.m.Y H:i:s') . "::--::--::grafiker";
        $newdoc2[] = $pdf2[0];
        $newdoc2name[] = $filename2[0];
        $indocarray = true;
        }
    }
    

    $usermail = $thistendersusers->getEmail();
    $subjetsys = "Neue Nachricht von - " . $users->getUsername();
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo($usermail)
    ->setBody('Der Grafiker "'.$users->getUsername().'" hat Ihnen eine Nachricht geschickt.<br /><br />Hier kommen Sie zur Nachricht:<br /><a href="https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getid().'/'.$thistendersoffers->getid().'">https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getid().'/'.$thistendersoffers->getid().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

    $subjetsys = "Neue Nachricht von - " . $users->getUsername();
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo('hbroeskamp@printshopcreator.com')
    ->setBody('Der Grafiker "'.$users->getUsername().'" hat Ihnen eine Nachricht geschickt.<br /><br />Hier kommen Sie zur Nachricht:<br /><a href="https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getid().'/'.$thistendersoffers->getid().'">https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getid().'/'.$thistendersoffers->getid().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

} else {}
        if($thistendersoffers) {
            $joboffer = $thistendersoffers;
        } else {
            $joboffer = new Jobsoffers();
        }
        if(isset($request->request->all()["currency-field"]) and $request->request->all()["currency-field"] != "" and !$thistendersoffers) {
            $grafikerprice = \explode(",", $request->request->all()["currency-field"]);
            $grafikerpricenew = \str_replace(".", "", $grafikerprice[0]);
            $joboffer->setOfferprice($grafikerpricenew);
            $joboffer->setOfferpriceout($request->request->all()["currency-field"]);
        }
            $chatarrayb = array();
            if($request->request->all()["messenges"] != "") {
            $chatarrayb[$count] = $request->request->all()["messenges"] . "::--::--::" . date('d.m.Y H:i:s') . "::--::--::grafiker";
            } else if((isset($pdf2[0]) or isset($pdf[0])) AND $request->request->all()["messenges"] == "") {
                $chatarrayb[$count] = "Es wurde eine neue Datei gesendet!::--::--::" . date('d.m.Y H:i:s') . "::--::--::grafiker";
            } else if(!$thistendersoffers) {
                $chatarrayb[$count] = "Es wurde ein Angebot abgegeben!::--::--::" . date('d.m.Y H:i:s') . "::--::--::grafiker";
            }
            if($chatarrayb) {
            $joboffer->setStartdate($dates);
            if($thistendersoffers) { 
                //array_merge($chatgrafikerpusharrayin, $chatarray);
                $chatgrafikerpusharrayin = $chatarraya + $chatarrayb;
                //\var_dump($chatgrafikerpusharrayin);
                $joboffer->setChatdata($chatgrafikerpusharrayin);
            } else {
                $joboffer->setChatdata($chatarrayb);
            }
            $joboffer->setUsername($users->getID());
            if(isset($pdf[0])) {
            $joboffer->setDocpdf1([$pdf[0]]);
            }
            if(isset($pdf2[0])) {
            if($indocarray) {
            $joboffer->setDocpdf2($newdoc2);
            } else {
                $joboffer->setDocpdf2([$pdf2[0]]);
            }
            }
            if(isset($filename[0])) {
            $joboffer->setDocpdf1name([$filename[0]]);
            }
            if(isset($filename2[0])) {
                if($indocarray) {
            $joboffer->setDocpdf2name($newdoc2name);
                } else {
                    $datesarray[] = date('d.m.Y H:i:s') . "::--::--::grafiker";
                    $joboffer->setDocpdf2name([$filename2[0]]);
                    $joboffer->setDoc2date($datesarray);
                }
            $joboffer->setDoc2date($datesarray);
            }
            $joboffer->setJobid($id);
            $this->_doctrine_mongodb->getManager()->persist($joboffer);
            $this->_doctrine_mongodb->getManager()->flush();

            if(!$thistendersoffers) {
            $usermail = $thistendersusers->getEmail();
            $subjetsys = "Neues Angebot - " . $users->getUsername();
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
            ->setTo($usermail)
            ->setBody('Der Grafiker "'.$users->getUsername().'" hat ein Angebot auf Ihre Auschreibung gemacht.<br /><br />Hier kommen Sie zum Angebot und zur Nachricht:<br /><a href="https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getID().'/'.$joboffer->getID().'">https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getID().'/'.$joboffer->getID().'</a><br /><br />Hier kommen Sie auf Ihre Angebotsliste: <a href="https://grafiker.de/service/tenderslist/customer/'.$thistendersusers->getID().'">https://grafiker.de/service/tenderslist/customer/'.$thistendersusers->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
            $mailer->send($message);
    
            $subjetsys = "Neues Angebot - " . $users->getUsername();
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
            ->setTo('hbroeskamp@printshopcreator.com')
            ->setBody('Der Grafiker "'.$users->getUsername().'" hat ein Angebot auf Ihre Auschreibung gemacht.<br /><br />Hier kommen Sie zum Angebot und zur Nachricht:<br /><a href="https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getID().'/'.$joboffer->getID().'">https://grafiker.de/service/tenders/customer/details/'.$id.'/'.$users->getID().'/'.$joboffer->getID().'</a><br /><br />Hier kommen Sie auf Ihre Angebotsliste: <a href="https://grafiker.de/service/tenderslist/customer/'.$thistendersusers->getID().'">https://grafiker.de/service/tenderslist/customer/'.$thistendersusers->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
            $mailer->send($message);
            }
        }
            //\var_dump($joboffer);

            $toroutes = "service_tendersoffer";
            return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id)));

    } 
        $thistendersoffers = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("jobid" => $id, "username" => $users->getID()));
        $newmessengerarray = array();
        if($thistendersoffers) {
        if($thistendersoffers->getChatdata() != NULL) {
            $newmessengerarray = $thistendersoffers->getChatdata();
        }
        }
if($thistenders->getPrice()) {
$testprice = $thistenders->getPrice();
$outprice = explode(".", $testprice);
if(isset($outprice[1])) {
$outprice = $outprice[0]. "," . $outprice[1];
} else {
    $outprice = $outprice[0]. ",00 €";
}
} else {
    $outprice = "";
}
$tendersoffermystorno = array();
$thistendersinlisstorno = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("username" => $users->getID(), "jobid" => $id, "storno" => true));
    if($thistendersinlisstorno) {
        $tendersoffermystorno[$id] = true;
    } else {
        $tendersoffermystorno[$id] = false;
    }
        $thistendersofferscount = $emdsend->getRepository('App:Jobsoffers')->findBy(array("jobid" => $id));
        $thisoffescount = count($thistendersofferscount);
        return $this->render('service_tendersoffer/index.html.twig', [
            'controller_name' => 'ServiceTendersofferController',
            'thistenders' => $thistenders,
            'docname' => $docname,
            'newmessengerarray' => $newmessengerarray,
            'thisoffescount' => $thisoffescount,
            'jobout' => $jobout,
            'jobforyou' => $jobforyou,
            'thistendersoffers' => $thistendersoffers,
            'outprice' => $outprice,
            'serficecheck' => $serficecheck,
            'tendersoffermystorno' => $tendersoffermystorno,
            'thistenderscacheowner' => $thistenderscacheowner,
            'thistenderscachegrafiker' => $thistenderscachegrafiker,
            'thiswidget' => $thiswidget,
        ]);
    }

    /**
     * @Route("/service/tenders/customer/details/{id}/{uid}/{offerid}", name="service_tendersoffer_customer")
     */
    public function customerAction(Request $request, $id, $uid, $offerid, \Swift_Mailer $mailer)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }

        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistendersoffers = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("id" => $offerid));
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(array("id" => $id));
        $thistenderscache = $emdsend->getRepository('App:JobsCache')->findOneBy(array("id" => $thistenders->getCacheid()));

        $thistenderscacheowner = array();
        $thistenderscachegrafiker = array();
        $controlarray = array("productName", "productID", "productPriceCurrency", "productPriceExTax", "productPriceTax", "productPriceTaxValue", "productPriceIncTax", "productGrafikerKeywords");
        foreach($thistenderscache->getDatacache() as $key => $thistenderscacheall) {
            if (!in_array($key, $controlarray)) {
            $thistenderscacheowner[$key] = $thistenderscacheall;
            } else {
            $thistenderscachegrafiker[$key] = $thistenderscacheall;
            }
        }

        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$thistenders->getPayout()["widgetid"]]);
$serficecheck = array();
foreach($thistenders->getService() as $key => $service) {
if($service == true) {
    $serficecheck[$key] = $key;
}
}

if(!$userexist) {
    $users = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistendersoffers->getUsername()));
}

$thistendersoffersuser = $emdsend->getRepository('App:User')->findOneBy(array("id" => $uid));
        $docname = array();
        foreach($thistenders->getDocname() as $key => $thistendersdocname) {
            $docname[$thistenders->getID()][$key] = $thistendersdocname;
        }
        //\var_dump($_POST);
        //\var_dump($_FILES);
        if(isset($_POST["offerform"])) {
        $upload_dir = 'upload/uploads/ausschreibungenoffers/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
        //\var_dump($_FILES["docpdf1"]);

        $file_name2=$_FILES["docpdf2"]["name"];
        $file_tmp2=$_FILES["docpdf2"]["tmp_name"];
        $tmp2 = explode('.', $file_name2);
        $file_ext2 = end($tmp2);
      
        $expensions2= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors2 = false;
        $error2 = 1;
        if(in_array($file_ext2,$expensions2)=== false){
           $errors2=true;
           $error2=2;
        }
        $filename2 = array();
        $infilename2 = array();
        if(!$errors2) {
            //echo "ERROR: " . $error;
            $pdf2[0]=md5($tmp2[0].time()).'.'.$tmp2[1];
            $filename2[0]=$file_name2;
            //\var_dump($pdf);
            move_uploaded_file($file_tmp2,$upload_dir.$pdf2[0]);
        }

        
        $format = 'd.m.Y H:i';
        $dates = \DateTime::createFromFormat($format, date('d.m.Y H:i'));
$newmessengerarray = array();
$chatarraya = array();
$newdoc2 = array();
$newdoc2name = array();
$datesarray = array();
$count= 0;
if($thistendersoffers) {
    if($thistendersoffers->getChatdata() != NULL) {
        $count = count($thistendersoffers->getChatdata());
        $chatarraya = $thistendersoffers->getChatdata();
    }
    if($thistendersoffers->getDocpdf2() != NULL and isset($pdf2[0])) {
    $aktdates[] = date('d.m.Y H:i:s') . "::--::--::customer";
    $newdoc2 = array_merge($thistendersoffers->getDocpdf2(), $pdf2);
    $newdoc2name = array_merge($thistendersoffers->getDocpdf2name(), $filename2);
    $datesarray = array_merge($thistendersoffers->getDoc2date(), $aktdates);
    } else {
        if(isset($pdf2[0])) {
        $datesarray[] = date('d.m.Y H:i:s') . "::--::--::customer";
        $newdoc2[] = $pdf2[0];
        $newdoc2name[] = $filename2[0];
        }
    }


    $usermail = $thistendersoffersuser->getEmail();
    $subjetsys = "Neue Nachricht zu deinem Angebot (Ausschreibung - " . $thistenders->getAstitle() . ")";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo($usermail)
    ->setBody('Neue Nachricht zu deinem Angebot (Ausschreibung - "' . $thistenders->getAstitle() . '").<br /><br />Hier kommst du zu dem Angebot:<br /><a href="http://grafiker.de/service/tendersoffer/'.$id.'">http://grafiker.de/service/tendersoffer/'.$id.'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

    $subjetsys = "Neue Nachricht zu deinem Angebot (Ausschreibung - " . $thistenders->getAstitle() . ")";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo('hbroeskamp@printshopcreator.com')
    ->setBody('Neue Nachricht zu deinem Angebot (Ausschreibung - "' . $thistenders->getAstitle() . '")<br /><br />>Hier kommst du zu dem Angebot:<br /><a href="http://grafiker.de/service/tendersoffer/'.$id.'">http://grafiker.de/service/tendersoffer/'.$id.'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);
}
        if($thistendersoffers) {
            $joboffer = $thistendersoffers;
        } else {
            $joboffer = new Jobsoffers();
        }
            $chatarrayb = array();
            if($request->request->all()["messenges"] != "") {
            $chatarrayb[$count] = $request->request->all()["messenges"] . "::--::--::" . date('d.m.Y H:i:s') . "::--::--::customer";
            } else if((isset($pdf2[0]) or isset($pdf[0])) AND $request->request->all()["messenges"] == "") {
                $chatarrayb[$count] = "Es wurde eine neue Datei gesendet!::--::--::" . date('d.m.Y H:i:s') . "::--::--::customer";
            }
            if($chatarrayb) {
            $joboffer->setStartdate($dates);
            if($thistendersoffers) { 
                //array_merge($chatgrafikerpusharrayin, $chatarray);
                $chatgrafikerpusharrayin = $chatarraya + $chatarrayb;
                //\var_dump($chatgrafikerpusharrayin);
                $joboffer->setChatdata($chatgrafikerpusharrayin);
            } else {
                $joboffer->setChatdata($chatarrayb);
            }
            if(isset($pdf2[0])) {
            $joboffer->setDocpdf2($newdoc2);
            }
            if(isset($filename2[0])) {
            $joboffer->setDocpdf2name($newdoc2name);
            $joboffer->setDoc2date($datesarray);
            }
            $joboffer->setJobid($id);
            $this->_doctrine_mongodb->getManager()->persist($joboffer);
            $this->_doctrine_mongodb->getManager()->flush();
        }

            $toroutes = "service_tendersoffer_customer";
            return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id, 'uid' => $uid, 'offerid' => $offerid)));

    } 
        $thistendersoffers = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("id" => $offerid));
        $newmessengerarray = array();
        if($thistendersoffers) {
        if($thistendersoffers->getChatdata() != NULL) {
            $newmessengerarray = $thistendersoffers->getChatdata();
        }
        }

        $thistendersofferscount = $emdsend->getRepository('App:Jobsoffers')->findBy(array("id" => $offerid));
        $thistendersoffersokay = $emdsend->getRepository('App:Jobsoffers')->findBy(array("jobid" => $id, "jobokay" => true));
        

        $thisoffescount = count($thistendersofferscount);
        return $this->render('service_tenders_customer_details/index.html.twig', [
            'controller_name' => 'ServiceTendersofferController',
            'thistenders' => $thistenders,
            'docname' => $docname,
            'newmessengerarray' => $newmessengerarray,
            'thisoffescount' => $thisoffescount,
            'offersusername' => $thistendersoffersuser->getUsername(),
            'offersuserpic' => $thistendersoffersuser->getUserpic(),
            'uid' => $uid,
            'id' => $id,
            'users' => $users,
            'offerid' => $offerid,
            'jobokaycount' => count($thistendersoffersokay),
            'thistendersoffers' => $thistendersoffers,
            'thiswidgetlogo' => $thiswidget->getCssPosition(),
            'thiswidget' => $thiswidget,
            'serficecheck' => $serficecheck,
            'thistenderscacheowner' => $thistenderscacheowner,
            'thistenderscachegrafiker' => $thistenderscachegrafiker,
        ]);
    }
    /**
     * @Route("/service/tenders/customer/okay/{id}/{uid}/{offerid}", name="service_tendersoffer_customerdata")
     */
    public function customerdataAction(Request $request, $id, $uid, $offerid, \Swift_Mailer $mailer)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(array("id" => $id));
        $thistendersoffers = $emdsend->getRepository('App:Jobsoffers')->findOneBy(array("id" => $offerid));
        $thistendersoffersuser = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistendersoffers->getUsername()));
        $thistenderssuser = $emdsend->getRepository('App:User')->findOneBy(array("id" => $thistenders->getUsername()));

        if(isset($_POST["ok"]) AND $_POST["ok"] == "1") {
        $thistenders->setJobokay(true);
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();
        $thistendersoffers->setJobokay(true);
        $this->_doctrine_mongodb->getManager()->persist($thistendersoffers);
        $this->_doctrine_mongodb->getManager()->flush();
        $toroutes = "service_tendersoffer_customer";

    $usermail = $thistendersoffersuser->getEmail();
    $subjetsys = "Dein Angebot auf die Ausschreibung ".$thistenders->getAstitle()." wurde angenommen";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo($usermail)
    ->setBody('Der Auftraggeber hat dein Angebot zur Ausschreibung - "' . $thistenders->getAstitle() . '" angenommen.<br /><br />Hier kommst du zu dem Angebot:<br /><a href="http://grafiker.de/service/tendersoffer/'.$id.'">http://grafiker.de/service/tendersoffer/'.$id.'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

    $subjetsys = "Dein Angebot auf die Ausschreibung ".$thistenders->getAstitle()." wurde angenommen";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo('hbroeskamp@printshopcreator.com')
    ->setBody('Der Auftraggeber hat dein Angebot zur Ausschreibung - "' . $thistenders->getAstitle() . '" angenommen.<br /><br />Hier kommst du zu dem Angebot:<br /><a href="http://grafiker.de/service/tendersoffer/'.$id.'">http://grafiker.de/service/tendersoffer/'.$id.'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);


    $usermail = $thistenderssuser->getEmail();
    $subjetsys = "Sie haben das Angebot auf die Ausschreibung ".$thistenders->getAstitle()." angenommen";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo($usermail)
    ->setBody('Sie haben das Angebot auf die Ausschreibung - "' . $thistenders->getAstitle() . '" angenommen.<br /><br />Hier kommen Sie zu dem Angebot:<br /><a href="http://grafiker.de/service/tenders/customer/details/'.$id.'/'.$thistenderssuser->getID().'/'.$thistendersoffers->getID().'">http://grafiker.de/service/tenders/customer/details/'.$id.'/'.$thistenderssuser->getID().'/'.$thistendersoffers->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

    $subjetsys = "Sie haben das Angebot auf die Ausschreibung ".$thistenders->getAstitle()." wurde angenommen";
    $message = (new \Swift_Message())
    ->setSubject($subjetsys)
    ->setFrom(['notifications@grafiker.de' => 'Grafiker.de - Ausschreibungsservice'])
    ->setTo('hbroeskamp@printshopcreator.com')
    ->setBody('Sie haben das Angebot auf die Ausschreibung - "' . $thistenders->getAstitle() . '" angenommen.<br /><br /Hier kommen Sie zu dem Angebot:<br /><a href="http://grafiker.de/service/tenders/customer/details/'.$id.'/'.$thistenderssuser->getID().'/'.$thistendersoffers->getID().'">http://grafiker.de/service/tenders/customer/details/'.$id.'/'.$thistenderssuser->getID().'/'.$thistendersoffers->getID().'</a><br /><br />Mit besten Grüßen,<br />Ihr Grafker Team');
    $mailer->send($message);

        return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id, 'uid' => $uid, 'offerid' => $offerid, )));
        }
        if(isset($_POST["storno"]) AND $_POST["storno"] == "1") {
            $thistenders->setStorno(true);
            $this->_doctrine_mongodb->getManager()->persist($thistenders);
            $this->_doctrine_mongodb->getManager()->flush();
        $toroutes = "service_tenders_customerlist";
        return new RedirectResponse($this->urlGenerator->generate($toroutes, array('uid' => $thistenders->getUsername())));
        }
        if(isset($_POST["out"]) AND $_POST["out"] == "1") {
            $thistenders->setJobende(true);
            $this->_doctrine_mongodb->getManager()->persist($thistenders);
            $this->_doctrine_mongodb->getManager()->flush();
            $toroutes = "service_tendersoffer_customer";
        return new RedirectResponse($this->urlGenerator->generate($toroutes, array('id' => $id, 'uid' => $uid, 'offerid' => $offerid, )));
        }
    }
    /**
     * @Route("/service/tenders/customer/storno/{id}", name="service_tendersoffer_customerstorno")
     */
    public function customerstornoAction(Request $request, $id, \Swift_Mailer $mailer)
    {
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(array("id" => $id));
        if(isset($_POST["storno"]) AND $_POST["storno"] == "1") {
            $thistenders->setStorno(true);
            $this->_doctrine_mongodb->getManager()->persist($thistenders);
            $this->_doctrine_mongodb->getManager()->flush();
        $toroutes = "service_tenders_customerlist";
        return new RedirectResponse($this->urlGenerator->generate($toroutes, array('uid' => $thistenders->getUsername())));
        }
    }
}