<?php

namespace App\Controller;

use App\Document\Banner;
use App\Document\Bannerreload;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class WerbungController extends Controller
{
    /**
     * Create
     * @Route("/banner/{position}", name="werbung")
     */
    public function createAction(Request $request, $position)
    {
        $emdstart = $this->get('doctrine_mongodb')->getManager();
        $bannerobenklicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('oben')->field('maxKlicks')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();
        $bannerobenviews = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('oben')->field('maxViews')->equals(0)->field('reloadViews')->equals(true)->getQuery()->toArray();
        $bannerobenviewsClicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('oben')->field('maxViews')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();
        $bannerrechtsklicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('rechts')->field('maxKlicks')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();
        $bannerrechtsviews = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('rechts')->field('maxViews')->equals(0)->field('reloadViews')->equals(true)->getQuery()->toArray();
        $bannerrechtsviewsClicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('rechts')->field('maxViews')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();
        $bannermobilklicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('mobil')->field('maxKlicks')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();
        $bannermobilviews = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('mobil')->field('maxViews')->equals(0)->field('reloadViews')->equals(true)->getQuery()->toArray();
        $bannermobilviewsClicks = $emdstart->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('mobil')->field('maxViews')->equals(0)->field('reloadClicks')->equals(true)->getQuery()->toArray();

        if($bannerobenklicks) {
            foreach($bannerobenklicks as $bannerobenklicks) {
            $bannerobenklicks->setMaxKlicks(100000000);
            }
        }
        if($bannerobenviews) {
            foreach($bannerobenviews as $bannerobenviews) {
            $bannerobenviews->setMaxViews(100000000);
            }
        }
        if($bannerobenviewsClicks) {
            foreach($bannerobenviewsClicks as $bannerobenviewsClicks) {
            $bannerobenviewsClicks->setMaxViews(100000000);
            }
        }
        if($bannerrechtsklicks) {
            foreach($bannerrechtsklicks as $bannerrechtsklicks) {
                $bannerrechtsklicks->setMaxKlicks(100000000);
                }
        }
        if($bannerrechtsviews) {
            foreach($bannerrechtsviews as $bannerrechtsviews) {
                $bannerrechtsviews->setMaxViews(100000000);
                }
        }
        if($bannerrechtsviewsClicks) {
            foreach($bannerrechtsviewsClicks as $bannerrechtsviewsClicks) {
                $bannerrechtsviewsClicks->setMaxViews(100000000);
                }
        }
        if($bannermobilklicks) {
            foreach($bannermobilklicks as $bannermobilklicks) {
                $bannermobilklicks->setMaxKlicks(100000000);
                }
        }
        if($bannermobilviews) {
            foreach($bannermobilviews as $bannermobilviews) {
                $bannermobilviews->setMaxViews(100000000);
                }
        }
        if($bannermobilviewsClicks) {
            foreach($bannermobilviewsClicks as $bannermobilviewsClicks) {
                $bannermobilviewsClicks->setMaxViews(100000000);
                }
        }
        $emdstart->flush();

        if (! isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {

            $client_ip = $_SERVER['REMOTE_ADDR'];
            
            }
            
            else {
            
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            
            }
        
        if($position == "oben") {
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $bannerobencount = $emdsend->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('oben')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->count()->getQuery()->execute();
        if($bannerobencount) {
        $countoben = mt_rand(0, $bannerobencount-1);
        $banneroben = $emdsend->createQueryBuilder('App:Banner')->limit(1)->skip($countoben)->field('disenable')->equals(NULL)->field('position')->equals('oben')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->getQuery()->toArray();
        foreach($banneroben as $banneroben) {
        if($banneroben->getBannerlinkClick() != "") {
            $javascriptinfo = 'linkwerbung';
            $target = $banneroben->getTarget();
            $bannerviewlink = $banneroben->getBannerlinkView();
            $bannerclicklink = "/werbung/" . $banneroben->getId(); //$banneroben->getBannerlinkClick()
            $alttag = $banneroben->getAlttag();
        } else {
            $bannerviewlink = $banneroben->getQuelltext();
            $bannerclicklink = false;
            $alttag = false;
            $target = false;
            $javascriptinfo = 'quelltext';
        }
        $banneroben->setViews($banneroben->getViews()+1);
        $banneroben->setMaxViews($banneroben->getMaxViews()-1);
        $emdsend->flush();
    }
} else {
    $bannerviewlink = false;
    $bannerclicklink = false;
    $alttag = false;
    $target = false;
    $javascriptinfo = false;
}
$em = $this->get('doctrine_mongodb')->getManager();
$infotext = $em->getRepository('App:Infotext')->findAll();
if($infotext) {
    foreach($infotext as $infotext) {
        $infoausgabe = $infotext->getInfotext();
    }
} else {
    $infoausgabe = false;
}
        return $this->render('werbung/index.html.twig', [
            'controller_name' => 'WerbungController',
            'bannerviewlink' => $bannerviewlink,
            'bannerclicklink' => $bannerclicklink,
            'alttag' => $alttag,
            'target' => $target,            
            'javascriptinfo' => $javascriptinfo,
            'allbanneroben' => $bannerobencount,
            'infoausgabe' => $infoausgabe,
        ]);
        } else if($position == "rechts") {
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $bannerrechtscount = $emdsend->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('rechts')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->count()->getQuery()->execute();
        if($bannerrechtscount) {
        $countrechts = mt_rand(0, $bannerrechtscount-1);
        $bannerrechts = $emdsend->createQueryBuilder('App:Banner')->limit(1)->skip($countrechts)->field('disenable')->equals(NULL)->field('position')->equals('rechts')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->getQuery()->toArray();
        foreach($bannerrechts as $bannerrechts) {
        if($bannerrechts->getBannerlinkClick() != "") {
            $javascriptinfo = 'linkwerbung';
            $target = $bannerrechts->getTarget();
            $bannerviewlink = $bannerrechts->getBannerlinkView();
            $bannerclicklink = "/werbung/" . $bannerrechts->getId(); //$banneroben->getBannerlinkClick()
            $alttag = $bannerrechts->getAlttag();
        } else {
            $bannerviewlink = $bannerrechts->getQuelltext();
            $bannerclicklink = false;
            $alttag = false;
            $target = false;
            $javascriptinfo = 'quelltext';
        }
        $bannerrechts->setViews($bannerrechts->getViews()+1);
        $bannerrechts->setMaxViews($bannerrechts->getMaxViews()-1);
        $emdsend->flush();
    }
} else {
    $bannerviewlink = false;
    $bannerclicklink = false;
    $alttag = false;
    $target = false;
    $javascriptinfo = false;
}
            return $this->render('werbung/index.html.twig', [
                'controller_name' => 'WerbungController',
                'bannerviewlink' => $bannerviewlink,
                'bannerclicklink' => $bannerclicklink,
                'alttag' => $alttag,
                'target' => $target,
                'javascriptinfo' => $javascriptinfo,
                'allbannerrechts' => $bannerrechtscount,
                'infoausgabe' => false,
            ]);
        } else {
            $emdsend = $this->get('doctrine_mongodb')->getManager();
        $bannermobilcount = $emdsend->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('mobil')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->count()->getQuery()->execute();
        if($bannermobilcount) {
        $countmobil = mt_rand(0, $bannermobilcount-1);
        $bannermobil = $emdsend->createQueryBuilder('App:Banner')->limit(1)->skip($countmobil)->field('disenable')->equals(NULL)->field('position')->equals('mobil')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->getQuery()->toArray();
        foreach($bannermobil as $bannermobil) {
        if($bannermobil->getBannerlinkClick() != "") {
            $javascriptinfo = 'linkwerbung';
            $target = $bannermobil->getTarget();
            $bannerviewlink = $bannermobil->getBannerlinkView();
            $bannerclicklink = "/werbung/" . $bannermobil->getId(); //$banneroben->getBannerlinkClick()
            $alttag = $bannermobil->getAlttag();
        } else {
            $bannerviewlink = $bannermobil->getQuelltext();
            $bannerclicklink = false;
            $alttag = false;
            $target = false;
            $javascriptinfo = 'quelltext';
        }
        $bannermobil->setViews($bannermobil->getViews()+1);
        $bannermobil->setMaxViews($bannermobil->getMaxViews()-1);
        $emdsend->flush();
    }
} else {
    $bannerviewlink = false;
    $bannerclicklink = false;
    $alttag = false;
    $target = false;
    $javascriptinfo = false;
}
            return $this->render('werbung/index.html.twig', [
                'controller_name' => 'WerbungController',
                'bannerviewlink' => $bannerviewlink,
                'bannerclicklink' => $bannerclicklink,
                'alttag' => $alttag,
                'target' => $target,
                'javascriptinfo' => $javascriptinfo,
                'allbannermobil' => $bannermobilcount,
                'infoausgabe' => false,
            ]);
        }
    }
    /**
     * Click
     * @Route("/werbung/{click}", name="click_werbung")
     */
    public function clickAction(Request $request, $click)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $link = $em->getRepository('App:Banner')->findOneById($click);
        $link->setClicks($link->getClicks()+1);
        $em->flush();
        return $this->render('werbung/click.html.twig', [
            'controller_name' => 'WerbungController',
            'bannerclicklink' => $link->getBannerlinkClick(),
        ]);
    }
}
