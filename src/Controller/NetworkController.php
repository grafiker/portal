<?php

namespace App\Controller;

use App\AppBundle\Form\NetworkFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class NetworkController extends Controller
{
    /**
     * @Route("/profile/network/{getusername}", name="network")
     */
    public function formActon(RequestStack $requestStack, Request $request, $getusername, \Swift_Mailer $mailer)
    {
        $builder = $this->createForm(NetworkFormType::class);
        $builder->handleRequest($request);

            if ($builder->isSubmitted() && $builder->isValid()) {
                $task = $builder->getData();

            }
        return $this->render('userprofil/networkform.html.twig', [
            'form' => $builder->createView(),
            'controller_name' => 'NetworkController',
            'getusername' => $getusername,
        ]);
    }
}
