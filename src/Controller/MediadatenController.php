<?php

namespace App\Controller;

class MediadatenController
{
    function mediadaten($dm, $table, $field, $search, $field2 = false, $search2 = false) {
        if(!$field) {
            //MongoDB Count
            $mediadata = $dm->createQueryBuilder('App:'.$table)->count()->getQuery()->execute();
        } else {
            if(!$field2) {
            //MongoDB Count mit Einschränkung
                $mediadata = $dm->createQueryBuilder('App:'.$table)->field($field)->equals($search)->count()->getQuery()->execute();
            } else {
                $mediadata = $dm->createQueryBuilder('App:'.$table)->field($field)->equals($search)->field($field2)->equals($search2)->count()->getQuery()->execute();    
            }
        }
        return $mediadata;
    }
}
