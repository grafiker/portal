<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceMyinvoicesController extends AbstractController
{
    /**
     * @Route("/service/myinvoices", name="service_myinvoices")
     */
    public function index()
    {
        return $this->render('service_myinvoices/index.html.twig', [
            'controller_name' => 'ServiceMyinvoicesController',
        ]);
    }
}
