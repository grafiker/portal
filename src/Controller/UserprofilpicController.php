<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserprofilpicController extends AbstractController
{
    /**
     * @Route("/profile/userpic", name="userprofil_userpic")
     */
    public function index()
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}
$upload_dir = 'upload/uploads/userpics/';
$name = $upload_dir.'/'.$username . '_userpic';

        if(file_exists($name.".png")) {
          $logoexist=true;
          $logolink=$name.".png";
        }elseif(file_exists($name.".gif")) {
            $logoexist=true;
            $logolink=$name.".gif";
        } elseif(file_exists($name.".jpg")) {
            $logoexist=true;
            $logolink=$name.".jpg";
        } else {
            $logoexist=false;
            $logolink=false;
        }
        return $this->render('userprofil/userpic.html.twig', [
            'controller_name' => 'UserprofilFirmlogoController',
            'logoexist' => $logoexist,
            'logolink' => $logolink,
        ]);
    }
}
