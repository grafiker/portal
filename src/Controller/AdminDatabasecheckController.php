<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class AdminDatabasecheckController extends Controller
{
    /**
     * @Route("/admin/databasecheck", name="admin_databasecheck")
     */
    public function index()
    {
        $m = $this->container->get('doctrine_mongodb.odm.default_connection'); //Mongeverbindung wird hergestellt
        $db = $m->selectDatabase('grafiker'); //Datenbank wird ausgewählt
        $colnames = array();
        foreach($db->listCollections() as $listall) // eine Schleife mit den Tabellen wird gestartet
        {
            $colnames[] .= $listall->getCollection()->getCollectionName(); // Namen aller Tabellen wird in der Schleife ermittelt

        }
        return $this->render('admin/databasecheck.html.twig', [
            'controller_name' => 'AdminDatabasecheckController',
            'tablesname' => $colnames,
            'output' => false,
        ]);
}
    /**
     * @Route("/admin/databasecheck/search/{table}/{order}/{anz}", name="admin_databasecheck_searching")
     */
    public function searchOutput(Request $request, $table, $order, $anz)
    {
        $temp = (array) $request->request->all();
        $searchQuery = array(
            '$or' => array(
                array(
                    $temp["spalte"] => new \MongoRegex('/'.$temp["searchtext"].'/si'),
                    ),
                )
            );
        $db = $this->get('doctrine_mongodb')->getManager();
        $tableoutput = $db->getRepository('App:'.$table)->findBy($searchQuery,array('id' => $order),$anz);
        echo"<pre>";
        print_r($tableoutput);
        echo"</pre>";
        return $this->render('admin/databasecheck-output.html.twig', [
            'controller_name' => 'AdminDatabasecheckController',
            'output' => true,
            'tableoutput' => $tableoutput,
        ]);
    }

    /**
     * @Route("/admin/databasecheck/{table}/{order}/{anz}", name="admin_databasecheck_output")
     */
    public function createOutput($table, $order, $anz)
    {
        $db = $this->get('doctrine_mongodb')->getManager();
        $tableoutput = $db->getRepository('App:'.$table)->findBy(array(),array('id' => $order),$anz);
        echo"<pre>";
        var_dump($tableoutput);
        echo"</pre>";
        return $this->render('admin/databasecheck-output.html.twig', [
            'controller_name' => 'AdminDatabasecheckController',
            'output' => true,
            'tableoutput' => $tableoutput,
        ]);
    }
}
