<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShopsController extends AbstractController
{
    /**
     * @Route("/shops", name="shops")
     */
    public function index()
    {
        return $this->render('shops/index.html.twig', [
            'controller_name' => 'ShopsController',
        ]);
    }
}
