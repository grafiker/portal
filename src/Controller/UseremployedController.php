<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UseremployedController extends Controller
{
    /**
     * @Route("/useremployed", name="useremployed")
     */
    public function index(Request $request)
    {
        $newarray=$request->request->all();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $dm->getRepository('App:User')->findOneByUsername($newarray["username"]);
        if($newarray["setemploy"] == "true") {
            $user->setEmploy(true);
            $ausgabe="Employ = true";
        } else {
            $user->setEmploy(false);  
            $ausgabe="Employ = false";
        }
        $dm->persist($user);
        $dm->flush();


        return $this->render('useremployed/index.html.twig', [
            'controller_name' => 'UseremployedController',
            'ausgabe' => $ausgabe,
        ]);
    }
}
