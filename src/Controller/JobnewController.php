<?php

namespace App\Controller;
use App\Document\Job;
use App\AppBundle\Form\JobFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class JobnewController extends Controller
{
    /**
     * @Route("/job/new", name="job_new")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {   
if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
    $firma = $user->getFirma();
} else {
    $username = 'Kein Mitglied!';
}
$tempnurrequest = (array) $request;
$temppostrequestohne = (array) $_POST;
$temppostrequest = (array) $_POST["form"];
$tempfilesrequest = (array) $_FILES["form"];
//$tempohne = (array) $request->request->all();
$temp = $request->request->get('form');
//$temp = (array) $_POST["form"];
if($username == "Sniky") {
echo"<pre>";
var_dump($temp);
echo"</pre>";
}
        $task = new Job();
        $pdf = array();
        $upload_dir = 'upload/uploads/pdf/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
        $file_name=$_FILES["form"]["name"]["docpdf1"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf1"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
            $pdf[0]=md5($tmp[0].time()).'.'.$tmp[1];
            move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[0]);
        }
        $file_name=$_FILES["form"]["name"]["docpdf2"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf2"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
            $pdf[1]=md5($tmp[0].time()).'.'.$tmp[1];
            move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[1]);
        }
        $file_name=$_FILES["form"]["name"]["docpdf3"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf3"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
        $pdf[2]=md5($tmp[0].time()).'.'.$tmp[1];
        move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[2]);
        }
        $date = \DateTime::createFromFormat('U',  mktime(0, 0, 0, $temp["endDateMonat"], $temp["endDateDay"], $temp["endDateYear"]));
        $dateCreate = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $task->setStartdate($dateCreate);
        $task->setEnddate($date);
        if($username == "Sniky") {
            echo"<pre>";
            var_dump($date);
            echo"</pre>";
            }
        $task->setUsername($username);
        $task->setFirma($firma);
        $task->setAstitle($temp["astitle"]);
        if(strlen($temp["astitle"]) >= 100) {
            $pinlinkindb=preg_replace("/[^ ]*$/", '', substr($temp["astitle"], 0, 100));
            $pinlinkindb=substr($pinlinkindb, 0, -1);
            } else {
                $pinlinkindb = $temp["astitle"];
            }
            $pinlinkindb = str_replace(" ", "_", $pinlinkindb);
            $pinlinkindb = str_replace("(", "", $pinlinkindb);
            $pinlinkindb = str_replace(")", "", $pinlinkindb);
            $pinlinkindb = str_replace("/", "", $pinlinkindb);
            $pinlinkindb = str_replace("?", "", $pinlinkindb);
            $pinlinkindb = str_replace("&", "", $pinlinkindb);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $pindatagesamt = $dm->getRepository('App:Job')->findBy(array('astitle' => $temp["astitle"]),array('id' => 'ASC'));
            $anzindb=count($pindatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $pinlinkindb = $pinlinkindb.'_'.$anzindb;
            } else {
                $pinlinkindb = $pinlinkindb;
            }
        $task->setJobLink($pinlinkindb);
        $task->setJobTyp($temp["jobTyp"]);
        $task->setJobSuche($temp["jobSuche"]);
        $task->setContent($temp["content"]);
        $task->setFinde($temp["jobTyp"]);
        $task->setFor($temp["for"]);
        $task->setJobort($temp["jobort"]);
        $task->setEndDateDay($temp["endDateDay"]);
        $task->setEndDateMonat($temp["endDateMonat"]);
        $task->setEndDateYear($temp["endDateYear"]);
        $task->setAktiv(false);
        $task->setDocpdf($pdf);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($task);
        $dm->flush();
        $memberfilter = new MembersearchfilterController();
        $anz = 10;
        $page = 0;
        $sort = 'ASC';

        $subjetsys = "Neues Jobangebot zum freischalten!";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom('jobangebot@grafiker.de')
        ->setTo('mail@grafiker.de')
        ->setBody('Es wurde ein neues Jobangebot von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
        $mailer->send($message);
        $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "jobangebot@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde ein neues Jobangebot von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');

        /*if($this->container->get( 'security.authorization_checker' )->isGranted( 'ROLE_ADMIN' ) or $this->container->get( 'security.authorization_checker' )->isGranted( 'ROLE_PROFI' ) or $this->container->get( 'security.authorization_checker' )->isGranted( 'ROLE_PLATIN' )) {
        if($request->request->all()['for'] != "Alle") {
            $filterkretik = 'spezieals';
            $filtersearch = $request->request->all()['for'];
        } else {
            $filterkretik = 'skills';
            $filtersearch = $request->request->all()['finde'];
        }
        return $this->render('job/angebote.html.twig', array(
            'name' => 'JobController',
            'memberdata' => $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), 'skills', $request->request->all()['form']['jobTyp'], $anz, $page, $sort),
            'searching' => $request->request->all()['form']['jobTyp'],
            'filter' => 'spezials',
            'jobType' => $request->request->all()['form']['jobTyp'],
        ));*/
        //} else {
            return $this->render('job/ende.html.twig', array(
                'name' => 'JobController',
            ));    
        //}
    }
}
