<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ProfilbesucherController extends Controller
{
    /**
     * @Route("profile/profilbesucher", name="profilbesucher")
     */
    public function index()
    {
        $memberfilter = new MembersearchfilterController();
        $userpic = new CheckUserdataController();
        $anz = 20;
        $page = 0;
        $sort = 'DESC';
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //$userpicread = $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching, $anz, $page, $sort);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $userpicreada = $dm->createQueryBuilder('App:Profilbesucher')->sort(array('viewdate' => $sort))->limit($anz)->skip($page)->field('username')->equals($users->getUsername())->getQuery()->toArray();    
        $filename = array();
        $piclink = array();
        $file_exists = array();
        $memberdatas = array();

foreach($userpicreada as $key => $userpicreada) {
    
    $userpicread = $dm->createQueryBuilder('App:User')->sort(array('id' => $sort))->limit(1)->skip(0)->field('username')->equals($userpicreada->getBesucher())->getQuery()->toArray();
    
//
        foreach($userpicread as $key => $userpicread) {
            $memberdatas[] = $userpicread;
            
        //var_dump($userpicread->getUsername());
        $filenamejpg = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUserpic(); 
        $filenamepng = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png'; 
        $filenamegif = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gif'; 
//var_dump($filenamejpg);
     if(file_exists($filenamejpg)) {
            $filename[$userpicread->getUsername()] = $filenamejpg;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUserpic();
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamepng)) {
            $filename[$userpicread->getUsername()] = $filenamepng;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamegif)) {
            $filename[$userpicread->getUsername()] = $filenamegif;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gif';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else {
            //$file_exists[$userpicread->getUsername()] = false;
            $piclink[$userpicread->getUsername()] = false;
        }
        }
    }
    $userdatas = $dm->getRepository('App:Profilbesucher')->findBy(["username"=>$users->getUsername(), "new" => true]);
    foreach($userdatas as $userdatas) {
    $userdatas->setNew(false);
    //var_dump($userdatas);
    $dm->flush();
    }
    //$memberdatas = (object) $memberdatas;
    
        return $this->render('userprofil/profilbesucher.html.twig', [
            'controller_name' => 'ProfilbesucherController',
            'memberdata' => $memberdatas,
            'filename' => $filename,
            'piclink' => $piclink,
        ]);
    }
}
