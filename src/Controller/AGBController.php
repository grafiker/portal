<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AGBController extends AbstractController
{
    /**
     * @Route("/agb", name="agb")
     */
    public function index()
    {
        return $this->render('agb/index.html.twig', [
            'controller_name' => 'AGBController',
        ]);
    }
}
