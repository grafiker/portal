<?php

namespace App\Controller;

class JoboutputController
{
    function jobFilterOutput($dm, $anz, $sites, $sort, $filter, $owner, $autor, $location, $adminfilter) {
        //var_dump($location);
        if($owner == "Firma") {
            if(!isset($sites) or $sites == "all") {
                $page=0;
            } else {
                $page=$sites*$anz;
            }
                /*$searchQuery = array(
                    '$or' => array(
                        array(
                            'finde' => new \MongoRegex('/'.strtolower($filter).'/si'),
                            ),
                        array(
                            'jobort' => new \MongoRegex('/'.$filter.'/si'),
                            ),
                        )
                    );*/

                $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('firma' => $filter, 'aktiv' => $adminfilter),array('id' => 'ASC'));
                    $jobdata = $dm->createQueryBuilder('App:Job')->sort(array('id' => $sort))->field('enddate')->gte(new \DateTime())->limit($anz)->skip($page)->field('firma')->equals($filter)->field('aktiv')->equals($adminfilter)->getQuery()->toArray();
                    //var_dump($jobdata);
                    //return $jobdata;
         
return $jobdata;
                    
                    
                    $dm->flush();
        } else {
            if($location == "biete" or $location == "suche") {
                $withsearch = true;
            } else {
                $withsearch = false;
            }
        if(!isset($sites) or $sites == "all") {
            $page=0;
        } else {
            $page=$sites*$anz;
        }
            /*$searchQuery = array(
                '$or' => array(
                    array(
                        'finde' => new \MongoRegex('/'.strtolower($filter).'/si'),
                        ),
                    array(
                        'jobort' => new \MongoRegex('/'.$filter.'/si'),
                        ),
                    )
                );*/
                if($filter == "all") {
                    //var_dump($adminfilter);
            $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('aktiv' => $adminfilter),array('id' => 'ASC'));
            //var_dump($jobdatagesamt);
            //$jobdata = $dm->getRepository('App:Job')->findBy(array('aktiv' => $adminfilter),array('id' => $sort), $anz, $page);
            $jobdata = $dm->createQueryBuilder('App:Job')->sort(array('id' => $sort))->field('enddate')->gte(new \DateTime())->limit($anz)->skip($page)->field('aktiv')->equals($adminfilter)->getQuery()->toArray();

            $dm->flush();
            return $jobdata;
                } else {
                    if($withsearch) {
                        $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('jobTyp' => $filter, 'aktiv' => $adminfilter),array('id' => 'ASC'));
                        $jobdata = $dm->createQueryBuilder('App:Job')->sort(array('id' => $sort))->field('enddate')->gte(new \DateTime())->limit($anz)->skip($page)->field('jobTyp')->equals($filter)->field('jobSuche')->equals($location)->field('aktiv')->equals($adminfilter)->getQuery()->toArray();
                        $dm->flush();
                        return $jobdata;
                    } else {
                $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('jobTyp' => $filter, 'aktiv' => $adminfilter),array('id' => 'ASC'));
                $jobdata = $dm->createQueryBuilder('App:Job')->sort(array('id' => $sort))->field('enddate')->gte(new \DateTime())->limit($anz)->skip($page)->field('jobTyp')->equals($filter)->field('aktiv')->equals($adminfilter)->getQuery()->toArray();
                $dm->flush();
                return $jobdata;
                    }
                }
            }
       
    }
}
