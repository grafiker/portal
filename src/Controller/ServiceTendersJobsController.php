<?php

namespace App\Controller;

use App\Document\Job;
use App\AppBundle\Form\UserType;
use App\AppBundle\Form\UsereditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\AppBundle\Form\JobFormWidgetType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceTendersJobsController extends Controller
{
    function __construct(ManagerRegistry $doctrine_mongodb, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->urlGenerator = $urlGenerator;
    }
     /**
     * @Route("/service/tenders/jobs/{id}", name="service_tenders_jobs_new")
     */
    public function jobnewAction($id, Request $request, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $username = $user->getUsername();
            $firma = $user->getFirma();
            $userexist = true;
        } else {
            $username = 'Kein Mitglied!';
            $userexist = false;
        }
        $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
        $lastID = null;
        if(!$userexist) {
            $builder = $this->createForm(UserType::class);
        return $this->render('service_tenders/register.html.twig', [
            'allwidget' => $thiswidget,
            'id' => $id,
            'lastid' => $lastID,
            'widgetid' => $id,
            'lastid' => $lastID,
            'jobid' => $lastID,
            'form' => $builder->createView(),
            'type' => $_GET["type"],
            'jobtype' => $_GET["type"],
            'controller_name' => 'ServiceTendersController',
        ]);
        } else {
            return new RedirectResponse($this->urlGenerator->generate('service_tenders_jobs', array('id' => $id, 'lastid' => $lastID, )));
        }
    }
    /**
     * @Route("/service/tenders/jobs/new/{id}/{widgetusername}", name="service_tenders_jobs")
     */
    public function index($id, $widgetusername, Request $request, \Swift_Mailer $mailer)
    {
        $dm = $this->_doctrine_mongodb;
if($widgetusername != "login") {
    $user = $dm->getRepository('App:User')->findOneBy(array('username' => $widgetusername));
    $username = $user->getUsername();
    $firma = $user->getFirma();
    $firmapic = $user->getFirmapic();
} else {

        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $username = $user->getUsername();
            $firma = $user->getFirma();
            $firmapic = $user->getFirmapic();
        } else {
            $username = 'Kein Mitglied!';
            $firmapic = null;
        }
    }
        $builder = $this->createForm(JobFormWidgetType::class);

        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            //var_dump($request->request->all());
            $temp = $builder->getData();
//\var_dump($temp);


$task = new Job();
        $pdf = array();
        $upload_dir = 'upload/uploads/pdf/';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
        $file_name=$_FILES["form"]["name"]["docpdf1"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf1"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
            $pdf[0]=md5($tmp[0].time()).'.'.$tmp[1];
            move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[0]);
        }
        $file_name=$_FILES["form"]["name"]["docpdf2"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf2"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
            $pdf[1]=md5($tmp[0].time()).'.'.$tmp[1];
            move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[1]);
        }
        $file_name=$_FILES["form"]["name"]["docpdf3"];
        $file_tmp=$_FILES["form"]["tmp_name"]["docpdf3"];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
      
        $expensions= array("pdf", "doc", "docx", "jpg", "jpeg", "png", "zip", "PDF", "DOC", "DOCX", "JPG", "JPEG", "PNG", "ZIP");
        $errors = false;
        if(in_array($file_ext,$expensions)=== false){
           $errors=true;
        }
        
        if(!$errors) {
        $pdf[2]=md5($tmp[0].time()).'.'.$tmp[1];
        move_uploaded_file($file_tmp,"upload/uploads/pdf/".$pdf[2]);
        }
        $teile = explode(".", $temp["enddate"]);
        $date = \DateTime::createFromFormat('U',  mktime(0, 0, 0, $teile[1], $teile[0], $teile[2]));
        $dateCreate = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $task->setStartdate($dateCreate);
        $task->setEnddate($date);
        $task->setUsername($username);
        $task->setFirma($firma);
        $task->setAstitle($temp["astitle"]);
        if(strlen($temp["astitle"]) >= 100) {
            $pinlinkindb=preg_replace("/[^ ]*$/", '', substr($temp["astitle"], 0, 100));
            $pinlinkindb=substr($pinlinkindb, 0, -1);
            } else {
                $pinlinkindb = $temp["astitle"];
            }
            $pinlinkindb = str_replace(" ", "_", $pinlinkindb);
            $pinlinkindb = str_replace("(", "", $pinlinkindb);
            $pinlinkindb = str_replace(")", "", $pinlinkindb);
            $pinlinkindb = str_replace("/", "", $pinlinkindb);
            $pinlinkindb = str_replace("?", "", $pinlinkindb);
            $pinlinkindb = str_replace("&", "", $pinlinkindb);
            
            $pindatagesamt = $dm->getRepository('App:Job')->findBy(array('astitle' => $temp["astitle"]),array('id' => 'ASC'));
            $anzindb=count($pindatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $pinlinkindb = $pinlinkindb.'_'.$anzindb;
            } else {
                $pinlinkindb = $pinlinkindb;
            }
        $task->setJobLink($pinlinkindb);
        $task->setJobTyp($temp["jobTyp"]);
        $task->setJobSuche($temp["jobSuche"]);
        $task->setContent($temp["content"]);
        $task->setFinde($temp["jobTyp"]);
        $task->setFor($temp["for"]);
        $task->setJobort($temp["jobort"]);
        $task->setEndDateDay($teile[0]);
        $task->setEndDateMonat($teile[1]);
        $task->setEndDateYear($teile[2]);
        $task->setAktiv(false);
        $task->setDocpdf($pdf);
        $task->setFirmalogo($firmapic);

        //\var_dump($task);
        $dm = $this->_doctrine_mongodb;
        $this->_doctrine_mongodb->getManager()->persist($task);
        $this->_doctrine_mongodb->getManager()->flush();

        $subjetsys = "Neues Jobangebot zum freischalten!";
        $message = (new \Swift_Message())
        ->setSubject($subjetsys)
        ->setFrom('jobangebot@grafiker.de')
        ->setTo('mail@grafiker.de')
        ->setBody('Es wurde ein neues Jobangebot von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
        //$mailer->send($message);
        //$maillog = new MaillogController();
            //$maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "jobangebot@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde ein neues Jobangebot von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
            $emdsend = $this->_doctrine_mongodb->getManager();
        $thiswidget = $emdsend->getRepository('App:ServiceWidget')->findOneBy(["id"=>$id]);
            return $this->render('service_tenders/ende.html.twig', [
                'allwidget' => $thiswidget,
                'username' => $user->getUsername(),
                'userid' => $user->getID(),
                'widgetid' => $id,
                'lastid' => null,
                'jobid' => $task->getId(),
                'jobtype' => "jobs",
                'controller_name' => 'ServiceTendersController',
            ]);

        } else {

        return $this->render('service_tenders_jobs/index.html.twig', [
            'form' => $builder->createView(),
            'username' => $username,
            'firmapic' => $firmapic,
            'id' => $id,
            'controller_name' => 'ServiceTendersJobsController',
        ]);
    }
    }
}
