<?php

namespace App\Controller;
use App\Document\Ausschreibung;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AusschreibungController extends Controller
{
    /**
     * @Route("/ausschreibung", name="ausschreibung")
     */
    public function new(Request $request)
    {

if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}

        $task = new Ausschreibung();
        $builder = $this->createFormBuilder($task)
        ->add('username', HiddenType::class, array(
            'data' => $username,
        ))
        ->add('astitle', TextType::class, ['label' => 'Ausschreibungstitel: '])
        ->add('enddate', DateType::class, array(
            'widget' => 'single_text',
            'label' => 'Ablaufdatum:',
            'years' => range(date('Y'), date('Y')+10),
            'months' => range(date('m'), date('m')+12),
            'days' => range(date('d'), date('d')+31),
        ))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-right'),'label' => 'weiter...'))
        ->getForm();

        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);*/

            return $this->render('ausschreibung/nextstep.html.twig', array(
                'name' => 'hier geht es weiter...',
            ));
        }

        return $this->render('ausschreibung/index.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'AusschreibungController',
        ));
    }
}
