<?php

namespace App\Controller;
use App\Document\Pins;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminPinviewController extends Controller
{
    /**
     * @Route("/admin/pinview/{pinid}", name="admin_pinview")
     */
    public function index($pinid)
    {
        $userdata = new CheckUserdataController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pindata = $dm->getRepository('App:Pins')->findBy(array('id' => $pinid));
        $picsarray = array();
        foreach($pindata as $key => $pinpics) {
            $picsarray = $pinpics->getPicname();
            $content = HashToLinkController::HashToLink($pinpics->getContent());
        }
        foreach($pindata as $key => $pinuser) {
            $fileexistarray[$pinuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
            $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
        }
        return $this->render('admin/pinview.html.twig', [
            'controller_name' => 'AdminPinviewController',
            'pindata' => $pindata,
            'content' => $content,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'picsarray' => $picsarray,
        ]);
    }
}
