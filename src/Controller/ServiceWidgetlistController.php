<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ServiceWidgetlistController extends AbstractController
{
    function __construct(ManagerRegistry $doctrine_mongodb, UrlGeneratorInterface $urlGenerator)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->urlGenerator = $urlGenerator;
    }
    /**
     * @Route("/service/widgetlist", name="service_widgetlist")
     */
    public function index(Request $request)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }
if($userexist == false) {
    $toroutes = "app_service_login";
        return new RedirectResponse($this->urlGenerator->generate($toroutes));
}

        $emdsend = $this->_doctrine_mongodb->getManager();
        $allwidget = $emdsend->getRepository('App:ServiceWidget')->findBy(["accountName"=>$users->getID()]);

        return $this->render('service_widgetlist/index.html.twig', [
            'controller_name' => 'ServiceWidgetlistController',
            'username' => $users->getUsername(),
            'allwidget' => $allwidget,
        ]);
    }
}
