<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserprofilNotificationsController extends AbstractController
{
    /**
     * @Route("/profile/notifications", name="userprofil_notifications")
     */
    public function index()
    {
        
        return $this->render('userprofil/notifications.html.twig', [
            'controller_name' => 'UserprofilNotificationsController',
        ]);
    }
}
