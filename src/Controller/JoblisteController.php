<?php

namespace App\Controller; 

use App\Document\Jobs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class JoblisteController extends Controller
{
    /**
     * @Route("/jobliste", name="jobliste")
     */
    public function index()
    {
        $datejetzt = new \DateTime(date('m/d/Y', time()));

        $dm = $this->get('doctrine_mongodb')->getManager();
        //$pindatagesamt = $dm->createQueryBuilder('App:Job')->sort(array('startdate' => 'asc'))->limit(4)->skip(2)->field('aktiv')->getQuery()->toArray();
//var_dump($pindatagesamt);
        $jobdata = new JoboutputController();
        $anz = 30;
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,true)->getUsername());
        $logoexist = array();
        $logolink = array();
        $jobarray = array();
        $logofirma = array();
        $jobarray = $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), $anz,0,'DESC','all','','',7,true);
        foreach($jobarray as $key=>$value){
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:User')->findOneBy(["username"=>$value->getUsername()]);
            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
        
                if($userdata->getFirmapic() and $userdata->getFirmapic() != 'del') {
                  $logoexist[$value->getUsername()]=true;
                  $logolink[$value->getUsername()]=$userdata->getFirmapic();
                  $logofirma[$value->getUsername()] = true;
                } elseif($userdata->getUserpic() and $userdata->getUserpic() != 'del') {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$userdata->getUserpic();
                    $logofirma[$value->getUsername()] = false;
                } else {
                    $logoexist[$value->getUsername()]=false;
                    $logolink[$value->getUsername()]=false;
                }
        }


        return $this->render('jobliste/index.html.twig', [
            'controller_name' => 'JoblisteController',
            'jobdata' => $jobarray,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'logofirma' => $logofirma,
            'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true),
        ]);
    }

    /**
     * @Route("/jobliste/{filter}/{what}", name="jobliste_filter")
     */
    public function actionCreate(Request $request, $filter, $what)
    {
        $zahltest = ctype_digit($filter);
        //var_dump($zahltest);
        if($zahltest) {
            $page = $filter;
            $filter = "all";
        } else {
            $page = 0;
        }
        $anz = 30;
        $jobdata = new JoboutputController();
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,true)->getUsername());
        $logoexist = array();
        $logolink = array();
        $jobarray = array();
        $logofirma = array();
        $jobarray = $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), $anz,$page,'DESC',$filter,'','',$what,true);
        foreach($jobarray as $key=>$value){

            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:User')->findOneBy(["username"=>$value->getUsername()]);
            //var_dump($userdata->getFirmapic());
                if($userdata->getFirmapic() and $userdata->getFirmapic() != 'del') {
                    //var_dump("HIER");
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$userdata->getFirmapic();
                    $logofirma[$value->getUsername()] = true;
                  } elseif($userdata->getUserpic() and $userdata->getUserpic() != 'del') {
                      $logoexist[$value->getUsername()]=true;
                      $logolink[$value->getUsername()]=$userdata->getUserpic();
                      $logofirma[$value->getUsername()] = false;
                  } else {
                      $logoexist[$value->getUsername()]=false;
                      $logolink[$value->getUsername()]=false;
                      $logofirma[$value->getUsername()] = false;
                  }
        }

if($zahltest) {
    return $this->render('jobliste/loaddata.html.twig', [
        'controller_name' => 'JoblisteController',
        'jobdata' => $jobarray,
        'logoexist' => $logoexist,
        'logofirma' => $logofirma,
        'logolink' => $logolink,
        'loadjobID' => $page * $anz,
        'filter' => $filter,
        'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true, 'finde', strtolower($filter)),
    ]);
} else {
    $dm = $this->get('doctrine_mongodb')->getManager();
        return $this->render('jobliste/index.html.twig', [
            'controller_name' => 'JoblisteController',
            'jobdata' => $jobarray,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'logofirma' => $logofirma,
            'loadjobID' => $page * $anz,
            'filter' => $filter,
            'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true, 'finde', strtolower($filter)),
        ]);
}
    }
    /**
     * @Route("/firmajobs/{filter}", name="firma_jobliste_filter")
     */
    public function actionFirma(Request $request, $filter)
    {
        $zahltest = ctype_digit($filter);
        //var_dump($zahltest);
        if($zahltest) {
            $page = $filter;
            $filter = "all";
        } else {
            $page = 0;
        }
        $anz = 30;
        $jobdata = new JoboutputController();
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,true)->getUsername());
        $logoexist = array();
        $logolink = array();
        $jobarray = array();
        $logofirma = array();
        $jobarray = $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), $anz,$page,'DESC',$filter,'Firma','',7,true);
//var_dump($jobarray);
        foreach($jobarray as $key=>$value){

            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:User')->findOneBy(["username"=>$value->getUsername()]);
            //var_dump($userdata->getFirmapic());
                if($userdata->getFirmapic() and $userdata->getFirmapic() != 'del') {
                    //var_dump("HIER");
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$userdata->getFirmapic();
                    $logofirma[$value->getUsername()] = true;
                  } elseif($userdata->getUserpic() and $userdata->getUserpic() != 'del') {
                      $logoexist[$value->getUsername()]=true;
                      $logolink[$value->getUsername()]=$userdata->getUserpic();
                      $logofirma[$value->getUsername()] = false;
                  } else {
                      $logoexist[$value->getUsername()]=false;
                      $logolink[$value->getUsername()]=false;
                      $logofirma[$value->getUsername()] = false;
                  }
        }

if($zahltest) {
    return $this->render('jobliste/loaddata.html.twig', [
        'controller_name' => 'JoblisteController',
        'jobdata' => $jobarray,
        'logoexist' => $logoexist,
        'logofirma' => $logofirma,
        'logolink' => $logolink,
        'loadjobID' => $page * $anz,
        'filter' => $filter,
        'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true, 'firma', $filter),
    ]);
} else {
    $dm = $this->get('doctrine_mongodb')->getManager();
        return $this->render('jobliste/index.html.twig', [
            'controller_name' => 'JoblisteController',
            'jobdata' => $jobarray,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'logofirma' => $logofirma,
            'loadjobID' => $page * $anz,
            'filter' => $filter,
            'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true, 'firma', $filter),
        ]);
}
    }
    /**
     * @Route("/jobliste/{filter}/{sites}", name="jobliste_filter_sites")
     */
    public function actionPagination(Request $request, $filter, $sites)
    {
        $jobdata = new JoboutputController();
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,true)->getUsername());
        $logoexist = array();
        $logolink = array();
        $jobarray = array();
        $page = $sites;
        $anz = 30;
        $jobarray = $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), $anz,$page,'DESC',$filter,'','',7,true);
        foreach($jobarray as $key=>$value) {

            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
        
                if(file_exists($name.".png")) {
                  $logoexist[$value->getUsername()]=true;
                  $logolink[$value->getUsername()]=$name.".png";
                }elseif(file_exists($name.".gif")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".gif";
                } elseif(file_exists($name.".jpg")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".jpg";
                } else {
                    $logoexist[$value->getUsername()]=false;
                    $logolink[$value->getUsername()]=false;
                }
        }


        return $this->render('jobliste/loaddata.html.twig', [
            'controller_name' => 'JoblisteController',
            'jobdata' => $jobarray,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'loadjobID' => $page * $anz,
            'filter' => $filter,
        ]);
    }
}
