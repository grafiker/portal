<?php

namespace App\Controller;

class CheckUserdataController
{
    function getCheckPicExist($root, $users){
        $filenamejpg = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.jpg'; 
        $filenamepng = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.png'; 
        $filenamegif = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.gib'; 
     if(file_exists($filenamejpg)) {
            $filename=$filenamejpg;
            $file_exists = file_exists($filename);
            return $file_exists;
        } else if(file_exists($filenamepng)) {
            $filename=$filenamepng;
            $file_exists = file_exists($filename);
            return $file_exists;
        } else if(file_exists($filenamegif)) {
            $filename=$filenamegif;
            $file_exists = file_exists($filename);
            return $file_exists;
        } else {
            return false;
        }
    }
    function getCheckPicLink($root, $users){
        $filenamejpg = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.jpg'; 
        $filenamepng = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.png'; 
        $filenamegif = $root.'/public/upload/uploads/userpics/'.$users.'_userpic.gib'; 
     if(file_exists($filenamejpg)) {
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.jpg';
            return $piclink;
        } else if(file_exists($filenamepng)) {
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.png';
            return $piclink;
        } else if(file_exists($filenamegif)) {
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.gif';
            return $piclink;
        } else {
            return false;
        }
    }
}