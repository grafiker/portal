<?php

namespace App\Controller;

use App\Document\Jobs;
use App\AppBundle\Form\AdminJobFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminJobverwaltungController extends Controller
{
    /**
     * @Route("/admin/jobverwaltung/{page}", name="admin_jobverwaltung")
     */
    public function index(Request $request, $page)
    {
        $infotxt = '';
        $newarray=$request->request->all();
        if($newarray) {
        //var_dump($newarray);
        $infotxt = 'Die Ausschreibung wurde überarbeitet.';
        $em = $this->get('doctrine_mongodb')->getManager();
        $jobcheck = $em->getRepository('App:Job')->findOneBy(["id"=>$newarray["form"]["jobId"]]);
        //var_dump();
        $jobcheck->setContent($newarray["form"]["content"]);
        $jobcheck->setJobort($newarray["form"]["jobort"]);
        $jobcheck->setJobTyp($newarray["form"]["jobTyp"]);
        $jobcheck->setAstitle($newarray["form"]["astitle"]);
        $em->flush();
        }
        $jobdata = new JoboutputController();
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,false));
        $logoexist = array();
        $logolink = array();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $jobdata = $dm->createQueryBuilder('App:Job')->sort(array('id' => 'DESC'))->field('enddate')->gte(new \DateTime())->limit(20)->skip(0)->field('aktiv')->equals(false)->getQuery()->toArray();
        foreach($jobdata as $key => $value){

            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
        
                if(file_exists($name.".png")) {
                  $logoexist[$value->getUsername()]=true;
                  $logolink[$value->getUsername()]=$name.".png";
                }elseif(file_exists($name.".gif")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".gif";
                } elseif(file_exists($name.".jpg")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".jpg";
                } else {
                    $logoexist[$value->getUsername()]=false;
                    $logolink[$value->getUsername()]=false;
                }
        }
        return $this->render('admin/Jobverwaltung.html.twig', [
            'controller_name' => 'AdminJobverwaltungController',
            'jobdata' => $jobdata,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'infotxt' => $infotxt,
        ]);
    }
    /**
     * @Route("/admin/jobverwaltung/{jobid}/{action}", name="admin_jobverwaltung_edit")
     */
    public function actionCreate(RequestStack $requestStack, Request $request, $jobid, $action)
    {
        $jobdata = new JoboutputController();
        $infotxt = '';
        if($action == "edit") {
            $emedit = $this->get('doctrine_mongodb')->getManager();
            $jobcheck = $emedit->getRepository('App:Job')->findOneBy(["id"=>$jobid]);
            $builder = $this->createForm(AdminJobFormType::class, $jobcheck);
            $emedit->flush();

            return $this->render('admin/jobedit.html.twig', [
                'controller_name' => 'AdminJobverwaltungController',
                'form' => $builder->createView(),
                'jobid' => $jobid,
            ]);
        } else {

        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $jobcheck = $emdel->getRepository('App:Job')->findOneBy(["id"=>$jobid]);
            foreach($jobcheck->getDocpdf() as $key => $delpdf) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/".$delpdf)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/".$delpdf);
                } else {}
            }
            $emdel->remove($jobcheck);
            $emdel->flush();
            $infotxt = 'Die Ausschreibung wurde gelöscht.';
        }
        if($action == "ok") {
            $infotxt = 'Die Ausschreibung wurde freigeschaltet.';
        $em = $this->get('doctrine_mongodb')->getManager();
        $jobcheck = $em->getRepository('App:Job')->findOneBy(["id"=>$jobid]);
        //var_dump();
        $jobcheck->setAktiv(true);
        
        $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/jobview/'.$jobcheck->getJoblink();
$linkToViewNew='<h3>'.$jobcheck->getAstitle().'</h3><br />' . $jobcheck->getContent() . '<br /><br /><a href="'.$linkToView .'">Hier klicken um die Anzeige auf grafiker.de zu betrachten.</a>';
        $dm = $this->get('doctrine_mongodb')->getManager();
        $userdatagesamt = $dm->getRepository('App:User')->findBy(array('notifications' => 2),array('id' => 'ASC'));
        $dm->flush();
        $threadBuilder = $this->get('fos_message.composer')->newThread();

        foreach ($userdatagesamt as $recipient) {
            if($recipient->getUsername() != $jobcheck->getUsername()) {
            $threadBuilder->addRecipient($recipient);
            }
        }
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $usersender = $emdsend->getRepository('App:User')->findOneBy(["username"=>'grafiker.de']);
        $threadBuilder->setSender($usersender);
        $threadBuilder->setSubject('%%Systemmail%% Es gibt eine neue Ausschreibung auf grafiker.de');
        $threadBuilder->setBody($linkToViewNew);
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        $viewOK = 'Hallo '. $jobcheck->getUsername() .',<br />Ihre Ausschreibung mit dem Titel: ' . $jobcheck->getAstitle() . ' wurde freigegeben und ist jetzt für Besucher und Mitglieder, auf grafiker.de zu finden.<br /><br />Wir bedanken uns für Ihr Vertrauen in unseren Service.<br /><br />Mit freundlichen Grüßen,<br />grafiker.de<br /><br /><a href="'.$linkToView .'">Hier klicken um die Anzeige auf grafiker.de zu betrachten.</a>';
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $emdjabsender = $this->get('doctrine_mongodb')->getManager();
        $userjobsender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>$jobcheck->getUsername()]);
        $threadBuilder->addRecipient($userjobsender);

        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $usersender = $emdsend->getRepository('App:User')->findOneBy(["username"=>'grafiker.de']);
        $threadBuilder->setSender($usersender);
        $threadBuilder->setSubject('%%Systemmail%% Ihre Ausschreibung wurde freigegeben.');
        $threadBuilder->setBody($viewOK);
         
         
        $sender = $this->get('fos_message.sender');

        $sender->send($threadBuilder->getMessage());
        $em->flush();

        }
        $logoexist = array();
        $logolink = array();
        foreach($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,false) as $key=>$value){

            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
        
                if(file_exists($name.".png")) {
                  $logoexist[$value->getUsername()]=true;
                  $logolink[$value->getUsername()]=$name.".png";
                }elseif(file_exists($name.".gif")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".gif";
                } elseif(file_exists($name.".jpg")) {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$name.".jpg";
                } else {
                    $logoexist[$value->getUsername()]=false;
                    $logolink[$value->getUsername()]=false;
                }
        }
        return $this->render('admin/Jobverwaltung.html.twig', [
            'controller_name' => 'AdminJobverwaltungController',
            'jobdata' => $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,false),
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'infotxt' => $infotxt,
        ]);
        }
    }
}
