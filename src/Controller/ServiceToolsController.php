<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceToolsController extends AbstractController
{
    /**
     * @Route("/service/tools", name="service_tools")
     */
    public function index()
    {
        return $this->render('service_tools/index.html.twig', [
            'controller_name' => 'ServiceToolsController',
        ]);
    }
}
