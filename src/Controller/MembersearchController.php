<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class MembersearchController extends Controller
{
    /**
     * @Route("/membersearch/{filter}/{searching}", name="membersearch")
     */
    public function index($filter, $searching)
    {
        $memberfilter = new MembersearchfilterController();
        $userpic = new CheckUserdataController();
        $anz = 30;
        $page = 0;
        $sort = 'ASC';

        $userpicread = $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching, $anz, $page, $sort);
        $filename = array();
        $piclink = array();
        $file_exists = array();
        foreach($userpicread as $key => $userpicread) {
        //var_dump($userpicread->getUsername());
        $filenamejpg = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUserpic(); 
        $filenamepng = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png'; 
        $filenamegif = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gib'; 
//var_dump($filenamejpg);
     if(file_exists($filenamejpg)) {
            $filename[$userpicread->getUsername()] = $filenamejpg;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUserpic();
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamepng)) {
            $filename[$userpicread->getUsername()] = $filenamepng;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamegif)) {
            $filename[$userpicread->getUsername()] = $filenamegif;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gif';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else {
            //$file_exists[$userpicread->getUsername()] = false;
            $piclink[$userpicread->getUsername()] = false;
        }
        }
        //var_dump($piclink);
        //var_dump($memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching));
        return $this->render('membersearch/index.html.twig', [
            'controller_name' => 'MembersearchController',
            'memberdata' => $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching, $anz, $page, $sort),
            'searching' => $searching,
            'filter' => $filter,
            'filename' => $filename,
            'piclink' => $piclink,
        ]);
    }

    /**
     * @Route("/membersearch/{filter}/{searching}/{sites}", name="sites_membersearch")
     */
    public function createAction($filter, $searching, $sites)
    {
        $memberfilter = new MembersearchfilterController();
        $userpic = new CheckUserdataController();
        $anz = 30;
        $sort = 'ASC';

        $userpicread = $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching, $anz, $sites, $sort);
        $filename = array();
        $piclink = array();
        $file_exists = array();
        foreach($userpicread as $key => $userpicread) {
        //var_dump($userpicread->getUsername());
        $filenamejpg = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUserpic(); 
        $filenamepng = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png'; 
        $filenamegif = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gib'; 
//var_dump($filenamejpg);
     if(file_exists($filenamejpg)) {
            $filename[$userpicread->getUsername()] = $filenamejpg;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUserpic();
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamepng)) {
            $filename[$userpicread->getUsername()] = $filenamepng;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.png';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else if(file_exists($filenamegif)) {
            $filename[$userpicread->getUsername()] = $filenamegif;
            $piclink[$userpicread->getUsername()] = '/upload/uploads/userpics/'.$userpicread->getUsername().'_userpic.gif';
            //$file_exists[$userpicread->getUsername()] = file_exists($filename);
        } else {
            //$file_exists[$userpicread->getUsername()] = false;
            $piclink[$userpicread->getUsername()] = false;
        }
        }
        return $this->render('membersearch/loaddata.html.twig', [
            'controller_name' => 'MembersearchController',
            'searching' => $searching,
            'filter' => $filter,
            'memberdata' => $memberfilter->memberFilterOutput($this->get('doctrine_mongodb')->getManager(), $filter, $searching, $anz, $sites, $sort),
            'filename' => $filename,
            'piclink' => $piclink,
            'sites' => $sites+1,
            'loadpinID' => $anz * $sites,
        ]);
    }
}
