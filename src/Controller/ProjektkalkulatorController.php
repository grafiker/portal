<?php
/** Nur um den Projektrechner auf der Startseite zu verlinken... */
namespace App\Controller;
use App\AppBundle\Form\ProjektkalkulatorFormType;
use App\Document\Projektrechner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ProjektkalkulatorController extends Controller
{
    /**
     * @Route("/projektkalkulator", name="projektkalkulator")
     */

    public function new(Request $request)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $username = $user->getUsername();
        } else {
            $username = 'Kein Mitglied!';
        }
$dm = $this->get('doctrine_mongodb')->getManager();
$kategorien = $dm->getRepository('App:Projektrechnerkategorien')->findall();
$dm->flush();
$extarray=array();
//$extarray['- gewünschte Projektart wählen -'] = '- gewünschte Projektart wählen -';
foreach($kategorien as $key=>$value){
    $extarray[$value->getKatname()] = $value->getKatname();
  }
  $builder = $this->createForm(ProjektkalkulatorFormType::class, $extarray);
    $builder->add('username', HiddenType::class, array(
    'data' => $username,
    ));
    $builder->add('steps', HiddenType::class, array(
    'data' => 1,
    ));
        $task = new Projektrechner();
        
            $builder->handleRequest($request);

            $dmall = $this->get('doctrine_mongodb')->getManager();
            
            $data = $builder->getData();    
            if(isset($data["prstart"])) {


$datenabfrage = $data["prstart"];
$katdauer = $dmall->getRepository('App:Projektrechnerkategorien')->findOneByKatname($datenabfrage);
$dmall->flush();
$task->setDauer($katdauer->getDauer());
$basis_preis = round($data["stundensatz"]*$katdauer->getDauer(), 2);
if($data["schwer"] == 1) {
    $wertSchwierigkeit = 0;
    $dauer=$katdauer->getDauer();
} else if($data["schwer"] == 2) {
    $wertSchwierigkeit = $basis_preis * 0.5;
    $dauer=$katdauer->getDauer() * 1.5;
} else if($data["schwer"] == 3) {
    $wertSchwierigkeit = $basis_preis * 1.0;
    $dauer=$katdauer->getDauer() * 2.0;
}
if($data["nart"] == 1) {
    $wertNutzungsart = 0;
} else if($data["nart"]  == 2) {
    $wertNutzungsart = $basis_preis * 0.8;
}
if($data["numfang"] == 1) {
    $wertNutzungsumfang = 0;
} else if($data["numfang"] == 2) {
    $wertNutzungsumfang = $basis_preis * 0.5;
} else if($data["numfang"] == 3) {
    $wertNutzungsumfang = $basis_preis * 1.2;
}

$dauer = ( !preg_match('/\.[1-9]+$/is',$dauer) ) ? intval($dauer) : number_format($dauer,2,',','.');
$preis_total = round($basis_preis+$wertSchwierigkeit+$wertNutzungsart+$wertNutzungsumfang,2);
$preis_totalalt = $preis_total;
$preis_total = number_format($preis_total,2,',','.');
$stundensatz = number_format($data["stundensatz"],2,',','.');

                $task->setUsername($data["username"]);
                $task->setStundensatz($data["stundensatz"]);
                $task->setPrstart($data["prstart"]);
                $task->setSchwer($data["schwer"]);
                $task->setNart($data["nart"]);
                $task->setNumfang($data["numfang"]);
                $task->setErgebnis($preis_totalalt);
                $task->setDauer($dauer);
                $dmall->persist($task);
                $dmall->flush();
            } else {
                $datenabfrage = "Auftragsbestätigung";
                $data["stundensatz"] = 55;
                $data["schwer"] = 1;
                $data["nart"] = 1;
                $data["numfang"] = 1;
            }
            $ausschreibungen = $dmall->getRepository('App:Projektrechner')->findby(array(), array('id' => 'DESC'), 25);
            $katdauer = $dmall->getRepository('App:Projektrechnerkategorien')->findOneByKatname($datenabfrage);
            $dmall->flush();
//$task->setDauer($katdauer->getDauer());
$basis_preis = round($data["stundensatz"]*$katdauer->getDauer(), 2);
if($data["schwer"] == 1) {
    $wertSchwierigkeit = 0;
    $dauer=$katdauer->getDauer();
} else if($data["schwer"] == 2) {
    $wertSchwierigkeit = $basis_preis * 0.5;
    $dauer=$katdauer->getDauer() * 1.5;
} else if($data["schwer"] == 3) {
    $wertSchwierigkeit = $basis_preis * 1.0;
    $dauer=$katdauer->getDauer() * 2.0;
}
if($data["nart"] == 1) {
    $wertNutzungsart = 0;
} else if($data["nart"]  == 2) {
    $wertNutzungsart = $basis_preis * 0.8;
}
if($data["numfang"] == 1) {
    $wertNutzungsumfang = 0;
} else if($data["numfang"] == 2) {
    $wertNutzungsumfang = $basis_preis * 0.5;
} else if($data["numfang"] == 3) {
    $wertNutzungsumfang = $basis_preis * 1.2;
}

$dauer = ( !preg_match('/\.[1-9]+$/is',$dauer) ) ? intval($dauer) : number_format($dauer,2,',','.');
$preis_total = round($basis_preis+$wertSchwierigkeit+$wertNutzungsart+$wertNutzungsumfang,2);
$preis_total = number_format($preis_total,2,',','.');
$stundensatz = number_format($data["stundensatz"],2,',','.');

return $this->render('projektrechner/start.html.twig', array(
                'form' => $builder->createView(),
                'lastausschreibungen' => $ausschreibungen,
                'ausgabedauer' => $dauer . 'h ',
                'ausgabestundensatz' =>  $stundensatz . ' €',
                'ausgabegesamt' => $preis_total . ' €',
                'name' => 'hier geht es weiter...' . $task->getSteps(),
            ));
}
public function dauercall($kat) {
    echo $kat+5;
}
}
