<?php

namespace App\Controller;
use App\Document\Pdf;
use App\AppBundle\Form\PdfrechnungFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class PdfController extends Controller
{
    /**
     * Start
     * @Route("/pdf/generate/{username}/{pin}", name="pdf")
     */
    public function crateIndex(RequestStack $requestStack, Request $request, $username, $pin)
    {
       $task = new Pdf();
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $usercheck = $emdel->getRepository('App:User')->findOneBy(["username"=>$username]);

        $pdfcheck = $emdel->getRepository('App:Pdf')->findOneBy(array("rechnungsnummer" => intval($request->request->all()['form']['rechnungsnummer']), 'reart' => intval($request->request->all()['form']['reart'])));

        //var_dump($usercheck->getFirstName());
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled',true);   
        $pdfOptions->set('defaultFont', 'Arial');
        
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        $redate = date("d.m.Y H:i:s");   
        $rechnungsnummer = $request->request->all()['form']['rechnungsnummer'];
        $task->setRechnungsnummer($rechnungsnummer);
        $menge = $request->request->all()['form']['menge'];
        $task->setMenge($menge);
        $angebotstitel = $request->request->all()['form']['retitle'];
        $task->setRetitle($angebotstitel);
        $task->setPinid($pin);
        $task->setReart($request->request->all()['form']['reart']);
        $rechnungsempfaengerfirma = "".$usercheck->getFirma()."";
        $rechnungsempfaenger = "".$usercheck->getFirstName()." ".$usercheck->getLastName()."";
        $rechnungsempfaengerstrasse = "".$usercheck->getStrassenummer()."";
        $rechnungsempfaengerplzort = "".$usercheck->getPlz()." ".$usercheck->getWohnort()."";
        $rechnungsempfaengerLand = "".$usercheck->getLand()."";
        $rechnungsempfaengermail = "".$usercheck->getEmail()."";
        $einzelpreis = $request->request->all()['form']['einzelpreis'];
        $angebotshinweis = $request->request->all()['form']['angebotshinweis'];
        $task->setEinzelpreis($einzelpreis);
        $task->setAngebotshinweis($angebotshinweis);
        if($request->request->all()['form']['reart'] == "1") {
            $subjetsys = "grafiker.de: Rechnung für ". $angebotstitel;
            $body = "Hallo " . $usercheck->getAnrede() . ' ' . $usercheck->getLastName() . ',<br /><br />im Anhang finden Sie eine Rechnung für Ihren Pin mit Werbung auf grafiker.de. Wir bedanken uns für Ihr Vertrauen in uns!<br /><br />Mit freundlichen Grüßen,<br />Ihr grafiker.de Team';
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('pdfrechnung/index.html.twig', [
            'title' => "Welcome to our PDF Test",
            'redate' => $redate,
            'rechnungsempfaenger' => $rechnungsempfaenger,
            'rechnungsempfaengerfirma' => $rechnungsempfaengerfirma,
            'rechnungsempfaengerLand' => $rechnungsempfaengerLand,
            'rechnungsempfaengerstrasse' => $rechnungsempfaengerstrasse,
            'rechnungsempfaengerplzort' => $rechnungsempfaengerplzort,
            'rechnungsnummer' => $rechnungsnummer,
            'angebotstitel' => $angebotstitel,
            'einzelpreis' => $einzelpreis,
            'angebotshinweis' => $angebotshinweis,
            'menge' => $menge
        ]);
        } else {
            $subjetsys = "grafiker.de: Angebot für ". $angebotstitel;
            $body = "Hallo " . $usercheck->getAnrede() . ' ' . $usercheck->getLastName() . ',<br /><br />im Anhang finden Sie ein Angebot für Ihren Pin mit Werbung. Wenn Sie uns dieses Angebot jetzt bestätigen, bekommen Sie im nächsten Schritt eine Rechnung von uns und Ihr Pin wird freigeschaltet!<br /><br />Mit freundlichen Grüßen,<br />Ihr grafiker.de Team';
            // Retrieve the HTML generated in our twig file
        $html = $this->renderView('pdfangebot/index.html.twig', [
            'title' => "Welcome to our PDF Test",
            'redate' => $redate,
            'rechnungsempfaenger' => $rechnungsempfaenger,
            'rechnungsempfaengerfirma' => $rechnungsempfaengerfirma,
            'rechnungsempfaengerLand' => $rechnungsempfaengerLand,
            'rechnungsempfaengerstrasse' => $rechnungsempfaengerstrasse,
            'rechnungsempfaengerplzort' => $rechnungsempfaengerplzort,
            'rechnungsnummer' => $rechnungsnummer,
            'angebotstitel' => $angebotstitel,
            'einzelpreis' => $einzelpreis,
            'angebotshinweis' => $angebotshinweis,
            'menge' => $menge
        ]);
        }

        
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Store PDF Binary Data
        $output = $dompdf->output();
        
        // In this case, we want to write the file in the public directory
        $publicDirectory = $this->get('kernel')->getProjectDir() . '/public';
        $upload_dir = $publicDirectory . '/upload/uploads/grafikerpdf';
        if(!is_dir($upload_dir)){
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }
        // e.g /var/www/project/public/mypdf.pdf
        if($request->request->all()['form']['reart'] == "1") {
            $pdfname = 'rechnung' . $rechnungsnummer . '.pdf';
        $pdfFilepath =  $upload_dir . '/rechnung' . $rechnungsnummer . '.pdf';
        } else {
            $pdfname = 'angebot' . $rechnungsnummer . '.pdf';
        $pdfFilepath =  $upload_dir . '/angebot' . $rechnungsnummer . '.pdf';
        }
        // Write file to the desired path
        file_put_contents($pdfFilepath, $output);
        if(!$pdfcheck) {
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            $agb =  $publicDirectory . '/unterlagen/Grafiker-AGB.pdf';    
        $message = (new \Swift_Message())
                            ->setSubject($subjetsys)
                            ->setFrom(['angebot@grafiker.de' => 'Grafiker.de:Buchhaltung'])
                            ->setTo($rechnungsempfaengermail)
                            ->setBody($body)
                            ->attach(\Swift_Attachment::fromPath($pdfFilepath))
                            ->attach(\Swift_Attachment::fromPath($agb));
                            $this->get('mailer')->send($message);
                            $maillog = new MaillogController();
                    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "angebot@grafiker.de", $rechnungsempfaengermail, $subjetsys, $body);

                    $message = (new \Swift_Message())
                            ->setSubject($subjetsys)
                            ->setFrom(['angebot@grafiker.de' => 'Grafiker.de:Buchhaltung'])
                            ->setTo("mail@grafiker.de")
                            ->setBody($body)
                            ->attach(\Swift_Attachment::fromPath($pdfFilepath))
                            ->attach(\Swift_Attachment::fromPath($agb));
                            $this->get('mailer')->send($message);
                            $maillog = new MaillogController();
                    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "angebot@grafiker.de", "mail@grafiker.de", $subjetsys, $body);
                        }    
                            $emdel->flush();
        return $this->render('pdf/index.html.twig', [
            'controller_name' => 'PdfController',
            'gesendet' => 1,
            'pdfname' => $pdfname,
        ]);
    }

    /**
     * Create
     * @Route("/pdf/start/{pinid}/{pin}", name="pdf_start")
     */
    public function createStart($pinid, $pin)
    { 
        $builder = $this->createForm(PdfrechnungFormType::class);
        return $this->render('pdf/index.html.twig', [
            'form' => $builder->createView(),
            'pin' => $pin,
            'pinid' => $pinid,
            'controller_name' => 'PdfController',
        ]);
    }
}
