<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class SavepindataController extends Controller
{
    /**
     * @Route("/savepindata", name="savepindata")
     */
    public function index(Request $request)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $username = $user->getUsername();
        } else {
            $username = 'Kein Mitglied!';
        }

        $newarray=$request->request->all();
        $formtitle=$request->request->all()["formtitle"];
        $formkategorie=$request->request->all()["formkategorie"];
        $textareacontent=$request->request->all()["textareacontent"];
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
if($pincheck) {

    $pincheck->setTitle($formtitle);
            if(strlen($formtitle) >= 100) {
            $pinlinkindb=preg_replace("/[^ ]*$/", '', substr($data["titel"], 0, 100));
            $pinlinkindb=substr($pinlinkindb, 0, -1);
            } else {
                $pinlinkindb = $formtitle;
            }
            $pinlinkindb = str_replace(" ", "_", $pinlinkindb);
            $pindatagesamt = $emdel->getRepository('App:Pins')->findBy(array('title' => $formtitle),array('id' => 'ASC'));
            $anzindb=count($pindatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $pinlinkindb = $pinlinkindb.'_'.$anzindb;
            } else {
                $pinlinkindb = $pinlinkindb;
            }

            $picarray=array();
    $pincheck->setPinlink($pinlinkindb);
    $pincheck->setContent($textareacontent);
    $pincheck->setPinkategorie($formkategorie);
    if(!$pincheck->getPicname()) {
    $pincheck->setPicname($picarray);
    }
    $emdel->flush(); 
} 
        //var_dump($pincheck);

        return $this->render('savepindata/index.html.twig', [
            'controller_name' => 'SavepindataController',
        ]);
    }
}
