<?php
namespace App\Controller;
use App\AppBundle\Form\ProjektkalkulatorFormType;
use App\Document\Projektrechner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ProjektrechnerergebnisController extends Controller
{
    /**
     * @Route("/projektrechnerergebnis", name="projektrechnerergebnis")
     */
    public function new(Request $request)
    {

if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}
$dm = $this->get('doctrine_mongodb')->getManager();
$kategorien = $dm->getRepository('App:Projektrechnerkategorien')->findall();
$dm->flush();
$extarray=array();

foreach($kategorien as $key=>$value){
    $extarray[$value->getKatname()] = $value->getKatname();
  }
        

$task = new Projektrechner();

$builder = $this->createForm(ProjektkalkulatorFormType::class, $extarray);
$builder->add('username', HiddenType::class, array(
    'data' => $username,
));
$builder->handleRequest($request);
$data = $builder->getData();
$task->setUsername($data["username"]);
$task->setStundensatz($data["stundensatz"]);
$task->setPrstart($data["prstart"]);
$task->setSchwer($data["schwer"]);
$task->setNart($data["nart"]);
$task->setNumfang($data["numfang"]);

$dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            $dms = $this->get('doctrine_mongodb')->getManager();
            $katdauer = $dms->getRepository('App:Projektrechnerkategorien')->findOneByKatname($data["prstart"]);
            $dms->flush();
$task->setDauer($katdauer->getDauer());
$basis_preis = round($data["stundensatz"]*$katdauer->getDauer(), 2);
if($data["schwer"] == 1) {
    $wertSchwierigkeit = 0;
    $dauer=$katdauer->getDauer();
} else if($data["schwer"] == 2) {
    $wertSchwierigkeit = $basis_preis * 0.5;
    $dauer=$katdauer->getDauer() * 1.5;
} else if($data["schwer"] == 3) {
    $wertSchwierigkeit = $basis_preis * 1.0;
    $dauer=$katdauer->getDauer() * 2.0;
}
if($data["nart"] == 1) {
    $wertNutzungsart = 0;
} else if($data["nart"]  == 2) {
    $wertNutzungsart = $basis_preis * 0.8;
}
if($data["numfang"] == 1) {
    $wertNutzungsumfang = 0;
} else if($data["numfang"] == 2) {
    $wertNutzungsumfang = $basis_preis * 0.5;
} else if($data["numfang"] == 3) {
    $wertNutzungsumfang = $basis_preis * 1.2;
}

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);
            $dms = $this->get('doctrine_mongodb')->getManager();
            $katdauer = $dms->getRepository('App:Projektrechnerkategorien')->findOneByKatname($task->getPrstart());
            $dms->flush();*/

$dauer = ( !preg_match('/\.[1-9]+$/is',$dauer) ) ? intval($dauer) : number_format($dauer,2,',','.');
$preis_total = round($basis_preis+$wertSchwierigkeit+$wertNutzungsart+$wertNutzungsumfang,2);
$preis_total = number_format($preis_total,2,',','.');
$stundensatz = number_format($data["stundensatz"],2,',','.');
return $this->render('projektrechner/laststep.html.twig', array(
    'form' => $builder->createView(),    
    'ausgabedauer' => $dauer . 'h ',
        'ausgabestundensatz' =>  $stundensatz . ' €',
        'ausgabegesamt' => $preis_total . ' €',
        'berechnung' => '',
    ));

}
}
