<?php

namespace App\Controller;
use App\Document\Article;
use App\Document\Ratingblock;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Maparticleg\Annotations as MongoDB;
class UserArticleviewController extends Controller
{
    /**
     * @Route("/articleview/{articleid}", name="user_articleview")
     */
    public function index($articleid)
    {
        $nextarticledata = new Article();
        $prevarticledatab = new Article();
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        //var_dump($articleid);
        $articledata = $dm->getRepository('App:Article')->findBy(array('articlelink' => $articleid));
        //var_dump($articledata);
//var_dump($articledata);
        $picsarray = array();
        foreach($articledata as $key => $articlepics) {

            $prevarticledata = $dm->createQueryBuilder('App:Article')->sort(array('startdate' => 'ASC'))->field('startdate')->gte($articlepics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('articlekategorie')->equals($articlepics->getArticlekategorie())->getQuery()->toArray();
            $nextarticledata = $dm->createQueryBuilder('App:Article')->sort(array('startdate' => 'DESC'))->field('startdate')->lte($articlepics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('articlekategorie')->equals($articlepics->getArticlekategorie())->getQuery()->toArray();

            $picsarray = $articlepics->getPicname();
            $content = HashToLinkController::HashToLink($articlepics->getContent());

        }
        foreach($articledata as $key => $articleuser) {
            $dmu = $this->get('doctrine_mongodb')->getManager();
        $userdatas = $dmu->getRepository('App:User')->findOneBy(["username"=>$articleuser->getUsername()]);
        $fileexistarray[$articleuser->getUsername()] = $userdatas->getUserpic();
        $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
        $articlelastLogin[$userdatas->getUsername()] = $userdatas->getUpdatedAt();
        $useremploy[$userdatas->getUsername()] = $userdatas->getEmploy();
        
        }
        $dms = $this->get('doctrine_mongodb')->getManager();
        //var_dump($articleid);
        foreach($articledata as $key => $articlepics) {
        $articledatas = $dms->getRepository('App:Article')->find($articlepics->id);
        //var_dump($articledatas);
        $articleviews = $articledatas->getArticleviews()+1;
        $articledatas->setArticleviews($articleviews);
        $date = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $olddate = $articledatas->getViewdate();
        $articledatas->setViewdate($date); 
        $titlealt=$articledatas->getTitle();
        $emdelblock = $this->get('doctrine_mongodb')->getManager();
        $ratingblockcheck = $emdelblock->getRepository('App:Ratingblock')->findOneBy(array("articleid"=>$articlepics->id, "username"=>$username));
        if(!$ratingblockcheck) {
            $ratingok=true;
        } else {
            $ratingok=false;
        }
        $emdelblock->flush();
        }
        $dm->flush();

$title = str_replace("!", "", $titlealt);
$title = str_replace("?", "", $title);
$title = str_replace("%", "", $title);
$title = str_replace("$", "", $title);
$title = str_replace(".....", "", $title);
$title = str_replace("....", "", $title);
$title = str_replace("...", "", $title);
$title = str_replace(" - ", " ", $title);
$title = explode(" ",$title);
$articledatacount = array();
foreach($title as $key => $searchtitle) {
    

            /*$searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    )
            );*/

                $articledatacount[] = $dm->createQueryBuilder('App:Article')->sort(array('isads' => 'DESC'))->field('content')->equals(new \MongoRegex('/'.$searchtitle.'/si'))->limit(6)->getQuery()->toArray();
                //$articledatacount[$key] = $dm->getRepository('App:Article')->findBy($searchQuery,array('startdate' => 'DESC'));
}
$articlefileexistarray=array();
$articlefilelinkarray=array();
$userexist=array();
$wohnort=array();
$articleuseremploy=array();
$articlelastLoginUser=array();
foreach($articledatacount as $articledatacounts) {
    foreach($articledatacounts as $articledatacounts) {
        if($titlealt != $articledatacounts->getTitle()) {
            $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$articledatacounts->getUsername()]);
            if($userdatas) {
            $articlefileexistarray[$articledatacounts->getUsername()] =$userdatas->getUserpic();
            $articlefilelinkarray[$articledatacounts->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
            $wohnort[$articledatacounts->getUsername()] = $userdatas->getWohnort();
            $articlelastLoginUser[$articledatacounts->getUsername()] = $userdatas->getUpdatedAt();
            $articleuseremploy[$articledatacounts->getUsername()] = $userdatas->getEmploy();
            $userexist[$articledatacounts->getUsername()] = true;
            } else {
            $userexist[$articledatacounts->getUsername()] = false;
            }
        }
    }
}
//$articledatacount[] = array_unique($articledatacount);
shuffle($articledatacount);
        return $this->render('userprofil/articleview.html.twig', [
            'controller_name' => 'AdminArticleviewController',
            'articledata' => $articledata,
            'contenttxt' => $content,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'picsarray' => $picsarray,
            'olddate' => $olddate,
            'articlelastLogin' => $articlelastLogin,
            'useremploy' => $useremploy,
            'ratingok' => $ratingok,
            'articleviewcontroller' => 1,
            'nextarticledata' => $nextarticledata,
            'prevarticledata' => $prevarticledata,
            'articledatacount' => $articledatacount,
            'userexist' => $userexist,
            'articlefileexistarray' => $articlefileexistarray,
            'articlefilelinkarray' => $articlefilelinkarray,
            'wohnort' => $wohnort,
            'articleuseremploy' => $articleuseremploy,
            'articlelastLoginUser' => $articlelastLoginUser,
        ]);
    }

}
