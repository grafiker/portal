<?php

namespace App\Controller;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\AppBundle\Form\ProfileTypeUpdate;
use Symfony\Component\HttpFoundation\Request;
class UserprofilTypeController extends Controller
{
    /**
     * @Route("/profile/type", name="userprofil_type")
     */
    public function index(Request $request)
    {
        $searching = new User();
        $searchingbuilder = $this->createForm(ProfileTypeUpdate::class, $searching);
        $searchingbuilder->handleRequest($request);
        return $this->render('userprofil/type.html.twig', [
            'form' => $searchingbuilder->createView(),
            'controller_name' => 'UserprofilTypeController',
        ]);
    }
}
