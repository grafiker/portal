<?php

namespace App\Controller;
use App\Document\Network;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class MembernetworkController extends Controller
{
    /**
     * @Route("/profile/membernetwork/all", name="membernetwork")
     */
    public function allAction()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        
        $page= 0;
        $anz = 10;
        $searchQuery = array(
            '$or' => array(
                array(
                    'username' => $users->getUsername(), 'accepted' => 1, 'blocked' => NULL,
                    ),
                array(
                    'friend' => $users->getUsername(), 'accepted' => 1, 'blocked' => NULL,
                    ),
                )
            );
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:Network')->findBy($searchQuery,array('startdate' => 'DESC'), $anz, $page);
            $printnewarray = array();
            $networkreason = array();
            foreach($userdata as $key => $userdata) {
                if($userdata->getUsername() == $users->getUsername()) {
                    $searching = $userdata->getFriend();
                    $networkreason[$searching] = $userdata->getReason();
                } else {
                    $searching = $userdata->getUsername();
                    $networkreason[$searching] = false;
                }
                $userdatagesamt[$key] = $dm->getRepository('App:User')->findOneBy(array('username' => $searching));
                $printnewarray[$key] = $userdatagesamt;
            }
            
            foreach($printnewarray as $key => $printnewarray) {
                //var_dump($printnewarray->getUsername());
            }
            return $this->render('userprofil/membernetwork.html.twig', [
            'controller_name' => 'All MembernetworkController',
            'memberdata' => $printnewarray,
            'site' => 'all',
            'networkreason' => $networkreason,
            
        ]);
    }
    /**
     * @Route("/profile/membernetwork/in", name="in_membernetwork")
     */
    public function inAction()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        
        $page= 0;
        $anz = 10;
        $searchQuery = array(
            '$or' => array(
                array(
                    'friend' => $users->getUsername(), 'accepted' => 0, 'blocked' => NULL,
                    ),
                )
            );
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:Network')->findBy($searchQuery,array('startdate' => 'DESC'), $anz, $page);
            $printnewarray = array();
            $networkask = array();
            foreach($userdata as $key => $userdata) {
                if($userdata->getUsername() == $users->getUsername()) {
                    $searching = $userdata->getFriend();
                } else {
                    $searching = $userdata->getUsername();
                }
                $userdatagesamt[$key] = $dm->getRepository('App:User')->findOneBy(array('username' => $searching));
                $printnewarray[$key] = $userdatagesamt;
                $networkask[$searching] = $userdata->getRequest();
            }
            
            foreach($printnewarray as $key => $printnewarray) {
                //var_dump($printnewarray->getUsername());
            }
            return $this->render('userprofil/membernetwork.html.twig', [
            'controller_name' => 'In MembernetworkController',
            'memberdata' => $printnewarray,
            'site' => 'in',
            'networkask' => $networkask,
        ]);
    }
    /**
     * @Route("/profile/membernetwork/out", name="out_membernetwork")
     */
    public function outAction()
    {

        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        
        $page= 0;
        $anz = 10;
        $searchQuery = array(
            '$or' => array(
                array(
                    'username' => $users->getUsername(), 'accepted' => 0, 'blocked' => NULL,
                    ),
                )
            );
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:Network')->findBy($searchQuery,array('startdate' => 'DESC'), $anz, $page);
            $printnewarray = array();
            $networkask = array();
            $networkreason = array();
            foreach($userdata as $key => $userdata) {
                if($userdata->getUsername() == $users->getUsername()) {
                    $searching = $userdata->getFriend();
                } else {
                    $searching = $userdata->getUsername();
                }
                $userdatagesamt[$key] = $dm->getRepository('App:User')->findOneBy(array('username' => $searching));
                $printnewarray[$key] = $userdatagesamt;
                $networkreason[$searching] = $userdata->getReason();
                $networkask[$searching] = $userdata->getRequest();
            }
            //var_dump($userdatagesamt);
            
            foreach($printnewarray as $key => $printnewarray) {
                //var_dump($printnewarray->getUsername());
            }
            return $this->render('userprofil/membernetwork.html.twig', [
            'controller_name' => 'Out MembernetworkController',
            'memberdata' => $printnewarray,
            'site' => 'out',
            'networkreason' => $networkreason,
            'networkask' => $networkask,
        ]);
    }

    /**
     * @Route("/profile/membernetwork/{username}/{friendid}/{site}", name="update_membernetwork")
     */
    public function outUpdate(RequestStack $requestStack, $username, $friendid, $site)
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $searchQuery = array(
            '$or' => array(
                array(
                    'username' => $users->getUsername(), 'friend' => $username,
                    ),
                array(
                    'friend' => $users->getUsername(), 'username' => $username,
                    ),
                )
            );
        //$friendsdatas = new Network();
        if($site == "out" or $site == "stop" or $site == "not") {
            //Anfrage zurückziehen!
            $dm = $this->get('doctrine_mongodb')->getManager();
            $friendsdatas = $dm->getRepository('App:Network')->findOneBy($searchQuery);
            var_dump($friendsdatas);
            $dm->remove($friendsdatas);
            $dm->flush();
            if($site == "not") {
                $emdjabsender = $this->get('doctrine_mongodb')->getManager();
                $usersender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>$username]);
                $userabsender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>'grafiker.de']);
                $threadBuilder = $this->get('fos_message.composer')->newThread();
                $threadBuilder
                ->addRecipient($usersender) // Retrieved from your backend, your user manager or ...
                ->setSender($userabsender)
                ->setSubject('%%Systemmail%% Netzwerkbeitrittsanfrage wurde abgelehnt!')
                ->setBody('Ihre Netzwerkanfrage wurde von ' . $users->getUsername() . ' abgelehnt!<br /><br /><a href="' .$requestStack->getCurrentRequest()->getSchemeAndHttpHost(). '/profile/membernetwork/all">Klicken Sie bitte hier um Ihr Netzwerk aufzurufen!</a>');
             
             
            $sender = $this->get('fos_message.sender');
            
            $sender->send($threadBuilder->getMessage());
            }

        } else if($site == "ok") {
            //Anfrage annehmen!
            $dm = $this->get('doctrine_mongodb')->getManager();
            $friendsdatas = $dm->getRepository('App:Network')->findOneBy($searchQuery);
            

            $emdjabsender = $this->get('doctrine_mongodb')->getManager();
                $usersender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>$username]);
                $userabsender = $emdjabsender->getRepository('App:User')->findOneBy(["username"=>'grafiker.de']);
                $threadBuilder = $this->get('fos_message.composer')->newThread();
                $threadBuilder
                ->addRecipient($usersender) // Retrieved from your backend, your user manager or ...
                ->setSender($userabsender)
                ->setSubject('%%Systemmail%% Netzwerkbeitrittsanfrage wurde angenommen!')
                ->setBody('Ihre Netzwerkanfrage wurde von ' . $users->getUsername() . ' angenommen und befindet sich jetzt mit in Ihren Netzwerk!<br /><br /><a href="' .$requestStack->getCurrentRequest()->getSchemeAndHttpHost(). '/profile/membernetwork/all">Klicken Sie bitte hier um Ihr Netzwerk aufzurufen!</a>');
             
             
            $sender = $this->get('fos_message.sender');
            
            $sender->send($threadBuilder->getMessage());


            var_dump($friendsdatas);
        $friendsdatas->setAccepted(1);
        $dm->flush();
        }
        return $this->render('networkupdate/loaddata.html.twig', [
            'controller_name' => 'Out MembernetworkController',
        ]);
    }
}
