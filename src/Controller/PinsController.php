<?php

namespace App\Controller;
use App\AppBundle\Form\PinsFormType;
use App\AppBundle\Form\PinsCropFormType;
use App\AppBundle\Form\PinsEditFormType;
use App\Controller\CheckUserdataController;
use App\Document\Pins;
use App\Controller\CropController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class PinsController extends Controller
{
    /**
     * @Route("/profile/selection/start", name="pins_selection")
     */
    public function selectionAction(Request $request)
    {
        return $this->render('userprofil/pinstart.html.twig', array(
            'controller_name' => 'ShowcaseController',
        ));
    }

    /**
     * @Route("/profile/pins/start", name="pins_start")
     */
    public function createStart(Request $request, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        
        $pindatesoldcheck = $emdel->getRepository('App:Pins')->findOneBy(['username'=>$username, 'astitle'=>'Pinstart', 'aktiv'=>true]);
        if($pindatesoldcheck) {
            //var_dump($pindates);
            $pindatesoldcheck->setAstitle("Pininsert");
            $emdel->flush();
            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/pinview/'.$pindatesoldcheck->getPinlink();
            $subjetsys = "Es gibt einen Pin zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('pins@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "pins@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);
        }
        $pindatesoldcheck = $emdel->getRepository('App:Pins')->findOneBy(['username'=>$username, 'astitle'=>'Ruhezustand', 'aktiv'=>true]);
        if($pindatesoldcheck) {
            //var_dump($pindates);
            $pindatesoldcheck->setAstitle("Pinstart");
            $emdel->flush();
        }
        $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
    if($pincheck) {
        if($pincheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($pincheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/pinpics/".$delpics)) {
                    //unlink("./upload/uploads/pinpics/".$delpics);
                }
            }
        }
    }
    if($pincheck) {
        if(!$pincheck->getPicname()) {

        //$emdel->remove($pincheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
        $casepics = array();
        if (!$pincheck) {
            $pinergebnis=0;
            //$casepics={};
            $pinid = 0;
        } else {
            $pinergebnis=1;
            $casepics=$pincheck->getPicname();
            $pinid = $pincheck->getId();
        }
        $reset = false;
        $pinergebnis=0;
        $builder = $this->createForm(PinsFormType::class);
        return $this->render('userprofil/pinsmedia.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'pinergebnis' => $pinergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'pinID' => $pinid,
        ));
    }

    /**
     * @Route("/profile/pins/{pinid}/editor", name="pins_erditor")
     */
    public function createEdit(RequestStack $requestStack, Request $request, $pinid, \Swift_Mailer $mailer)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $emdel = $this->get('doctrine_mongodb')->getManager();
        $pindatesoldcheck = $emdel->getRepository('App:Pins')->findOneBy(['username'=>$username, 'astitle'=>'Pinstart', 'aktiv'=>NULL]);
        if($pindatesoldcheck) {
            //var_dump($pindates);
            $pindatesoldcheck->setAstitle("Ruhezustand");
            $emdel->flush();
        }

        $pindatesoldcheck = $emdel->getRepository('App:Pins')->findOneBy(['username'=>$username, 'astitle'=>'Pinstart', 'aktiv'=>true]);
        if($pindatesoldcheck) {
            //var_dump($pindates);
            if($pindatesoldcheck->getID() != $pinid)
            $pindatesoldcheck->setAstitle("Pininsert");
            $emdel->flush();

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/pinview/'.$pindatesoldcheck->getPinlink();
            $subjetsys = "Es gibt einen Pin zum prüfen!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('pins@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "pins@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von ' .$username. ' ein Pin auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Pin: ' . $linkToView);
        }
        
        $pindates = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "id"=>$pinid]);
        
        if($pindates) {
            //var_dump($pindates);
            $pindates->setAstitle("Pinstart");
            $emdel->flush();
        }
        $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
    if($pincheck) {
        if($pincheck->getPicname() and $username != 'Kein Mitglied!') {
            foreach($pincheck->getPicname() as $key => $delpics) {
                if(file_exists("./upload/uploads/pinpics/".$delpics)) {
                    //unlink("./upload/uploads/pinpics/".$delpics);
                }
            }
        }
    }
    if($pincheck) {
        if(!$pincheck->getPicname()) {

        //$emdel->remove($pincheck);
        //$emdel->flush();
        }
    }

    $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
        $casepics = array();
        if (!$pincheck) {
            $pinergebnis=0;
            //$casepics={};
            $pinid = 0;
        } else {
            $pinergebnis=1;
            $casepics=$pincheck->getPicname();
            $pinid = $pincheck->getId();
        }
        $reset = false;
        $pinergebnis=0;
        $builder = $this->createForm(PinsFormType::class);
        return $this->render('userprofil/pinsmedia.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'pinergebnis' => $pinergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'pinID' => $pinid,
        ));
    }
    /**
     * @Route("/profile/pins/poststart", name="pins_poststart")
     */
    public function createPoststart(Request $request)
    {
        $reset = false;
        $task = new Pins();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Pinskategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } else {}
        }
        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
        $casepics = array();
        $pinergebnis=1;
        if($pincheck) {
        $casepics=$pincheck->getPicname();
        } else {

            $task->setUsername($username);
            $data[] = NULL;
            //$task->setPicname($data);
            $task->setAstitle("Pinstart");
            $task->setPinkategorie("Showcase");
            $task->setPicname($casepics);
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();
            $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
            $casepics=$pincheck->getPicname();
        }
        $newextarray=array(array("pinkategorie" => $pincheck->getPinkategorie(),"isads" => $pincheck->getIsads(),"bewerten" => $pincheck->getBewerten(),"title" => $pincheck->getTitle(),"content" => $pincheck->getContent(),"rechte" => $pincheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(PinsEditFormType::class, $newextarray);
        return $this->render('userprofil/pins.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ShowcaseController',
            'pinergebnis' => $pinergebnis,
            'picsarray' => $casepics,
            'reset' => $reset,
            'pinid' => $pincheck->getId(),
        ));
    }
}
    /**
     * @Route("/profile/pins/{getdata}", name="pins")
     */
    public function createAction(Request $request, $getdata, \Swift_Mailer $mailer)
    {
        $crop = new CropController();
        $task = new Pins();
        $reset = false;
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            if($getdata == "reset") {
                $emdel = $this->get('doctrine_mongodb')->getManager();
                $pincheck = $emdel->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
            if($pincheck->getPicname()) {
                foreach($pincheck->getPicname() as $key => $delpics) {
                    if(file_exists("./upload/uploads/pinpics/".$delpics)) {
                        unlink("./upload/uploads/pinpics/".$delpics);
                    }
                }
            }
                $emdel->remove($pincheck);
                $emdel->flush();
                $reset = true;
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Pinskategorien')->findall();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        if($username != 'Kein Mitglied!') {
        foreach($kategorien as $key=>$value){
            if (in_array("Admin", $value->getRolegroup()) AND in_array('ROLE_ADMIN', $user->getRoles())) {
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
                } else if (!in_array("Admin", $value->getRolegroup())) {
                $extarray[$value->getPinkatname()] = $value->getPinkatname();
                }
                if (in_array("Free", $value->getRolegroup()) AND in_array('ROLE_USER', $user->getRoles())) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } 
                if (in_array("Standart", $value->getRolegroup()) AND (in_array('ROLE_PROFI', $user->getRoles()) or in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } 
                if (in_array("Platin", $value->getRolegroup()) AND (in_array('ROLE_PLATIN', $user->getRoles()) or in_array('ROLE_ADMIN', $user->getRoles()))) {
                    $nextextarray[$value->getPinkatname()] = $value->getPinkatname();
                } else {}
        }
    }
        $em = $this->get('doctrine_mongodb')->getManager();
        $pincheck = $em->getRepository('App:Pins')->findOneBy(["username"=>$username, "astitle"=>"Pinstart"]);
        $casepics = array();
        if (!$pincheck) {
            $pinergebnis=0;
            $casepics[0]="keins";
            $builder = $this->createForm(PinsFormType::class, $extarray);
        } else {
            $pinergebnis=1;
            $casepics=$pincheck->getPicname();
            $newextarray=array(array("pinkategorie" => $pincheck->getPinkategorie(),"isads" => $pincheck->getIsads(),"bewerten" => $pincheck->getBewerten(),"title" => $pincheck->getTitle(),"content" => $pincheck->getContent(),"rechte" => $pincheck->getRechte(), "data" => array($extarray)));
            $builder = $this->createForm(PinsEditFormType::class, $newextarray);
        }

        $builder->handleRequest($request);
        $data = $builder->getData();
        $temp = (array) $request->request->all();
        //var_dump($temp);
        if(isset($temp["form"]["pinpay"])) {
            $pinpay = true;
        } else {
            $pinpay = false;
        }
        if(isset($temp["form"]["update"])) {
            $update = true;
        } else {
            $update = false;
        }
        
        if((isset($data["titel"])) or (isset($data["w"]))) {
            if((!isset($data["w"]) or ($data["w"] == 0))) {
                //var_dump("thumbnail_".$data["picselected"]);
            $date = \DateTime::createFromFormat('U', time()+3600);
            $date->setTimezone(new \DateTimeZone('UTC'));
            $pincheck->setPinKategorie($data["pinkategorie"]);
            $pincheck->setTitle($data["titel"]);
            if(strlen($data["titel"]) >= 100) {
            $pinlinkindb=preg_replace("/[^ ]*$/", '', substr($data["titel"], 0, 100));
            $pinlinkindb=substr($pinlinkindb, 0, -1);
            } else {
                $pinlinkindb = $data["titel"];
            }
            $pinlinkindb = str_replace(" ", "_", $pinlinkindb);
            $pindatagesamt = $dm->getRepository('App:Pins')->findBy(array('title' => $data["titel"]),array('id' => 'ASC'));
            $anzindb=count($pindatagesamt);
            //var_dump($anzindb);
            if($anzindb) {
                $anzindb = $anzindb+1;
                $pinlinkindb = $pinlinkindb.'_'.$anzindb;
            } else {
                $pinlinkindb = $pinlinkindb;
            }
            //var_dump($data["picselected"]);
            if($data["picselected"] != "0") {
                $data["picselected"] = $data["picselected"];
                //var_dump($pincheck->getPicname()[0]);
            } else {
                $data["picselected"] = $pincheck->getPicname()[0];
                //var_dump($pincheck->getPicname()[0]);
            }
            //var_dump($data);
            $pincheck->setPinlink($pinlinkindb);
            $pincheck->setContent($data["content"]);
            $pincheck->setIsads($data["isads"]); 
            $pincheck->setBewerten($data["bewerten"]);
            $pincheck->setRechte($data["rechte"]);
            $pincheck->setAktiv(false);
            $pincheck->setStartdate($date); 
            //$pincheck->setThumbnail("thumbnail_".$data["picselected"]);      
            $em->flush();    
        }   
            

            $upload_dir = "upload/uploads/pinpics"; 				// The directory for the images to be saved in
            $upload_path = $upload_dir."/";				// The path to where the image will be saved
            $large_image_prefix = "resize_"; 			// The prefix name to large image
            $thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
            $large_image_name = $data["picselected"];     // New name of the large image (append the timestamp to the filename)
            $thumb_image_name = "".$thumb_image_prefix.$data["picselected"];
            $large_image_location = $upload_path.$large_image_name;
            $thumb_image_location = $upload_path.$thumb_image_name;
            $thumb_width = 181;
            $thumb_height = 168;
            $max_width = 600;
            $width = $large_image_location;
			$height = $large_image_location;
			//Scale the image if it is greater than the width set above
			if ($width > $max_width){
				$scale = $max_width/$width;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}else{
				$scale = 1;
				//$uploaded = $crop->resizeImage($large_image_location,$width,$height,$scale);
			}		
            //$current_large_image_width = $crop->getWidth($large_image_location);
            //$current_large_image_height = $crop->getHeight($large_image_location);

            if (file_exists($large_image_location)){
                if(file_exists($thumb_image_location)){
                    $thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name."\" alt=\"Thumbnail Image\"/>";
                }else{
                    $thumb_photo_exists = "";
                }
                   $large_photo_exists = "<img src=\"".$upload_path.$large_image_name."\" alt=\"Large Image\"/>";
            } else {
                   $large_photo_exists = "";
                $thumb_photo_exists = "";
            }
            if ((isset($data["w"]) AND ($data["w"] != 0)) && strlen($large_photo_exists)>0) {
                //Get the new coordinates to crop the image.
                $x1 = $data["x1"];
                $y1 = $data["y1"];
                $x2 = $data["x2"];
                $y2 = $data["y2"];
                $w = $data["w"];
                $h = $data["h"];
                //Scale the image to the thumb_width set above
                $scale = $thumb_width/$w;
                //$cropped = $crop->resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
                $pincheck->setAstitle('Pininsert');
                $pincheck->setIsads($data["isads"]); 
                $pincheck->setBewerten($data["bewerten"]);
                $pincheck->setRechte($data["rechte"]);
                $em->flush();
            $subjetsys = "Neuer Showcase zum freischalten!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('showcase@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde ein neuer Showcase von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
            $mailer->send($message);
            $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "showcase@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde ein neuer Showcase von: ' .$username. ' auf Grafiker.de eingetragen und wartet auf Freischaltung!');
                return $this->render('userprofil/pinsend.html.twig', array(
                    'controller_name' => 'ShowcaseController',
                ));
            } else {

                $builder = $this->createForm(PinsCropFormType::class);
                $builder->add('picselected', HiddenType::class, array(
                    'data' => $data["picselected"],
                    'required' => false,
                ));
if(!in_array($data["pinkategorie"], $nextextarray)) {
                $builder->add('pinkategorie', ChoiceType::class, array(
                    'label' => 'Kategorie wechseln? ',
                    'choices' => $nextextarray,
                ));
                $builder->add('update', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Account updaten...'));
                $builder->add('pinpay', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Pin kaufen...'));
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Pin mit neuer Kategorie eintragen...'));
} else {
                $builder->add('profi', SubmitType::class, array('attr' => array('class' => 'pininsert bdnmini2 btn btn-primary pull-left'),'label' => 'Showcase eintragen...'));
}

                


                $emp = $this->get('doctrine_mongodb')->getManager();
                $pinkategoriecheck = $emp->getRepository('App:Pinskategorien')->findOneBy(["pinkatname"=>$data["pinkategorie"]]);
                $rolein = 0;
                if(in_array("Admin", $pinkategoriecheck->getRolegroup())) { $rolein = "Admin"; }
                if(in_array("Free", $pinkategoriecheck->getRolegroup())) { $rolein = "Free"; }
                if(in_array("Standart", $pinkategoriecheck->getRolegroup())) { $rolein = "Profi"; }
                if(in_array("Platin", $pinkategoriecheck->getRolegroup())) { $rolein = "Platin"; }
                $emp->flush();
                $userdata = new CheckUserdataController();               
                return $this->render('userprofil/pinsnext.html.twig', array(
                    'form' => $builder->createView(),
                    'controller_name' => 'ShowcaseController',
                    'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $username),
                    'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $username),
                    'pinergebnis' => $pinergebnis,
                    'picsarray' => $casepics,
                    'pinTitel' => $data["titel"],
                    'pinKategorie' => $data["pinkategorie"],
                    'pinContent' => html_entity_decode(HashToLinkController::HashToLink($data["content"])),
                    'pinroles' => $rolein,
                    'thumb_width' => $thumb_width,
                    'thumb_height' => $thumb_height,
                    'current_large_image_width' => $thumb_width,
                    'current_large_image_height' => $thumb_height,
                    'pinauswahlpic' => $large_image_location,
                    'picselected' => $data["picselected"],
                    'reset' => $reset,
                ));
            }
        } else {

            return $this->render('userprofil/pinsmedia.html.twig', array(
                'form' => $builder->createView(),
                'controller_name' => 'ShowcaseController',
                'pinergebnis' => $pinergebnis,
                'picsarray' => $casepics,
                'reset' => $reset,
            ));
        }
        
    }

    public function hashToLink($texttolink) {
        $tweet = $texttolink;
        $regex = "/#+([a-zA-Z0-9_]+)/";
        $text = preg_replace($regex, '<a href="/filter/Search/$1">#$1</a>', $tweet);
        
        return $text;
        }
}
