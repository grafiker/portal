<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PinstartController extends AbstractController
{
    /**
     * @Route("/profile/pinstart", name="pinstart")
     */
    public function index()
    {
        return $this->render('userprofil/pinstart.html.twig', [
            'controller_name' => 'PinstartController',
        ]);
    }
}
