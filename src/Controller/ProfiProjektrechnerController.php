<?php
namespace App\Controller;
use App\Document\ProfiProjektrechner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class ProfiProjektrechnerController extends Controller
{
    /**
     * @Route("/profi/projektrechner", name="profi_projektrechner")
     */
    public function new(Request $request)
    {

if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
{
    $user = $this->container->get('security.token_storage')->getToken()->getUser();
    $username = $user->getUsername();
} else {
    $username = 'Kein Mitglied!';
}

        $task = new ProfiProjektrechner();
        $builder = $this->createFormBuilder($task)
        ->add('username', HiddenType::class, array(
            'data' => $username,
        ))
        /*->add('startdate', DateType::class, array(
            'widget' => 'single_text',
            'label' => 'Projektstart: ',
            'years' => range(date('Y'), date('Y')+10),
            'months' => range(date('m'), date('m')+12),
            'days' => range(date('d'), date('d')+31),
        ))*/
        ->add('prstart', ChoiceType::class, array(
            'label' => 'Profi-Projektart: ',
            'choices' => array(
                'Projektart' => array(
                    '- gewünschte Projektart wählen -' => '0',
                    'Abzeichen' => '1',
                )
            ),
        ))
        ->add('stundensatz', TextType::class, array('label' => 'Stundensatz: ','data' => '55',))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-right'),'label' => 'weiter...'))
        ->getForm();

        $builder->handleRequest($request);
        if ($builder->isSubmitted() && $builder->isValid()) {
            $task = $builder->getData();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($task);
            $dm->flush();

            /*$message = (new \Swift_Message('Hello Email'))
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('adgigant@gmail.com');
            $this->get('mailer')->send($message);*/

            return $this->render('profi_projektrechner/nextstep.html.twig', array(
                'name' => 'hier geht es weiter...',
            ));
        }

        return $this->render('profi_projektrechner/index.html.twig', array(
            'form' => $builder->createView(),
            'controller_name' => 'ProfiProjektrechnerController',
        ));
    }
}
