<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Document\User;
use App\AppBundle\Form\AdminMailerFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class AdminMailerController extends Controller
{
    /**
     * @Route("/admin/mailer", name="admin_mailer")
     */
    public function index(RequestStack $requestStack, Request $request, \Swift_Mailer $mailer)
    {
        set_time_limit(3000);
        $newarray=$request->request->all();
        //var_dump($newarray);
        if($newarray) {
            $dm = $this->get('doctrine_mongodb')->getManager();
            //var_dump('Jetzt versenden...');
            /*$searchQuery = array();
            $serchdata1 = array();
            $serchdata2 = array();
            $serchdata3 = array();
            $serchdata4 = array();
            $serchdata5 = array();
            $serchdata6 = array();
            $serchdata7 = array();
            

        if(isset($newarray['form']["berufsgruppe1"]) or isset($newarray['form']["berufsgruppe2"]) or isset($newarray['form']["berufsgruppe3"]) or isset($newarray['form']["berufsgruppe4"]) or isset($newarray['form']["berufsgruppe5"]) or isset($newarray['form']["berufsgruppe6"]) or isset($newarray['form']["berufsgruppe7"])) {
            
            if(isset($newarray['form']["berufsgruppe1"])) {
                $serchdata1 = $dm->getRepository('App:User')->findBy(array("berufsgruppe1" => true),array('id' => 'ASC'));
            }    
            if(isset($newarray['form']["berufsgruppe2"])) {
                $serchdata2 = $dm->getRepository('App:User')->findBy(array("berufsgruppe2" => true),array('id' => 'ASC'));
            }  
            if(isset($newarray['form']["berufsgruppe3"])) {
                $serchdata3 = $dm->getRepository('App:User')->findBy(array("berufsgruppe3" => true),array('id' => 'ASC'));
            }  
            if(isset($newarray['form']["berufsgruppe4"])) {
                $serchdata4 = $dm->getRepository('App:User')->findBy(array("berufsgruppe4" => true),array('id' => 'ASC'));
            }  
            if(!isset($newarray['form']["berufsgruppe5"])) {
                $serchdata5 = $dm->getRepository('App:User')->findBy(array("berufsgruppe5" => true),array('id' => 'ASC'));
            }  
            if(!isset($newarray['form']["berufsgruppe6"])) {
                $serchdata6 = $dm->getRepository('App:User')->findBy(array("berufsgruppe6" => true),array('id' => 'ASC'));
            }  
            if(!isset($newarray['form']["berufsgruppe7"])) {
                $serchdata7 = $dm->getRepository('App:User')->findBy(array("berufsgruppe7" => true),array('id' => 'ASC'));
            }  
            $searchQuery = array(
                '$or' => array($serchdata1 + $serchdata2 + $serchdata3 + $serchdata4 + $serchdata5 + $serchdata6 + $serchdata7)
                );

        } else {
            $serchdata7 = $dm->getRepository('App:User')->findBy(array(),array('id' => 'ASC'));
        }
$userdatagesamt = $serchdata1 + $serchdata2 + $serchdata3 + $serchdata4 + $serchdata5 + $serchdata6 + $serchdata7;           
            
            $threadBuilder = $this->get('fos_message.composer')->newThread();

        foreach ($userdatagesamt as $recipient) {
            $threadBuilder->addRecipient($recipient);
            //var_dump($recipient->getEmail());
        }*/

        if(isset($newarray['form']["extrasend"])) {
            $teile = explode(";", $newarray['form']["extramails"]);
            foreach($teile as $key => $teile) {
            $newbody = $newarray['form']['content'];

            $message = (new \Swift_Message())
            ->setSubject($newarray['form']["titel"])
            ->setFrom(['mail@grafiker.de' => 'Grafiker.de Systemmail'])
            ->setTo($teile)
            ->setBody($newbody . "<br /><br /><br /><br /><br />Pflichtinformationen gemäß Artikel 13 DSGVO Im Falle des Erstkontakts sind wir gemäß Art. 12, 13 DSGVO verpflichtet, Ihnen folgende datenschutzrechtliche Pflichtinformationen zur Verfügung zu stellen: Wenn Sie uns per E-Mail kontaktieren, verarbeiten wir Ihre personenbezogenen Daten nur, soweit an der Verarbeitung ein berechtigtes Interesse besteht (Art. 6 Abs. 1 lit. f DSGVO), Sie in die Datenverarbeitung eingewilligt haben (Art. 6 Abs. 1 lit. a DSGVO), die Verarbeitung für die Anbahnung, Begründung, inhaltliche Ausgestaltung oder Änderung eines Rechtsverhältnisses zwischen Ihnen und uns erforderlich sind (Art. 6 Abs. 1 lit. b DSGVO) oder eine sonstige Rechtsnorm die Verarbeitung gestattet. Ihre personenbezogenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere steuer- und handelsrechtliche Aufbewahrungsfristen – bleiben unberührt. Sie haben jederzeit das Recht, unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Ihnen steht außerdem ein Recht auf Widerspruch, auf Datenübertragbarkeit und ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Ferner können Sie die Berichtigung, die Löschung und unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten verlangen. Details entnehmen Sie unserer Datenschutzerklärung (https://grafiker.de/datenschutz).<br /><br />Information Provided as Mandated by Article 13 GDPR If this is your first interaction with us, Art. 12, 13 GDPR mandates that we make available to you the following mandatory data protection related information: If you are contacting us via e-mail, we will process your personal data only if we have a legitimate interest in the processing of this data  (Art. 6 Sect. 1 lit. f GDPR), if you have consented to the processing of your data (Art. 6 Sect. 1 lit. a GDPR), if the processing of the data is required for the development, establishment, content or modification of a legal relationship between you and our company (Art. 6 Sect. 1 lit. b GDPR) or if any other legal provision permits the processing of this data. Your personal data will remain in our possession until you ask us to delete the data or you revoke your consent to store the data or if the purpose the data stored is required for no longer exists (e.g. once your request has been conclusively processed). This shall be without prejudice to any compelling statutory provisions – in particular tax and commercial law based retention periods. You have the right to at any time receive free information concerning the origins, recipients and purpose of your data archived by us. You also have a right to object, to data portability and a right to log a complaint with the competent supervisory agency. Moreover, you can demand the correction, eradication and, under certain circumstances, the restriction of the processing of your personal data. For more details, please consult our Data Privacy Policy (https://grafiker.de/datenschutz).");

            $mailer->send($message);
            }

        } else {

            if($newarray['form']["extramails"]) {
                $teile = explode(";", $newarray['form']["extramails"]);
                foreach($teile as $key => $teile) {
            $newbody = $newarray['form']['content'];

                $message = (new \Swift_Message())
                ->setSubject($newarray['form']["titel"])
                ->setFrom(['mail@grafiker.de' => 'Grafiker.de Systemmail'])
                ->setTo($teile)
                ->setBody($newbody . "<br /><br /><br /><br /><br />Pflichtinformationen gemäß Artikel 13 DSGVO Im Falle des Erstkontakts sind wir gemäß Art. 12, 13 DSGVO verpflichtet, Ihnen folgende datenschutzrechtliche Pflichtinformationen zur Verfügung zu stellen: Wenn Sie uns per E-Mail kontaktieren, verarbeiten wir Ihre personenbezogenen Daten nur, soweit an der Verarbeitung ein berechtigtes Interesse besteht (Art. 6 Abs. 1 lit. f DSGVO), Sie in die Datenverarbeitung eingewilligt haben (Art. 6 Abs. 1 lit. a DSGVO), die Verarbeitung für die Anbahnung, Begründung, inhaltliche Ausgestaltung oder Änderung eines Rechtsverhältnisses zwischen Ihnen und uns erforderlich sind (Art. 6 Abs. 1 lit. b DSGVO) oder eine sonstige Rechtsnorm die Verarbeitung gestattet. Ihre personenbezogenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere steuer- und handelsrechtliche Aufbewahrungsfristen – bleiben unberührt. Sie haben jederzeit das Recht, unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Ihnen steht außerdem ein Recht auf Widerspruch, auf Datenübertragbarkeit und ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Ferner können Sie die Berichtigung, die Löschung und unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten verlangen. Details entnehmen Sie unserer Datenschutzerklärung (https://grafiker.de/datenschutz).<br /><br />Information Provided as Mandated by Article 13 GDPR If this is your first interaction with us, Art. 12, 13 GDPR mandates that we make available to you the following mandatory data protection related information: If you are contacting us via e-mail, we will process your personal data only if we have a legitimate interest in the processing of this data  (Art. 6 Sect. 1 lit. f GDPR), if you have consented to the processing of your data (Art. 6 Sect. 1 lit. a GDPR), if the processing of the data is required for the development, establishment, content or modification of a legal relationship between you and our company (Art. 6 Sect. 1 lit. b GDPR) or if any other legal provision permits the processing of this data. Your personal data will remain in our possession until you ask us to delete the data or you revoke your consent to store the data or if the purpose the data stored is required for no longer exists (e.g. once your request has been conclusively processed). This shall be without prejudice to any compelling statutory provisions – in particular tax and commercial law based retention periods. You have the right to at any time receive free information concerning the origins, recipients and purpose of your data archived by us. You also have a right to object, to data portability and a right to log a complaint with the competent supervisory agency. Moreover, you can demand the correction, eradication and, under certain circumstances, the restriction of the processing of your personal data. For more details, please consult our Data Privacy Policy (https://grafiker.de/datenschutz).");

                $mailer->send($message);
                }
    
            }


        for($i = $newarray['form']['schleifevon']; $i <= $newarray['form']['schleifebis']; $i = $i + $newarray['form']['schleifestep']) {
            //echo $i . "<br />";
            $serchdatasinDB = $dm->getRepository('App:User')->findBy(array('newsletterempfang' => NULL),array('id' => 'ASC'), 1, $i);
            foreach($serchdatasinDB as $serchdatasinDB) {
           
            //$pinsinsert = $dm->createQueryBuilder('App:Pins')->field('astitle')->equals('Pininsert')->field('username')->equals($serchdatasinDB->getUsername())->count()->getQuery()->execute();
            $newbody = $newarray['form']['content'];

            $newbody = str_replace("## username ##", $serchdatasinDB->getUsername(), $newbody);
            $newbody = str_replace("## vorname ##", $serchdatasinDB->getFirstName(), $newbody);
            $newbody = str_replace("## nachname ##", $serchdatasinDB->getLastName(), $newbody);
            $newbody = str_replace("## emailadresse ##", $serchdatasinDB->getEmail(), $newbody);

            /*echo"<pre>";
            var_dump($pinsinsert);
            echo"</pre>";
            echo"<pre>";
            var_dump($newbody);
            echo"</pre>";*/

            $message = (new \Swift_Message())
            ->setSubject($newarray['form']["titel"])
            ->setFrom(['mail@grafiker.de' => 'Grafiker.de Systemmail'])
            ->setTo($serchdatasinDB->getEmail())
            ->setBody($newbody . "<br /><br /><br /><br /><br />Pflichtinformationen gemäß Artikel 13 DSGVO Im Falle des Erstkontakts sind wir gemäß Art. 12, 13 DSGVO verpflichtet, Ihnen folgende datenschutzrechtliche Pflichtinformationen zur Verfügung zu stellen: Wenn Sie uns per E-Mail kontaktieren, verarbeiten wir Ihre personenbezogenen Daten nur, soweit an der Verarbeitung ein berechtigtes Interesse besteht (Art. 6 Abs. 1 lit. f DSGVO), Sie in die Datenverarbeitung eingewilligt haben (Art. 6 Abs. 1 lit. a DSGVO), die Verarbeitung für die Anbahnung, Begründung, inhaltliche Ausgestaltung oder Änderung eines Rechtsverhältnisses zwischen Ihnen und uns erforderlich sind (Art. 6 Abs. 1 lit. b DSGVO) oder eine sonstige Rechtsnorm die Verarbeitung gestattet. Ihre personenbezogenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere steuer- und handelsrechtliche Aufbewahrungsfristen – bleiben unberührt. Sie haben jederzeit das Recht, unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Ihnen steht außerdem ein Recht auf Widerspruch, auf Datenübertragbarkeit und ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Ferner können Sie die Berichtigung, die Löschung und unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten verlangen. Details entnehmen Sie unserer Datenschutzerklärung (https://grafiker.de/datenschutz).<br /><br />Information Provided as Mandated by Article 13 GDPR If this is your first interaction with us, Art. 12, 13 GDPR mandates that we make available to you the following mandatory data protection related information: If you are contacting us via e-mail, we will process your personal data only if we have a legitimate interest in the processing of this data  (Art. 6 Sect. 1 lit. f GDPR), if you have consented to the processing of your data (Art. 6 Sect. 1 lit. a GDPR), if the processing of the data is required for the development, establishment, content or modification of a legal relationship between you and our company (Art. 6 Sect. 1 lit. b GDPR) or if any other legal provision permits the processing of this data. Your personal data will remain in our possession until you ask us to delete the data or you revoke your consent to store the data or if the purpose the data stored is required for no longer exists (e.g. once your request has been conclusively processed). This shall be without prejudice to any compelling statutory provisions – in particular tax and commercial law based retention periods. You have the right to at any time receive free information concerning the origins, recipients and purpose of your data archived by us. You also have a right to object, to data portability and a right to log a complaint with the competent supervisory agency. Moreover, you can demand the correction, eradication and, under certain circumstances, the restriction of the processing of your personal data. For more details, please consult our Data Privacy Policy (https://grafiker.de/datenschutz).");
    
            $mailer->send($message);
            $serchdatasinDB->setNewsletterEmpfang(1);
            }        

        }
    }
        /*if(isset($newarray['form']["system"])) {
            $subjetsys = "%%Systemmail%%";
            foreach ($userdatagesamt as $recipient) {
            $message = (new \Swift_Message())
            ->setSubject($newarray['form']["titel"])
            ->setFrom('system@grafiker.de')
            ->setTo($recipient->getEmail())
            ->setBody($newarray['form']['content']);
    
            $mailer->send($message);
            }
        }
        if(isset($newarray['form']["letter"])) {
            $subjetsys = "%%Newsletter%%";
        }
        $subject = $subjetsys . ' ' . $newarray['form']["titel"];
        $emdsend = $this->get('doctrine_mongodb')->getManager();
        $usersender = $emdsend->getRepository('App:User')->findOneBy(["username"=>'grafiker.de']);
        $threadBuilder->setSender($usersender);
        $threadBuilder->setSubject($subject);
        $threadBuilder->setBody($newarray['form']['content']);
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());*/
        $dm->flush();
        }

        $builder = $this->createForm(AdminMailerFormType::class);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
            }
        return $this->render('admin/mailer.html.twig', [
            'controller_name' => 'AdminMailerController',
            'form' => $builder->createView(),
        ]);
    }
}