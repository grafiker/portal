<?php

namespace App\Controller;
use App\Document\Gutscheine;
use App\Document\Ratingblock;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\Mapgutscheineg\Annotations as MongoDB;
class UserGutscheineviewController extends Controller
{
    /**
     * @Route("/gutscheineview/{gutscheineid}", name="user_gutscheineview")
     */
    public function index($gutscheineid)
    {
        $nextgutscheinedata = new Gutscheine();
        $prevgutscheinedatab = new Gutscheine();
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        //var_dump($gutscheineid);
        $gutscheinedata = $dm->getRepository('App:Gutscheine')->findBy(array('gutscheinelink' => $gutscheineid));
        //var_dump($gutscheinedata);
//var_dump($gutscheinedata);
        $picsarray = array();
        foreach($gutscheinedata as $key => $gutscheinepics) {

            $prevgutscheinedata = $dm->createQueryBuilder('App:Gutscheine')->sort(array('startdate' => 'ASC'))->field('startdate')->gte($gutscheinepics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('gutscheinekategorie')->equals($gutscheinepics->getGutscheinekategorie())->getQuery()->toArray();
            $nextgutscheinedata = $dm->createQueryBuilder('App:Gutscheine')->sort(array('startdate' => 'DESC'))->field('startdate')->lte($gutscheinepics->getStartdate())->limit(1)->skip(1)->field('aktiv')->equals(true)->field('gutscheinekategorie')->equals($gutscheinepics->getGutscheinekategorie())->getQuery()->toArray();

            $picsarray = $gutscheinepics->getPicname();
            $content = HashToLinkController::HashToLink($gutscheinepics->getContent());

        }
        foreach($gutscheinedata as $key => $gutscheineuser) {
            $dmu = $this->get('doctrine_mongodb')->getManager();
        $userdatas = $dmu->getRepository('App:User')->findOneBy(["username"=>$gutscheineuser->getUsername()]);
        $fileexistarray[$gutscheineuser->getUsername()] = $userdatas->getUserpic();
        $filelinkarray[$gutscheineuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
        $gutscheinelastLogin[$userdatas->getUsername()] = $userdatas->getUpdatedAt();
        $useremploy[$userdatas->getUsername()] = $userdatas->getEmploy();
        
        }
        $dms = $this->get('doctrine_mongodb')->getManager();
        //var_dump($gutscheineid);
        foreach($gutscheinedata as $key => $gutscheinepics) {
        $gutscheinedatas = $dms->getRepository('App:Gutscheine')->find($gutscheinepics->id);
        //var_dump($gutscheinedatas);
        $gutscheineviews = $gutscheinedatas->getGutscheineviews()+1;
        $gutscheinedatas->setGutscheineviews($gutscheineviews);
        $date = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $olddate = $gutscheinedatas->getViewdate();
        $gutscheinedatas->setViewdate($date); 
        $titlealt=$gutscheinedatas->getTitle();
        $emdelblock = $this->get('doctrine_mongodb')->getManager();
        $ratingblockcheck = $emdelblock->getRepository('App:Ratingblock')->findOneBy(array("gutscheineid"=>$gutscheinepics->id, "username"=>$username));
        if(!$ratingblockcheck) {
            $ratingok=true;
        } else {
            $ratingok=false;
        }
        $emdelblock->flush();
        }
        $dm->flush();

$title = str_replace("!", "", $titlealt);
$title = str_replace("?", "", $title);
$title = str_replace("%", "", $title);
$title = str_replace("$", "", $title);
$title = str_replace(".....", "", $title);
$title = str_replace("....", "", $title);
$title = str_replace("...", "", $title);
$title = str_replace(" - ", " ", $title);
$title = explode(" ",$title);
$gutscheinedatacount = array();
foreach($title as $key => $searchtitle) {
    

            /*$searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$searchtitle.'/si'),
                        ),
                    )
            );*/

                $gutscheinedatacount[] = $dm->createQueryBuilder('App:Gutscheine')->sort(array('isads' => 'DESC'))->field('content')->equals(new \MongoRegex('/'.$searchtitle.'/si'))->limit(6)->getQuery()->toArray();
                //$gutscheinedatacount[$key] = $dm->getRepository('App:Gutscheine')->findBy($searchQuery,array('startdate' => 'DESC'));
}
$gutscheinefileexistarray=array();
$gutscheinefilelinkarray=array();
$userexist=array();
$wohnort=array();
$gutscheineuseremploy=array();
$gutscheinelastLoginUser=array();
foreach($gutscheinedatacount as $gutscheinedatacounts) {
    foreach($gutscheinedatacounts as $gutscheinedatacounts) {
        if($titlealt != $gutscheinedatacounts->getTitle()) {
            $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$gutscheinedatacounts->getUsername()]);
            if($userdatas) {
            $gutscheinefileexistarray[$gutscheinedatacounts->getUsername()] =$userdatas->getUserpic();
            $gutscheinefilelinkarray[$gutscheinedatacounts->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $gutscheineuser->getUsername());
            $wohnort[$gutscheinedatacounts->getUsername()] = $userdatas->getWohnort();
            $gutscheinelastLoginUser[$gutscheinedatacounts->getUsername()] = $userdatas->getUpdatedAt();
            $gutscheineuseremploy[$gutscheinedatacounts->getUsername()] = $userdatas->getEmploy();
            $userexist[$gutscheinedatacounts->getUsername()] = true;
            } else {
            $userexist[$gutscheinedatacounts->getUsername()] = false;
            }
        }
    }
}
//$gutscheinedatacount[] = array_unique($gutscheinedatacount);
shuffle($gutscheinedatacount);
        return $this->render('userprofil/gutscheineview.html.twig', [
            'controller_name' => 'AdminGutscheineviewController',
            'gutscheinedata' => $gutscheinedata,
            'contenttxt' => $content,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'picsarray' => $picsarray,
            'olddate' => $olddate,
            'gutscheinelastLogin' => $gutscheinelastLogin,
            'useremploy' => $useremploy,
            'ratingok' => $ratingok,
            'gutscheineviewcontroller' => 1,
            'nextgutscheinedata' => $nextgutscheinedata,
            'prevgutscheinedata' => $prevgutscheinedata,
            'gutscheinedatacount' => $gutscheinedatacount,
            'userexist' => $userexist,
            'gutscheinefileexistarray' => $gutscheinefileexistarray,
            'gutscheinefilelinkarray' => $gutscheinefilelinkarray,
            'wohnort' => $wohnort,
            'gutscheineuseremploy' => $gutscheineuseremploy,
            'gutscheinelastLoginUser' => $gutscheinelastLoginUser,
        ]);
    }

    /**
     * @Route("/gutscheineshow", name="user_gutscheineshow")
     */
    public function showAction()
    {
        return $this->render('userprofil/gutscheineshow.html.twig', [
            'controller_name' => 'AdminGutscheineviewController',
            ]);
    }

}
