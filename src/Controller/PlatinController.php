<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PlatinController extends AbstractController
{
    /**
     * @Route("/platin", name="platin")
     */
    public function index()
    {
        return $this->render('printbuyer/index.html.twig', [
            'controller_name' => 'PrintbuyerController',
        ]);
    }
}
