<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoginerrorController extends AbstractController
{
    /**
     * @Route("/loginerror", name="loginerror")
     */
    public function index()
    {
        return $this->render('loginerror/index.html.twig', [
            'controller_name' => 'LoginerrorController',
        ]);
    }
}
