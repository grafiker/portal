<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UseronlinestatusController extends Controller
{
    /**
     * @Route("/useronlinestatus", name="useronlinestatus")
     */
    public function index(Request $request)
    {
        $newarray=$request->request->all();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $dm->getRepository('App:User')->findOneByUsername($newarray["username"]);
        $date = \DateTime::createFromFormat('U', time());
        $date->setTimezone(new \DateTimeZone('UTC'));
        $user->setUpdatedAt($date);
        $employ=$user->getEmploy();
        $online=true;
        $dm->persist($user);
        $dm->flush();

        $provider = $this->container->get('fos_message.provider');
        $mailanz = $provider->getNbUnreadMessages();
        return $this->render('useronlinestatus/index.html.twig', [
            'controller_name' => 'UseronlinestatusController',
            'mailanz' => $mailanz,
            'employ' => $employ,
            'online' => $online,
        ]);
    }
}
