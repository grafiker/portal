<?php

namespace App\Controller;

use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
class LogineditController extends Controller
{
    /**
     * @Route("/profile/loginedit", name="loginedit")
     */
    public function index(RequestStack $requestStack, Request $request, \Swift_Mailer $mailer)
    {
        $task = new User();
        $dm = $this->get('doctrine_mongodb')->getManager();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $email = $user->getEmail();
                $userID = $user->getId();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $newarray=$request->request->all();
        $infotext = false;
        if(isset($newarray["mailadresse"]) and $newarray["mailadresse"] == $newarray["mailadressewd"]) {
            $userdatas = $dm->getRepository('App:User')->findOneBy(["email"=>$newarray["mailadresse"]]);
                if($userdatas) {
                    $infotext = "Diese Mailadresse wird bereits von einem anderen Mitglied verwendet!";
                } else {
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $taskuser = $dm->getRepository('App:User')->find(["id"=>$userID]);
                    
                    $taskuser->setNewMail($newarray["mailadresse"]);
                    $dm->persist($taskuser);
                    $dm->flush();
                    $infotext = "Vielen Dank, Sie bekommen jetzt eine Bestätigungsmail!";

                    $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/profile/mailok/'.$userID;
                $subjetsys = "Bitte bestätigen Sie Ihre neue Mailadresse!";
                $body = "Hallo " . $taskuser->getFirstName() . " " . $taskuser->getLastName() . ",<br /><br />Bitte bestätigen Sie den folgenden Link um Ihre Mailadressenänderung abzuschließen!<br /><br />" . $linkToView . "<br /><br />Vielen Dank und weiterhin viel Spaß auf grafiker.de!<br /><br />Ihr grafiker.de Team";
                //var_dump($body);
                    $message = (new \Swift_Message())
                    ->setSubject($subjetsys)
                    ->setFrom('newmail@grafiker.de')
                    ->setTo($newarray["mailadresse"])
                    ->setBody($body);
                    $mailer->send($message);
                    $maillog = new MaillogController();
                    $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "newmail@grafiker.de", $newarray["mailadresse"], $subjetsys, $body);
                }
        } else if(isset($newarray["mailadresse"]) and $newarray["mailadresse"] != $newarray["mailadressewd"]) {
            $infotext = "Ihre eingegebenen Mailadressen sind nicht gleich, bitte wiederholen Sie Ihre Eingabe!";
        }
        return $this->render('userprofil/loginedit.html.twig', [
            'controller_name' => 'LogineditController',
            'infotext' => $infotext,
        ]);
    }
}
