<?php

namespace App\Controller;
use App\Document\Skills;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProfilsucheController extends Controller
{
    /**
     * @Route("/profilsuche", name="profilsuche")
     */
    public function index()
    {
        $skills = new Skills();
        $dmskill = $this->get('doctrine_mongodb')->getManager();
        $skillsDruckerei = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Druckerei']);
        $skillsGrafiker = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Grafiker']);
        $skillsFotograf = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Fotograf']);
        $skillsEntwickler = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Programmierer']);
        $skillsTexter = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Texter']);
        $skillsIllustrator = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Illustrator']);
        $skillsSonstiges = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Sonstiges']);
        $druckerei=array();
        $grafiker=array();
        $fotograf=array();
        $entwickler=array();
        $texter=array();
        $illusator=array();
        $sonstiges=array();
        $druckerei = $skillsDruckerei->getSkillname();
        $grafiker = $skillsGrafiker->getSkillname();
        $fotograf = $skillsFotograf->getSkillname();
        $entwickler = $skillsEntwickler->getSkillname();
        $texter = $skillsTexter->getSkillname();
        $illusator = $skillsIllustrator->getSkillname();
        $sonstiges = $skillsSonstiges->getSkillname();
        
        $dmskill->flush();
        return $this->render('profilsuche/index.html.twig', [
            'controller_name' => 'ProfilsucheController',
            'druckerei' => $druckerei,
            'grafiker' => $grafiker,
            'fotograf' => $fotograf,
            'entwickler' => $entwickler,
            'texter' => $texter,
            'illustrator' => $illusator,
            'sonstiges' => $sonstiges,
            'usernumbers' => MediadatenController::mediadaten($dmskill, 'User', false, false),
        ]);
    }
}
