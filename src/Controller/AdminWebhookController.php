<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class AdminWebhookController extends Controller
{
    /**
     * @Route("/admin/webhook", name="admin_webhook")
     */
    public function index()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $webhookdata = $dm->getRepository('App:Webhook')->findBy(array(),array('id' => 'DESC'), 100);


$webhookarray=array();
foreach($webhookdata as $key => $newwebhookdata) {
$webhookarray[$key] = $newwebhookdata->getWebhook();
}
        return $this->render('admin/webhook.html.twig', [
            'controller_name' => 'AdminDatabasecheckController',
            'webhookarray' => $webhookarray,
        ]);
}
}
