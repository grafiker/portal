<?php

namespace App\Controller;

use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class StimmezuController extends Controller
{
    /**
     * @Route("/stimmezu", name="stimmezu")
     */
    public function index(Request $request)
    {
        $newarray=$request->request->all();
        if($newarray) {
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if($users != "anon.") {
            $em = $this->get('doctrine_mongodb')->getManager();
        $userdb = $em->getRepository('App:User')->findOneBy(["username"=>$users]);
        $users->setAgb($newarray['agb']);
        $users->setDatenschutz($newarray['datenschutz']);
        $em->flush();

            
            $erledigt=true;
        } 
        } else {
            $erledigt = false;
        }
        return $this->render('userprofil/stimmezu.html.twig', [
            'controller_name' => 'StimmezuController',
            'erledigt' => $erledigt,
        ]);
    }
}
