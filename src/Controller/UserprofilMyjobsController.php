<?php

namespace App\Controller;
use App\Document\Job;
use App\Document\Bewerbung;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserprofilMyjobsController extends Controller
{
    /**
     * @Route("/profile/myjobs", name="userprofil_myjobs")
     */
    public function index()
    {
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            $anzprosite = 10;
            if(!isset($page) or $page == "all") {
                $page=0;
            } else {
                $page=$page*$anzprosite;
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('username' => $username),array('id' => 'ASC'));
            $jobdata = $dm->getRepository('App:Job')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
            $myArray = (array)$jobdata;
            $checkanzin = array();
            foreach($myArray as $key => $myArray) {
                if($checkanz = $dm->getRepository('App:Bewerbung')->findBy(array('jobID' => $myArray->getId()),array('id' => 'DESC'), $anzprosite, $page)) { 
                    $checkanzin[$myArray->getId()] = count($checkanz);
                } else {
                    $checkanzin[$myArray->getId()] = 0;
                }
            }

            $dm->flush();
        return $this->render('userprofil/myjobs.html.twig', [
            'controller_name' => 'UserprofilMyjobsController',
            'jobdata' => $jobdata,
            'checkanzin' => $checkanzin,
            'aktion' => false,
        ]);
    }
    /**
     * @Route("/profile/myjobs/{jobid}/{aktion}", name="userprofil_myjobs_action")
     */
    public function createAction($jobid, $aktion)
    {
if($aktion == "del") {
    //var_dump($jobid);
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $jobcheck = $emdel->getRepository('App:Bewerbung')->findBy(array('jobID'=>$jobid),array('id' => 'DESC'));
            //var_dump($jobcheck);
            $jobcheck = (array)$jobcheck;
            foreach($jobcheck as $delpdf) {
                foreach($delpdf->getDocpdf() as $key => $delpdfs) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/bewerbung/".$delpdfs)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/pdf/bewerbung/".$delpdfs->getDocpdf());
                } else {}
                }
            }
            /*foreach($jobcheck as $jobcheck) {
            $emdel->remove($jobcheck);
            $emdel->flush();
            }*/

            $jobdeleter = $emdel->getRepository('App:Job')->findOneBy(array('id'=>$jobid));
//var_dump($jobdeleter);
if($jobdeleter) {
$emdel->remove($jobdeleter);
            $emdel->flush();
}
        }
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
            $anzprosite = 10;
            if(!isset($page) or $page == "all") {
                $page=0;
            } else {
                $page=$page*$anzprosite;
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $jobdatagesamt = $dm->getRepository('App:Job')->findBy(array('username' => $username),array('id' => 'ASC'));
            $jobdata = $dm->getRepository('App:Job')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
            $myArray = (array)$jobdata;
            $checkanzin = array();
            foreach($myArray as $key => $myArray) {
                if($checkanz = $dm->getRepository('App:Bewerbung')->findBy(array('jobID' => $myArray->getId()),array('id' => 'DESC'), $anzprosite, $page)) { 
                    $checkanzin[$myArray->getId()] = count($checkanz);
                } else {
                    $checkanzin[$myArray->getId()] = 0;
                }
            }

            $dm->flush();
        return $this->render('userprofil/myjobs.html.twig', [
            'controller_name' => 'UserprofilMyjobsController',
            'jobdata' => $jobdata,
            'checkanzin' => $checkanzin,
            'aktion' => $aktion,
        ]);
    }
}
