<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserManagerInterface;

class ServicemenuController extends Controller
{
    private $userManager;
    function __construct(ManagerRegistry $doctrine_mongodb, UserManagerInterface $userManager)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_userManager = $userManager;
    }
    /**
     * @Route("/servicemenu", name="servicemenu")
     */
    public function index()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        //\var_dump($users);
        if($users != "anon.") {
            $userexist = true;
        } else {
            $userexist = false;
        }

        if(!$userexist) {
            //die("ENDE");
        }
        $emdsend = $this->_doctrine_mongodb->getManager();
        if($userexist) {
        $thistenders = $emdsend->getRepository('App:Jobs')->findBy(array("username" => $users->getID(), "aktiv" => true),array('enddate' => 'ASC'));
        $jobscounter = count($thistenders);
        } else {
            $jobscounter = 0;
        }
        $thisalltenders = $emdsend->getRepository('App:Jobs')->findBy(array("aktiv" => true),array('enddate' => 'ASC'));
        $counter = 0;

        $alljobscounter = count($thisalltenders);
        return $this->render('servicemenu/index.html.twig', [
            'controller_name' => 'ServicemenuController',
            'countjobs' => $jobscounter,
            'alljobscounter' => count($thisalltenders),
        ]);
    }
}
