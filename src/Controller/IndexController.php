<?php

namespace App\Controller;
use App\Document\Skills;
use App\Document\Job;
use App\Document\Projektrechner;
use App\AppBundle\Form\ProjektrechnerFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
class IndexController extends Controller
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    { 
//var_dump(HashToLinkController::HashToLink("Dieser Text endet wie viele auf Facebook mit Hashtags die dann in Links umgewandelt werden sollten... #homecooking #essen #frikassee #selbergekocht #lecker #foodforfoodies #foodpics #instafood #delish #foods #delicious #tasty #eat #eating #foodblogger #foodie #foodlover #foodgasm #foodgram #foodforthought #foodism ##urlaubsreif #niceview #worktravel #visititaly #traveler #love #worktravel #foodphotography #instafoodblogger"));
//exec("php ".$this->get('kernel')->getProjectDir()."/app/console cache:clear --env=prod");exec("rm -rf ".$this->get('kernel')->getProjectDir()."/app/cache/*");
//var_dump(exec("php ".$this->get('kernel')->getProjectDir()."/console cache:clear --env=prod"));
        $pindata = new PinoutputController();
        $articledata = new ArticleoutputController();
        $gutscheinedata = new GutscheineoutputController();
        $userdata = new CheckUserdataController();
        $pinlastLogin=false;
        $useremploy=false;
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $filenamejpg = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.jpg'; 
        $filenamepng = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.png'; 
        $filenamegif = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.gif'; 
     if(file_exists($filenamejpg)) {
            $filename=$filenamejpg;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.jpg';
            $file_exists = file_exists($filename);
        } else if(file_exists($filenamepng)) {
            $filename=$filenamepng;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.png';
            $file_exists = file_exists($filename);
        } else if(file_exists($filenamegif)) {
            $filename=$filenamegif;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.gif';
            $file_exists = file_exists($filename);
        } else {
            $file_exists= false;
            $piclink = false;
        }
        if($users != "anon.") {
            $provider = $this->container->get('fos_message.provider');
            $mailanz = $provider->getNbUnreadMessages();
        } else {
            $mailanz = false;
        }
            $fileexistarray=array();
            $filelinkarray=array();
            $userexist=array();
            $wohnort=array();
            foreach($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,0,'DESC','all','','',7,'Startinsert') as $key => $pinuser) {
                $dm = $this->get('doctrine_mongodb')->getManager();
                $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$pinuser->getUsername()]);
                if($userdatas) {
                $fileexistarray[$pinuser->getUsername()] =$userdatas->getUserpic();
                //var_dump($fileexistarray);
                
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $wohnort[$pinuser->getUsername()] = $userdatas->getWohnort();
                $pinlastLogin[$pinuser->getUsername()] = $userdatas->getUpdatedAt();
                $useremploy[$pinuser->getUsername()] = $userdatas->getEmploy();
                $userexist[$pinuser->getUsername()] = true;
            } else {
                $userexist[$pinuser->getUsername()] = false;
            }
                $dm->flush();
            }
            $skills = new Skills();
            $dmskill = $this->get('doctrine_mongodb')->getManager();
            $skillsDruckerei = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Druckerei']);
            $skillsGrafiker = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Grafiker']);
            $skillsFotograf = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Fotograf']);
            $skillsEntwickler = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Programmierer']);
            $skillsTexter = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Texter']);
            $skillsIllustrator = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Illustrator']);
            $skillsSonstiges = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Sonstiges']);
            $druckerei=array();
            $grafiker=array();
            $fotograf=array();
            $entwickler=array();
            $texter=array();
            $illusator=array();
            $sonstiges=array();
            $druckerei = $skillsDruckerei->getSkillname();
            $grafiker = $skillsGrafiker->getSkillname();
            $fotograf = $skillsFotograf->getSkillname();
            $entwickler = $skillsEntwickler->getSkillname();
            $texter = $skillsTexter->getSkillname();
            $illusator = $skillsIllustrator->getSkillname();
            $sonstiges = $skillsSonstiges->getSkillname();
            
            $dmskill->flush();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $kategorien = $dm->getRepository('App:Projektrechnerkategorien')->findall();
            $dm->flush();
            $extarray=array();

            foreach($kategorien as $key=>$value){
                $extarray[$value->getKatname()] = $value->getKatname();
              }
                    
                    $builder = $this->createForm(ProjektrechnerFormType::class, $extarray);
                    $builder->add('username', HiddenType::class, array(
                        'data' => $users,
                    ));
                    $builder->add('steps', HiddenType::class, array(
                        'data' => 1,
                    ));
                    $task = new Projektrechner();
                    $task = new Job();
                    $builders = $this->createFormBuilder($task)
                    ->add('username', HiddenType::class, array(
                        'data' => $users,
                    ))
                    ->add('jobSuche', ChoiceType::class, array(
                        'label' => 'Biete / Suche:',
                        'choices' => array(
                            'Ich biete einen Job für' => 'biete',
                            'Ich suche einen Job als' => 'suche',
                        ),
                    ))
                    /*->add('astitle', TextType::class, ['label' => 'Jobtitel:'])*/
                    ->add('jobTyp', ChoiceType::class, array(
                        'label' => 'Jobart:',
                        'choices' => array(
                                'Grafiker|in' => 'Grafiker',
                                'Druckerei/Vorstufe/Weiterverarbeitung' => 'Druckerei',
                                'Fotograf|in' => 'Fotograf',
                                'Entwickler|in' => 'Entwickler',
                                'Texter|in' => 'Texter',
                                'Illustrator|in' => 'Illustrator',
                                'Sonstige' => 'Sonstige',
                        ),
                    ))
                    
                    ->getForm();
                    $pindata = new PinoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
                $pincounterkats[$kats->getPinkatname()] = MediadatenController::mediadaten($dm, 'Pins', 'pinkategorie', $kats->getPinkatname());
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
                $pincounterkats[$kats->getPinkatname()] = false;
            }
        }

        $articlekats = $dm->getRepository('App:Articlekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $articlecounterdata = array(); 
        $articlecounter = array(); 
        foreach($articlekats as $key => $kats) {
            $articlecounter[$kats->getArticlekatname()] = $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getArticlekatname(),'','',7,'Startinsert');
            if($articlecounter[$kats->getArticlekatname()]) {
                $articlecounterdata[$kats->getArticlekatname()] = true;
                $articlecounterkats[$kats->getArticlekatname()] = MediadatenController::mediadaten($dm, 'Article', 'articlekategorie', $kats->getArticlekatname());
            } else {
                $articlecounterdata[$kats->getArticlekatname()] = false;
                $articlecounterkats[$kats->getArticlekatname()] = false;
            }
        }


        $gutscheinekats = $dm->getRepository('App:Gutscheinekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $gutscheinecounterdata = array();
        $gutscheinecounter = array(); 
        foreach($gutscheinekats as $key => $kats) {
            $gutscheinecounter[$kats->getGutscheinekatname()] = $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getGutscheinekatname(),'','',7,'Gutscheineinsert');
            if($gutscheinecounter[$kats->getGutscheinekatname()]) {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = true;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = MediadatenController::mediadaten($dm, 'Gutscheine', 'gutscheinekategorie', $kats->getGutscheinekatname());
            } else {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = false;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = false;
            }
        }


//var_dump($articlekats);
        $_dateTime = new \DateTime('2 minutes ago');
        $useronlinedata = $dm->createQueryBuilder('App:User')->sort(array('id' => 'DESC'))->field('lastLogin')->gte($_dateTime)->count()->getQuery()->execute();
        $last5userdata = $dm->getRepository('App:User')->findBy(array('enabled' => true),array('createdAt' => 'DESC'), 4, 0);
        $partnerbanner = $dm->createQueryBuilder('App:Banner')->field('disenable')->equals(NULL)->field('position')->equals('partner')->field('maxViews')->gt(0)->field('maxKlicks')->gt(0)->getQuery()->toArray();


        $datejetzt = new \DateTime(date('m/d/Y', time()));

        //$pindatagesamt = $dm->createQueryBuilder('App:Job')->sort(array('startdate' => 'asc'))->limit(4)->skip(2)->field('aktiv')->getQuery()->toArray();
//var_dump($pindatagesamt);
        $jobdata = new JoboutputController();
        $anz = 5;
        //var_dump($jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), 20,0,'DESC','all','','',7,true)->getUsername());
        $logoexist = array();
        $logolink = array();
        $jobarray = array();
        $logofirma = array();
        $jobarray = $jobdata->jobFilterOutput($this->get('doctrine_mongodb')->getManager(), $anz,0,'DESC','all','','',7,true);
        foreach($jobarray as $key=>$value){
            $dm = $this->get('doctrine_mongodb')->getManager();
            $userdata = $dm->getRepository('App:User')->findOneBy(["username"=>$value->getUsername()]);
            $upload_dir = 'upload/uploads/firma/';
            $name = $upload_dir.'/'.$value->getUsername() . '_firma';
        
                if($userdata->getFirmapic() and $userdata->getFirmapic() != 'del') {
                  $logoexist[$value->getUsername()]=true;
                  $logolink[$value->getUsername()]=$userdata->getFirmapic();
                  $logofirma[$value->getUsername()] = true;
                } elseif($userdata->getUserpic() and $userdata->getUserpic() != 'del') {
                    $logoexist[$value->getUsername()]=true;
                    $logolink[$value->getUsername()]=$userdata->getUserpic();
                    $logofirma[$value->getUsername()] = false;
                } else {
                    $logoexist[$value->getUsername()]=false;
                    $logolink[$value->getUsername()]=false;
                }
        }
        //var_dump($gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 3,0,'DESC','all','','',7,'Gutscheineinsert'));
        $dm->flush();
        return $this->render('index.html.twig', [
            'controller_name' => 'UserStartController',
            'jobdata' => $jobarray,
            'logoexist' => $logoexist,
            'logolink' => $logolink,
            'logofirma' => $logofirma,
            'jobcounter' => MediadatenController::mediadaten($dm, 'Job', 'aktiv', true),
            'form' => $builder->createView(),
            'forms' => $builders->createView(),
            'file_exists' => $file_exists,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'pincounterkats' => $pincounterkats,
            'articlecounterkats' => $articlecounterkats,
            'druckerei' => $druckerei,
            'grafiker' => $grafiker,
            'fotograf' => $fotograf,
            'entwickler' => $entwickler,
            'texter' => $texter,
            'illustrator' => $illusator,
            'sonstiges' => $sonstiges,
            'piclink' => $piclink,
            'unreads' => $mailanz,
            'wohnort' => $wohnort,
            'pinlastLogin' => $pinlastLogin,
            'useremploy' => $useremploy,
            'pinkats' => $pinkats,
            'last5userdata' => $last5userdata,
            'useronlinedata' => $useronlinedata,
            'pinsnumbers' => MediadatenController::mediadaten($dm, 'Pins', false, false),
            'usernumbers' => MediadatenController::mediadaten($dm, 'User', false, false),
            'pincounterdata' => $pincounterdata,
            'articlenumbers' => MediadatenController::mediadaten($dm, 'Article', "astitle", "Startinsert", "aktiv", true),
            'articlecounterdata' => $articlecounterdata,
            'articleoutput' => $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 6,0,'DESC','all','','',7,'Startinsert'),
            'articlekats' => $articlekats,
            'gutscheinenumbers' => MediadatenController::mediadaten($dm, 'Gutscheine', "astitle", "Gutscheineinsert", "aktiv", true),
            'gutscheinecounterdata' => $gutscheinecounterdata,
            'gutscheineoutput' => $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 3,0,'DESC','all','','',7,'Gutscheineinsert'),
            'gutscheinekats' => $gutscheinekats,
            'counter' => count($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,0,'DESC','all','','',7,'Startinsert')),
            'pindata' => $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,0,'DESC','all','','',7,'Startinsert'),
            'userexist' => $userexist,
            'partnerdata' => $partnerbanner,
        ]);
    }
    /**
     * @Route("/filter/{filter}/{pages}", name="filter")
     */
    public function actionCreate(Request $request, $filter, $pages)
    {
        $pindata = new PinoutputController();
        $articledata = new ArticleoutputController();
        $gutscheinedata = new GutscheineoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $pinkats = $dm->getRepository('App:Pinskategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $pincounter = array(); 
        foreach($pinkats as $key => $kats) {
            $pincounter[$kats->getPinkatname()] = $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getPinkatname(),'','',7,'Startinsert');
            if($pincounter[$kats->getPinkatname()]) {
                $pincounterdata[$kats->getPinkatname()] = true;
                $pincounterkats[$kats->getPinkatname()] = MediadatenController::mediadaten($dm, 'Pins', 'pinkategorie', $kats->getPinkatname());
            } else {
                $pincounterdata[$kats->getPinkatname()] = false;
                $pincounterkats[$kats->getPinkatname()] = false;
            }
        }
        //var_dump($pincounterdata);
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $dm = $this->get('doctrine_mongodb')->getManager();
            $kategorien = $dm->getRepository('App:Projektrechnerkategorien')->findall();
            $dm->flush();
            $extarray=array();
            $extarray['- gewünschte Projektart wählen -'] = '- gewünschte Projektart wählen -';
            foreach($kategorien as $key=>$value){
                $extarray[$value->getKatname()] = $value->getKatname();
              }
                    
                    $builder = $this->createForm(ProjektrechnerFormType::class, $extarray);
                    $builder->add('username', HiddenType::class, array(
                        'data' => $users,
                    ));
                    $builder->add('steps', HiddenType::class, array(
                        'data' => 1,
                    ));
                    $task = new Projektrechner();
                    $task = new Job();
                    $builders = $this->createFormBuilder($task)
                    ->add('username', HiddenType::class, array(
                        'data' => $users,
                    ))
                    ->add('astitle', TextType::class, ['label' => 'Jobtitel:'])
                    ->add('jobTyp', ChoiceType::class, array(
                        'label' => 'Jobart:',
                        'choices' => array(
                                'Druckaufträge' => 'Druckaufträge',
                                'Microjob' => 'Microjob',
                                'Freelancer' => 'Freelancer',
                                'Festanstellung' => 'Festanstellung',
                                'Ausbildung / Praktikum' => 'Ausbildung / Praktikum',
                                'Sonstige' => 'Sonstige',
                        ),
                    ))
                    ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-right'),'label' => 'weiter...'))
                    ->getForm();        
        $skills = new Skills();
            $dmskill = $this->get('doctrine_mongodb')->getManager();
            $skillsDruckerei = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Druckerei']);
            $skillsGrafiker = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Grafiker']);
            $skillsFotograf = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Fotograf']);
            $skillsEntwickler = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Programmierer']);
            $skillsTexter = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Texter']);
            $skillsIllustrator = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Illustrator']);
            $skillsSonstiges = $dmskill->getRepository('App:Skills')->findOneBy(["beruf"=>'Sonstiges']);
            $druckerei=array();
            $grafiker=array();
            $fotograf=array();
            $entwickler=array();
            $texter=array();
            $illusator=array();
            $sonstiges=array();
            $druckerei = $skillsDruckerei->getSkillname();
            $grafiker = $skillsGrafiker->getSkillname();
            $fotograf = $skillsFotograf->getSkillname();
            $entwickler = $skillsEntwickler->getSkillname();
            $texter = $skillsTexter->getSkillname();
            $illusator = $skillsIllustrator->getSkillname();
            $sonstiges = $skillsSonstiges->getSkillname();
            
            $dmskill->flush();

        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        $filenamejpg = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.jpg'; 
        $filenamepng = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.png'; 
        $filenamegif = $this->get('kernel')->getProjectDir() . '/public/upload/uploads/userpics/'.$users.'_userpic.gif'; 
     if(file_exists($filenamejpg)) {
            $filename=$filenamejpg;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.jpg';
            $file_exists = file_exists($filename);
        } else if(file_exists($filenamepng)) {
            $filename=$filenamepng;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.png';
            $file_exists = file_exists($filename);
        } else if(file_exists($filenamegif)) {
            $filename=$filenamegif;
            $piclink = '/upload/uploads/userpics/'.$users.'_userpic.gif';
            $file_exists = file_exists($filename);
        } else {
            $file_exists= false;
            $piclink = false;
        }
        if($users != "anon.") {
            $provider = $this->container->get('fos_message.provider');
            $mailanz = $provider->getNbUnreadMessages();
        } else {
            $mailanz = false;
        }
        
        $fileexistarray=array();
        $filelinkarray=array();
        if($filter != "Search") {
            $filter = explode("-", $filter);
            if($users == $filter[1]) {
                $ownerfilter = $filter[1];
                $userfilter = '';
            } else if('ALL' == $filter[1]) {
                $ownerfilter = '';
                $userfilter = '';
            } else {
                $ownerfilter = '';
                $userfilter = $filter[1];
            }
            $pinlastLogin=false;
            $wohnort=array();
            $useremploy=false;
            $userexist=false;
        foreach($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,0,'DESC',$filter[0],$ownerfilter,$userfilter,7,'Startinsert') as $key => $pinuser) {
                $dm = $this->get('doctrine_mongodb')->getManager();
                $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$pinuser->getUsername()]);
                if($userdatas) {
                $fileexistarray[$pinuser->getUsername()] =$userdatas->getUserpic();
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $wohnort[$pinuser->getUsername()] = $userdatas->getWohnort();
                $pinlastLogin[$pinuser->getUsername()] = $userdatas->getUpdatedAt();
                $useremploy[$pinuser->getUsername()] = $userdatas->getEmploy();
                $userexist[$pinuser->getUsername()] = true;
            } else {
                $userexist[$pinuser->getUsername()] = false;
            }
                $dm->flush();
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $_dateTime = new \DateTime('2 minutes ago');
        $useronlinedata = $dm->createQueryBuilder('App:User')->sort(array('id' => 'DESC'))->field('lastLogin')->gte($_dateTime)->count()->getQuery()->execute();
        
        $articlekats = $dm->getRepository('App:Articlekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $articlecounterdata = array(); 
        $articlecounter = array(); 
        foreach($articlekats as $key => $kats) {
            $articlecounter[$kats->getArticlekatname()] = $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getArticlekatname(),'','',7,'Startinsert');
            if($articlecounter[$kats->getArticlekatname()]) {
                $articlecounterdata[$kats->getArticlekatname()] = true;
                $articlecounterkats[$kats->getArticlekatname()] = MediadatenController::mediadaten($dm, 'Article', 'articlekategorie', $kats->getArticlekatname());
            } else {
                $articlecounterdata[$kats->getArticlekatname()] = false;
                $articlecounterkats[$kats->getArticlekatname()] = false;
            }
        }

        $gutscheinekats = $dm->getRepository('App:Gutscheinekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $gutscheinecounterdata = array(); 
        $gutscheinecounter = array(); 
        foreach($gutscheinekats as $key => $kats) {
            $gutscheinecounter[$kats->getGutscheinekatname()] = $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getGutscheinekatname(),'','',7,'Gutscheineinsert');
            if($gutscheinecounter[$kats->getGutscheinekatname()]) {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = true;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = MediadatenController::mediadaten($dm, 'Gutscheine', 'gutscheinekategorie', $kats->getGutscheinekatname());
            } else {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = false;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = false;
            }
        }

        $dm->flush();
        return $this->render('index.html.twig', [
            'controller_name' => 'UserStartController',
            'file_exists' => $file_exists,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'pincounterkats' => $pincounterkats,
            'druckerei' => $druckerei,
            'grafiker' => $grafiker,
            'fotograf' => $fotograf,
            'entwickler' => $entwickler,
            'texter' => $texter,
            'illustrator' => $illusator,
            'sonstiges' => $sonstiges,
            'username' => $filter[1],
            'piclink' => $piclink,
            'unreads' => $mailanz,
            'wohnort' => $wohnort,
            'pinlastLogin' => $pinlastLogin,
            'filter' => $filter[0].'-'.$filter[1],
            'useremploy' => $useremploy,
            'form' => $builder->createView(),
            'forms' => $builders->createView(),
            'pinkats' => $pinkats,
            'pincounterdata' => $pincounterdata,
            'articlenumbers' => MediadatenController::mediadaten($dm, 'Article', false, false),
            'articlecounterdata' => $articlecounterdata,
            'articleoutput' => $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 6,0,'DESC','all','','',7,'Startinsert'),
            'articlekats' => $articlekats,
            'gutscheinenumbers' => MediadatenController::mediadaten($dm, 'Gutscheine', false, false),
            'gutscheinecounterdata' => $gutscheinecounterdata,
            'gutscheineoutput' => $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 3,0,'DESC','all','','',7,'Gutscheineinsert'),
            'gutscheinekats' => $gutscheinekats,
            'userexist' => $userexist,
            'useronlinedata' => $useronlinedata,
            'pinsnumbers' => MediadatenController::mediadaten($dm, 'Pins', false, false),
            'usernumbers' => MediadatenController::mediadaten($dm, 'User', false, false),
            'counter' => count($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,$pages,'DESC',$filter[0],$ownerfilter,$userfilter,7,'Startinsert')),
            'pindata' => $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,$pages,'DESC',$filter[0],$ownerfilter,$userfilter,7,'Startinsert'),
        ]);
        } else {
            $pinlastLogin=false;
            $useremploy=false;
            $fileexistarray=array();
            $filelinkarray=array();
            $userexist=array();
            $wohnort=array();
            if(isset($_POST['fulltext'])) {
                $_POST['fulltext'] = $_POST['fulltext'];
            } else {
                $_POST['fulltext'] = $pages;
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $searchQuery = array(
                '$or' => array(
                    array(
                        'title' => new \MongoRegex('/'.$_POST['fulltext'].'/si'),
                        ),
                    array(
                        'content' => new \MongoRegex('/'.$_POST['fulltext'].'/si'),
                        ),
                    )
                );
                $pindatacount = $dm->getRepository('App:Pins')->findBy($searchQuery,array('startdate' => 'DESC'));
            //$pind
            foreach($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,0,'DESC','Search-'.$_POST['fulltext'],$users,'',7,'Startinsert') as $key => $pinuser) {
                $userdatas = $dm->getRepository('App:User')->findOneBy(["username"=>$pinuser->getUsername()]);
                if($userdatas) {
                $fileexistarray[$pinuser->getUsername()] =$userdatas->getUserpic();
                //var_dump($fileexistarray);
                
                $filelinkarray[$pinuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $pinuser->getUsername());
                $wohnort[$pinuser->getUsername()] = $userdatas->getWohnort();
                $pinlastLogin[$pinuser->getUsername()] = $userdatas->getUpdatedAt();
                $useremploy[$pinuser->getUsername()] = $userdatas->getEmploy();
                $userexist[$pinuser->getUsername()] = true;
            } else {
                $userexist[$pinuser->getUsername()] = false;
            }
                $dm->flush();
            }
            $blindarray=array();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $_dateTime = new \DateTime('2 minutes ago');
            $useronlinedata = $dm->createQueryBuilder('App:User')->sort(array('id' => 'DESC'))->field('lastLogin')->gte($_dateTime)->count()->getQuery()->execute();
            
            $articlekats = $dm->getRepository('App:Articlekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        
        $articlecounterdata = array(); 
        $articlecounter = array(); 
        foreach($articlekats as $key => $kats) {
            $articlecounter[$kats->getArticlekatname()] = $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getArticlekatname(),'','',7,'Startinsert');
            if($articlecounter[$kats->getArticlekatname()]) {
                $articlecounterdata[$kats->getArticlekatname()] = true;
                $articlecounterkats[$kats->getArticlekatname()] = MediadatenController::mediadaten($dm, 'Article', 'articlekategorie', $kats->getArticlekatname());
            } else {
                $articlecounterdata[$kats->getArticlekatname()] = false;
                $articlecounterkats[$kats->getArticlekatname()] = false;
            }
        }

        $gutscheinekats = $dm->getRepository('App:Gutscheinekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $gutscheinecounterdata = array(); 
        $gutscheinecounter = array(); 
        foreach($gutscheinekats as $key => $kats) {
            $gutscheinecounter[$kats->getGutscheinekatname()] = $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getGutscheinekatname(),'','',7,'Gutscheineinsert');
            if($gutscheinecounter[$kats->getGutscheinekatname()]) {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = true;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = MediadatenController::mediadaten($dm, 'Gutscheine', 'gutscheinekategorie', $kats->getGutscheinekatname());
            } else {
                $gutscheinecounterdata[$kats->getGutscheinekatname()] = false;
                $gutscheinecounterkats[$kats->getGutscheinekatname()] = false;
            }
        }
            $dm->flush();
            //var_dump($wohnort);
            
            return $this->render('index.html.twig', [
                'controller_name' => 'UserStartController', 
                'file_exists' => $file_exists,
                'fileexistarray' => $fileexistarray,
                'filelinkarray' => $filelinkarray,
                'pincounterkats' => $pincounterkats,
                'druckerei' => $druckerei,
                'grafiker' => $grafiker,
                'fotograf' => $fotograf,
                'entwickler' => $entwickler,
                'texter' => $texter,
                'illustrator' => $illusator,
                'sonstiges' => $sonstiges,
                'username' => $users,
                'piclink' => $piclink,
                'unreads' => $mailanz,
                'wohnort' => $wohnort,
                'pinlastLogin' => $pinlastLogin,
                'filter' => $filter.'-'.$_POST['fulltext'],
                'useremploy' => $useremploy,
                'form' => $builder->createView(),
                'forms' => $builders->createView(),
                'pinkats' => $pinkats,
                'userexist' => $userexist,
                'searchfindcount' => count($pindatacount),
                'useronlinedata' => $useronlinedata,
                'pinsnumbers' => MediadatenController::mediadaten($dm, 'Pins', false, false),
                'usernumbers' => MediadatenController::mediadaten($dm, 'User', false, false),
                'articlenumbers' => MediadatenController::mediadaten($dm, 'Article', false, false),
                'articlecounterdata' => $articlecounterdata,
                'articleoutput' => $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 9,0,'DESC','all','','',7,'Startinsert'),
                'articlekats' => $articlekats,
                'gutscheinenumbers' => MediadatenController::mediadaten($dm, 'Gutscheine', false, false),
                'gutscheinecounterdata' => $gutscheinecounterdata,
                'gutscheineoutput' => $gutscheinedata->gutscheineFilterOutput($this->get('doctrine_mongodb')->getManager(), 3,0,'DESC','all','','',7,'Gutscheineinsert'),
                'gutscheinekats' => $gutscheinekats,
                'pincounterdata' => $pincounterdata,
                'counter' => count($pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,$pages,'DESC','Search-'.$_POST['fulltext'],$users,'',7,'Startinsert')),
                'pindata' => $pindata->pinFilterOutput($this->get('doctrine_mongodb')->getManager(), 10,$pages,'DESC','Search-'.$_POST['fulltext'],$users,'',7,'Startinsert'),
                ]);
        }
    }

    /*public function hashToLink($texttolink) {
        $tweet = $texttolink;
        $regex = "/#+([a-zA-Z0-9_]+)/";
        $text = preg_replace($regex, '<a href="/hashtag/$1">#$1</a>', $tweet);
        
        return $text;
        } */   
}
