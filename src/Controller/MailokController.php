<?php

namespace App\Controller;

use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
class MailokController extends Controller
{
    /**
     * @Route("/profile/mailok/{userid}", name="mailok")
     */
    public function index($userid)
    {
        $task = new User();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $taskuser = $dm->getRepository('App:User')->find(["id"=>$userid]);
        if($taskuser->getNewMail() != Null) {

            $curl = curl_init();
            $usermailout = urlencode($taskuser->getEmail());
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/" . $usermailout,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_POSTFIELDS => "{
                \"attributes\": {\"email\":\"".$taskuser->getNewMail()."\",\"name\":\"".$taskuser->getLastName()."\",\"vorname\":\"".$taskuser->getFirstName()."\",\"uuid\":\"".$taskuser->getId()."\"}
              }",
              CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "api-key: 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",
                "content-type: application/json"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }

        $taskuser->setEmail($taskuser->getNewMail());
        $taskuser->setEmailCanonical($taskuser->getNewMail());
        $taskuser->setNewMail(Null);
        $dm->persist($taskuser);



        }
        $dm->flush();

        return $this->render('userprofil/mailok.html.twig', [
            'controller_name' => 'MailokController',
        ]);
    }
}
