<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShopstartController extends AbstractController
{
    /**
     * @Route("/shopstart", name="shopstart")
     */
    public function index()
    {
        return $this->render('shopstart/index.html.twig', [
            'controller_name' => 'ShopstartController',
        ]);
    }
}
