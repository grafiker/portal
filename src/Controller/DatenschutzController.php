<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DatenschutzController extends AbstractController
{
    /**
     * @Route("/datenschutz", name="datenschutz")
     */
    public function index()
    {
        return $this->render('datenschutz/index.html.twig', [
            'controller_name' => 'DatenschutzController',
        ]);
    }
}
