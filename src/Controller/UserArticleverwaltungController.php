<?php

namespace App\Controller;
use App\Document\Article;
use App\Controller\CheckUserdataController;
use App\AppBundle\Form\AdminArticleEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserArticleverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/profile/articleverwaltung/{page}", name="user_articleverwaltung")
     */
    public function createAction(RequestStack $requestStack, Request $request, $page)
    {
        $articledata = new ArticleoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $articlekats = $dm->getRepository('App:Articlekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $articlecounter = array(); 
        foreach($articlekats as $key => $kats) {
            $articlecounter[$kats->getArticlekatname()] = $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getArticlekatname(),'','',7,'Startinsert');
            if($articlecounter[$kats->getArticlekatname()]) {
                $articlecounterdata[$kats->getArticlekatname()] = true;
            } else {
                $articlecounterdata[$kats->getArticlekatname()] = false;
            }
        }    
        $userdata = new CheckUserdataController();
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('username' => $username),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
            if($users != "anon.") {
                $provider = $this->container->get('fos_message.provider');
                $mailanz = $provider->getNbUnreadMessages();
            } else {
                $mailanz = false;
            }
            $articlelastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/articleverwaltung.html.twig', array(
            'file_exists' => $users->getUserpic(),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'unreads' => $mailanz,
            'articledata' => $articledata,
            'articlelastLogin' => $articlelastLogin,
            'useremploy' => $useremploy,
            'count' => count($articledatagesamt)/$anzprosite,
            'counter' => count($articledatagesamt),
            'articlekats' => $articlekats,
            'articlecounterdata' => $articlecounterdata,
        ));
    }

    /**
     * edit
     * @Route("/profile/articleverwaltung/{articleID}/{action}", name="user_articleverwaltung_edit")
     */
    
    public function editAction(RequestStack $requestStack, Request $request, $articleID, $action, \Swift_Mailer $mailer)
    {
        $articledata = new ArticleoutputController();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $articlekats = $dm->getRepository('App:Articlekategorien')->findBy(array('aktiv' => 1),array('sort' => 'ASC'));         
        $dm->flush();
        $articlecounter = array(); 
        foreach($articlekats as $key => $kats) {
            $articlecounter[$kats->getArticlekatname()] = $articledata->articleFilterOutput($this->get('doctrine_mongodb')->getManager(), 1,0,'DESC',$kats->getArticlekatname(),'','',7,'Startinsert');
            if($articlecounter[$kats->getArticlekatname()]) {
                $articlecounterdata[$kats->getArticlekatname()] = true;
            } else {
                $articlecounterdata[$kats->getArticlekatname()] = false;
            }
        }    
        $task = new Article();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $articlecheck = $emdel->getRepository('App:Article')->findOneBy(["id"=>$articleID]);

            $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/articleview/'.$articlecheck->getArticlelink();
            $subjetsys = "Ein Beitrag wurde gelöscht!";
            $message = (new \Swift_Message())
            ->setSubject($subjetsys)
            ->setFrom('article@grafiker.de')
            ->setTo('mail@grafiker.de')
            ->setBody('Es wurde von: ' .$articlecheck->getUsername(). ' ein Beitrag auf Grafiker.de gelöscht!<br /><br />Der Link zum Beitrag war: ' . $linkToView);
            $mailer->send($message);     
            $maillog = new MaillogController();
            $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "article@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$articlecheck->getUsername(). ' ein Beitrag auf Grafiker.de gelöscht!<br /><br />Der Link zum Beitrag war: ' . $linkToView);
            if($articlecheck->getPicname()) {
            foreach($articlecheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics);
                } else {}
            }
        }
            if ($articlecheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$articlecheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$articlecheck->getThumbnail());
            } else {}
            $emdel->remove($articlecheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $userdata = new CheckUserdataController();
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
                {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
            } else {
                $username = 'Kein Mitglied!';
            }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('username' => $username, 'astitle' => 'Startinsert'),array('id' => 'DESC'), $anzprosite, $page);
            $articlelastLogin[$users->getUsername()] = $users->getUpdatedAt();
            $useremploy[$users->getUsername()] = $users->getEmploy();
        return $this->render('userprofil/articleverwaltung.html.twig', array(
            'file_exists' => $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $users),
            'piclink' => $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $users),
            'articledata' => $articledata,
            'count' => count($articledatagesamt)/$anzprosite,
            'counter' => count($articledatagesamt),
            'articlekats' => $articlekats,
            'articlelastLogin' => $articlelastLogin,
            'useremploy' => $useremploy,
            'articlecounterdata' => $articlecounterdata,
        ));
        } else if ($action == "edit" or $action == "all") {
            $userdata = new CheckUserdataController();
            $users = $this->container->get('security.token_storage')->getToken()->getUser();
            if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
                {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
            } else {
                $username = 'Kein Mitglied!';
            }
        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["id"=>$articleID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Articlekategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
        }
        $newextarray=array(array("articleID" => $articleID,"articlekategorie" => $articlecheck->getArticlekategorie(),"kommentare" => $articlecheck->getKommentare(),"bewerten" => $articlecheck->getBewerten(),"title" => $articlecheck->getTitle(),"content" => $articlecheck->getContent(),"rechte" => $articlecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminArticleEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $articledata = $dms->getRepository('App:Article')->find($articleID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($articledata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics);
    } else {}
    }
                $articlecheck->setAstitle('Articleinsert');
                $articledata->setTitle($task["titel"]);
                $articledata->setArticlekategorie($task["articlekategorie"]);
                $articledata->setContent($task["content"]);
                $articledata->setIsads($task["isads"]);
                $articledata->setBewerten($task["bewerten"]);
                $articledata->setPicname($array);
                $dms->flush();
                $linkToView=$requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/articleview/'.$articledata->getArticlelink();
                $subjetsys = "Ein überarbeiteter Beitrag zum Freischalten!";
                $message = (new \Swift_Message())
                ->setSubject($subjetsys)
                ->setFrom('article@grafiker.de')
                ->setTo('mail@grafiker.de')
                ->setBody('Es wurde von: ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);
                $mailer->send($message);
                $maillog = new MaillogController();
                $maillog->mailtoDb($this->get('doctrine_mongodb')->getManager(), "article@grafiker.de", "mail@grafiker.de", $subjetsys, 'Es wurde von: ' .$username. ' ein Beitrag auf Grafiker.de überarbeitet und wartet auf Freischaltung!<br /><br />Der Link zum Beitrag: ' . $linkToView);                

            }
            $casepics = array();
            $casepics=$articlecheck->getPicname(); 
        return $this->render('userprofil/articleedit.html.twig', array(
            'form' => $builder->createView(),
            'articleID' => $articleID,
            'username' => $articlecheck->getUsername(),
            'picsarray' => $casepics,
            'articlekats' => $articlekats,
            'articlecounterdata' => $articlecounterdata,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["id"=>$articleID]);

        $articlecheck->setAstitle("Startinsert");
        $articlecheck->setAktiv(true);
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$articlecheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('Ihr Beitrag wurde freigeschaltet!')
            ->setBody('Ihr Beitrag mit dem Titel: <strong style="color:red;">' . $articlecheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'DESC'), $anzprosite, $page);

        return $this->render('userprofil/articleverwaltung.html.twig', array(
            'articledata' => $articledata,
            'count' => count($articledatagesamt)/$anzprosite,
            'articlekats' => $articlekats,
            'articlecounterdata' => $articlecounterdata,
        ));
    }
    }
}
