<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HierwerbenController extends AbstractController
{
    /**
     * @Route("/hierwerben", name="hierwerben")
     */
    public function index()
    {
        return $this->render('hierwerben/index.html.twig', [
            'controller_name' => 'HierwerbenController',
        ]);
    }
}
