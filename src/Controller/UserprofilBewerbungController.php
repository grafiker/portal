<?php

namespace App\Controller;
use App\Document\Job;
use App\Document\Bewerbung;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
class UserprofilBewerbungController extends Controller
{
    /**
     * @Route("/profile/bewerbung/out", name="userprofil_bewerbung_out")
     */
    public function index()
    {
        $users = $this->container->get('security.token_storage')->getToken()->getUser();
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
            {
                $user = $this->container->get('security.token_storage')->getToken()->getUser();
                $username = $user->getUsername();
                $usergroup = $user->getRoles();
            } else {
                $username = 'Kein Mitglied!';
            }
        $anzprosite = 10;
            if(!isset($page) or $page == "all") {
                $page=0;
            } else {
                $page=$page*$anzprosite;
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $checkanz = $dm->getRepository('App:Bewerbung')->findBy(array('username' => $username),array('id' => 'DESC'), $anzprosite, $page);
        $checkanzahl = $checkanz;
        //var_dump($checkanz);
        $ausname = array();
        foreach($checkanz as $checkanzahl) {
            //var_dump($checkanzahl->getJobId());
            $checkname = $dm->getRepository('App:Job')->findBy(array('id' => $checkanzahl->getJobId())); 
            //var_dump($checkanzahl->getJobId());
            if($checkname) {
            foreach($checkname as $checkname) {
                $ausname[$checkanzahl->getJobId()] =  $checkname->getAstitle();
            }
            } else {
                $ausname[$checkanzahl->getJobId()] = "del";
            }
        }
        //var_dump($ausname);
            $textausgabe = "Meine Ausschreibungs-Bewerbungen: ";
        return $this->render('userprofil/bewerbung.html.twig', [
            'controller_name' => 'UserprofilBewerbungController',
            'textausgabe' => $textausgabe,
            'checkanz' => $checkanz,
            'ausname' => $ausname,
            'inout' => 'out',
        ]);
    }
    /**
     * @Route("/profile/bewerbung/in/{jobid}", name="userprofil_bewerbung_in")
     */
    public function createAction($jobid)
    {
        $anzprosite = 10;
            if(!isset($page) or $page == "all") {
                $page=0;
            } else {
                $page=$page*$anzprosite;
            }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $checkanz = $dm->getRepository('App:Bewerbung')->findBy(array('jobID' => $jobid),array('id' => 'DESC'), $anzprosite, $page);

            //var_dump($checkanzahl->getJobId());
            $checkname = $dm->getRepository('App:Job')->findOneBy(array('id' => $checkanz[0]->getJobId())); 
            //var_dump($checkname);
        $ausname =  $checkname->getAstitle();

        $textausgabe = "Bewerbung auf Ihre Ausschreibungen: ";
        return $this->render('userprofil/bewerbung.html.twig', [
            'controller_name' => 'UserprofilBewerbungController',
            'textausgabe' => $textausgabe,
            'checkanz' => $checkanz,
            'ausname' => $ausname,
            'inout' => 'in',
        ]);
    }
}
