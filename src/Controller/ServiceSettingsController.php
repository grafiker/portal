<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceSettingsController extends AbstractController
{
    /**
     * @Route("/service/settings", name="service_settings")
     */
    public function index()
    {
        return $this->render('service_settings/index.html.twig', [
            'controller_name' => 'ServiceSettingsController',
        ]);
    }
}
