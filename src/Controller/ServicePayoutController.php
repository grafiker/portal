<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserManagerInterface;

class ServicePayoutController extends Controller
{
    private $userManager;
    function __construct(ManagerRegistry $doctrine_mongodb, UserManagerInterface $userManager)
    {
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_userManager = $userManager;
    }
    /**
     * @Route("/service/payout", name="service_payout")
     */
    public function index()
    {
        $emdsend = $this->_doctrine_mongodb->getManager();

        $thistenders = $emdsend->getRepository('App:Jobs')->findOneBy(array("id" => $_POST["jobid"]));
        if($thistenders) {
        $thistenders->setPayout($_POST);
        $thistenders->setAktiv(true);
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();
        } else {
        $thistenders = $emdsend->getRepository('App:Job')->findOneBy(array("id" => $_POST["jobid"]));
        $thistenders->setPayout($_POST);
        $thistenders->setAktiv(true);
        $this->_doctrine_mongodb->getManager()->persist($thistenders);
        $this->_doctrine_mongodb->getManager()->flush();
        }
        return $this->render('service_payout/index.html.twig', [
            'controller_name' => 'ServicePayoutController',
            'output' => 1,
        ]);
    }
}
