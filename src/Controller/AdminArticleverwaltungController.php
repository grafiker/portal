<?php

namespace App\Controller;
use App\Document\Article;
use App\Document\User;
use App\AppBundle\Form\AdminArticleEditFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class AdminArticleverwaltungController extends Controller
{
    /**
     * Create
     * @Route("/admin/articleverwaltung/{page}", name="admin_articleverwaltung")
     */
    public function createAction(Request $request, $page)
    {
        
        $userdata = new CheckUserdataController();
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            $filelinkarray=array();
            $reexistkarray=array();
            $anbexistkarray=array();
            $usernameId=array();
            foreach($articledata as $key => $articleuser) {
                $fileexistarray[$articleuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
                $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
            }
foreach($articledata as $key => $articleids) {
$pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 1));
$pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 2));
$userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $articleids->getUsername()));
$usernameId[$articleids->getId()] = $userdata->getId();
if($pdfcheckre) {
    $reexistkarray[$articleids->getId()] = $pdfcheckre->getRechnungsnummer();
    }
    if($pdfcheckanb) {
    $anbexistkarray[$articleids->getId()] = $pdfcheckanb->getRechnungsnummer();
    }
if(!$pdfcheckre) {
$reexistkarray[$articleids->getId()] = false;
}
if(!$pdfcheckanb) {
$anbexistkarray[$articleids->getId()] = false;
}
}
        return $this->render('admin/articleverwaltung.html.twig', array(
            'articledata' => $articledata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($articledatagesamt)/$anzprosite,
        ));
    }

    /**
     * edit
     * @Route("/admin/articleverwaltung/{articleID}/{action}", name="admin_articleverwaltung_edit")
     */
    
    public function editAction(Request $request, $articleID, $action)
    {
        $userdata = new CheckUserdataController();
        $task = new Article();
        if($action == "del") {
            $emdel = $this->get('doctrine_mongodb')->getManager();
            $articlecheck = $emdel->getRepository('App:Article')->findOneBy(["id"=>$articleID]);
if($articlecheck->getPicname()) {
            foreach($articlecheck->getPicname() as $key => $delpics) {
                if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics)){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics);
                } else {}
            }
        }
            
            if ($articlecheck->getThumbnail() != '' and file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$articlecheck->getThumbnail())){
                unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$articlecheck->getThumbnail());
            } else {}
            $emdel->remove($articlecheck);
            $emdel->flush();

            $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'DESC'), $anzprosite, $page);

            $fileexistarray=array();
            $filelinkarray=array();
            foreach($articledata as $key => $articleuser) {
                $fileexistarray[$articleuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
                $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($articledata as $key => $articleids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $articleids->getUsername()));
                $usernameId[$articleids->getId()] = $userdata->getId();
                if($pdfcheckre) {
                    $reexistkarray[$articleids->getId()] = $pdfcheckre->getRechnungsnummer();
                    }
                    if($pdfcheckanb) {
                    $anbexistkarray[$articleids->getId()] = $pdfcheckanb->getRechnungsnummer();
                    }
                if(!$pdfcheckre) {
                $reexistkarray[$articleids->getId()] = false;
                }
                if(!$pdfcheckanb) {
                $anbexistkarray[$articleids->getId()] = false;
                }
                }
        return $this->render('admin/articleverwaltung.html.twig', array(
            'articledata' => $articledata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($articledatagesamt)/$anzprosite,
        ));
        } else if ($action == "edit" or $action == "all") {

        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["id"=>$articleID]);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $kategorien = $dm->getRepository('App:Articlekategorien')->findall();
        $em->flush();
        $dm->flush();
        $extarray=array();
        $nextextarray=array();
        foreach($kategorien as $key=>$value){
                $extarray[$value->getArticlekatname()] = $value->getArticlekatname();
        }
        if(!$articlecheck->getIsads()) {
            $isads = false;
        } else {
            $isads = $articlecheck->getIsads();
        }
        var_dump($articlecheck->getIsads());
        $newextarray=array(array("articleID" => $articleID,"articlekategorie" => $articlecheck->getArticlekategorie(),"isads" => $isads,"bewerten" => $articlecheck->getBewerten(),"title" => $articlecheck->getTitle(),"content" => $articlecheck->getContent(),"rechte" => $articlecheck->getRechte(), "data" => array($extarray)));
        $builder = $this->createForm(AdminArticleEditFormType::class, $newextarray);
        $builder->handleRequest($request);
            if ($builder->isSubmitted() && $builder->isValid()) {
                $dms = $this->get('doctrine_mongodb')->getManager();
                $articledata = $dms->getRepository('App:Article')->find($articleID);
                $task = $builder->getData();

                $teile = explode(",", $task["picselected"]);
                unset($teile[count($teile)-1]);
                $array = array_diff($articledata->getPicname(), $teile);

    foreach($teile as $key => $delpics) {
        if (file_exists($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics)){
        unlink($this->get('kernel')->getProjectDir() . "/public/upload/uploads/articlepics/".$delpics);
    } else {}
    }

                $articledata->setTitle($task["titel"]);
                $articledata->setArticlekategorie($task["articlekategorie"]);
                $articledata->setContent($task["content"]);
                $articledata->setIsads($task["isads"]);
                $articledata->setBewerten($task["bewerten"]);
                $articledata->setPicname($array);
                $dms->flush();
            }
            $casepics = array();
            $casepics=$articlecheck->getPicname(); 
        return $this->render('admin/articleedit.html.twig', array(
            'form' => $builder->createView(),
            'articleID' => $articleID,
            'username' => $articlecheck->getUsername(),
            'picsarray' => $casepics,
        ));
    } else if($action == "ok") {
        $em = $this->get('doctrine_mongodb')->getManager();
        $articlecheck = $em->getRepository('App:Article')->findOneBy(["id"=>$articleID]);

        $articlecheck->setAstitle("Startinsert");
        $articlecheck->setAktiv(true);
        $em->flush();
        
        $sender = $this->get('security.token_storage')->getToken()->getUser();
        
        $emd = $this->get('doctrine_mongodb')->getManager();
        $user = $emd->getRepository('App:User')->findOneBy(["username"=>$articlecheck->getUsername()]);
        
        $threadBuilder = $this->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($user) // Retrieved from your backend, your user manager or ...
            ->setSender($sender)
            ->setSubject('%%Systemmail%% Ihr Beitrag wurde freigeschaltet!')
            ->setBody('Ihr Beitrag mit dem Titel: <strong style="color:red;">' . $articlecheck->getTitle() . '</strong> wurde soeben freigeschaltet!');
         
         
        $sender = $this->get('fos_message.sender');
        
        $sender->send($threadBuilder->getMessage());
        
        $anzprosite = 10;
        if(!isset($page) or $page == "all") {
            $page=0;
        } else {
            $page=$page*$anzprosite;
        }
            $dm = $this->get('doctrine_mongodb')->getManager();
            $articledatagesamt = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'ASC'));
            $articledata = $dm->getRepository('App:Article')->findBy(array('astitle' => 'Articleinsert'),array('id' => 'DESC'), $anzprosite, $page);
            $fileexistarray=array();
            $filelinkarray=array();
            $usernameId=array();
            foreach($articledata as $key => $articleuser) {
                $fileexistarray[$articleuser->getUsername()] = $userdata->getCheckPicExist($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
                $filelinkarray[$articleuser->getUsername()] = $userdata->getCheckPicLink($this->get('kernel')->getProjectDir(), $articleuser->getUsername());
            }
            $reexistkarray = array();
            $anbexistkarray = array();
            foreach($articledata as $key => $articleids) {
                $pdfcheckre = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 1));
                $pdfcheckanb = $dm->getRepository('App:Pdf')->findOneBy(array("articleid" => $articleids->getId(), 'reart' => 2));
                $userdata = $dm->getRepository('App:User')->findOneBy(array("username" => $articleids->getUsername()));
                $usernameId[$articleids->getId()] = $userdata->getId();
                
                if($pdfcheckre) {
                $reexistkarray[$articleids->getId()] = $pdfcheckre->getRechnungsnummer();
                } else {
                    $reexistkarray[$articleids->getId()] = false;
                    }
                if($pdfcheckanb) {
                $anbexistkarray[$articleids->getId()] = $pdfcheckanb->getRechnungsnummer();
                } else {
                    $anbexistkarray[$articleids->getId()] = false;
                    }
                }
        return $this->render('admin/articleverwaltung.html.twig', array(
            'articledata' => $articledata,
            'fileexistarray' => $fileexistarray,
            'filelinkarray' => $filelinkarray,
            'reexistkarray' => $reexistkarray,
            'anbexistkarray' => $anbexistkarray,
            'usernameId' => $usernameId,
            'count' => count($articledatagesamt)/$anzprosite,
        ));
    }
    
    }

    function deleteFilesFromDirectory($ordnername){
        $this->deleteFilesFromDirectory("./upload/uploads/userpics/");
        //überprüfen ob das Verzeichnis überhaupt existiert
        if (is_dir($ordnername)) {
        //Ordner öffnen zur weiteren Bearbeitung
        if ($dh = opendir($ordnername)) {
        //Schleife, bis alle Files im Verzeichnis ausgelesen wurden
        while (($file = readdir($dh)) !== false) {
        //Oft werden auch die Standardordner . und .. ausgelesen, diese sollen ignoriert werden
        if ($file!="." AND $file !="..") {
        //Files vom Server entfernen
        unlink("".$ordnername."".$file."");
        }
        }
        //geöffnetes Verzeichnis wieder schließen
        closedir($dh);
        }
        }
        var_dump(is_dir($ordnername));
        }
         
        //Funktionsaufruf - Directory immer mit endendem / angeben
        
}
