<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $urlGenerator;
    private $csrfTokenManager;
    

    public function __construct(UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function supports(Request $request)
    {
        return 'app_service_login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        // Load / create our user however you need.
        // You can do this by calling the user provider, or with custom logic here.
        $user = $userProvider->loadUserByUsername($credentials['email']);

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email could not be found.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $toroutes = "";
        // Check the user's password or other credentials and return true or false
        // If there are no credentials to check, you can just return true
        //\var_dump($credentials);
        //\var_dump($user->getRoles());

        //if(\in_array("ROLE_ADMIN", $user->getRoles())) {
        //    $toroutes = "admin_admin";
        //    return new RedirectResponse($this->urlGenerator->generate($toroutes));
        //}
       return true;
        //throw new \Exception('TODO: check the credentials inside '.__FILE__);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        //\var_dump($request->request->all());
        //die("STOP");
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }
if($request->request->all()["widgetlogin"] == 1) {
    return new RedirectResponse($this->urlGenerator->generate('service_tenders_control', array('id' => $request->request->all()["id"], 'lastid' => $request->request->all()["lastid"], )));
} else if($request->request->all()["widgetjoblogin"] == 1) {
    return new RedirectResponse($this->urlGenerator->generate('service_tenders_jobs', array('id' => $request->request->all()["id"], 'widgetusername' => 'login', 'cacheid' => $_GET["cacheid"])));
} else {
if($token->getRoles()[0]->getRole() == "ROLE_ADMIN") {
        return new RedirectResponse($this->urlGenerator->generate('admin_admin'));
} else if($token->getRoles()[0]->getRole() == "ROLE_CUSTOMER") {
return new RedirectResponse($this->urlGenerator->generate('admin_customer'));
    } else if($token->getRoles()[0]->getRole() == "ROLE_PRINTSHOP") {
    return new RedirectResponse($this->urlGenerator->generate('admin_printshop'));
        }
    }
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate('app_service_login');
    }
}
