<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class PinmeldungFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $task)
    {
        //var_dump($extarray);
        $builder

        ->add('emailadresse', TextType::class, ['label' => 'Ihre Mailadresse: '])
        ->add('grund',ChoiceType::class,
        array('label' => 'Grund der Meldung: ','label_attr' => array('class' => 'meldegrund'),'choices' => array(
                'Rechte am Inhalt (Text oder Bilder)' => 'Rechte am Inhalt (Text oder Bilder)',
                'Gewalt / Belästigung' => 'Gewalt / Belästigung',
                'Falschmeldung' => 'Falschmeldung',
                'anderer Grund' => 'anderer Grund'),
        'multiple'=>false,'expanded'=>true))

        ->add('subject', TextType::class, ['label' => 'Betreff: '])
        ->add('content', TextareaType::class, ['label' => 'Nachricht :']);
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
