<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = 1930; $i <= date('Y')-15; $i++) {
        $years[$i] = $i;
        }
        $land = array("Deutschland","Schweiz","Österreich","------------------------","Aegypten","Aequatorial-Guinea","Aethiopien","Afghanistan","Albanien","Algerien","Andorra","Angola","Antigua und Barbuda","Argentinien","Armenien","Aserbaidschan","Australien","Bahamas","Bahrain","Bangladesch","Barbados","Belarus","Belgien","Belize","Benin","Bhutan","Bolivien","Bosnien-Herzegowina","Botswana","Brasilien","Brunei","Bulgarien","Burkina Faso","Burundi","Chile","China","Costa Rica","Cote d Ivoire","Daenemark","Deutschland","Djibouti","Dominika","Dominikanische Republik","Ecuador","El Salvador","Eritrea","Estland","Fidschi","Finnland","Frankreich","Gabun","Gambia","Georgien","Ghana","Grenada","Griechenland","Grossbritannien","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Indien","Indonesien","Irak","Iran","Irland","Island","Israel","Italien","Jamaika","Japan","Jemen","Jordanien","Kambodscha","Kamerun","Kanada","Kap Verde","Kasachstan","Katar","Kenia","Kirgistan","Kiribati","Kolumbien","Komoren","Kongo","Kongo, Dem. Republik","Kroatien","Kuba","Kuwait","Laos","Lesotho","Lettland","Libanon","Liberia","Libyen","Liechtenstein","Litauen","Luxemburg","Madagaskar","Malawi","Malaysia","Malediven","Mali","Malta","Marokko","Marschall-Inseln","Mauretanien","Mauritius","Mazedonien","Mexiko","Mikronesien","Moldawien","Monaco","Mongolei","Montenegro","Mosambik","Myanmar","Namibia","Nepal","Neuseeland","Nicaragua","Niederlande","Niger","Nigeria","Nordkorea","Norwegen","Oesterreich","Oman","Osttimor","Pakistan","Palau","Panama","Papua-Neuguinea","Paraguay","Peru","Philippinen","Polen","Portugal","Ruanda","Rumaenien","Russland","Saint Kitts und Nevis","Saint Lucia","Saint Vincent","Salomonen","Sambia","Samoa","San Marino","Sao Tome","Saudi-Arabien","Schweden","Schweiz","Senegal","Serbien","Seychellen","Sierra Leone","Simbabwe","Singapur","Slowakei","Slowenien","Somalia","Spanien","Sri Lanka","Sudan","Suedafrika","Suedkorea","Surinam","Swasiland","Syrien","Tadschikistan","Taiwan","Tansania","Thailand","Togo","Tonga","Trinidad und Tobago","Tschad","Tschechien","Tuerkei","Tunesien","Turkmenistan","Tuvalu","Uganda","Ukraine","Ungarn","Uruguay","USA","Usbekistan","Vanuatu","Vatikanstadt","Venezuela","V. A. Emirate","Vietnam","Zentralafrika, Republik","Zypern");
        $landnew=array();
        foreach($land as $land) {
            $landnew[$land] = $land;
        }

        //var_dump($landnew);
        $builder
        ->add('username', null, array('label' => 'Benutzername', 'attr' => array('readonly' => true)))
        ->add('berufsgruppe8',CheckboxType::class,
        array('label' => 'Ich bin Auftraggeber',
        'required' => false))
        ->add('berufsgruppe6',CheckboxType::class,
            array('label' => 'Ich bin Druckerei',
            'required' => false))
        ->add('berufsgruppe9',CheckboxType::class,
            array('label' => 'Druckerei-Mitarbeiter',
            'required' => false))
        ->add('berufsgruppe1',CheckboxType::class,
            array('label' => 'Ich bin Grafiker',
            'required' => false))
        ->add('berufsgruppe2',CheckboxType::class,
            array('label' => 'Ich bin Fotograf',
            'required' => false))
        ->add('berufsgruppe3',CheckboxType::class,
            array('label' => 'Ich bin Programmierer',
            'required' => false))
        ->add('berufsgruppe4',CheckboxType::class,
            array('label' => 'Ich bin Texter',
            'required' => false))
        ->add('berufsgruppe5',CheckboxType::class,
            array('label' => 'Ich bin Illustrator',
            'required' => false))
        ->add('berufsgruppe7',CheckboxType::class,
            array('label' => 'Ich bin Sonstiges',
            'required' => false))
        ->add('firma', null, array('label' => 'Firma',
        'required' => false))
        ->add('anrede', ChoiceType::class, array(
            'choices' => array(
                'Anrede*' => array(
                    'Herr' => 'Herr',
                    'Frau' => 'Frau',
                )
            ),
        ))

        ->add('titel', ChoiceType::class, array(
            'label' => 'Akd. Grad',
            'choices' => array(
                    'Autodidakt' => 'Autodidakt',
                    'In Ausbildung' => 'In Ausbildung',
                    'abgeschlossene Ausbildung' => 'abgeschlossene Ausbildung',
                    'Bachelor' => 'Bachelor',
                    'Ökonom' => 'Ökonom',
                    'Master' => 'Master',
                    'Diplom' => 'Diplom',
                    'Dr.' => 'Dr.',
                    'Prof.' => 'Prof.',
                    'Prof. Dr.' => 'Prof. Dr.',
            ),
        ))

        ->add('employment', ChoiceType::class, array(
            'label' => 'Beschäftigungsart',
            'choices' => array(
                    'Angestellt (Vollzeit)' => 'Angestellt Vollzeit',
                    'Angestellt (Teilzeit)' => 'Angestellt Teilzeit',
                    'Praktikum' => 'Praktikum',
                    'Inhaber/-in' => 'Inhaber/-in',
                    'Vorstandsmitglied' => 'Vorstandsmitglied',
                    'Ehrenamtlich' => 'Ehrenamtlich',
                    'Selbstständig' => 'Selbstständig',
                    'Gesellschafter/-in' => 'Gesellschafter/-in',
                    'Beamtet' => 'Beamtet',
                    'Personalvermittler/-in' => 'Personalvermittler/-in',
                    'Freiberuflich' => 'Freiberuflich',
                    'Partner/-in' => 'Partner/-in',
                    'Jobsuchend' => 'Jobsuchend',
            ),
        ))

        ->add('berufsbezeichnung', null, array('label' => 'Berufsbezeichnung', 'attr' => array('placeholder' => 'z.B. Grafikdesigner, Offsetdrucker'),
            'required' => false))
        ->add('stundenpreis', null, array('label' => 'Ihr Stundenpreis',
        'required' => false))
        ->add('strassenummer', null, array('label' => 'Strasse, Hausnummer',
        'required' => false))
        ->add('plz', null, array('label' => 'Postleitzahl',
        'required' => false))
        ->add('wohnort', null, array('label' => 'Wohnort',
        'required' => false))

        ->add('land', ChoiceType::class, array(
            'label' => 'Land',
            'required' => false,
            'choices' => array(
                'Land' => $landnew
            ),
        ))

        ->add('website', null, array('label' => 'Webseite','required' => false))
        ->add('deutsch',CheckboxType::class,
            array('label' => 'Ich spreche Deutsch',
            'required' => false))
        
        ->add('englisch',CheckboxType::class,
            array('label' => 'Ich spreche Englisch',
            'required' => false))

        ->add('franzoesisch',CheckboxType::class,
            array('label' => 'Ich spreche Französisch',
            'required' => false))
        
        ->add('italienisch',CheckboxType::class,
            array('label' => 'Ich spreche Italienisch',
            'required' => false))

        ->add('russisch',CheckboxType::class,
            array('label' => 'Ich spreche Russisch',
            'required' => false))

        ->add('spanisch',CheckboxType::class,
            array('label' => 'Ich spreche Spanisch',
            'required' => false))

        ->add('chinesisch',CheckboxType::class,
            array('label' => 'Ich spreche Chinesisch',
            'required' => false))

        ->add('aspeak', null, array('label' => 'andere Sprachen','required' => false))
        ->add('warumda1',CheckboxType::class,
            array('label' => 'Ich möchte mich für Jobs und Aufträge bewerben.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('warumda2',CheckboxType::class,
            array('label' => 'Ich möchte Jobs und Aufträge vergeben.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('warumda3',CheckboxType::class,
            array('label' => 'Ich möchte mein Kreativ-Netzwerk erweitern.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('warumda4',CheckboxType::class,
            array('label' => 'Ich möchte mein Portfolio im Web präsentieren.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('warumda5',HiddenType::class,
            array('label' => 'Ich möchte in den Foren zu diskutieren.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('warumda6',HiddenType::class,
            array('label' => 'Ich möchte bei Fragen schnelle Hilfe zu erhalten.','label_attr' => array('class' => 'warumdalabel'),'attr' => array('class' => 'warumda'),
            'required' => false))
        ->add('beschreibung', TextareaType::class, array('help' => 'Ihre Emailadresse und die folgenden persönlichen Daten werden im Profil nicht veröffentlicht!
        ','label' => 'Kurzbeschreibung',
        'required' => false))

        ->add('firstName', null, array('label' => 'Vorname*'))
        ->add('lastName', null, array('label' => 'Nachname*'))
        ->add('telefon', null, array('label' => 'Telefon',
        'required' => false))
        ->add('mobil', null, array('label' => 'Mobil',
        'required' => false))

        ->add('gebDay', ChoiceType::class, array(
            'label' => 'Geburtstag',
            'required' => false,
            'choices' => array(
                'Tag' => $days
            ),
        ))
        ->add('gebMon', ChoiceType::class, array(
            'label' => 'Geburtsmonat',
            'required' => false,
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))
        ->add('gebYear', ChoiceType::class, array(
            'label' => 'Geburtsjahr',
            'required' => false,
            'choices' => array(
                'Jahr' => $years
            ),
        ))


        ->add('email', HiddenType::class, array('label' => 'E-Mail'))
        ->remove('current_password');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }
    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
