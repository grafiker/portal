<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminMailerExportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($builder);
        $builder
        ->add('berufsgruppe8',CheckboxType::class,
            array('label' => 'Auftraggeber: ',
            'required' => false))
        ->add('berufsgruppe6',CheckboxType::class,
            array('label' => 'Druckerei: ',
            'required' => false))
        ->add('berufsgruppe9',CheckboxType::class,
            array('label' => 'Druckerei-Mitarbeiter: ',
            'required' => false))
        ->add('berufsgruppe1',CheckboxType::class,
            array('label' => 'Grafiker: ',
            'required' => false))
        ->add('berufsgruppe2',CheckboxType::class,
            array('label' => 'Fotograf: ',
            'required' => false))
        ->add('berufsgruppe3',CheckboxType::class,
            array('label' => 'Programmierer: ',
            'required' => false))
        ->add('berufsgruppe4',CheckboxType::class,
            array('label' => 'Texter: ',
            'required' => false))
        ->add('berufsgruppe5',CheckboxType::class,
            array('label' => 'Illustrator: ',
            'required' => false))
        ->add('alle',CheckboxType::class,
            array('label' => 'Alle Mitglieder: ',
            'required' => false))
        ->add('mailer',CheckboxType::class,
            array('label' => 'Newsletter / Montagsmailer: ',
            'required' => false))
        ->add('sysmailer',CheckboxType::class,
            array('label' => 'Systemmail: ',
            'required' => false))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'an SendBlue senden...'));        
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
