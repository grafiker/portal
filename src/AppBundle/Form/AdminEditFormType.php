<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $user)
    {
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = 1900; $i <= date('Y')-15; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('anrede', ChoiceType::class, array(
            'choices' => array(
                'Anrede' => array(
                    'Herr' => 'Herr',
                    'Frau' => 'Frau',
                )
            ),
        ))

        ->add('titel', ChoiceType::class, array(
            'label' => 'Titel', 'required' => false,
            'choices' => array(
                'Titel' => array(
                    'Autodidakt' => 'Autodidakt',
                    'In Ausbildung' => 'In Ausbildung',
                    'abgeschlossene Ausbildung' => 'abgeschlossene Ausbildung',
                    'Bachelor' => 'Bachelor',
                    'Ökonom' => 'Ökonom',
                    'Master' => 'Master',
                    'Diplom' => 'Diplom',
                    'Dr.' => 'Dr.',
                    'Prof.' => 'Prof.',
                    'Prof. Dr.' => 'Prof. Dr.',
                )
            ),
        ))


        ->add('firma', null, array('label' => 'Firma', 'required' => false,
        'required' => false))
        ->add('firstName', null, array('label' => 'Vorname', 'required' => false))
        ->add('lastName', null, array('label' => 'Nachname', 'required' => false))
        ->add('strassenummer', null, array('label' => 'Strasse und Hausnummer*', 'required' => false))
        ->add('plz', null, array('label' => 'Postleitzahl', 'required' => false))
        ->add('wohnort', null, array('label' => 'Wohnort', 'required' => false))
        ->add('telefon', null, array('label' => 'Telefon', 'required' => false))
        ->add('mobil', null, array('label' => 'Mobil', 'required' => false))
        ->add('username', null, array('label' => 'Benutzername', 'attr' => array('readonly' => true)))
                
        ->add('gebMon', ChoiceType::class, array(
            'label' => 'Monat', 'required' => false,
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))

        ->add('gebDay', ChoiceType::class, array(
            'label' => 'Tag', 'required' => false,
            'choices' => array(
                'Tag' => $days
            ),
        ))
        ->add('gebYear', ChoiceType::class, array(
            'label' => 'Jahr', 'required' => false,
            'choices' => array(
                'Jahr' => $years
            ),
        ))
        ->add('stundenpreis', null, array('label' => 'Durchschnittlicher Stundenpreis (in Euro pro Stunde)', 'required' => false))
        ->add('email', EmailType::class, array('label' => 'E-Mail', 'required' => false))
        ->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Mitglied bearbeiten...'));
        
    }

   
    public function getBlockPrefix()
    {
        return 'app_user_adminedit';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
