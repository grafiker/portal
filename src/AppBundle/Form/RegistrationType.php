<?php
namespace App\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = 1930; $i <= date('Y')-15; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('notifications', HiddenType::class, array(
            'data' => '2',
            'required' => false,
        ))
        ->add('firstLogin', HiddenType::class, array(
            'data' => true,
            'required' => false,
        ))
        ->add('anrede', ChoiceType::class, array(
            'choices' => array(
                'Anrede*' => array(
                    'Herr' => 'herr',
                    'Frau' => 'frau',
                )
            ),
        ))
    ->add('berufsgruppe8',CheckboxType::class,
        array('label' => 'Ich bin Auftraggeber',
        'required' => false))
    ->add('berufsgruppe6',CheckboxType::class,
        array('label' => 'Ich bin Druckerei',
        'required' => false))
    ->add('berufsgruppe9',CheckboxType::class,
        array('label' => 'Druckerei-Mitarbeiter',
        'required' => false))
    ->add('berufsgruppe1',CheckboxType::class,
        array('label' => 'Ich bin Grafiker',
        'required' => false))
    ->add('berufsgruppe2',CheckboxType::class,
        array('label' => 'Ich bin Fotograf',
        'required' => false))
    ->add('berufsgruppe3',CheckboxType::class,
        array('label' => 'Ich bin Programmierer',
        'required' => false))
    ->add('berufsgruppe4',CheckboxType::class,
        array('label' => 'Ich bin Texter',
        'required' => false))
    ->add('berufsgruppe5',CheckboxType::class,
        array('label' => 'Ich bin Illustrator',
        'required' => false))
    ->add('berufsgruppe7',CheckboxType::class,
        array('label' => 'Ich bin Sonstiges',
        'required' => false))        
        ->add('gebMon', ChoiceType::class, array(
            'label' => 'Geburtsmonat',
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))

        ->add('gebDay', ChoiceType::class, array(
            'label' => 'Geburtstag',
            'choices' => array(
                'Tag' => $days
            ),
        ))
        ->add('gebYear', ChoiceType::class, array(
            'label' => 'Geburtsjahr',
            'choices' => array(
                'Jahr' => $years
            ),
        ))
        ->add(
            'firstName',
            TextType::class,
            array(
                'label' => 'Vorname*'
            )
        )
        ->add(
            'lastName',
            TextType::class,
            array(
                'label' => 'Nachname*'
            )
        )
        ->add('email', EmailType::class, array('label' => 'E-Mail*'))
        ->add('username', null, array('label' => 'Benutzername*', 'required' => true))
        ->add('agb',CheckboxType::class,
            array('label' => 'Ich erkläre mich mit den AGB von grafiker.de einverstanden.*', 'label_attr' => array('class' => 'isokform'),
                  'required' => true))
        ->add('datenschutz',CheckboxType::class,
             array('label' => 'Ich erkläre mich mit der Datenschutzerklärung von grafiker.de einverstanden.*', 'label_attr' => array('class' => 'isokform'),
                   'required' => true));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
