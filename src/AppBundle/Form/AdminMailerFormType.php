<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminMailerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($builder);
        $builder
        ->add('titel', TextType::class, array('label' => 'Mail-Betreff: '))
        ->add('extramails', TextType::class, array('label' => 'Mailadressen: ','required' => false))
        ->add('schleifevon', TextType::class, array('label' => 'von: ','required' => false))
        ->add('schleifebis', TextType::class, array('label' => 'bis: ','required' => false))
        ->add('schleifestep', TextType::class, array('label' => 'steps: ','required' => false))
        ->add('extrasend',CheckboxType::class,
            array('label' => 'User überspringen! ',
            'required' => false))
        ->add('system',CheckboxType::class,
            array('label' => 'System-Email an alle User (ignoriert Usereinstellung!): ',
            'required' => false))
        ->add('letter',CheckboxType::class,
            array('label' => 'Interne Mitteilung (Newletter / Montagsmailer / Systemmeldung): ',
            'required' => false))
        ->add('berufsgruppe6',CheckboxType::class,
            array('label' => 'Druckerei: ',
            'required' => false))
        ->add('berufsgruppe1',CheckboxType::class,
            array('label' => 'Grafiker: ',
            'required' => false))
        ->add('berufsgruppe2',CheckboxType::class,
            array('label' => 'Fotograf: ',
            'required' => false))
        ->add('berufsgruppe3',CheckboxType::class,
            array('label' => 'Programmierer: ',
            'required' => false))
        ->add('berufsgruppe4',CheckboxType::class,
            array('label' => 'Texter: ',
            'required' => false))
        ->add('berufsgruppe5',CheckboxType::class,
            array('label' => 'Illustrator: ',
            'required' => false))
        ->add('berufsgruppe7',CheckboxType::class,
            array('label' => 'Sonstiges: ',
            'required' => false))
        ->add('anzahl', null, array('label' => 'Anzahl Empfänger: ', 'data' => 'alle',
            'required' => false))
        ->add('content', CKEditorType::class, array('label' => 'Mail-Text: ',
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))
        
        ->add('sendmail', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Mailer versenden?'));        
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
