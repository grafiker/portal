<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Newsletter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class NewsletterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $task)
    {
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = 2019; $i <= date('Y')+15; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('lettertyp',ChoiceType::class,
        array('label' => 'Mailertyp:','data' => 'montagsmailer','choices' => array(
                'Newsletter' => 'newsletter',
                'Montagsmailer' => 'montagsmailer'
            ),
        'multiple'=>false,'expanded'=>true))
        ->add('letterDay', ChoiceType::class, array(
            'label' => 'Tag','data' => date('j'),
            'choices' => array(
                'Tag' => $days
            ),
        ))
        ->add('letterMon', ChoiceType::class, array(
            'label' => 'Monat','data' => date('n'),
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))
        ->add('letterYear', ChoiceType::class, array(
            'label' => 'Jahr','data' => date('Y'),
            'choices' => array(
                'Jahr' => $years
            ),
        ))
        ->add('mailertext', CKEditorType::class, array('label' => 'Mailertext: ', 'required' => false))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'absenden'));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
