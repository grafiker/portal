<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class GutscheineEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray) {
        //var_dump(date('Y'));
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = date('Y'); $i <= date('Y')+5; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('picselected', HiddenType::class, array(
            'data' => 0,
            'required' => false,
        ))
        ->add('x1', HiddenType::class, array(
            'attr' => array('id' => 'x1'),
            'required' => false,
        ))
        ->add('y1', HiddenType::class, array(
            'attr' => array('id' => 'y1'),
            'required' => false,
        ))
        ->add('x2', HiddenType::class, array(
            'attr' => array('id' => 'x2'),
            'required' => false,
        ))
        ->add('y2', HiddenType::class, array(
            'attr' => array('id' => 'y2'),
            'required' => false,
        ))
        ->add('w', HiddenType::class, array(
            'attr' => array('id' => 'w'),
            'required' => false,
        ))
        ->add('h', HiddenType::class, array(
            'attr' => array('id' => 'h'),
            'required' => false,
        ))
        ->add('startdate', HiddenType::class, array(
            'required' => false,
        ))
        ->add('titel', TextType::class, array('label' => 'Beitrag-Titel: ','data' => $extarray["data"][0]["title"],))
        ->add('gutscheinekategorie', ChoiceType::class, array(
            'label' => 'Gutscheine-Kategorie: ',
            'choices' => $extarray["data"][0]["data"][0],
        ))
        
        ->add('content', CKEditorType::class, array('label' => 'Content', 'data' => $extarray["data"][0]["content"],
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))


        ->add('startDateDay', ChoiceType::class, array(
            'label' => 'Tag: ','data' => date('j',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Tag' => $days
            ),
        ))

        ->add('startDateMonat', ChoiceType::class, array(
            'label' => 'Monat: ','data' => date('n',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))

        ->add('startDateYear', ChoiceType::class, array(
            'label' => 'Jahr: ','data' => date('Y',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Jahr' => $years
            ),
        ))


        ->add('endDateDay', ChoiceType::class, array(
            'label' => 'Tag: ','data' => date('j',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Tag' => $days
            ),
        ))

        ->add('endDateMonat', ChoiceType::class, array(
            'label' => 'Monat: ','data' => date('n',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))

        ->add('endDateYear', ChoiceType::class, array(
            'label' => 'Jahr: ','data' => date('Y',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Jahr' => $years
            ),
        ))
        
        
        ->add('bewerten',CheckboxType::class,
            array('label' => 'Ich erlaube Bewertungen.', 'data' => $extarray["data"][0]["bewerten"],
                'required' => false))
        ->add('rechte',CheckboxType::class, 
            array('label' => 'Ich bin Rechteinhaber an dem Bild / den Bildern', 'data' => $extarray["data"][0]["rechte"],
                'required' => true))
        ->add('isads',CheckboxType::class,
            array('label' => 'Mein Beitrag enthält werbliche Inhalte.', 'data' => $extarray["data"][0]["isads"],
                    'required' => false));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
