<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class GutscheineFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {

        //var_dump(date('Y'));
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = date('Y'); $i <= date('Y')+5; $i++) {
        $years[$i] = $i;
        }
        //var_dump($extarray);
        $builder
        ->add('picselected', HiddenType::class, array(
            'data' => '0',
            'required' => false,
        ))
        ->add('x1', HiddenType::class, array(
            'attr' => array('id' => 'x1'),
            'data' => '0',
            'required' => false,
        ))
        ->add('y1', HiddenType::class, array(
            'attr' => array('id' => 'y1'),
            'data' => '0',
            'required' => false,
        ))
        ->add('x2', HiddenType::class, array(
            'attr' => array('id' => 'x2'),
            'data' => '0',
            'required' => false,
        ))
        ->add('y2', HiddenType::class, array(
            'attr' => array('id' => 'y2'),
            'data' => '0',
            'required' => false,
        ))
        ->add('w', HiddenType::class, array(
            'attr' => array('id' => 'w'),
            'data' => '0',
            'required' => false,
        ))
        ->add('h', HiddenType::class, array(
            'attr' => array('id' => 'h'),
            'data' => '0',
            'required' => false,
        ))
        ->add('x1', HiddenType::class, array(
            'data' => '0',
            'required' => false,
        ))

        

        /*->add('titel', TextType::class, array('label' => 'Gutscheine-Titel: ','data' => '',))
        ->add('gutscheinekategorie', ChoiceType::class, array(
            'label' => 'Gutscheine-Kategorie: ',
            'choices' => $extarray['data'],
        ))
        ->add('rechte',CheckboxType::class,
            array('label_attr' => array('style' => 'display: none;'), 'attr' => array('style' => 'display: none;'), 'label' => 'Ich bin Rechteinhaber an dem Bild / den Bildern ',
            'required' => true))
        ->add('content', CKEditorType::class, array(
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))
        
        ->add('bewerten',CheckboxType::class,
            array('label' => 'Ich erlaube Bewertungen. ',
                'required' => false))

        ->add('isads',CheckboxType::class,
            array('label' => 'Mein Beitrag enthält werbliche Inhalte. ',
                    'required' => false))*/
        ;
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
