<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class NetworkFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $task)
    {
        //var_dump($extarray);
        $builder
        ->add('networkask', HiddenType::class, array(
            'data' => '1',
            'required' => false,
        ))
        ->add('content', TextareaType::class, ['label' => 'Ihre Nachricht zur Kontaktaufnahme'])
        ->add('notiz', TextareaType::class, ['label' => 'Notizen zum Kontakt.'])
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'absenden'));;
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
