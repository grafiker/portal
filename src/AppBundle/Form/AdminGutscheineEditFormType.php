<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Gutscheine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminGutscheineEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($extarray);        
        $builder
        ->add('picselected', HiddenType::class, array(
            'data' => '0',
            'required' => false,
        ))
        ->add('gutscheineID', HiddenType::class, array(
            'data' => $extarray["data"][0]["gutscheineID"],
            'required' => false,
        ))
        ->add('titel', TextType::class, array('label' => 'Pin-Titel: ','data' => $extarray["data"][0]["title"],))

        ->add('gutscheinekategorie', ChoiceType::class, array(
            'label' => 'Pin-Kategorie: ',
            'choices' => $extarray["data"][0]["data"][0],
            'data' => $extarray["data"][0]["gutscheinekategorie"],
        ))
        ->add('content', CKEditorType::class, array('label' => 'Pin-Text: ', 'data' => $extarray["data"][0]["content"],
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))
        
        ->add('bewerten',CheckboxType::class,
            array('label' => 'Bewertung erlauben?', 'data' => $extarray["data"][0]["bewerten"],
                'required' => false))
        ->add('isads',CheckboxType::class,
            array('label' => 'Mein Pin enthält werbliche Inhalte.', 'data' => false,
                    'required' => false))
        ->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Pin überarbeiten?'));        
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
