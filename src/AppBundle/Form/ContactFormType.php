<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $task)
    {
        //var_dump($extarray);
        $builder
        ->add('anrede', ChoiceType::class, array(
            'choices' => array(
                'Anrede' => array(
                    'Herr' => 'herr',
                    'Frau' => 'frau',
                )
            ),
        ))

        ->add('frage',ChoiceType::class,
        array('label' => 'Abteilung:','choices' => array(
                'Support' => 1,
                'Marketing' => 2,
                'Rechnung' => 3),
        'multiple'=>false,'expanded'=>true))

        ->add('subject', TextType::class, ['label' => 'Betreff'])
        ->add('content', TextareaType::class, ['label' => 'Nachricht'])
        ->add('send', SubmitType::class, ['label' => 'Speichern']);
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
