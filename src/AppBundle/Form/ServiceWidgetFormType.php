<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\ServiceWidget;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class ServiceWidgetFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $extarray)
    {
        $builder
        ->add('ID', HiddenType::class)
        ->add('accountURL', TextType::class, array('label' => 'ShopURL'))
        ->add('cssMargin', TextType::class, array('label' => 'CSS Margin', 'required' => false))
        ->add('cssPadding', TextType::class, array('label' => 'CSS Padding', 'required' => false))
        ->add('cssPosition', TextType::class, array('label' => 'Brandinglogo', 'required' => false))
        ->add('cssColor', TextType::class, array('label' => 'CSS Color', 'required' => false))
        ->add('cssBackgroundColor', TextType::class, array('label' => 'CSS BackgroundColor', 'required' => false))
        ->add('cssWidth', TextType::class, array('label' => 'CSS Width', 'required' => false))
        ->add('cssText', TextType::class, array('label' => 'CSS Text (z.B.: right, center, left)', 'required' => false));
        if(count($extarray['data'])) {
            $builder
            ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'Widget bearbeiten'));
        } else {
        $builder
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'Widget erstellen'));
        }
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
