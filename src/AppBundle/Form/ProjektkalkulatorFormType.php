<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class ProjektkalkulatorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($extarray);
        $builder
        ->add('prstart', ChoiceType::class, array(
            'label' => 'Projektart: ', 'required' => true, 
            'choices' => $extarray['data'],
        ))
        ->add('schwer', ChoiceType::class, array(
            'label' => 'Schwierigkeit: ','data' => '1',
            'choices' => array(
                'Projektart' => array(
                    'einfach' => '1',
                    'mittel' => '2',
                    'schwer' => '3',
                )
            ),
        ))
        ->add('stundensatz', TextType::class, array('label' => 'Stundensatz: ','data' => '55',))
        ->add('nart',ChoiceType::class,
            array('label' => 'Nutzungsart:','data' => '1','choices' => array(
                    'einfach' => 1,
                    'exklusiv' => 2),
            'multiple'=>false,'expanded'=>true))

            ->add('numfang',ChoiceType::class,
            array('label' => 'Nutzungsumfang:','data' => '1','choices' => array(
                    'klein' => 1,
                    'mittel' => 2,
                    'groß' => 3,),
            'multiple'=>false,'expanded'=>true))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'btn btn-primary pull-right'),'label' => 'neu berechnen...'))
        /*->add('profi', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Profi-Projektrechner'))*/
        ;
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
