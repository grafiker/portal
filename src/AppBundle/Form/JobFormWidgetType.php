<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Jobs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class JobFormWidgetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $extarray)
    {
        //var_dump(date('Y'));
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = date('Y'); $i <= date('Y')+5; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('astitle', TextType::class, array('label' => 'Jobtitel (z.B. Grafiker / Mediengestalter (w/m/d) für Web):',))
        ->add('jobSuche', HiddenType::class, array('data' => "biete"))
        ->add('jobort', TextType::class, array('label' => 'Arbeitsort (z.B. Homeoffice oder Berlin):',))
        ->add('jobTyp', ChoiceType::class, array(
            'label' => 'Aufgabenbereich:',
            'choices' => array(
                    'Grafiker|in' => 'Grafiker',
                    'Druckerei/Vorstufe/Weiterverarbeitung' => 'Druckerei',
                    'Fotograf|in' => 'Fotograf',
                    'Entwickler|in' => 'Entwickler',
                    'Texter|in' => 'Texter',
                    'Illustrator|in' => 'Illustrator',
                    'Sonstige' => 'Sonstige',
            ),
        ))
        ->add('for', ChoiceType::class, array(
            'label' => 'Jobart:',
            'choices' => array(
                'Für ein Projekt/Auftrag' => 'Projekt_Auftrag',
                'Für ein Teilprojekt/Microjob' => 'Teilprojekt_Microjob',
                'zur Festanstellung' => 'Festeinstellung',
                'Für einen Ausbildungsplatz' => 'Ausbildungsplatz',
                'Für eine Praktikumsstelle' => 'Praktikumsstelle',
                'Für Illustrator|in' => 'Illustrator',
                'ohne Angabe' => 'ohne',
            ),
        ))
        ->add('enddate', TextType::class, array('label' => 'Ablaufdatum','attr' => ['class' => 'js-datepicker'], 'label_attr' => array('class' => 'date_entreelabel')))
        
        ->add('content', CKEditorType::class, array('label' => 'Beschreibung / Vergabekriterien:',
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))
        ->add('docpdf1', FileType::class, array('label' => 'Dateianhang (PDF, PNG, JPG, Zip file)','required' => false))
        ->add('docpdf2', FileType::class, array('label' => 'Dateianhang (PDF, PNG, JPG, Zip file)','required' => false))
        ->add('docpdf3', FileType::class, array('label' => 'Dateianhang (PDF, PNG, JPG, Zip file)','required' => false))
        ->add('agb',CheckboxType::class,array('attr' => array('class' => 'form-control'), 'label' => 'Ich akzeptiere die #AGB_LINK# / #DATENSCHUTZ_LINK# von Grafiker.de',
                  'required' => true))
        ->add('widerrufsbelehrung',CheckboxType::class,array('attr' => array('class' => 'form-control'), 'label' => 'Ich verlange ausdrücklich und stimme gleichzeitig zu, dass Sie mit der in Auftrag gegebenen Dienstleistung vor Ablauf der Widerrufsfrist beginnen. Ich weiß, dass mein #WIDERRUF_LINK# bei vollständiger Erfüllung des Vertrages erlischt.',
                   'required' => true))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary'),'label' => 'JETZT FÜR GESAMTPREIS € 195,00 (inkl. MwSt.) BESTELLEN'));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
