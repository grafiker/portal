<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class ArticleCropFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($extarray);
        $builder
        ->add('x1', HiddenType::class, array(
            'attr' => array('id' => 'x1'),
            'data' => '0',
            'required' => false,
        ))
        ->add('y1', HiddenType::class, array(
            'attr' => array('id' => 'y1'),
            'data' => '0',
            'required' => false,
        ))
        ->add('x2', HiddenType::class, array(
            'attr' => array('id' => 'x2'),
            'data' => '181',
            'required' => false,
        ))
        ->add('y2', HiddenType::class, array(
            'attr' => array('id' => 'y2'),
            'data' => '168',
            'required' => false,
        ))
        ->add('w', HiddenType::class, array(
            'attr' => array('id' => 'w'),
            'data' => '181',
            'required' => false,
        ))
        ->add('h', HiddenType::class, array(
            'attr' => array('id' => 'h'),
            'data' => '168',
            'required' => false,
        ))
        ->add('rechte',CheckboxType::class,
            array('label' => 'Ich bin Rechteinhaber an dem Bild / den Bildern ',
            'required' => true))
        
        ->add('bewerten',CheckboxType::class,
            array('label' => 'Ich erlaube Bewertungen. ',
                'required' => false))

        ->add('isads',CheckboxType::class,
            array('label' => 'Mein Beitrag enthält werbliche Inhalte. ',
                    'required' => false));
        
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
