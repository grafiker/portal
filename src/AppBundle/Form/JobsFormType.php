<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Jobs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class JobsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $extarray)
    {
        //var_dump(date('Y'));
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = date('Y'); $i <= date('Y')+5; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('service1',CheckboxType::class,array('label' => 'Logo Design', 'label_attr' => array('class' => 'service1'),'required' => false))
        ->add('service2',CheckboxType::class,array('label' => 'Grafikdesign', 'label_attr' => array('class' => 'service2'),'required' => false))
        ->add('service3',CheckboxType::class,array('label' => 'Text Erstellung', 'label_attr' => array('class' => 'service3'),'required' => false))
        ->add('service4',CheckboxType::class,array('label' => 'Sonstiges', 'label_attr' => array('class' => 'service4'),'required' => false))
        ->add('astitle', TextType::class, array('label' => 'für (mein Projektname)*:', 'label_attr' => array('class' => 'astitle')))
        ->add('date_entree', TextType::class, array('label' => 'Ablaufdatum','attr' => ['class' => 'js-datepicker'], 'label_attr' => array('class' => 'date_entreelabel','style' => 'display:none')))
        ->add('time_entree', TextType::class, array('label' => 'Ablaufzeit','attr' => ['class' => 'timepicker'], 'label_attr' => array('class' => 'time_entreelabel','style' => 'display:none')))
        ->add('offertype', ChoiceType::class, array('label' => 'Angebotstyp:', 'label_attr' => array('class' => 'offertype','style' => 'display:none'), 'attr' => array('onchange' => 'typchange()'), 'choices' => array('Ein Angebot' => '1','Das fertige Design/Produkt zum Festpreis von' => '2',),'expanded'=>true))
        ->add('price', TextType::class, array('label' => 'Preis', 'required' => false,'label_attr' => array('style' => 'display:none')))
        ->add('content', TextareaType::class, array('label' => 'Beschreibung:', 'label_attr' => array('class' => 'content')))
        ->add('docpdf1', FileType::class, array('label' => 'Dateianhang:','required' => false, 'label_attr' => array('class' => 'pdf1')))
        ->add('docpdf2', FileType::class, array('label' => 'Dateianhang','required' => false, 'label_attr' => array('class' => 'pdf2','style' => 'display:none')))
        ->add('docpdf3', FileType::class, array('label' => 'Dateianhang','required' => false, 'label_attr' => array('class' => 'pdf3','style' => 'display:none')))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary'),'label' => 'weiter'));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
