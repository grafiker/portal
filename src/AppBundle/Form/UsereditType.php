<?php

namespace App\AppBundle\Form;

use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UsereditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $land = array("Deutschland","Schweiz","Österreich","------------------------","Aegypten","Aequatorial-Guinea","Aethiopien","Afghanistan","Albanien","Algerien","Andorra","Angola","Antigua und Barbuda","Argentinien","Armenien","Aserbaidschan","Australien","Bahamas","Bahrain","Bangladesch","Barbados","Belarus","Belgien","Belize","Benin","Bhutan","Bolivien","Bosnien-Herzegowina","Botswana","Brasilien","Brunei","Bulgarien","Burkina Faso","Burundi","Chile","China","Costa Rica","Cote d Ivoire","Daenemark","Deutschland","Djibouti","Dominika","Dominikanische Republik","Ecuador","El Salvador","Eritrea","Estland","Fidschi","Finnland","Frankreich","Gabun","Gambia","Georgien","Ghana","Grenada","Griechenland","Grossbritannien","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Indien","Indonesien","Irak","Iran","Irland","Island","Israel","Italien","Jamaika","Japan","Jemen","Jordanien","Kambodscha","Kamerun","Kanada","Kap Verde","Kasachstan","Katar","Kenia","Kirgistan","Kiribati","Kolumbien","Komoren","Kongo","Kongo, Dem. Republik","Kroatien","Kuba","Kuwait","Laos","Lesotho","Lettland","Libanon","Liberia","Libyen","Liechtenstein","Litauen","Luxemburg","Madagaskar","Malawi","Malaysia","Malediven","Mali","Malta","Marokko","Marschall-Inseln","Mauretanien","Mauritius","Mazedonien","Mexiko","Mikronesien","Moldawien","Monaco","Mongolei","Montenegro","Mosambik","Myanmar","Namibia","Nepal","Neuseeland","Nicaragua","Niederlande","Niger","Nigeria","Nordkorea","Norwegen","Oesterreich","Oman","Osttimor","Pakistan","Palau","Panama","Papua-Neuguinea","Paraguay","Peru","Philippinen","Polen","Portugal","Ruanda","Rumaenien","Russland","Saint Kitts und Nevis","Saint Lucia","Saint Vincent","Salomonen","Sambia","Samoa","San Marino","Sao Tome","Saudi-Arabien","Schweden","Schweiz","Senegal","Serbien","Seychellen","Sierra Leone","Simbabwe","Singapur","Slowakei","Slowenien","Somalia","Spanien","Sri Lanka","Sudan","Suedafrika","Suedkorea","Surinam","Swasiland","Syrien","Tadschikistan","Taiwan","Tansania","Thailand","Togo","Tonga","Trinidad und Tobago","Tschad","Tschechien","Tuerkei","Tunesien","Turkmenistan","Tuvalu","Uganda","Ukraine","Ungarn","Uruguay","USA","Usbekistan","Vanuatu","Vatikanstadt","Venezuela","V. A. Emirate","Vietnam","Zentralafrika, Republik","Zypern");
        $landnew=array();
        foreach($land as $land) {
            $landnew[$land] = $land;
        }
        $builder
        ->add('username', null, array('label' => 'Benutzername'))
        ->add('email', null, array('label' => 'Email'))
        ->add('anrede', ChoiceType::class, array(
            'choices' => array(
                'Anrede*' => array(
                    'Herr' => 'Herr',
                    'Frau' => 'Frau',
                )
            ),
        ))
        ->add('firmenname', null, array('label' => 'Firmenname',
        'required' => false))
        ->add('ustid', null, array('label' => 'UStId',
        'required' => false))
        ->add('strassenummer', null, array('label' => 'Strasse, Hausnummer',
        'required' => false))
        ->add('plz', null, array('label' => 'Postleitzahl',
        'required' => false))
        ->add('wohnort', null, array('label' => 'Wohnort',
        'required' => false))

        ->add('land', ChoiceType::class, array(
            'label' => 'Land',
            'required' => false,
            'choices' => array(
                'Land' => $landnew
            ),
        ))

        ->add('firstName', null, array('label' => 'Vorname*'))
        ->add('lastName', null, array('label' => 'Nachname*'))
        ->add('telefon', null, array('label' => 'Telefon',
        'required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
