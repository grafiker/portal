<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Pdf;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class PdfrechnungFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $extarray)
    {
        //var_dump($extarray);
        $builder
        ->add('rechnungsnummer', TextType::class, array('label' => 'Angebots / Rechnungs-/Angebotsnummer: ',))
        ->add('retitle', TextType::class, array('label' => 'Titel / Produktbezeichnung: ',))
        ->add('einzelpreis', TextType::class, array('label' => 'Einzelpreis: ',))
        ->add('menge', TextType::class, array('label' => 'Menge: ',))
        ->add('angebotshinweis', TextType::class, array('label' => 'Angebotshinweis: ',))
        ->add('reart',ChoiceType::class,
            array('label' => 'Art:','choices' => array(
                    'Rechnung' => 1,
                    'Angebot' => 2),
            'multiple'=>false,'expanded'=>true));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
