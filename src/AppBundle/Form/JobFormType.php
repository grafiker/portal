<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class JobFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $extarray)
    {
        //var_dump(date('Y'));
        $days = array();

        for($i = 1; $i <= 31; $i++) {
        $days[$i] = $i;
        }
        $years = array();

        for($i = date('Y'); $i <= date('Y')+5; $i++) {
        $years[$i] = $i;
        }
        $builder
        ->add('astitle', TextType::class, array('label' => 'Ausschreibungstitel (z.B. Foto freistellen, Kommunikationsdesigner Digital & Print (w/m)):',))
        ->add('jobSuche', ChoiceType::class, array(
            'label' => 'Biete / Suche:','data' => $extarray['data']['form']['jobSuche'],
            'choices' => array(
                'Ich biete einen Job für' => 'biete',
                'Ich suche einen Job als' => 'suche',
            ),
        ))
        ->add('jobort', TextType::class, array('label' => 'Arbeitsort (z.B. Homeoffice oder Berlin):',))
        ->add('jobTyp', ChoiceType::class, array(
            'label' => 'Aufgabenbereich:','data' => $extarray['data']['form']['jobTyp'],
            'choices' => array(
                    'Grafiker|in' => 'Grafiker',
                    'Druckerei/Vorstufe/Weiterverarbeitung' => 'Druckerei',
                    'Fotograf|in' => 'Fotograf',
                    'Entwickler|in' => 'Entwickler',
                    'Texter|in' => 'Texter',
                    'Illustrator|in' => 'Illustrator',
                    'Sonstige' => 'Sonstige',
            ),
        ))
        ->add('for', ChoiceType::class, array(
            'label' => 'Jobart:',
            'choices' => array(
                'Für ein Projekt/Auftrag' => 'Projekt_Auftrag',
                'Für ein Teilprojekt/Microjob' => 'Teilprojekt_Microjob',
                'zur Festanstellung' => 'Festeinstellung',
                'Für einen Ausbildungsplatz' => 'Ausbildungsplatz',
                'Für eine Praktikumsstelle' => 'Praktikumsstelle',
                'Für Illustrator|in' => 'Illustrator',
                'ohne Angabe' => 'ohne',
            ),
        ))
        ->add('endDateDay', ChoiceType::class, array(
            'label' => 'Tag: ','data' => date('j',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Tag' => $days
            ),
        ))

        ->add('endDateMonat', ChoiceType::class, array(
            'label' => 'Monat: ','data' => date('n',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Monat' => array(
                    'Januar' => 1,
                    'Februar' => 2,
                    'März' => 3,
                    'April' => 4,
                    'Mai' => 5,
                    'Juni' => 6,
                    'Juli' => 7,
                    'August' => 8,
                    'September' => 9,
                    'Oktober' => 10,
                    'November' => 11,
                    'Dezember' => 12,
                )
            ),
        ))

        ->add('endDateYear', ChoiceType::class, array(
            'label' => 'Jahr: ','data' => date('Y',strtotime("+1 month")),
            'required' => false,
            'choices' => array(
                'Jahr' => $years
            ),
        ))
        
        ->add('content', CKEditorType::class, array('label' => 'Beschreibung / Vergabekriterien:',
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
            ),
        ))
        ->add('docpdf1', FileType::class, array('label' => 'Dateianhang (PDF file)','required' => false))
        ->add('docpdf2', FileType::class, array('label' => 'Dateianhang (PDF file)','required' => false))
        ->add('docpdf3', FileType::class, array('label' => 'Dateianhang (PDF file)','required' => false))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'Job einstellen'));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
