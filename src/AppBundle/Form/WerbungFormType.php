<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Routing\Annotation\Route;
class WerbungFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $task)
    {
        //var_dump($extarray);
        $builder
        ->add('position',ChoiceType::class,
        array('label' => 'Position:','choices' => array(
                'Über der Seite' => 'oben',
                'Rechts neben der Seite' => 'rechts',
                'Banner für Mobile Geräte' => 'mobil',
                'Partnerslider' => 'partner'
            ),
        'multiple'=>false,'expanded'=>true))

        ->add('sponsorname', TextType::class, array('label' => 'Sponsorname / Titel: '))
        ->add('bannerlinkView', TextType::class, array('label' => 'Banner-Viewlink: ', 'required' => false))
        ->add('bannerlinkClick', TextType::class, array('label' => 'Banner-Klicklink: ', 'required' => false))
        ->add('alttag', TextType::class, array('label' => 'Alttag: ', 'required' => false))
        ->add('target', ChoiceType::class, array(
            'label' => 'Seite öffnen im: ',
            'choices' => array(
                'neuen Fenster' => '_blank',
                'selben Fenster' => '_self',
            ),
        ))
        ->add('quelltext', TextareaType::class, array('label' => 'Quelltext: ', 'required' => false))
        ->add('reload', TextType::class, array('label' => 'Reloadsperre in Minuten: ', 'required' => false))
        ->add('maxKlicks', TextType::class, array('label' => 'Anzahl gebuchte Klicks: ', 'required' => false))
        ->add('maxViews', TextType::class, array('label' => 'Anzahl gebuchte Views: ', 'required' => false))
        ->add('send', SubmitType::class, array('attr' => array('class' => 'bdnmini btn btn-primary pull-left'),'label' => 'absenden'));
    }

   
    public function getBlockPrefix()
    {
        return 'form';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
