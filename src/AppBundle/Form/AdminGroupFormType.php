<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\AppBundle\Form;
use App\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminGroupFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, $user)
    {
        $builder
        ->add('roles', ChoiceType::class, array(
            'label' => 'Rechte setzen:',
            'choices' => array(
                    'ADMIN' => 'ROLE_ADMIN',
                    'FREE' => 'ROLE_USER',
                    'PROFI' => 'ROLE_PROFI',
                    'PLATIN' => 'ROLE_PLATIN',
                    
            ),'multiple'=>true,'expanded'=>true, 'required' => false,
        ))

                    ->add('send', SubmitType::class, array('attr' => array('class' => 'pull-left'),'label' => 'Rechte setzen...'));      
    }

   
    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }    
}
