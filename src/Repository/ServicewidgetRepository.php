<?php

namespace App\Repository;

use App\Entity\Servicewidget;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Servicewidget|null find($id, $lockMode = null, $lockVersion = null)
 * @method Servicewidget|null findOneBy(array $criteria, array $orderBy = null)
 * @method Servicewidget[]    findAll()
 * @method Servicewidget[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServicewidgetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Servicewidget::class);
    }

    // /**
    //  * @return Servicewidget[] Returns an array of Servicewidget objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Servicewidget
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
