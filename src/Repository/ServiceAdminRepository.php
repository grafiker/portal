<?php

namespace App\Repository;

use App\Entity\ServiceAdmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ServiceAdmin|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceAdmin|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceAdmin[]    findAll()
 * @method ServiceAdmin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceAdminRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServiceAdmin::class);
    }

    // /**
    //  * @return ServiceAdmin[] Returns an array of ServiceAdmin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ServiceAdmin
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
