<?php
// src/App/Document/User.php

namespace App\Document;

use Sonata\UserBundle\Document\BaseUser as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use FOS\MessageBundle\Model\ParticipantInterface;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @MongoDB\Document
 */
class User extends BaseUser implements ParticipantInterface, UserInterface, EncoderAwareInterface, \Serializable
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $newMail;
    public function getNewMail()
    {
        return $this->newMail;
    }
    public function setNewMail($newMail)
    {
        $this->newMail = $newMail;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $email;
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $searching;
    public function getSearching()
    {
        return $this->searching;
    }
    public function setSearching($searching)
    {
        $this->searching = $searching;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $beschreibung;
    public function getBeschreibung()
    {
        return $this->beschreibung;
    }
    public function setBeschreibung($beschreibung)
    {
        $this->beschreibung = $beschreibung;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $anrede;
    public function getAnrede()
    {
        return $this->anrede;
    }
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firma;
    public function getFirma()
    {
        return $this->firma;
    }
    public function setFirma($firma)
    {
        $this->firma = $firma;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $telefon;
    public function getTelefon()
    {
        return $this->telefon;
    }
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $mobil;
    public function getMobil()
    {
        return $this->mobil;
    }
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $titel;
    public function getTitel()
    {
        return $this->titel;
    }
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $berufsbezeichnung;
    public function getBerufsbezeichnung()
    {
        return $this->berufsbezeichnung;
    }
    public function setBerufsbezeichnung($berufsbezeichnung)
    {
        $this->berufsbezeichnung = $berufsbezeichnung;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $plz;
    public function getPlz()
    {
        return $this->plz;
    }
    public function setPlz($plz)
    {
        $this->plz = $plz;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $wohnort;
    public function getWohnort()
    {
        return $this->wohnort;
    }
    public function setWohnort($wohnort)
    {
        $this->wohnort = $wohnort;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $land;
    public function getLand()
    {
        return $this->land;
    }
    public function setLand($land)
    {
        $this->land = $land;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $strassenummer;
    public function getStrassenummer()
    {
        return $this->strassenummer;
    }
    public function setStrassenummer($strassenummer)
    {
        $this->strassenummer = $strassenummer;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firstName;

    public function getFirstName()
    {
        return $this->firstName;
    }
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $lastName;

    public function getLastName()
    {
        return $this->lastName;
    }
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda1;

    public function getWarumda1()
    {
        return $this->warumda1;
    }
    public function setWarumda1($warumda1)
    {
        $this->warumda1 = $warumda1;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda2;

    public function getWarumda2()
    {
        return $this->warumda2;
    }
    public function setWarumda2($warumda2)
    {
        $this->warumda2 = $warumda2;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda3;

    public function getWarumda3()
    {
        return $this->warumda3;
    }
    public function setWarumda3($warumda3)
    {
        $this->warumda3 = $warumda3;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda4;

    public function getWarumda4()
    {
        return $this->warumda4;
    }
    public function setWarumda4($warumda4)
    {
        $this->warumda4 = $warumda4;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda5;

    public function getWarumda5()
    {
        return $this->warumda5;
    }
    public function setWarumda5($warumda5)
    {
        $this->warumda5 = $warumda5;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $warumda6;

    public function getWarumda6()
    {
        return $this->warumda6;
    }
    public function setWarumda6($warumda6)
    {
        $this->warumda6 = $warumda6;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe1;

    public function getBerufsgruppe1()
    {
        return $this->berufsgruppe1;
    }
    public function setBerufsgruppe1($berufsgruppe1)
    {
        $this->berufsgruppe1 = $berufsgruppe1;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe2;

    public function getBerufsgruppe2()
    {
        return $this->berufsgruppe2;
    }
    public function setBerufsgruppe2($berufsgruppe2)
    {
        $this->berufsgruppe2 = $berufsgruppe2;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe3;

    public function getBerufsgruppe3()
    {
        return $this->berufsgruppe3;
    }
    public function setBerufsgruppe3($berufsgruppe3)
    {
        $this->berufsgruppe3 = $berufsgruppe3;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe4;

    public function getBerufsgruppe4()
    {
        return $this->berufsgruppe4;
    }
    public function setBerufsgruppe4($berufsgruppe4)
    {
        $this->berufsgruppe4 = $berufsgruppe4;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe5;

    public function getBerufsgruppe5()
    {
        return $this->berufsgruppe5;
    }
    public function setBerufsgruppe5($berufsgruppe5)
    {
        $this->berufsgruppe5 = $berufsgruppe5;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe6;

    public function getBerufsgruppe6()
    {
        return $this->berufsgruppe6;
    }
    public function setBerufsgruppe6($berufsgruppe6)
    {
        $this->berufsgruppe6 = $berufsgruppe6;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe7;

    public function getBerufsgruppe7()
    {
        return $this->berufsgruppe7;
    }
    public function setBerufsgruppe7($berufsgruppe7)
    {
        $this->berufsgruppe7 = $berufsgruppe7;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe8;

    public function getBerufsgruppe8()
    {
        return $this->berufsgruppe8;
    }
    public function setBerufsgruppe8($berufsgruppe8)
    {
        $this->berufsgruppe8 = $berufsgruppe8;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $berufsgruppe9;

    public function getBerufsgruppe9()
    {
        return $this->berufsgruppe9;
    }
    public function setBerufsgruppe9($berufsgruppe9)
    {
        $this->berufsgruppe9 = $berufsgruppe9;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $gebDay;

    public function getGebDay()
    {
        return $this->gebDay;
    }
    public function setGebDay($gebDay)
    {
        $this->gebDay = $gebDay;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $gebMon;

    public function getGebMon()
    {
        return $this->gebMon;
    }
    public function setGebMon($gebMon)
    {
        $this->gebMon = $gebMon;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $gebYear;

    public function getGebYear()
    {
        return $this->gebYear;
    }
    public function setGebYear($gebYear)
    {
        $this->gebYear = $gebYear;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $fastbill;

    public function getFastbill()
    {
        return $this->fastbill;
    }
    public function setFastbill($fastbill)
    {
        $this->fastbill = $fastbill;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $agb;
    public function getAgb()
    {
        return $this->agb;
    }
    public function setAgb($agb)
    {
        $this->agb = $agb;
    }

    /**
     * @MongoDB\Field(type="int")
     */
    protected $datenschutz;
    public function getDatenschutz()
    {
        return $this->datenschutz;
    }
    public function setDatenschutz($datenschutz)
    {
        $this->datenschutz = $datenschutz;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $sendblueid;
    public function getSendBlueID()
    {
        return $this->sendblueid;
    }
    public function setSendBlueID($sendblueid)
    {
        $this->sendblueid = $sendblueid;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $aspeak;
    public function getAspeak()
    {
        return $this->aspeak;
    }
    public function setAspeak($aspeak)
    {
        $this->aspeak = $aspeak;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $website;
    public function getWebsite()
    {
        return $this->website;
    }
    public function setWebsite($website)
    {
        $this->website = $website;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $stundenpreis;
    public function getStundenpreis()
    {
        return $this->stundenpreis;
    }
    public function setStundenpreis($stundenpreis)
    {
        $this->stundenpreis = $stundenpreis;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $chinesisch;
    public function getChinesisch()
    {
        return $this->chinesisch;
    }
    public function setChinesisch($chinesisch)
    {
        $this->chinesisch = $chinesisch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $deutsch;
    public function getDeutsch()
    {
        return $this->deutsch;
    }
    public function setDeutsch($deutsch)
    {
        $this->deutsch = $deutsch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $englisch;
    public function getEnglisch()
    {
        return $this->englisch;
    }
    public function setEnglisch($englisch)
    {
        $this->englisch = $englisch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $gast;
    public function getGast()
    {
        return $this->gast;
    }
    public function setGast($gast)
    {
        $this->gast = $gast;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $franzoesisch;
    public function getFranzoesisch()
    {
        return $this->franzoesisch;
    }
    public function setFranzoesisch($franzoesisch)
    {
        $this->franzoesisch = $franzoesisch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $italienisch;
    public function getItalienisch()
    {
        return $this->italienisch;
    }
    public function setItalienisch($italienisch)
    {
        $this->italienisch = $italienisch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $russisch;
    public function getRussisch()
    {
        return $this->russisch;
    }
    public function setRussisch($russisch)
    {
        $this->russisch = $russisch;
    }

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $spanisch;
    public function getSpanisch()
    {
        return $this->spanisch;
    }
    public function setSpanisch($spanisch)
    {
        $this->spanisch = $spanisch;
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected $useraccount;
    public function getUseraccount()
    {
        return $this->useraccount;
    }
    public function setUseraccount($useraccount)
    {
        $this->useraccount = $useraccount;
    }
    /**
     * @MongoDB\Field(type="hash")
     */
    protected $skills;
    public function getSkills()
    {
        return $this->skills;
    }
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $skillsCloud;
    public function getSkillsCloud()
    {
        return $this->skillsCloud;
    }
    public function setSkillsCloud($skillsCloud)
    {
        $this->skillsCloud = $skillsCloud;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $employ;
    public function getEmploy()
    {
        return $this->employ;
    }
    public function setEmploy($employ)
    {
        $this->employ = $employ;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $employment;
    public function getEmployment()
    {
        return $this->employment;
    }
    public function setEmployment($employment)
    {
        $this->employment = $employment;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $capacity;
    public function getCapacity()
    {
        return $this->capacity;
    }
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $notifications;
    public function getNotifications()
    {
        return $this->notifications;
    }
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $notificationsMail;
    public function getNotificationsMail()
    {
        return $this->notificationsMail;
    }
    public function setNotificationsMail($notificationsMail)
    {
        $this->notificationsMail = $notificationsMail;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $newsletter;
    public function getNewsletter()
    {
        return $this->newsletter;
    }
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $newsletterMail;
    public function getNewsletterMail()
    {
        return $this->newsletterMail;
    }
    public function setNewsletterMail($newsletterMail)
    {
        $this->newsletterMail = $newsletterMail;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $userpic;
    public function getUserpic()
    {
        return $this->userpic;
    }
    public function setUserpic($userpic)
    {
        $this->userpic = $userpic;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firmapic;
    public function getFirmapic()
    {
        return $this->firmapic;
    }
    public function setFirmapic($firmapic)
    {
        $this->firmapic = $firmapic;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firmenname;
    public function getFirmenname()
    {
        return $this->firmenname;
    }
    public function setFirmenname($firmenname)
    {
        $this->firmenname = $firmenname;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $ustid;
    public function getUstid()
    {
        return $this->ustid;
    }
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $firstLogin;
    public function getFirstLogin()
    {
        return $this->firstLogin;
    }
    public function setFirstLogin($firstLogin)
    {
        $this->firstLogin = $firstLogin;
    }
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $enabled;
    public function getEnabled()
    {
        return $this->enabled;
    }
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $newsletterempfang;
    public function getNewsletterEmpfang()
    {
        return $this->newsletterempfang;
    }
    public function setNewsletterEmpfang($newsletterempfang)
    {
        $this->newsletterempfang = $newsletterempfang;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $oldPassword;
    public function getOldPassword()
    {
        return $this->oldPassword;
    }
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }
    
    public function hasLegacyPassword()
    {
        return null !== $this->oldPassword;
    }

    /**
     * {@inheritDoc}
     */
    public function getEncoderName()
    {
        if ($this->hasLegacyPassword()) {
            // User is configured with a legacy password, make use of the legacy encoder
            // configured in security.yml
            return 'legacy_encoder';
        }

        // User is configured with the default password system, make use of the default encoder
        return null;
    }

    protected $password;

    public function getPassword()
    {
        return null === $this->password ? $this->oldPassword : $this->password;
    }
}