<?php
// src/Entity/Projektrechnerkategorien.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Projektrechnerkategorien
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $katid;
    public function getKatid()
    {
        return $this->katid;
    }
    public function setKatid($katid)
    {
        $this->katid = $katid;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $dauer;
    public function getDauer()
    {
        return $this->dauer;
    }
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $katname;
    public function getKatname()
    {
        return $this->katname;
    }
    public function setKatname($katname)
    {
        $this->katname = $katname;
    }
}