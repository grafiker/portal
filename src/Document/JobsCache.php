<?php
// src/Entity/JobsCache.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class JobsCache
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $timestamp;
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $ipadresse;
    public function getIpadresse()
    {
        return $this->ipadresse;
    }
    public function setIpadresse($ipadresse)
    {
        $this->ipadresse = $ipadresse;
    }
    /**
     * @MongoDB\Field(type="hash")
     */
    protected $datacache;
    public function getDatacache()
    {
        return $this->datacache;
    }
    public function setDatacache($datacache)
    {
        $this->datacache = $datacache;
    }
}