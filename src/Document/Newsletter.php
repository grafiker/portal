<?php
// src/App/Document/Banner.php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Newsletter
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getId()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $lettertyp;
    public function getLettertyp()
    {
        return $this->lettertyp;
    }
    public function setLettertyp($lettertyp)
    {
        $this->lettertyp = $lettertyp;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $letterDay;
    public function getLetterDay()
    {
        return $this->letterDay;
    }
    public function setLetterDay($letterDay)
    {
        $this->letterDay = $letterDay;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $letterMon;
    public function getLetterMon()
    {
        return $this->letterMon;
    }
    public function setLetterMon($letterMon)
    {
        $this->letterMon = $letterMon;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $letterYear;
    public function getLetterYear()
    {
        return $this->letterYear;
    }
    public function setLetterYear($letterYear)
    {
        $this->letterYear = $letterYear;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $mailertext;
    public function getMailertext()
    {
        return $this->mailertext;
    }
    public function setMailertext($mailertext)
    {
        $this->mailertext = $mailertext;
    }
}