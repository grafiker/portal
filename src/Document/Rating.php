<?php
// src/Entity/Skills.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Rating
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $pinid;
    public function getPinid()
    {
        return $this->pinid;
    }
    public function setPinid($pinid)
    {
        $this->pinid = $pinid;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r05;
    public function getR05()
    {
        return $this->r05;
    }
    public function setR05($r05)
    {
        $this->r05 = $r05;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r10;
    public function getR10()
    {
        return $this->r10;
    }
    public function setR10($r10)
    {
        $this->r10 = $r10;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r15;
    public function getR15()
    {
        return $this->r15;
    }
    public function setR15($r15)
    {
        $this->r15 = $r15;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r20;
    public function getR20()
    {
        return $this->r20;
    }
    public function setR20($r20)
    {
        $this->r20 = $r20;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r25;
    public function getR25()
    {
        return $this->r25;
    }
    public function setR25($r25)
    {
        $this->r25 = $r25;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r30;
    public function getR30()
    {
        return $this->r30;
    }
    public function setR30($r30)
    {
        $this->r30 = $r30;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r35;
    public function getR35()
    {
        return $this->r35;
    }
    public function setR35($r35)
    {
        $this->r35 = $r35;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r40;
    public function getR40()
    {
        return $this->r40;
    }
    public function setR40($r40)
    {
        $this->r40 = $r40;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r45;
    public function getR45()
    {
        return $this->r45;
    }
    public function setR45($r45)
    {
        $this->r45 = $r45;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $r50;
    public function getR50()
    {
        return $this->r50;
    }
    public function setR50($r50)
    {
        $this->r50 = $r50;
    }
}
