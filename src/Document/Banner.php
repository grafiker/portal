<?php
// src/App/Document/Banner.php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Banner
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getId()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $position;
    public function getPosition()
    {
        return $this->position;
    }
    public function setPosition($position)
    {
        $this->position = $position;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $sponsorname;
    public function getSponsorname()
    {
        return $this->sponsorname;
    }
    public function setSponsorname($sponsorname)
    {
        $this->sponsorname = $sponsorname;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $alttag;
    public function getAlttag()
    {
        return $this->alttag;
    }
    public function setAlttag($alttag)
    {
        $this->alttag = $alttag;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $bannerlinkClick;
    public function getBannerlinkClick()
    {
        return $this->bannerlinkClick;
    }
    public function setBannerlinkClick($bannerlinkClick)
    {
        $this->bannerlinkClick = $bannerlinkClick;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $bannerlinkView;
    public function getBannerlinkView()
    {
        return $this->bannerlinkView;
    }
    public function setBannerlinkView($bannerlinkView)
    {
        $this->bannerlinkView = $bannerlinkView;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $clicks;
    public function getClicks()
    {
        return $this->clicks;
    }
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $views;
    public function getViews()
    {
        return $this->views;
    }
    public function setViews($views)
    {
        $this->views = $views;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $reload;
    public function getReload()
    {
        return $this->reload;
    }
    public function setReload($reload)
    {
        $this->reload = $reload;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $quelltext;
    public function getQuelltext()
    {
        return $this->quelltext;
    }
    public function setQuelltext($quelltext)
    {
        $this->quelltext = $quelltext;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $maxKlicks;
    public function getMaxKlicks()
    {
        return $this->maxKlicks;
    }
    public function setMaxKlicks($maxKlicks)
    {
        $this->maxKlicks = $maxKlicks;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $maxViews;
    public function getMaxViews()
    {
        return $this->maxViews;
    }
    public function setMaxViews($maxViews)
    {
        $this->maxViews = $maxViews;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $target;
    public function getTarget()
    {
        return $this->target;
    }
    public function setTarget($target)
    {
        $this->target = $target;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $reloadViews;
    public function getReloadViews()
    {
        return $this->reloadViews;
    }
    public function setReloadViews($reloadViews)
    {
        $this->reloadViews = $reloadViews;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $reloadClicks;
    public function getReloadClicks()
    {
        return $this->reloadClicks;
    }
    public function setReloadClicks($reloadClicks)
    {
        $this->reloadClicks = $reloadClicks;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $disenable;
    public function getDisenable()
    {
        return $this->disenable;
    }
    public function setDisenable($disenable)
    {
        $this->disenable = $disenable;
    }
}