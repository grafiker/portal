<?php
// src/Entity/Bewerbung.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Bewerbung
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $bewerbungDate;
    public function getBewerbungDate()
    {
        return $this->bewerbungDate;
    }
    public function setBewerbungDate($bewerbungDate)
    {
        $this->bewerbungDate = $bewerbungDate;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $jobID;
    public function getJobId()
    {
        return $this->jobID;
    }
    public function setJobId($jobID)
    {
        $this->jobID = $jobID;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $mailID;
    public function getMailId()
    {
        return $this->mailID;
    }
    public function setMailId($mailID)
    {
        $this->mailID = $mailID;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf;
    public function getDocpdf()
    {
        return $this->docpdf;
    }
    public function setDocpdf($docpdf)
    {
        $this->docpdf = $docpdf;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $joblink;
    public function getJobLink()
    {
        return $this->joblink;
    }
    public function setJobLink($joblink)
    {
        $this->joblink = $joblink;
    }
}