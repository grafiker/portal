<?php
// src/Entity/Article.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Article
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    public function getId()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;
    public function getTitle()
    {
        return $this->title;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $articlelink;
    public function getArticlelink()
    {
        return $this->articlelink;
    }
    public function setArticlelink($articlelink)
    {
        $this->articlelink = $articlelink;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $picselected;
    public function getPicselected()
    {
        return $this->picselected;
    }
    public function setPicselected($picselected)
    {
        $this->picselected = $picselected;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $hashtags;
    public function getHashTags()
    {
        return $this->hashtags;
    }
    public function setHashTags($hashtags)
    {
        $this->hashtags = $hashtags;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $astitle;
    public function getAstitle()
    {
        return $this->astitle;
    }
    public function setAstitle($astitle)
    {
        $this->astitle = $astitle;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $articlekategorie;
    public function getArticleKategorie()
    {
        return $this->articlekategorie;
    }
    public function setArticleKategorie($articlekategorie)
    {
        $this->articlekategorie = $articlekategorie;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $content;
    public function getContent()
    {
        return $this->content;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $kommentare;
    public function getKommentare()
    {
        return $this->kommentare;
    }
    public function setKommentare($kommentare)
    {
        $this->kommentare = $kommentare;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $bewerten;
    public function getBewerten()
    {
        return $this->bewerten;
    }
    public function setBewerten($bewerten)
    {
        $this->bewerten = $bewerten;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $rechte;
    public function getRechte()
    {
        return $this->rechte;
    }
    public function setRechte($rechte)
    {
        $this->rechte = $rechte;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $aktiv;
    public function getAktiv()
    {
        return $this->aktiv;
    }
    public function setAktiv($aktiv)
    {
        $this->aktiv = $aktiv;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $startdate;
    public function getStartdate()
    {
        return $this->startdate;
    }
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $picname;
    public function getPicname()
    {
        return $this->picname;
    }
    public function setPicname($picname)
    {
        $this->picname = $picname;
    }

    /**
     * @MongoDB\Field(type="int")
     */
    protected $x1;
    public function getX1()
    {
        return $this->x1;
    }
    public function setX1($x1)
    {
        $this->x1 = $x1;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $y1;
    public function getY1()
    {
        return $this->y1;
    }
    public function setY1($y1)
    {
        $this->y1 = $y1;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $x2;
    public function getX2()
    {
        return $this->x2;
    }
    public function setX2($x2)
    {
        $this->x2 = $x2;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $y2;
    public function getY2()
    {
        return $this->y2;
    }
    public function setY2($y2)
    {
        $this->y2 = $y2;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $w;
    public function getW()
    {
        return $this->w;
    }
    public function setW($w)
    {
        $this->w = $w;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $h;
    public function getH()
    {
        return $this->h;
    }
    public function setH($h)
    {
        $this->h = $h;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $thumbnail;
    public function getThumbnail()
    {
        return $this->thumbnail;
    }
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $wohnort;
    public function getWohnort()
    {
        return $this->wohnort;
    }
    public function setWohnort($wohnort)
    {
        $this->wohnort = $wohnort;
    }    
    /**
     * @MongoDB\Field(type="int")
     */
    protected $articleviews;
    public function getArticleviews()
    {
        return $this->articleviews;
    }
    public function setArticleviews($articleviews)
    {
        $this->articleviews = $articleviews;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $viewdate;
    public function getViewdate()
    {
        return $this->viewdate;
    }
    public function setViewdate($viewdate)
    {
        $this->viewdate = $viewdate;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $rating;
    public function getRating()
    {
        return $this->rating;
    }
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isads;
    public function getIsads()
    {
        return $this->isads;
    }
    public function setIsads($isads)
    {
        $this->isads = $isads;
    }     
}