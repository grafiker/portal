<?php
// src/App/Document/Infotext.php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Infotext
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getId()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $infotext;
    public function getInfotext()
    {
        return $this->infotext;
    }
    public function setInfotext($infotext)
    {
        $this->infotext = $infotext;
    }
}