<?php
// src/Entity/Job.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Jobsoffers
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $startdate;
    public function getStartdate()
    {
        return $this->startdate;
    }
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf1;
    public function getDocpdf1()
    {
        return $this->docpdf1;
    }
    public function setDocpdf1($docpdf1)
    {
        $this->docpdf1 = $docpdf1;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf2;
    public function getDocpdf2()
    {
        return $this->docpdf2;
    }
    public function setDocpdf2($docpdf2)
    {
        $this->docpdf2 = $docpdf2;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf1name;
    public function getDocpdf1name()
    {
        return $this->docpdf1name;
    }
    public function setDocpdf1name($docpdf1name)
    {
        $this->docpdf1name = $docpdf1name;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf2name;
    public function getDocpdf2name()
    {
        return $this->docpdf2name;
    }
    public function setDocpdf2name($docpdf2name)
    {
        $this->docpdf2name = $docpdf2name;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $doc2date;
    public function getDoc2date()
    {
        return $this->doc2date;
    }
    public function setDoc2date($doc2date)
    {
        $this->doc2date = $doc2date;
    }
    /**
     * @MongoDB\Field(type="hash")
     */
    protected $chatdata;
    public function getChatdata()
    {
        return $this->chatdata;
    }
    public function setChatdata($chatdata)
    {
        $this->chatdata = $chatdata;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $offerprice;
    public function getOfferprice()
    {
        return $this->offerprice;
    }
    public function setOfferprice($offerprice)
    {
        $this->offerprice = $offerprice;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $offerpriceout;
    public function getOfferpriceout()
    {
        return $this->offerpriceout;
    }
    public function setOfferpriceout($offerpriceout)
    {
        $this->offerpriceout = $offerpriceout;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $jobid;
    public function getJobid()
    {
        return $this->jobid;
    }
    public function setJobid($jobid)
    {
        $this->jobid = $jobid;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $jobokay;
    public function getJobokay()
    {
        return $this->jobokay;
    }
    public function setJobokay($jobokay)
    {
        $this->jobokay = $jobokay;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $storno;
    public function getStorno()
    {
        return $this->storno;
    }
    public function setStorno($storno)
    {
        $this->storno = $storno;
    }
}