<?php
// src/App/Document/User.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Webhook
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    /**
     * @MongoDB\Field(type="hash")
     */
    protected $webhook;
    public function getWebhook()
    {
        return $this->webhook;
    }
    public function setWebhook($webhook)
    {
        $this->webhook = $webhook;
    }

}