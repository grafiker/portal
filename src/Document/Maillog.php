<?php
// src/App/Document/Maillog.php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use FOS\MessageBundle\Document\Message as BaseMessage;

/**
 * @MongoDB\Document
 */
class Maillog extends BaseMessage
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $senddate;
    public function getSenddate()
    {
        return $this->senddate;
    }
    public function setSenddate($senddate)
    {
        $this->senddate = $senddate;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $sender;
    public function getSender()
    {
        return $this->sender;
    }
    public function setSender($sender)
    {
        $this->sender = $sender;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $empfaenger;
    public function getEmpfaenger()
    {
        return $this->empfaenger;
    }
    public function setEmpfaenger($empfaenger)
    {
        $this->empfaenger = $empfaenger;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $subjekt;
    public function getSubjekt()
    {
        return $this->subjekt;
    }
    public function setSubjekt($subjekt)
    {
        $this->subjekt = $subjekt;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $body;
    public function getBody()
    {
        return $this->body;
    }
    public function setBody($body)
    {
        $this->body = $body;
    }
}