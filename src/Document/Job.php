<?php
// src/Entity/Job.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Job
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $startdate;
    public function getStartdate()
    {
        return $this->startdate;
    }
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firmalogo;
    public function getFirmalogo()
    {
        return $this->firmalogo;
    }
    public function setFirmalogo($firmalogo)
    {
        $this->firmalogo = $firmalogo;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $firma;
    public function getFirma()
    {
        return $this->firma;
    }
    public function setFirma($firma)
    {
        $this->firma = $firma;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $astitle;
    public function getAstitle()
    {
        return $this->astitle;
    }
    public function setAstitle($astitle)
    {
        $this->astitle = $astitle;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $content;
    public function getContent()
    {
        return $this->content;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $jobTyp;
    public function getJobTyp()
    {
        return $this->jobTyp;
    }
    public function setJobTyp($jobTyp)
    {
        $this->jobTyp = $jobTyp;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $finde;
    public function getFinde()
    {
        return $this->finde;
    }
    public function setFinde($finde)
    {
        $this->finde = $finde;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $for;
    public function getFor()
    {
        return $this->for;
    }
    public function setFor($for)
    {
        $this->for = $for;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $docpdf;
    public function getDocpdf()
    {
        return $this->docpdf;
    }
    public function setDocpdf($docpdf)
    {
        $this->docpdf = $docpdf;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $enddate;
    public function getEnddate()
    {
        return $this->enddate;
    }
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    }
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $aktiv;
    public function getAktiv()
    {
        return $this->aktiv;
    }
    public function setAktiv($aktiv)
    {
        $this->aktiv = $aktiv;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $joblink;
    public function getJobLink()
    {
        return $this->joblink;
    }
    public function setJobLink($joblink)
    {
        $this->joblink = $joblink;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $jobort;
    public function getJobort()
    {
        return $this->jobort;
    }
    public function setJobort($jobort)
    {
        $this->jobort = $jobort;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $endDateDay;
    public function getEndDateDay()
    {
        return $this->endDateDay;
    }
    public function setEndDateDay($endDateDay)
    {
        $this->endDateDay = $endDateDay;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $endDateMonat;
    public function getEndDateMonat()
    {
        return $this->endDateMonat;
    }
    public function setEndDateMonat($endDateMonat)
    {
        $this->endDateMonat = $endDateMonat;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $endDateYear;
    public function getEndDateYear()
    {
        return $this->endDateYear;
    }
    public function setEndDateYear($endDateYear)
    {
        $this->endDateYear = $endDateYear;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $contactEmail;
    public function getContactEmail()
    {
        return $this->contactEmail;
    }
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $contactTel;
    public function getContactTel()
    {
        return $this->contactTel;
    }
    public function setContactTel($contactTel)
    {
        $this->contactTel = $contactTel;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $contactName;
    public function getContactName()
    {
        return $this->contactName;
    }
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }
/**
     * @MongoDB\Field(type="string")
     */
    protected $jobSuche;
    public function getJobSuche()
    {
        return $this->jobSuche;
    }
    public function setJobSuche($jobSuche)
    {
        $this->jobSuche = $jobSuche;
    }
    /*PAYOUTVARIABLEN*/
    /**
     * @MongoDB\Field(type="hash")
     */
    protected $payout;
    public function getPayout()
    {
        return $this->payout;
    }
    public function setPayout($payout)
    {
        $this->payout = $payout;
    }
}