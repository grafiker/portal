<?php
// src/Entity/Skills.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Ratingarticleblock
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $articleid;
    public function getArticleid()
    {
        return $this->articleid;
    }
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
}
