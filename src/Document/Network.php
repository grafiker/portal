<?php
// src/Entity/Network.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Network
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $startdate;
    public function getStartdate()
    {
        return $this->startdate;
    }
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $friend;
    public function getFriend()
    {
        return $this->friend;
    }
    public function setFriend($friend)
    {
        $this->friend = $friend;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $request;
    public function getRequest()
    {
        return $this->request;
    }
    public function setRequest($request)
    {
        $this->request = $request;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $reason;
    public function getReason()
    {
        return $this->reason;
    }
    public function setReason($reason)
    {
        $this->reason = $reason;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $acceptreason;
    public function getAcceptreason()
    {
        return $this->acceptreason;
    }
    public function setAcceptreason($acceptreason)
    {
        $this->acceptreason = $acceptreason;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $accepted;
    public function getAccepted()
    {
        return $this->accepted;
    }
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $blocked;
    public function getBlocked()
    {
        return $this->blocked;
    }
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }
}