<?php
// src/Entity/Projektrechnerkategorien.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Pinskategorien
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $katid;
    public function getKatid()
    {
        return $this->katid;
    }
    public function setKatid($katid)
    {
        $this->katid = $katid;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $dauer;
    public function getDauer()
    {
        return $this->dauer;
    }
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $pinkatname;
    public function getPinkatname()
    {
        return $this->pinkatname;
    }
    public function setPinkatname($pinkatname)
    {
        $this->pinkatname = $pinkatname;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $picdir;
    public function getPicdir()
    {
        return $this->picdir;
    }
    public function setPicdir($picdir)
    {
        $this->picdir = $picdir;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $rolegroup;
    public function getRoleGroup()
    {
        return $this->rolegroup;
    }
    public function setRoleGroup($rolegroup)
    {
        $this->rolegroup = $rolegroup;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $undergroup;
    public function getUndergroup()
    {
        return $this->undergroup;
    }
    public function setUndergroup($undergroup)
    {
        $this->undergroup = $undergroup;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $sort;
    public function getSort()
    {
        return $this->sort;
    }
    public function setSort($sort)
    {
        $this->sort = $sort;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $aktiv;
    public function getAktiv()
    {
        return $this->aktiv;
    }
    public function setAktiv($aktiv)
    {
        $this->aktiv = $aktiv;
    }
}