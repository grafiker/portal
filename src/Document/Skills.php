<?php
// src/Entity/Skills.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Skills
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $beruf;
    public function getBeruf()
    {
        return $this->beruf;
    }
    public function setBeruf($beruf)
    {
        $this->beruf = $beruf;
    }
    /**
     * @MongoDB\Field(type="collection")
     */
    protected $skillname;
    public function getSkillname()
    {
        return $this->skillname;
    }
    public function setSkillname($skillname)
    {
        $this->skillname = $skillname;
    }
}
