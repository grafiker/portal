<?php
// src/Entity/Projektrechner.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Projektrechner
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $steps;
    public function getSteps()
    {
        return $this->steps;
    }
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $stundensatz;
    public function getStundensatz()
    {
        return $this->stundensatz;
    }
    public function setStundensatz($stundensatz)
    {
        $this->stundensatz = $stundensatz;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $astitle;
    public function getAstitle()
    {
        return $this->astitle;
    }
    public function setAstitle($astitle)
    {
        $this->astitle = $astitle;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $prstart;
    public function getPrstart()
    {
        return $this->prstart;
    }
    public function setPrstart($prstart)
    {
        $this->prstart = $prstart;
    }
        /**
     * @MongoDB\Field(type="string")
     */
    protected $schwer;
    public function getSchwer()
    {
        return $this->schwer;
    }
    public function setSchwer($schwer)
    {
        $this->schwer = $schwer;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $nart;
    public function getNart()
    {
        return $this->nart;
    }
    public function setNart($nart)
    {
        $this->nart = $nart;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $numfang;
    public function getNumfang()
    {
        return $this->numfang;
    }
    public function setNumfang($numfang)
    {
        $this->numfang = $numfang;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $dauer;
    public function getDauer()
    {
        return $this->dauer;
    }
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $ergebnis;
    public function getErgebnis()
    {
        return $this->ergebnis;
    }
    public function setErgebnis($ergebnis)
    {
        $this->ergebnis = $ergebnis;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $startdate;
    public function getStartdate()
    {
        return $this->startdate;
    }
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }
}