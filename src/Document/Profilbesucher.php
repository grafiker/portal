<?php
// src/Entity/Profilbesucher.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Profilbesucher
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    public function getId()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $besucher;
    public function getBesucher()
    {
        return $this->besucher;
    }
    public function setBesucher($besucher)
    {
        $this->besucher = $besucher;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $viewdate;
    public function getViewdate()
    {
        return $this->viewdate;
    }
    public function setViewdate($viewdate)
    {
        $this->viewdate = $viewdate;
    }  
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $new;
    public function getNew()
    {
        return $this->new;
    }
    public function setNew($new)
    {
        $this->new = $new;
    }   
}