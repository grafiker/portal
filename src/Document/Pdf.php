<?php
// src/App/Document/User.php

namespace App\Document;

use Sonata\UserBundle\Document\BaseUser as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use FOS\MessageBundle\Model\ParticipantInterface;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @MongoDB\Document
 */
class Pdf
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    /**
     * @MongoDB\Field(type="int")
     */
    protected $rechnungsnummer;
    public function getRechnungsnummer()
    {
        return $this->rechnungsnummer;
    }
    public function setRechnungsnummer($rechnungsnummer)
    {
        $this->rechnungsnummer = $rechnungsnummer;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $retitle;
    public function getRetitle()
    {
        return $this->retitle;
    }
    public function setRetitle($retitle)
    {
        $this->retitle = $retitle;
    }
    /**
     * @MongoDB\Field(type="float")
     */
    protected $einzelpreis;
    public function getEinzelpreis()
    {
        return $this->einzelpreis;
    }
    public function setEinzelpreis($einzelpreis)
    {
        $this->einzelpreis = $einzelpreis;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $menge;
    public function getMenge()
    {
        return $this->menge;
    }
    public function setMenge($menge)
    {
        $this->menge = $menge;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $angebotshinweis;
    public function getAngebotshinweis()
    {
        return $this->angebotshinweis;
    }
    public function setAngebotshinweis($angebotshinweis)
    {
        $this->angebotshinweis = $angebotshinweis;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $reart;
    public function getReart()
    {
        return $this->reart;
    }
    public function setReart($reart)
    {
        $this->reart = $reart;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $pinid;
    public function getPinid()
    {
        return $this->pinid;
    }
    public function setPinid($pinid)
    {
        $this->pinid = $pinid;
    }

}