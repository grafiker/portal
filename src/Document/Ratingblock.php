<?php
// src/Entity/Skills.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Ratingblock
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }
    /**
     * @MongoDB\Id(strategy="auto")
     */
    public $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $pinid;
    public function getPinid()
    {
        return $this->pinid;
    }
    public function setPinid($pinid)
    {
        $this->pinid = $pinid;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $username;
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
}
