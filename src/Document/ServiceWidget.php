<?php
// src/Entity/Job.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class ServiceWidget
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    public function getID()
    {
        return $this->id;
    }
    /**
     * @MongoDB\Field(type="int")
     */
    protected $WidgetNumber;
    public function getWidgetNumber()
    {
        return $this->WidgetNumber;
    }
    public function setWidgetNumber($WidgetNumber)
    {
        $this->WidgetNumber = $WidgetNumber;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $accountName;
    public function getAccountName()
    {
        return $this->accountName;
    }
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $accountURL;
    public function getAccountURL()
    {
        return $this->accountURL;
    }
    public function setAccountURL($accountURL)
    {
        $this->accountURL = $accountURL;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssMargin;
    public function getCssMargin()
    {
        return $this->cssMargin;
    }
    public function setCssMargin($cssMargin)
    {
        $this->cssMargin = $cssMargin;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssPadding;
    public function getCssPadding()
    {
        return $this->cssPadding;
    }
    public function setCssPadding($cssPadding)
    {
        $this->cssPadding = $cssPadding;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssPosition;
    public function getCssPosition()
    {
        return $this->cssPosition;
    }
    public function setCssPosition($cssPosition)
    {
        $this->cssPosition = $cssPosition;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssColor;
    public function getCssColor()
    {
        return $this->cssColor;
    }
    public function setCssColor($cssColor)
    {
        $this->cssColor = $cssColor;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssBackgroundColor;
    public function getCssBackgroundColor()
    {
        return $this->cssBackgroundColor;
    }
    public function setCssBackgroundColor($cssBackgroundColor)
    {
        $this->cssBackgroundColor = $cssBackgroundColor;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssWidth;
    public function getCssWidth()
    {
        return $this->cssWidth;
    }
    public function setCssWidth($cssWidth)
    {
        $this->cssWidth = $cssWidth;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssText;
    public function getCssText()
    {
        return $this->cssText;
    }
    public function setCssText($cssText)
    {
        $this->cssText = $cssText;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $formname;
    public function getFormname()
    {
        return $this->formname;
    }
    public function setFormname($formname)
    {
        $this->formname = $formname;
    }
     /**
     * @MongoDB\Field(type="string")
     */
    protected $dataname;
    public function getDataname()
    {
        return $this->dataname;
    }
    public function setDataname($dataname)
    {
        $this->dataname = $dataname;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $cssFull;
    public function getCssFull()
    {
        return $this->cssFull;
    }
    public function setCssFull($cssFull)
    {
        $this->cssFull = $cssFull;
    }
}