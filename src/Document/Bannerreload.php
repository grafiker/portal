<?php
// src/App/Document/Bannerreload.php

namespace App\Document;

use Sonata\UserBundle\Document\BaseUser as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use FOS\MessageBundle\Model\ParticipantInterface;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @MongoDB\Document
 */
class Bannerreload
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $bannerId;
    public function getBannerId()
    {
        return $this->bannerId;
    }
    public function setBannerId($bannerId)
    {
        $this->bannerId = $bannerId;
    }
    /**
     * @MongoDB\Field(type="date")
     */
    protected $clicktime;
    public function getClicktime()
    {
        return $this->clicktime;
    }
    public function setClicktime($clicktime)
    {
        $this->clicktime = $clicktime;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $clickIp;
    public function getClickIp()
    {
        return $this->clickIp;
    }
    public function setClickIp($clickIp)
    {
        $this->clickIp = $clickIp;
    }
}