<?php
// src/Entity/Contact.php
namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Contact
{
    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $from;
    public function getFrom()
    {
        return $this->from;
    }
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @MongoDB\Field(type="int")
     */
    protected $frage;
    public function getFrage()
    {
        return $this->frage;
    }
    public function setFrage($frage)
    {
        $this->frage = $frage;
    }

    /**
     * @MongoDB\Field(type="int")
     */
    protected $agb;
    public function getAgb()
    {
        return $this->agb;
    }
    public function setAgb($agb)
    {
        $this->agb = $agb;
    }

    /**
     * @MongoDB\Field(type="int")
     */
    protected $datenschutz;
    public function getDatenschutz()
    {
        return $this->datenschutz;
    }
    public function setDatenschutz($datenschutz)
    {
        $this->datenschutz = $datenschutz;
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected $anrede;
    public function getAnrede()
    {
        return $this->anrede;
    }
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected $subject;
    public function getSubject()
    {
        return $this->subject;
    }
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    /**
     * @MongoDB\Field(type="string")
     */
    protected $content;
    public function getContent()
    {
        return $this->content;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
}